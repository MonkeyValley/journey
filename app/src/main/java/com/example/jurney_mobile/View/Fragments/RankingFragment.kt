package com.example.jurney_mobile.View.Fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Ranking.IRanking
import com.example.jurney_mobile.Presenter.RankingFragmentPresenter
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.activity_home.*

class RankingFragment:Fragment(), IRanking.View {

    private lateinit var presenter: IRanking.Presenter

    override fun onCreateView(  inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {

        val view  = inflater.inflate(R.layout.ranking_fragment, container, false)
        presenter = RankingFragmentPresenter(this, this, view)

        presenter.initView()
        return view
    }

}