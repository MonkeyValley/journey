package com.example.jurney_mobile.View.Fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Notifications.INotifications
import com.example.jurney_mobile.Presenter.NotificationsFragmentPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.NotificationAdapter
import com.example.jurney_mobile.View.Models.RealmModels.NotificationRealmModel
import kotlinx.android.synthetic.main.notifications_fragment.view.*

class NotificationsFragment:Fragment(), INotifications.View, notificationSelectedFragmentDelegate{

    private  lateinit var presenter:INotifications.Presenter
    private lateinit var exp:notificationSelectedHomeDelegate
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.notifications_fragment, container, false)
        initView(inf)
        return inf
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        this.exp = activity as notificationSelectedHomeDelegate
    }

    fun initView(v: View){
        presenter = NotificationsFragmentPresenter(this, this, v, this)
        presenter.initInfo()

    }


    override fun notificationselected(item: NotificationRealmModel) {
        exp.notificationselected(item)

    }
}
interface notificationSelectedHomeDelegate{
    fun notificationselected(item:NotificationRealmModel)
}
interface notificationSelectedFragmentDelegate{
    fun notificationselected(item:NotificationRealmModel)
}