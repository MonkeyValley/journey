package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.ComplaintsActivity
import kotlinx.android.synthetic.main.complaint_more_info_fragment.view.*

class ComplaintMoreInfoFragment(ctx:ComplaintsActivity):Fragment(), View.OnClickListener, View.OnFocusChangeListener{

    private var scrollview:ScrollView
    init {
        scrollview = ctx.findViewById(R.id.sv_complaints)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.complaint_more_info_fragment, container, false)
        intView(inf)
        return inf
    }

    fun intView(v:View){
        v.btn_next_step_finalize_complaint.setOnClickListener(this)
        v.txt_more_info_complaint.onFocusChangeListener = this
    }

    override fun onClick(v: View?) {

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

            scrollview.post { object : Runnable{
                    override fun run() {

                        if(hasFocus){
                            scrollview.fullScroll(ScrollView.FOCUS_DOWN)
                        }else{
                            scrollview.fullScroll(ScrollView.FOCUS_UP)
                        }
                    }
                }
            }
    }
}