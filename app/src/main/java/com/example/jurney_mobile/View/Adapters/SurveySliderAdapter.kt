package com.example.jurney_mobile.View.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Fragments.*
import com.example.jurney_mobile.View.Models.PreguntaModel

class SurveySliderAdapter(manager: FragmentManager, list: ArrayList<PreguntaModel>, clickCallBack: IClickItemDelegate.clickItemSurveyFragment, arrFragments:ArrayList<Fragment> ): FragmentPagerAdapter(manager) {

    var list : ArrayList<PreguntaModel>
    var click:IClickItemDelegate.clickItemSurveyFragment
    var arrFrag:ArrayList<Fragment>
    init{ this.list = list; click = clickCallBack; this.arrFrag = arrFragments }

    override fun getItem(position: Int): Fragment {
         var frag: Fragment

        var type = list.get(position).question_type_id

        if(type == 1){
            frag = arrFrag.get(position) as SurveyFragmentMultipleOption
            frag.data = list.get(position)
            frag.clickCalback = click
            frag.type = type
        }else if(type == 2){
            frag = arrFrag.get(position) as SurveyFragmentMultipleSelection
            frag.data = list.get(position)
            frag.clickCalback = click
            frag.type = type
        }else if(type == 4){
            frag = arrFrag.get(position) as SurveyTextFragment
            frag.data = list.get(position)
        }else{
            frag = arrFrag.get(position) as SurveyFragmentSatifaction
            frag.clickCalback = click
            frag.data = list.get(position)
        }
        return frag
    }
    override fun getCount(): Int = list.count()
}