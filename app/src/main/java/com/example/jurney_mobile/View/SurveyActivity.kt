package com.example.jurney_mobile.View

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.Surveys.ISurveys
import com.example.jurney_mobile.Presenter.SurveyActivityPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.View.Adapters.SurveySliderAdapter
import com.example.jurney_mobile.View.Fragments.HomeFragment
import com.example.jurney_mobile.View.Fragments.ManteinerFragment
import com.example.jurney_mobile.View.Models.ResponseSendSurvey
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_survey.*
import kotlinx.android.synthetic.main.toolbar_component.*

class SurveyActivity : AppCompatActivity(), View.OnClickListener, ViewPager.OnPageChangeListener, IClickItemDelegate.clickItemSurveyFragment, ISurveys.ViewActivity {

    lateinit var adapter: SurveySliderAdapter
    lateinit var  alert:LoadingDialog
    private lateinit var presenter:ISurveys.PresenterActivity
    private val gson = Gson()
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey)
        initView()
    }

    fun initView(){
        presenter = SurveyActivityPresenter(this, this)
        presenter.initView()
        val id = intent.getIntExtra("id", -1)
        if(id == -1){
            val sharedPreferences = this.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
            val idSurvey = sharedPreferences.getInt("_idsurvey", -1)
            presenter.getData(idSurvey)


//            val user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
//            myRef.child("data_base").child(user.id.toString()).child("new_survey").child("notificationAction").child("extras").child("id").addListenerForSingleValueEvent(object : ValueEventListener {
//                override fun onDataChange(dataSnapshot: DataSnapshot) {
//                    val value = dataSnapshot.getValue().toString()
//                    Log.e("VALUE",value)
//                    presenter.getData(value.toInt())
//                }
//                override fun onCancelled(databaseError: DatabaseError) {
//                    Log.w("TAG", "loadPost:onCancelled", databaseError.toException())
//                }
//            })
        }else {
            presenter.getData(id)
        }
        btn_back.setOnClickListener(this)
        btn_next_question.setOnClickListener(this)
        btn_previus_question.setOnClickListener(this)
        alert = LoadingDialog( this )
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorBackground)
        }
    }

    override fun closeAlert() {
        alert.dismissDialog()
        this.finish()
    }

    override fun onClick(v: View?) = presenter.onClickSurvey(v!!)
    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) = presenter.onPageSelected(position)
    override fun click(item: Int, position: Int, selected:Boolean) = presenter.clickCallBack(item, position, selected)
    override fun showAlert(response: ResponseSendSurvey) = alert.alertNot("Gracias por ayudarnos a conocer tu opinión. Sigue contestando más encuestas y gana vivacoins. Esta vez acabas de ganar:","¡Has terminado la encuesta!","Continuar", R.drawable.ic_surveys_green, this, this, true, true, "+"+response.coins)

}
