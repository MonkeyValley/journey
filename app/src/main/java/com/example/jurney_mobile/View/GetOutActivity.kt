package com.example.jurney_mobile.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jurney_mobile.Model.GetOut.IGetout
import com.example.jurney_mobile.Presenter.GetoutPresenter
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.activity_get_out.*
import kotlinx.android.synthetic.main.toolbar_component.view.*

class GetOutActivity : AppCompatActivity(), IGetout.view {

    val  presenter = GetoutPresenter(this, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_out)

        presenter.initView()
    }
}
