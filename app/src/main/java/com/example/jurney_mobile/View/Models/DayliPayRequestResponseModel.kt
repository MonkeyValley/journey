package com.example.jurney_mobile.View.Models

data class DayliPayRequestResponseModel ( var id:Int, var collaborator_id:Int, var pay_date:String, var errors:String)