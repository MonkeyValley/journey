package com.example.jurney_mobile.View.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Reviews.IReviews
import com.example.jurney_mobile.Model.Reviews.ReviewsModel
import com.example.jurney_mobile.Presenter.ReviewPresenter
import com.example.jurney_mobile.R

class ReviewsFragment(isReload:Boolean):Fragment(), IReviews.View {

    lateinit var presenter: IReviews.Presenter
    var reload:Boolean
    init {
        this.reload = isReload
    }
    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?,  savedInstanceState: Bundle?  ): View? {

        val inf = inflater.inflate(R.layout.reviews_fragment, container, false)
        initView(inf)
        return inf
    }

    private fun initView(v:View){
        presenter = ReviewPresenter(this, this, v)
        presenter.initView(v, reload)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) = presenter.onActivityResult(requestCode, resultCode, data)
}