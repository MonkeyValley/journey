package com.example.jurney_mobile.View.Models

data class CodeResponseModel (var code:String="", var employee_code:String="", var created_at:String, var cellphone:String="", var name:String="", var status:String, var prize_name:String="")