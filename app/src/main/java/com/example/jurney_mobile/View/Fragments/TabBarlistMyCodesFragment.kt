package com.example.jurney_mobile.View.Fragments

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.AwardsAdapter
import com.example.jurney_mobile.View.Adapters.MyCodesAdapter
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.tab_bar_list_my_codes_fragment.view.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList

class TabBarlistMyCodesFragment:Fragment(), View.OnClickListener {

    private lateinit var viewFrag:View
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: MyCodesAdapter

    var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences
    lateinit var token :String
    lateinit var user: UserResponseModel

    var data = ArrayList<CodeResponseModel>()
    var dataResp = ArrayList<CodeResponseModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewFrag = inflater.inflate(R.layout.tab_bar_list_my_codes_fragment, container, false)

        sharedPreferences = this.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val userStr = sharedPreferences.getString("_userinfo","")
        val dataStr = sharedPreferences.getString("_listcodes","")
        token = sharedPreferences.getString("_tokenuser","")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        if(dataStr != "") {
            val jsonArray = JSONArray(dataStr)
            for (i in 0 until jsonArray.length()) {
                data.add(gson.fromJson(jsonArray.get(i).toString(), CodeResponseModel::class.java))
                dataResp.add(gson.fromJson(jsonArray.get(i).toString(), CodeResponseModel::class.java))
            }
        }

        list = viewFrag.findViewById(R.id.list_my_codes)
        layoutManager = LinearLayoutManager(this.context)
        adaptador = MyCodesAdapter(data, this.context!!)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador

        viewFrag.btn_search_codes.setOnClickListener(this)

        return viewFrag
    }

    override fun onClick(v: View?) {

        val txtSearch = this.viewFrag.txtSearchCodes.text.toString()

        utilsClass().hideSoftKeyBoard(this.context!!, viewFrag)
        if(txtSearch != "") {
            data = ArrayList()
            for (item in dataResp) {
                if (item.prize_name.toLowerCase(Locale.ROOT).contains(txtSearch.toLowerCase(Locale.ROOT))) {
                    data.add(item)
                }
            }
        }
        else{
            data = dataResp
        }

        layoutManager = LinearLayoutManager(this.context)
        adaptador = MyCodesAdapter(data, this.context!!)
        list.layoutManager = layoutManager
        list.adapter = adaptador
        this.adaptador.notifyDataSetChanged()
    }

}