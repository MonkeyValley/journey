package com.example.jurney_mobile.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.jurney_mobile.Model.DailyPay.IDailypay
import com.example.jurney_mobile.Presenter.DailypayPresenter
import com.example.jurney_mobile.Presenter.PayrollPresenter
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.toolbar_component.*

class DailyPayActivity : AppCompatActivity(), IDailypay.View {

    private lateinit var presenter:IDailypay.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_pay)

        toolbar_title.text = "Depósito diario"
        presenter = DailypayPresenter(this, this)
        presenter.initInfo()

        btn_back.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                this@DailyPayActivity.finish()
            }
        })

    }
}
