package com.example.jurney_mobile.View

import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.jurney_mobile.Model.UserValidate.IUserValidate
import com.example.jurney_mobile.Presenter.UserValidatePresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_user_validate.*


class UserValidate : AppCompatActivity(), View.OnKeyListener, IUserValidate.View, TextWatcher, View.OnClickListener {

    private var presenter: IUserValidate.Presenter = UserValidatePresenter(this, this)
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_validate)
        initView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    private fun initView() {

        presenter.initTextFocus(txtOne)
        presenter.initInfo()
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        txtOne.setOnKeyListener(this)
        txtTwo.setOnKeyListener(this)
        txtThree.setOnKeyListener(this)
        txtFour.setOnKeyListener(this)

        txtOne.addTextChangedListener(this)
        txtTwo.addTextChangedListener(this)
        txtThree.addTextChangedListener(this)
        txtFour.addTextChangedListener(this)

        btnValidateCode.setOnClickListener(this)
    }

    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        return presenter.onKeyPressed(v!!, keyCode, event!!)
    }

    override fun afterTextChanged(s: Editable?) {
       presenter.afterTextChanged()
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    override fun onClick(v: View?) {

        presenter.onClick(v!!)
    }

    fun showAlert(isConnected:Boolean){

        val editor = sharedPreferences.edit()
        editor.putBoolean("_isConnected", isConnected)
        editor.apply()

        if(isConnected){
            SnackAlert.closeAlert()
        }else{
            SnackAlert.alert("Perdiste la conexión a internet. Asegúrate de tenerlo para usar VivaJourney", findViewById(R.id.layout_container_splash), "error", Snackbar.LENGTH_INDEFINITE, "BOTTOM")
            SnackAlert.showAlert()
        }
    }

}
