package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.ReviewsModelResponse
import com.example.jurney_mobile.View.Models.SupervisorsResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_reviews_list.view.*


class ReviewsAdapter (items:List<SupervisorsResponseModel>, clickDelegate: IClickItemDelegate.clickReviews): RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {

    var clickDelegate: IClickItemDelegate.clickReviews
    var list:List<SupervisorsResponseModel>
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.clickDelegate = clickDelegate
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_reviews_list, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblActivity.text = list.get(position).area_name
        holder.lblNameSupervisor.text = list.get(position).supervisor_name
        holder.lblLote.text = list.get(position).lots
        val image = "" + list.get(position).supervisor_image
        Picasso.get().load(image).error(R.drawable.no_image).into(holder.igSupervisor)

        if(!list.get(position).supervisor_review_status) {
            holder.btnSelected.visibility = View.VISIBLE
            holder.reviewIsView.visibility = View.GONE
            holder.btnSelected.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    clickDelegate.click(list.get(position))
                }
            })
        }else{
            holder.btnSelected.visibility = View.GONE
            holder.reviewIsView.visibility = View.VISIBLE
        }
    }
    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val igSupervisor = vista.img_supervisor_review
        val lblNameSupervisor = vista.lbl_name_supervisor_list
        val lblActivity = vista.lbl_activity_review_list
        val lblLote = vista.lbl_lote_list_review
        var btnSelected = vista.btn_selected_review
        var reviewIsView = vista.review_isView
    }
}
