package com.example.jurney_mobile.View.Models

data class ComplainPayrollResponseModel (var collaborator_id:Int, var comment:String, var date:String, var id:Int, var selected_reasons:ArrayList<ReasonsPayrollComplainModel>, var business_unit_name:String, var cause:ArrayList<String>, var cause_comment:ArrayList<String>?, var viability_status:String, var payroll_complaint_viability_status_id:Int?)