package com.example.jurney_mobile.View.Models

data class ComplaintPayrollRequest( var collaborator_id:Int, var date: String, var comment:String, var selected_reasons:List<Int>)