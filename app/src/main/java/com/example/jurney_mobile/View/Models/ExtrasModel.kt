package com.example.jurney_mobile.View.Models

data class ExtrasModel (
    var icon: String = "",
    var points:Double=0.0,
    var id:Int=0,
    var lvl:Int=0,
    var description:String="",
    var message_id:String="",
    var confirm_date:String="",
    var message_place:String=""
)