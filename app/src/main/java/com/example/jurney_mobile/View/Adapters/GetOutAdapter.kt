package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.ReasonsResponseModel
import kotlinx.android.synthetic.main.item_cat_getout.view.*

class GetOutAdapter (items:ArrayList<ReasonsResponseModel>, clickItem:IClickItemDelegate.clickItemReason, ctx: Context): RecyclerView.Adapter<GetOutAdapter.ViewHolder>() {

    var list:ArrayList<ReasonsResponseModel>
    lateinit var viewHolder:ViewHolder
    val clickItem:IClickItemDelegate.clickItemReason
    var ctx:Context

    init {
        this.list = items
        this.clickItem =  clickItem
        this.ctx = ctx
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cat_getout, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).name
        holder.lblTitle.setOnClickListener{
            Log.e("CLICK -> ",list.get(position).name)
            clearList()
            list.get(position).isSelected = true
            notifyDataSetChanged()
            clickItem.click(list.get(position))
        }

        if( list.get(position).isSelected ){
            holder.lblTitle.setBackgroundResource(R.drawable.corner_radius_cool_green)
            holder.lblTitle.setTextColor( ctx.resources.getColor(R.color.white))
        }else{
            holder.lblTitle.setBackgroundResource(R.drawable.corner_radius_item_white)
            holder.lblTitle.setTextColor( ctx.resources.getColor(R.color.charcoal))
        }
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_title_cat_getout
    }

    fun clearList(){
        list.forEach{
            it.isSelected = false
        }
    }

}
