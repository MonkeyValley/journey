package com.example.jurney_mobile.View.Models

data class VacancyRequestModel(var collaborator_id:Int, var vacant_id:Int, var origin:String, var start_date:String)