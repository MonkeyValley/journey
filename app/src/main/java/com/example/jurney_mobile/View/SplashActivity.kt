package com.example.jurney_mobile.View

import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.jurney_mobile.Model.Splash.ISplash
import com.example.jurney_mobile.Presenter.SplashPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.ConnectionReciver
import com.example.jurney_mobile.Utils.MyApplication
import com.example.jurney_mobile.Utils.SnackAlert
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity(), ISplash.View,
    ConnectionReciver.ConnectionReciverlistener {

    val reciverj = ConnectionReciver()
    lateinit var presenter:SplashPresenter
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        baseContext.registerReceiver( reciverj, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

//        val extras = intent.getExtras()
//        val keySet = extras!!.keySet()
//        val iterator = keySet.iterator()
//        while (iterator.hasNext()) {
//             val key = iterator.next()
//             val o = extras.get(key) as Object
//            Log.e("------------->", key + ":" + o)
//        }
        presenter = SplashPresenter(this, this,  layout_container_splash)

        if (intent.extras != null) {
            var value: Any?
            for (key in intent.extras!!.keySet()) {
                //if (key == "Message Key") {
                value = intent.extras!!.get(key) // value will represend your message body... Enjoy It
                Log.d("NotificationTag", "- " + key + "____" + value)
                //}
            }
        }

    }

    override fun onResume() {
        super.onResume()
        MyApplication.instance.setConnectionLsitenner(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        baseContext.unregisterReceiver(reciverj)
    }

    fun showAlert(isConnected:Boolean){

        val editor = sharedPreferences.edit()
        editor.putBoolean("_isConnected", isConnected)
        editor.apply()

        if(isConnected){
            SnackAlert.closeAlert()
            presenter.intiInfo()
        }else{
            SnackAlert.alert("Perdiste la conexión a internet. Asegúrate de tenerlo para usar VivaJourney", findViewById(R.id.layout_container_splash), "error", Snackbar.LENGTH_INDEFINITE, "BOTTOM")
            SnackAlert.showAlert()
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
       showAlert(isConnected)
    }
}


