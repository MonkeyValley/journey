package com.example.jurney_mobile.View.Models

data class SurveysListModel(var description:String, var id:Int, var name:String, var questions:Int, var is_active:Boolean, var type:Int, var vivacoins:Int, var audience_origin:String, var business_unit:ArrayList<BussinesUnitiIDModel>)