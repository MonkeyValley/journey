package com.example.jurney_mobile.View.Models

data class SendResResponseModel (var errors:String, var code:String, var collaborator_id:Int, var id:Int, var prize_id:Int, var status:String)