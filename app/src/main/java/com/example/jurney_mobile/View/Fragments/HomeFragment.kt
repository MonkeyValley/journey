package com.example.jurney_mobile.View.Fragments

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Home.IHome
import com.example.jurney_mobile.MyFirebaseMessagingService
import com.example.jurney_mobile.Presenter.HomeFragmentPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.HomeAdapter
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.home_fragment.view.*
import kotlinx.android.synthetic.main.single_vivacoint_component.*
import kotlinx.android.synthetic.main.vivacoin_header_component.*
import java.util.*


class HomeFragment: Fragment(), View.OnClickListener, IHome.ViewFragment {

    private lateinit var presenter:IHome.PresenterFragment
    private lateinit var listener: OnClickItemsDelegate
    private lateinit var activity:Activity
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var user:UserResponseModel
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()

    val TAG = "HomeFragment"


    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is OnClickItemsDelegate) {
            listener = context
        }
    }

    override fun onCreateView(  inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val infl = inflater.inflate(R.layout.home_fragment, container, false)
        initView(infl)
        return infl
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("","")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("","CREATE")
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
            this.activity = activity!!
            Log.e("---------------"," ATACH")
    }

    override fun onResume() {
        super.onResume()
        presenter.getInfoPayment()
        listener.onResumeDelegate()
    }
    fun initView(v:View){


        firebaseAnalytics = FirebaseAnalytics.getInstance(this.context!!)
        presenter = HomeFragmentPresenter(this, this, v)
        presenter.initNoNetwork(activity)
        presenter.initInfo()
        v.btn_goto_payroll.setOnClickListener(this)
        v.btn_goto_weekend_quality.setOnClickListener(this)
        v.btn_goto_challenges.setOnClickListener(this)


    }

    override fun onClick(v: View?) = presenter.onClickListener(v!!)
    override fun onClickNomina(isEnabled: Boolean) = listener.onClickNomina()
    override fun onClickCalidad() = listener.onClickCalidad()
    override fun onClickRetos() = listener.onClickRetos()

}

interface OnClickItemsDelegate {
    fun onClickNomina()
    fun onClickCalidad()
    fun onClickRetos()
    fun onResumeDelegate()
}
