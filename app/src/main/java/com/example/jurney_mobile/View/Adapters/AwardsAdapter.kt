package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_awards.view.*
import kotlinx.android.synthetic.main.item_challenge_challenges.view.*

class AwardsAdapter (items:ArrayList<AwardsResponseModel>, clickItem:IClickItemDelegate.clickItemAwards): RecyclerView.Adapter<AwardsAdapter.ViewHolder>() {

    var list:ArrayList<AwardsResponseModel>
    var itemSelected:IClickItemDelegate.clickItemAwards
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.itemSelected = clickItem
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_awards, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).name
        holder.lblDescription.text = list.get(position).description
        val price = list.get(position).coins.toString()
        holder.lblPrice.text =  utilsClass().milesFormat(price.toDouble())

       val image = EnviromentConstants().image_serverver + list.get(position).image
        Picasso.get().load(image).error(R.drawable.no_image).into(holder.imgAward)

        holder.btnChangeItme.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                itemSelected.click(list.get(position))
            }
        })
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_title_award_item
        val lblDescription = vista.lbl_description_award_item
        val imgAward = vista.img_award_item
        val lblPrice = vista.lbl_price_award_item
        val btnChangeItme = vista.btn_exchange_item
    }
}

