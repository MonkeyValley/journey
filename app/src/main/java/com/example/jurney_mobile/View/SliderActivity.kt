package com.example.jurney_mobile.View

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.transition.Slide
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewStub
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.GetOut.IGetout
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.View.Adapters.SliderAdapter
import com.example.jurney_mobile.View.Models.SliderModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_slider.*
import java.util.ArrayList

class SliderActivity : AppCompatActivity(), ViewPager.OnPageChangeListener, View.OnClickListener {

    lateinit var adapter:SliderAdapter
    var arrayFragments=ArrayList<SliderModel>()
    private lateinit var loading: LoadingDialog
    lateinit var sharedPreferences: SharedPreferences
    var position = 0
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    private lateinit var user: UserResponseModel
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slider)

        loading = LoadingDialog(this)
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        val responseStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )
        onGetArrayFirebase()

    }



    fun onGetArrayFirebase(){
        myRef.child("data_base").child("Slider").child("fragments").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){

                    arrayFragments = ArrayList<SliderModel>()
                    for(postSnapShot in dataSnapshot.getChildren())
                    {
                        val item = postSnapShot.getValue(SliderModel::class.java)
                        arrayFragments.add(item!!)
                    }

                    adapter = SliderAdapter(supportFragmentManager,  arrayFragments)
                    adapter.notifyDataSetChanged()
                    container_slider.adapter = adapter
                    container_slider.addOnPageChangeListener(this@SliderActivity)
                    btn_next_slider.setOnClickListener(this@SliderActivity)
                    container_slider.currentItem = 0
                    position = 0
                    btn_next_slider.setImageResource(R.drawable.ic_arrow_right)

                    progress_bar_slider.progress = 100/arrayFragments.count()
                    lbl_steps_for.text = "1 de "+ arrayFragments.count() + " pasos"
                    getpermitionGalery()
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }


    override fun onPageScrollStateChanged(state: Int) { }
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        this.position = position
        lbl_steps_for.text = "" + (position+1)+" de "+ arrayFragments.count() + " pasos"
        progress_bar_slider.progress = (((position+1)*100) / arrayFragments.count())

        if(this.position == arrayFragments.count() - 1){
            btn_next_slider.setImageResource(R.drawable.ic_check)
        }else{
            btn_next_slider.setImageResource(R.drawable.ic_arrow_right)
        }
        when(position){
            2 -> getpermitionGalery()
            3 -> getpermitionCamera()
        }
    }

    fun getpermitionCamera(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission( this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ) {

                val permission = arrayOf(android.Manifest.permission.CAMERA)
                requestPermissions(permission, 2)
            }else{
                Log.e("response","OK")
            }
        }else{
            Log.e("response","OK 2")
        }
    }

    fun getpermitionGalery(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission( this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission( this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

                val permission = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permission, 1)
            }else{
                Log.e("response","OK")
            }
        }else{
            Log.e("response","OK 2")
        }
    }

    override fun onClick(v: View?) {


        if(this.position == arrayFragments.count() - 1){

            val anim = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out)
            this.layout_conteiner_slider.startAnimation(anim)

            anim.setAnimationListener(object : Animation.AnimationListener{
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationStart(animation: Animation?) {}
                override fun onAnimationEnd(animation: Animation?) {

                    val intent = Intent( this@SliderActivity  , PromotionalVideoActivity::class.java)
                    startActivity(intent)
                    this@SliderActivity.finish()

                    /*val intent = Intent( this@SliderActivity  , Home::class.java)
                    intent.putExtra("isNotificastion","home")
                    startActivity(intent)
                    this@SliderActivity.finish()*/
                }
            })

        }else{
            container_slider.currentItem++
        }
    }

}
