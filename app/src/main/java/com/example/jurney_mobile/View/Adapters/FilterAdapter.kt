package com.example.jurney_mobile.View.Adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.item_filter_tag.view.*

class FilterAdapter (items:ArrayList<String>, click:ClickItemTagDelegate): RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    var list:ArrayList<String>
    var clickItem:ClickItemTagDelegate
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.clickItem = click
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_filter_tag, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position)
        holder.lblTitle.setOnClickListener{
            Log.e("CLICK -> ",list.get(position))
            clickItem.clickItem(list.get(position))
        }

    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.title_item_filter
    }
}
interface ClickItemTagDelegate{
    fun clickItem(item:String)
}