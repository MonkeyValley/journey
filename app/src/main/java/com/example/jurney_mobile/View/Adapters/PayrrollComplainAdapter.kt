package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.net.wifi.aware.IdentityChangedListener
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.ReasonsPayrollComplainModel
import kotlinx.android.synthetic.main.item_payroll_complain.view.*

class PayrrollComplainAdapter (items:List<ReasonsPayrollComplainModel>, onSelected:IClickItemDelegate.clickReasonPayrollComplain, ctx: Context): RecyclerView.Adapter<PayrrollComplainAdapter.ViewHolder>() {

    var list:List<ReasonsPayrollComplainModel>
    var ctx:Context
    var onClickReason:IClickItemDelegate.clickReasonPayrollComplain
    //var clickExhachenge:ExchangeDelegate
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.ctx = ctx
        this.onClickReason = onSelected
        //this.clickExhachenge = clickItem
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_payroll_complain, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).name
        if(!list.get(position).status){
            holder.btnItem.setBackgroundDrawable( ctx.resources.getDrawable( R.drawable.corner_radius_linear))
        }else{
            holder.btnItem.setBackgroundDrawable( ctx.resources.getDrawable( R.drawable.corner_radius_cool_green))
        }

        holder.btnItem.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {

                list.get(position).status = !list.get(position).status
                onClickReason.clickReason(list.get(position))
                if(!list.get(position).status){
                    holder.btnItem.setBackgroundDrawable( ctx.resources.getDrawable( R.drawable.corner_radius_linear))
                }else{
                    holder.btnItem.setBackgroundDrawable( ctx.resources.getDrawable( R.drawable.corner_radius_cool_green))
                }
            }
        })
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_name_reason_complain
        val btnItem = vista.item_selected_reason_complain
    }
}
