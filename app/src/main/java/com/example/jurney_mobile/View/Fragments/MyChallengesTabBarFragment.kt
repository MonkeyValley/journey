package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Adapters.ChallengeAdapter
import com.example.jurney_mobile.View.Adapters.MyChallengesAdapter
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.example.jurney_mobile.View.Models.ChallengesAvailableResponse
import com.example.jurney_mobile.View.Models.ChallengesCompleteResponseModel
import com.example.jurney_mobile.View.Models.ChallengesResponseModel
import com.google.gson.Gson

class MyChallengesTabBarFragment(callbackclickItemChallenge:IClickItemDelegate.clickItemChallenge):Fragment(), IClickItemDelegate.clickItemChallenge {

    var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private var adaptador: MyChallengesAdapter? = null
    private var callback :IClickItemDelegate.clickItemChallenge
    private lateinit var inf: View

    init{
        this.callback = callbackclickItemChallenge
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inf = inflater.inflate(R.layout.tab_bar_my_challenges_fragment, container, false)

        return inf
    }

    override fun onResume() {
        super.onResume()
        Log.e("dfvdfv","dfvdfv")
        initView()

        adaptador!!.notifyDataSetChanged()

    }


    fun initView(){

        sharedPreferences = this.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val data = sharedPreferences.getString("_listchallengescomplete", "")
        val challenges = gson.fromJson(data, ChallengesCompleteResponseModel::class.java)

        val challenge = ArrayList<ChallengeModel>()
        challenges.challenges.forEach {
            val obj = ChallengeModel(0,true,0,it.challente_image, it.challenge_name, it.challenge_points , it.challenge_description, 0.0, 0.0, true)
            challenge.add(obj)
        }

        list = inf.findViewById(R.id.list_my_challenges_fragment)
        list.adapter = null
        layoutManager = LinearLayoutManager(this.context)
        adaptador = MyChallengesAdapter(challenge, this)
        list.isNestedScrollingEnabled = true
        list.layoutManager = layoutManager
        list.adapter = adaptador

    }

    override fun click(item: ChallengeModel) {
        Log.e("--------------->","CLICK CLICK")
        callback.click(item)
    }

}