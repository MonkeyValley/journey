package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Vacancies.IVacancies
import com.example.jurney_mobile.Presenter.VacanciePresenter
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.vacancies_fragment.view.*

class VacainciesFragment:Fragment(), IVacancies.View {

    lateinit var presenter:IVacancies.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
         val v = inflater.inflate(R.layout.vacancies_fragment, container, false)
        presenter = VacanciePresenter(this, this)
        presenter.initInfo(v)
        return v
    }

}