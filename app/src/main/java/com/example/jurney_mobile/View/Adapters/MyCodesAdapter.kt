package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_awards.view.*
import kotlinx.android.synthetic.main.item_my_code.view.*

class MyCodesAdapter (items:ArrayList<CodeResponseModel>, ctx:Context): RecyclerView.Adapter<MyCodesAdapter.ViewHolder>() {

    var list:ArrayList<CodeResponseModel>
    var ctx:Context
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.ctx = ctx
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_my_code, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).prize_name
        holder.lblDate.text = list.get(position).created_at
        holder.lblCode.text = list.get(position).code
        val status = list.get(position).status

        if(status == "VALIDATING"){
            holder.statusLayer.background = ctx.resources.getDrawable(R.drawable.corner_radius_left_sun_yellow)
        }else if (status == "EXCHANGED"){
            holder.statusLayer.background = ctx.resources.getDrawable(R.drawable.corner_radius_left_red_pink)
        }else{
            holder.statusLayer.background = ctx.resources.getDrawable(R.drawable.corner_radius_left_dodger_blue)
        }

//
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_name_award_code_item
        val lblDate = vista.lbl_date_init_code_item
        val lblCode = vista.lbl_code_item
        var statusLayer = vista.layer_status_award_code_item
    }
}