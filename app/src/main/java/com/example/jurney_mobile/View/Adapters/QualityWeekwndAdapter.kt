package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.ActivitiesQualityScore
import com.google.firebase.database.core.Context
import kotlinx.android.synthetic.main.item_tasks_quality_weekend.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class QualityWeekwndAdapter(items:ArrayList<ActivitiesQualityScore>): RecyclerView.Adapter<QualityWeekwndAdapter.ViewHolder>() {

    var list:ArrayList<ActivitiesQualityScore>
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QualityWeekwndAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_tasks_quality_weekend, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val percent = list.get(position).score
        holder.lblActivity.text = list.get(position).name
        holder.lblPerformance.text = list.get(position).performance.toString() + " Camas"
        val decimal = BigDecimal(percent).setScale(2, RoundingMode.HALF_EVEN)
        holder.lblPercent.text =decimal.toString() + "%"

        if(percent < 50){
            holder.lblPercent.background = holder.itemView.resources.getDrawable(R.drawable.corner_radius_pink_red)
        }else if(percent >= 50 && percent < 70){
            holder.lblPercent.background = holder.itemView.resources.getDrawable(R.drawable.corner_radius_sun_yellow)
        }else{
            holder.lblPercent.background = holder.itemView.resources.getDrawable(R.drawable.corner_radius_cool_green)
        }


    }

    class ViewHolder(vista:View):RecyclerView.ViewHolder(vista){
        val lblActivity = vista.lbl_activity_tasks_quality_weekend
        val lblPerformance = vista.lbl_performance_tasks_quality_weekend
        val lblPercent = vista.lbl_percent_tasks_quality_weekend

    }
}