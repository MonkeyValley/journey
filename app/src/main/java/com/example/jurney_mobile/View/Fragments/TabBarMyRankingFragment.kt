package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Ranking.IRanking
import com.example.jurney_mobile.Presenter.MyRankingPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.RankingAdapter
import com.example.jurney_mobile.View.Models.UserRankingResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import org.json.JSONArray

class TabBarMyRankingFragment:Fragment(),  IRanking.ViewMy {


    private lateinit var presenter:IRanking.PresenterMy

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.tab_bar_myranking_fragment, container, false)

        presenter = MyRankingPresenter(this, this, inf)
        presenter.initView()

        return inf
    }
}