package com.example.jurney_mobile.View.Retrofit

import android.util.Log
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object OkHttpClientMultipart {

    private const val BASE_URL_DEV = "https://journey.vivasmart.com.mx"

    private val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(20, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .addInterceptor{
            val original = it.request()
            val requestBuilder = original.newBuilder()
                .method(original.method(), original.body())
            val request = requestBuilder.build()
            it.proceed(request)
        }.build()

    val instance:IAPI by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_DEV)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
        Log.e("URL", BASE_URL_DEV + retrofit.toString())
        retrofit.create(IAPI::class.java)
    }
}