package com.example.jurney_mobile.View.Models
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.View.Fragments.SurveyFragmentMultipleOption
import com.example.jurney_mobile.View.Fragments.SurveyFragmentMultipleSelection
import java.util.ArrayList

data class PreguntaModel (
    var question_id: Int,
    var question_type_id:Int,
    var description:String,
    var choices:ArrayList<RespuestaModel>,
    var required:Boolean
)