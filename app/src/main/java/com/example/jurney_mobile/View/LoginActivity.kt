package com.example.jurney_mobile.View

import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.jurney_mobile.Model.Login.ILogin
import com.example.jurney_mobile.Presenter.LoginPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.ConnectionReciver
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.MyApplication
import com.example.jurney_mobile.Utils.SnackAlert
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity(), ILogin.View , ConnectionReciver.ConnectionReciverlistener {

    private var presenter:ILogin.Presenter = LoginPresenter(this, this)
    val reciverj = ConnectionReciver()
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        baseContext.registerReceiver( reciverj, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
    }
    override fun onResume() {
        super.onResume()
        MyApplication.instance.setConnectionLsitenner(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        baseContext.unregisterReceiver(reciverj)
    }
    override fun onBackPressed() {
        super.onBackPressed()
        this.finish()
    }
    override fun onLoginError(msj: String) {
        SnackAlert.alert(msj, findViewById(R.id.contentlogin), "error", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }


    fun showAlert(isConnected:Boolean){
        val editor = sharedPreferences.edit()
        editor.putBoolean("_isConnected", isConnected)
        editor.apply()


        if(isConnected){
            SnackAlert.closeAlert()
            presenter.initInfo()
        }else{
            SnackAlert.alert("Perdiste la conexión a internet. Asegúrate de tenerlo para usar VivaJourney", findViewById(R.id.contentlogin), "error", Snackbar.LENGTH_INDEFINITE, "BOTTOM")
            SnackAlert.showAlert()
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showAlert(isConnected)
    }
}

