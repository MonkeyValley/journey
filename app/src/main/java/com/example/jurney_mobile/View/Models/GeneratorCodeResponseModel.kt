package com.example.jurney_mobile.View.Models

data class GeneratorCodeResponseModel (var code:String="", var collaborator_id:Int=-1, var id:Int=-1, var prize_id:Int = -1, var status :String="", var errors:String="")
