package com.example.jurney_mobile.View.Models
data class RankingHistoryResponseModel(var id:Int, var coins:Int, var date:String, var last_ranking_update:String, var level:Int, var level_category:String?)
