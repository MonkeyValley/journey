package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.SliderModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.slider_image_fragment.view.*

class SliderImageFragment: Fragment() {

    var item: SliderModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        var inf = inflater.inflate(R.layout.slider_image_fragment, container, false)

        if (item != null){
            Picasso.get().load(item!!.icon).error(R.drawable.no_image)
                .into(inf.findViewById<ImageView>(R.id.img_content))
        }else{
            inf.img_content.setImageDrawable( this.resources.getDrawable( R.drawable.no_image))
        }

        return inf
    }

    fun setData(item:SliderModel){
        this.item = item
    }
}
