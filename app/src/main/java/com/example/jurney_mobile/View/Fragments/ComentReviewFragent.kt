package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import kotlinx.android.synthetic.main.description_slider_fragment.view.*
import kotlinx.coroutines.flow.callbackFlow

class ComentReviewFragent:Fragment(), TextWatcher {

    lateinit var callBack:IClickItemDelegate.afterTextChangedDelegate
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val inf = inflater.inflate(R.layout.description_slider_fragment, container, false)

        inf.txt_comment_review.addTextChangedListener(this)
        inf.txt_comment_review.requestFocus();

        return inf
    }

    override fun afterTextChanged(s: Editable?) {
        callBack.afterTextChanged(s.toString())
    }
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {  }
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }
}