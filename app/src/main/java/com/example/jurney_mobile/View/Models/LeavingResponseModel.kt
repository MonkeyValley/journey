package com.example.jurney_mobile.View.Models

data class LeavingResponseModel (var id:Int, var reason_for_leaving:String, var date:String, var status:String)