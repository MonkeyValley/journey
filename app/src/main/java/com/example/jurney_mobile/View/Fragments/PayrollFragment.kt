package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Payroll.IPayroll
import com.example.jurney_mobile.Presenter.PayrollPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.PayrollAdapter
import com.example.jurney_mobile.View.Models.ExtrasModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user_validate.*
import kotlinx.android.synthetic.main.payroll_fragment.view.*
import java.util.ArrayList

class PayrollFragment: Fragment(), IPayroll.View, View.OnClickListener {

    private lateinit var presenter:IPayroll.Presenter
    val TAG = "PayrollFragment"
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val inf = inflater.inflate(R.layout.payroll_fragment, container, false)
        Log.e(TAG, "OnCreate")
        initView(inf)
        return inf
    }

    fun initView(v:View){

        sharedPreferences = context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val notificationData = sharedPreferences.getString("_data_notifitacion","")
        presenter = PayrollPresenter(this, this, v)
        presenter.initInfo()
        if (notificationData != ""){
            val data = Gson().fromJson(notificationData, ExtrasModel::class.java)
            presenter.getDetailFromNotification(data.id)
            val delete = sharedPreferences.edit()
            delete.remove("_data_notifitacion")
            delete.apply()
        }

        v.btnActualWeekend.setOnClickListener(this)
        v.btnLastWeekend.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        presenter.onClick(v!!)
    }

}

