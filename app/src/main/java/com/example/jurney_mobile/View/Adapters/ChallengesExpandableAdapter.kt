package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.getSystemService
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.example.jurney_mobile.View.Models.ChallengesAvailableResponse
import com.example.jurney_mobile.View.Models.HistoryChallengesResponseModel
import com.squareup.picasso.Picasso
import kotlin.collections.ArrayList

class ChallengesExpandableAdapter(ctx: Context, listParent:List<HistoryChallengesResponseModel>,  clickDelegate: IClickItemDelegate.clickItemChallenge ):BaseExpandableListAdapter(){

    var listP:List<HistoryChallengesResponseModel>
    var ctx:Context
    var click:IClickItemDelegate.clickItemChallenge

    init {
        this.listP = listParent
        this.ctx = ctx
        this.click = clickDelegate
    }

    override fun getGroup(groupPosition: Int): Any {
        return  listP.get(groupPosition)
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {

        var cv = convertView
        val chapter = getGroup(groupPosition) as HistoryChallengesResponseModel

        if(cv == null){
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            cv = inflater.inflate(R.layout.item_history_challenge, null)
        }

        var dateFrom = cv!!.findViewById<TextView>(R.id.lbl_date_from_history_challenge)
        var dateTo = cv.findViewById<TextView>(R.id.lbl_date_to_history_challenge)
        var coins = cv.findViewById<TextView>(R.id.lbl_number_challenges_history_child)
        var vc = cv.findViewById<TextView>(R.id.lbl_number_vc_history_child)


        val arrDateTo =  chapter.to_date.split("-")
        val arrDateFrom = chapter.from_date.split("-")


        coins.text = chapter.total_challenges.toString() + " Retos"
        vc.text = chapter.total_points.toString()  + " VC"
        dateFrom.text =  arrDateFrom.get(2)  + "/" + utilsClass().convertIntMonthToStringMonth(arrDateFrom.get(1).toInt())
        dateTo.text =  arrDateTo.get(2) + "/" + utilsClass().convertIntMonthToStringMonth(arrDateTo.get(1).toInt())


        return  cv

    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return listP.get(groupPosition).challenges.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return listP.get(groupPosition).challenges.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {

        var cv = convertView
        val chapter = getChild(groupPosition, childPosition) as ChallengesAvailableResponse

        if(cv == null){
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            cv = inflater.inflate(R.layout.item_history_child, null)
        }

        val img = cv!!.findViewById<ImageView>(R.id.img_challenge_history_child)
        val lblTitleChallenge = cv.findViewById<TextView>(R.id.lbl_name_history_child)
        val lblDescriptionChallenge = cv.findViewById<TextView>(R.id.lbl_description_history_child)
        var lblPintsChallenge  = cv.findViewById<TextView>(R.id.lbl_points_history_child)
        var btnSelected = cv.findViewById<LinearLayout>(R.id.btn_click_history_child)


        lblDescriptionChallenge.text = chapter.challenge_description
        lblTitleChallenge.text = chapter.challenge_name
        lblPintsChallenge.text = chapter.challenge_points.toString()
        val imageStr = chapter.challente_image

        if(imageStr != ""){
            Picasso.get().load(imageStr).error(R.drawable.no_image).into(img)
        }

        btnSelected.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                val obj = ChallengeModel(-1, true, -1, chapter.challente_image, chapter.challenge_name, chapter.challenge_points, chapter.challenge_description, 0.0, 0.0, true)
                click.click(obj)
            }
        })

        return  cv
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return listP.size
    }

}