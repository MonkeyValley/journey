package com.example.jurney_mobile.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jurney_mobile.Model.CreatePassword.ICreatePassword
import com.example.jurney_mobile.Presenter.CreatePasswordPresenter
import com.example.jurney_mobile.R

class CreatePasswordActivity : AppCompatActivity(), ICreatePassword.View {

    private lateinit var presenter:ICreatePassword.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_password)

        presenter = CreatePasswordPresenter(this, this)
        presenter.initInfo()
    }



}
