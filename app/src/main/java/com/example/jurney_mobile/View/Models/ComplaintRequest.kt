package com.example.jurney_mobile.View.Models

data class ComplaintRequest( var collaborator_id:Int, var date:String, var complaint_category_id:Int, var complaint_place_id:Int, var complaint_denounced_id:Int, var comment:String, var complaint_place_name:String, var complaint_place_type:Int, var complaint_date:String )