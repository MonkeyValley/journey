package com.example.jurney_mobile.View.Models
import android.graphics.drawable.Drawable

data class SliderModel (var title:String="", var description:String="", var icon:String="", var typeSlider:String="")