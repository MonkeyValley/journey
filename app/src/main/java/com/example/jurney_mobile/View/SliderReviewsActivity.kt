package com.example.jurney_mobile.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jurney_mobile.Model.Reviews.IReviews
import com.example.jurney_mobile.Presenter.SliderReviewsPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate

class SliderReviewsActivity : AppCompatActivity(), IReviews.SliderView   {

    lateinit var presenter:IReviews.SliderPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)

        presenter = SliderReviewsPresenter(this, this)
        presenter.initView()
    }

}
