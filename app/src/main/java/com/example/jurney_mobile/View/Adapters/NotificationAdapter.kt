package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.GetOut.IGetout
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.NotificationModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationRealmModel
import io.realm.RealmResults
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter ( ctx:Context, itemSelected:selectedNotificationDelegate, items:ArrayList<NotificationRealmModel>): RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    var list:ArrayList<NotificationRealmModel>
    lateinit var viewHolder:ViewHolder
    var itemSelected:selectedNotificationDelegate
    var ctx:Context
    init {
        this.list = items
        this.itemSelected = itemSelected
        this.ctx = ctx
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblActivity.text = list.get(position)!!.title
        holder.lblActivityDesc.text = list.get(position)!!.body
        if(list.get(position)!!.view){
            holder.indicatorRead.visibility = View.INVISIBLE
        }else{
            holder.indicatorRead.visibility = View.VISIBLE
        }

        holder.layoutSelected.setOnClickListener {
            itemSelected.selectedNotification(position)
            holder.layoutSelected.setBackgroundColor(ctx.resources.getColor(R.color.white) )
        }
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblActivity = vista.lbl_title_notification
        val lblActivityDesc = vista.lbl_description_notification
        var layoutSelected = vista.notificationSelected
        var indicatorRead = vista.layer_is_read
    }
}
interface selectedNotificationDelegate{
    fun selectedNotification(index:Int)
}
