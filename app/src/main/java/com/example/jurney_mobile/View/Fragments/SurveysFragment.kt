package com.example.jurney_mobile.View.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.Surveys.ISurveys
import com.example.jurney_mobile.Presenter.SurveysPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Adapters.SurveysAdapter
import com.example.jurney_mobile.View.Models.SurveysListModel
import com.example.jurney_mobile.View.SurveyActivity
import kotlinx.android.synthetic.main.surveys_fragment.view.*
import java.util.ArrayList

class SurveysFragment:Fragment(), IClickItemDelegate.clickItemSurvey, ISurveys.View {

    private lateinit var presenter:ISurveys.Presenter
    private lateinit var inf:View
    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        inf = inflater.inflate(R.layout.surveys_fragment, container, false)
        return  inf
    }

    override fun onResume() {
        super.onResume()
        initView(inf)
    }
    fun initView(v: View){
        presenter = SurveysPresenter(this, v, this, this)
        presenter.initView()
    }
    override fun click(item: SurveysListModel, position: Int) {
        presenter.onClickItemActivity(item, position)
    }
}