package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Awards.IAwards
import com.example.jurney_mobile.Presenter.AwardsPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.AwardsAdapter
import com.example.jurney_mobile.View.Adapters.FilterAdapter
import com.example.jurney_mobile.View.Adapters.HomeAdapter
import java.util.logging.Filter

class AwardsFragment:Fragment(), IAwards.View{

    lateinit var presenter:IAwards.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.awards_fragment, container, false)
        initView(inf)
        return inf
    }

    fun initView(v:View){
        presenter = AwardsPresenter(this, this, v )
        presenter.initInfo()
    }

}