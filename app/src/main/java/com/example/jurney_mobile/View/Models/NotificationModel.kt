package com.example.jurney_mobile.View.Models

data class NotificationModel (var title:String="", var body:String="", var view:Boolean=false, var type:String="", var extras:ExtrasModel=ExtrasModel("",0.0, 0), var extrasRedeem:ExtrasRedeemModel=ExtrasRedeemModel(0, "", 0, 0, ""), var description:String = "")