package com.example.jurney_mobile.View.Models

data class AwardsResponseModel(var coins:Int=-1, var description:String="", var  id:Int=-1, var image:String="", var name:String="", var status:Boolean = true)