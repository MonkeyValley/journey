package com.example.jurney_mobile.View.Models

data class EncuestaModel ( var errors:String, var survey_id:Int, var business_unit_id:ArrayList<Int>, var name:String, var description:String, var coins:Int, var is_active: Boolean, var questions:ArrayList<PreguntaModel>)