package com.example.jurney_mobile.View.Models

import java.util.ArrayList

class ReviewModelResponse(
    var business_unit_id: Int,
    var coins: Int,
    var description: String,
    var name:String,
    var review_id: Int,
    var reviewing_area_id: Int,
    var status: Boolean,
    var topics: ArrayList<TopictsModel>,
    var type_origin: String
    )