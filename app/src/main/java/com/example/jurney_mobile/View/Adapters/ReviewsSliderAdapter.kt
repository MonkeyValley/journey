package com.example.jurney_mobile.View.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Fragments.*
import com.example.jurney_mobile.View.Models.PreguntaModel
import com.example.jurney_mobile.View.Models.TopictsModel
import id.zelory.compressor.overWrite

class ReviewsSliderAdapter (manager: FragmentManager, list: ArrayList<TopictsModel>, clickCallBack: IClickItemDelegate.clickItemReviewFragment,  writeCallBack:IClickItemDelegate.afterTextChangedDelegate): FragmentPagerAdapter(manager) {

    var list : ArrayList<TopictsModel>
    var click:IClickItemDelegate.clickItemReviewFragment
    var write:IClickItemDelegate.afterTextChangedDelegate

    init{ this.list = list; click = clickCallBack; this.write = writeCallBack}

    override fun getItem(position: Int): Fragment {
        var frag:Fragment

        if(list.get(position).id >= 0) {
            frag =  ReviewSatisfactionFragment()
            frag.clickCalback = click
            frag.data = list.get(position)
        }else{
            frag =  ComentReviewFragent()
            frag.callBack = write
        }

        return frag
    }
    override fun getCount(): Int = list.size
}