package com.example.jurney_mobile.View.Retrofit

import android.accessibilityservice.GestureDescription
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.google.gson.Gson
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File


object RetrofitClient {
    var BASE_URL = EnviromentConstants().prod_server
    private val gson=Gson()
    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor{
            val original = it.request()
            val requestBuilder = original.newBuilder()
                .method(original.method(), original.body())
            val request = requestBuilder.build()
            Log.e("URL", request.url().toString())
            Log.e("BODY", gson.toJson(request.body()))
            it.proceed(request)
        }.build()

    val instance:IAPI by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        retrofit.create(IAPI::class.java)
    }
}