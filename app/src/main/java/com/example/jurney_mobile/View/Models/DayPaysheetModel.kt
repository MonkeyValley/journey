package com.example.jurney_mobile.View.Models

data class DayPaysheetModel (var date:String = "", var day:String = "", var discount:String = "", var income:String = "", var has_complaint:Boolean=false, var payroll_complaint_id:Int)