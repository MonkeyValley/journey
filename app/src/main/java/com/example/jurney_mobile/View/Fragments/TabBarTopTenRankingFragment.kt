package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.RankingAdapter
import com.example.jurney_mobile.View.Models.UserRankingResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.tab_bar_topten_ranking_fragment.*
import kotlinx.android.synthetic.main.tab_bar_topten_ranking_fragment.view.*
import org.json.JSONArray
import java.util.*
import kotlin.collections.ArrayList

class TabBarTopTenRankingFragment: Fragment(), View.OnClickListener {

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: RankingAdapter
    private lateinit var sharedPreferences:SharedPreferences
    lateinit var token:String
    lateinit var user:UserResponseModel
    var array = ArrayList<UserRankingResponseModel>()
    val arrayResp = ArrayList<UserRankingResponseModel>()
    var gson=Gson()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.tab_bar_topten_ranking_fragment, container, false)

        sharedPreferences = this.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!

        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        val topten = sharedPreferences.getString("_listtopten", "")
        if(topten != "") {
            val jsonArray = JSONArray(topten)
            for (i in 0 until jsonArray.length()) {
                array.add(gson.fromJson(jsonArray.get(i).toString(), UserRankingResponseModel::class.java))
                arrayResp.add(gson.fromJson(jsonArray.get(i).toString(), UserRankingResponseModel::class.java))
            }
        }
        inf.btn_search_ranking.setOnClickListener(this)

        list = inf.findViewById(R.id.list_ranking_golden)
        layoutManager = LinearLayoutManager(this.context)
        adaptador = RankingAdapter(array, "TopTen", this)
        list.layoutManager = layoutManager
        list.adapter = adaptador

        return inf
    }

    override fun onClick(v: View?) {

        val text = txt_search_topten.text.toString()
        if(text != ""){
            array = ArrayList()
            for (item in arrayResp){
                val completeName = item.name.toLowerCase(Locale.ROOT) + " " + item.first_name.toLowerCase(Locale.ROOT) + " " + item.last_name.toLowerCase(Locale.ROOT)
                if(completeName.contains(text.toLowerCase(Locale.ROOT))){
                    array.add(item)
                }
            }
        }else{
            array = arrayResp
        }

        layoutManager = LinearLayoutManager(this.context)
        adaptador = RankingAdapter(array, "TopTen", this)
        list.layoutManager = layoutManager
        list.adapter = adaptador
    }
}