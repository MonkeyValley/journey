package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.WeekendQuality.IQualityWeekend
import com.example.jurney_mobile.Presenter.QualityWeekendPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.QualityWeekwndAdapter
import kotlinx.android.synthetic.main.quality_weeken_fragment.view.*
import java.util.ArrayList

class QualityWeekenFragment:Fragment(), IQualityWeekend.View {
    private lateinit var presenter:IQualityWeekend.Presenter
    override fun onCreateView(  inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val inf = inflater.inflate(R.layout.quality_weeken_fragment, container, false)
        initView(inf)
        return inf
    }

    fun initView(v:View){
         presenter = QualityWeekendPresenter(this, this, v)
        presenter.initData()
        presenter.getQualityScore()

    }

}