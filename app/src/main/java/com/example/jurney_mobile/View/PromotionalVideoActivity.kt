package com.example.jurney_mobile.View

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.YouTubeConfig
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_promotional_video.*

class PromotionalVideoActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener, YouTubePlayer.PlayerStateChangeListener, View.OnClickListener  {

    lateinit var mYoutubePlayer: YouTubePlayerView
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var user: UserResponseModel
    private val gson = Gson()
    private var video_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promotional_video)

        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val responseStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )

        onGetVideoFromFirebase()
        btn_skipt_video.setOnClickListener(this)
    }

    fun onGetVideoFromFirebase(){
        val videoFromBussinesUnity = "video_url_" + user.business_unit_id

        myRef.child("data_base").child("Slider").child(videoFromBussinesUnity).addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){
                    val apiKey =  YouTubeConfig().getApiKeyYoutube()
                    this@PromotionalVideoActivity.video_id = dataSnapshot.getValue().toString()
                    youtubeView.initialize(apiKey, this@PromotionalVideoActivity)
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, p1: YouTubePlayer?, p2: Boolean) {
        //https://youtu.be/f2YROGsGBAU
        if(p1 != null) {
            p1.setPlayerStateChangeListener(this@PromotionalVideoActivity)
            p1.loadVideo(video_id)
        }else{
            Toast.makeText(this, "No se pudo reproducir el video", Toast.LENGTH_LONG).show()
        }
    }

    override fun onBackPressed() = nextStep()
    override fun onClick(v: View?) = nextStep()
    override fun onVideoEnded() = nextStep()
    override fun onError(p0: YouTubePlayer.ErrorReason?) = nextStep()
    override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {}
    override fun onAdStarted() {}
    override fun onLoading() {}
    override fun onVideoStarted() {}
    override fun onLoaded(p0: String?) {}


    fun nextStep(){
        val intent = Intent( this@PromotionalVideoActivity  , Home::class.java)
        intent.putExtra("isNotificastion","home")
        startActivity(intent)
        this@PromotionalVideoActivity.finish()
    }
}
