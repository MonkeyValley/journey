package com.example.jurney_mobile.View

import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.Complaints.IComplaint
import com.example.jurney_mobile.Presenter.ComplaintsActivityPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.ConnectionReciver
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.MyApplication
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.View.Adapters.ComplaintSliderAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_complaints.*

class ComplaintsActivity : AppCompatActivity(), ViewPager.OnPageChangeListener, View.OnClickListener, IComplaint.ViewActivity, IClickItemDelegate.clickItemSlide, ConnectionReciver.ConnectionReciverlistener {

    val reciverj = ConnectionReciver()
    lateinit var adapter: ComplaintSliderAdapter
    private var presenter = ComplaintsActivityPresenter(this, this)
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complaints)
        baseContext.registerReceiver( reciverj, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
       initView()
    }

    override fun onResume() {
        super.onResume()
        MyApplication.instance.setConnectionLsitenner(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        baseContext.unregisterReceiver(reciverj)
    }

    override fun onBackPressed() {
        this.finish()
    }

    fun initView(){
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorBackground)
        }

        presenter.initInfo()

        adapter = ComplaintSliderAdapter(supportFragmentManager, this, this)
        container_slider_complaint_dos.adapter = adapter
        container_slider_complaint_dos.addOnPageChangeListener(this)
    }

    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) = presenter.onPageSelected(position)
    override fun onClick(v: View?) = presenter.onClick(v!!)
    override fun close() {
        this.finish()
    }
    override fun click(item: Int, cat: String) = presenter.selectedItem(item, cat)

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showAlert(isConnected)
    }

    fun showAlert(isConnected:Boolean){
        val editor = sharedPreferences.edit()
        editor.putBoolean("_isConnected", isConnected)
        editor.apply()

        if(isConnected){
            SnackAlert.closeAlert()
        }else{
            SnackAlert.alert("Perdiste la conexión a internet. Asegúrate de tenerlo para usar VivaJourney", findViewById(R.id.container_surveys), "error", Snackbar.LENGTH_INDEFINITE, "BOTTOM")
            SnackAlert.showAlert()
        }
    }
}
