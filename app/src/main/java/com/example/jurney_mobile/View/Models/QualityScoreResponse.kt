package com.example.jurney_mobile.View.Models

data class QualityScoreResponse (var average_performance:Double, var average_score:Double, var activities:ArrayList<ActivitiesQualityScore>)