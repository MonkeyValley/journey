package com.example.jurney_mobile.View.Fragments

import android.net.wifi.aware.IdentityChangedListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.PreguntaModel
import kotlinx.android.synthetic.main.survey_satifaction_fragment.*
import kotlinx.android.synthetic.main.survey_satifaction_fragment.view.*

class SurveyFragmentSatifaction:Fragment(), View.OnClickListener {

    lateinit var data: PreguntaModel
    lateinit var type:String
    lateinit var clickCalback:IClickItemDelegate.clickItemSurveyFragment
    lateinit var viewint:View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewint = inflater.inflate(R.layout.survey_satifaction_fragment, container, false)
        initView()
        return viewint
    }

    fun initView(){
        viewint.lbl_question_name_satisfaction.text = data.description
        viewint.btn_verygood.setOnClickListener(this)
        viewint.btn_good.setOnClickListener(this)
        viewint.btn_soso.setOnClickListener(this)
        viewint.btn_bad.setOnClickListener(this)
        viewint.btn_verybad.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        viewint.linear_selected_verygood.setBackgroundColor(resources.getColor(R.color.colorBackground))
        viewint.linear_selected_good.setBackgroundColor(resources.getColor(R.color.colorBackground))
        viewint.linear_selected_soso.setBackgroundColor(resources.getColor(R.color.colorBackground))
        viewint.linear_selected_bad.setBackgroundColor(resources.getColor(R.color.colorBackground))
        viewint.linear_selected_verybad.setBackgroundColor(resources.getColor(R.color.colorBackground))

        when(v!!.id){
            R.id.btn_verygood -> {

                viewint.linear_selected_verygood.background = resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_verygood.setTextColor(resources.getColor(R.color.colorBackground))

                for (i in data.choices){
                    if(i.value == "Muy bien"){
                        clickCalback.click(i.question_choice_id, 1, true)
                    }
                }
            }
            R.id.btn_good -> {

                viewint.linear_selected_good.background = resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_good.setTextColor(resources.getColor(R.color.colorBackground))
                for (i in data.choices){
                    if(i.value == "Bien"){
                        clickCalback.click(i.question_choice_id, 1, true)
                    }
                }
            }
            R.id.btn_soso -> {

                viewint.linear_selected_soso.background = resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_soso.setTextColor(resources.getColor(R.color.colorBackground))
                for (i in data.choices){
                    if(i.value == "Normal"){
                        clickCalback.click(i.question_choice_id, 1, true)
                    }
                }
            }
            R.id.btn_bad -> {

                viewint.linear_selected_bad.background = resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_bad.setTextColor(resources.getColor(R.color.colorBackground))
                for (i in data.choices){
                    if(i.value == "Mal"){
                        clickCalback.click(i.question_choice_id, 1, true)
                    }
                }
            }
            R.id.btn_verybad -> {

                viewint.linear_selected_verybad.background = resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_verybad.setTextColor(resources.getColor(R.color.colorBackground))
                for (i in data.choices){
                    if(i.value == "Muy mal"){
                        clickCalback.click(i.question_choice_id, 1, true)
                    }
                }
            }
        }

    }

}