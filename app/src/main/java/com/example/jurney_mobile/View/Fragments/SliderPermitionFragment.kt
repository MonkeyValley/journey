package com.example.jurney_mobile.View.Fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.SliderModel
import com.example.jurney_mobile.View.SliderActivity
import kotlinx.android.synthetic.main.slider_permition_fragment.view.*
import java.util.jar.Manifest

class SliderPermitionFragment:Fragment() {

    var item:SliderModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.slider_permition_fragment, container, false)

        if(item != null) {
            if (item!!.title != "" || item!!.description != "" || item!!.icon != "" || item!!.typeSlider != "") {
                val drawableResourceId = this.resources.getIdentifier(
                    item!!.icon,
                    "drawable",
                    this.activity!!.getPackageName()
                )
                inf.img_permition.setImageResource(drawableResourceId)
                inf.title_permition.text = item!!.title
                inf.title_permition.setTextColor(resources.getColor(R.color.charcoal))
                inf.description_permition.text = item!!.description
            } else {
                inf.title_permition.text = "No se ha encontrado informacion para mostrar"
                inf.title_permition.setTextColor(resources.getColor(R.color.pinkish_red))
            }
        }
        return inf
    }

    override fun onResume() {
        super.onResume()

        Log.e("","")
    }

    fun setData(item: SliderModel){
        this.item = item
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            1 -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                }
                else{
                    //permission from popup denied
                }
            }
        }
    }


}