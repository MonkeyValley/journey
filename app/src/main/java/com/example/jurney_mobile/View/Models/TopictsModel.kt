package com.example.jurney_mobile.View.Models

data class TopictsModel (
    var description: String,
    var id: Int,
    var name: String,
    var review_id: Int
)