package com.example.jurney_mobile.View.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Profile.IProfile
import com.example.jurney_mobile.Presenter.ProfilePresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.View.Models.UploadImageResponse
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.profile_fragment.view.*

class ProfileFragment:Fragment(), IProfile.View, View.OnClickListener{

    lateinit var presenter:IProfile.Presenter
    lateinit var ref:View
    override fun onCreateView( inflater: LayoutInflater,  container: ViewGroup?,  savedInstanceState: Bundle? ): View? {
        ref = inflater.inflate(R.layout.profile_fragment, container, false)
        initView(ref)
        ref.iv_user_image_profile.setOnClickListener(this)
        return ref
    }

    fun initView(v:View){
        presenter = ProfilePresenter(this, this, v)
        presenter.initInfo()
    }

    override fun onClick(v: View?) {
        presenter.click(v!!)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter.onActivityResult(requestCode, resultCode, data)
    }

}