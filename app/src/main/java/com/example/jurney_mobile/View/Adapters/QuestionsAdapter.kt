package com.example.jurney_mobile.View.Adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.RespuestaModel
import kotlinx.android.synthetic.main.item_question_survey.view.*

class QuestionsAdapter(items:ArrayList<RespuestaModel>, ctx:Context, onItemSelected:IClickItemDelegate.clickAnswerSelected, type:Int): RecyclerView.Adapter<QuestionsAdapter.ViewHolder>() {

    var list:ArrayList<RespuestaModel>
    var ctx:Context
    var type:Int
    lateinit var viewHolder:ViewHolder
    private var onClickItem:IClickItemDelegate.clickAnswerSelected

    init {
        this.type = type
        this.list = items
        this.ctx = ctx
        this.onClickItem = onItemSelected
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_question_survey, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int = list.count()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).value
        holder.item.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if(type == 1){
                    clearList()
                    list.get(position).isSelected = true
                    onClickItem.click(list.get(position).question_choice_id, position, false)
                }else if(type == 2){
                    if(list.get(position).isSelected){
                        list.get(position).isSelected = false
                        onClickItem.click(list.get(position).question_choice_id, position, false)
                    }else{
                        list.get(position).isSelected = true
                        onClickItem.click(list.get(position).question_choice_id, position, true)
                    }
                }
                notifyDataSetChanged()

            }
        })

        if( list.get(position).isSelected ){
            holder.onItemSelected.setBackgroundResource(R.drawable.corner_radius_cool_green)
            holder.item_selected.visibility = View.VISIBLE
            holder.lbl_title_answer_item.setTextColor( Color.WHITE )
        }else{
            holder.onItemSelected.setBackgroundResource(R.drawable.corner_radius_linear)
            holder.item_selected.visibility = View.GONE
            holder.lbl_title_answer_item.setTextColor( ContextCompat.getColor(ctx, R.color.charcoal) )
        }
    }

    class ViewHolder(vista: View):RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_title_answer_item
        val item = vista.onItemSelected
         var onItemSelected = vista.onItemSelected
        var item_selected = vista.item_selected
        var lbl_title_answer_item = vista.lbl_title_answer_item
    }

    fun clearList(){
        list.forEach{
            it.isSelected = false
        }
    }
}
