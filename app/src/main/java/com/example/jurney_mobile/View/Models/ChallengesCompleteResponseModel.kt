package com.example.jurney_mobile.View.Models

data class ChallengesCompleteResponseModel (   var challenges:ArrayList<ChallengesAvailableResponse>,
                                               var challenges_no:Int,
                                              var date_from:String,
                                              var date_to:String,
                                              var points:Int)