package com.example.jurney_mobile.View.Adapters

import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.View.DailyPayActivity
import com.example.jurney_mobile.View.Models.DailyPayModel
import com.example.jurney_mobile.View.Models.DayPaysheetModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_dayli_pay.view.*

class DailypayAdapter (ctx: DailyPayActivity, items:List<DailyPayModel>, click:ClickItemDailypayDelegate): RecyclerView.Adapter<DailypayAdapter.ViewHolder>() {

    var list:List<DailyPayModel>
    var clickItem:ClickItemDailypayDelegate
    var ctx:DailyPayActivity
    init {
        this.list = items
        this.ctx = ctx
        this.clickItem = click
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_dayli_pay, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list.get(position)

        holder.lblTitle.text = "Día " + (position + 1).toString()

        val dateStr = list.get(position).date.split("-")
        if(dateStr.size > 0) {
            holder.lbldate.text = dateStr.get(2) + "/" + dateStr.get(1)
        }else{
            holder.lbldate.text = ""
        }


        if(!item.available){
            holder.btnSelectedIte.visibility = View.GONE
            holder.checkIcon.visibility = View.VISIBLE
            holder.loading.visibility = View.GONE

        }else{
            holder.btnSelectedIte.visibility = View.VISIBLE
            holder.checkIcon.visibility = View.GONE
            holder.loading.visibility = View.GONE
        }

        holder.btnSelectedIte.setOnClickListener{
            Log.e("CLICK -> ", Gson().toJson(item))

            holder.btnSelectedIte.visibility = View.GONE
            holder.checkIcon.visibility = View.GONE
            holder.loading.visibility = View.VISIBLE

            var alertConfirmDailyPay = LoadingDialog(ctx)
            alertConfirmDailyPay.alertConfirmDailypay(
                list.get(position).date,
                "",
                "Cancelar",
                "Confirmar",
                View.OnClickListener {
                    alertConfirmDailyPay.dismissDialog()
                    holder.btnSelectedIte.visibility = View.VISIBLE
                    holder.checkIcon.visibility = View.GONE
                    holder.loading.visibility = View.GONE
                },
                View.OnClickListener {
                    alertConfirmDailyPay.dismissDialog()
                    clickItem.clickItem(list.get(position), position)
                })
        }
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val btnSelectedIte = vista.btn_get_dailypay
        val checkIcon = vista.ic_check_dailypay
        val loading = vista.loading_send_dailypay
        val lblTitle = vista.lbl_title_day_dailypay
        val lbldate = vista.lbl_date_dailypay
    }
}
interface ClickItemDailypayDelegate{
    fun clickItem(item:DailyPayModel, position:Int)
}