package com.example.jurney_mobile.View.Models.RealmModels

import io.realm.RealmObject

open class NotificationExtrasRealModel(var notificationId:Int=0, var id:Int=0, var description:String="", var lvl:Int=0, var points:Int=0):RealmObject()