package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Awards.IAwards
import com.example.jurney_mobile.Presenter.AwardsCodesPresenter
import com.example.jurney_mobile.R
import com.google.gson.Gson

class TabBarListAwardsFragment:Fragment(), IAwards.ViewCodes {

    var gson = Gson()
    lateinit var inf:View
    lateinit var presenter:IAwards.PresenterCodes

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        inf = inflater.inflate(R.layout.tab_bar_list_awards_fragment, container, false)
        presenter = AwardsCodesPresenter(this, this, inf)
        presenter.initInfo()
        return inf
    }

}