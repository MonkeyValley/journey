package com.example.jurney_mobile.View.Models

data class HistoryChallengesResponseModel (var challenges: ArrayList<ChallengesAvailableResponse>, var total_points:Int=0, var total_challenges:Int=0 , var from_date:String, var to_date:String)