package com.example.jurney_mobile.View.Models

data class ReviewRequestModel(
    var ops_supervisor_review_id: Int,
    var comment: String,
    var topics: ArrayList<TopicsRequestModel>
)