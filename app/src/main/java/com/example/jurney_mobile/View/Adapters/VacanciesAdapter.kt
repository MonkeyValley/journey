package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.VacancyResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_awards.view.*
import kotlinx.android.synthetic.main.item_vacancies.view.*

class VacanciesAdapter (items:ArrayList<VacancyResponseModel>, clickItem:IClickItemDelegate.clickVacancies): RecyclerView.Adapter<VacanciesAdapter.ViewHolder>() {

    var list:ArrayList<VacancyResponseModel>
    var selecteditem:IClickItemDelegate.clickVacancies
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.selecteditem = clickItem
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_vacancies, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).name
        holder.btnChangeItme.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                selecteditem.click(list.get(position))
            }
        })
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_vacancie_title
        val btnChangeItme = vista.btn_vacancie_detail
    }
}
