package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Adapters.ChallengeAdapter
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.example.jurney_mobile.View.Models.ChallengesAvailableResponse
import com.google.gson.Gson

class AvailableChallengesTabBarFragment(callbackclickItemChallenge:IClickItemDelegate.clickItemChallenge): Fragment(),  IClickItemDelegate.clickItemChallenge  {


    var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private var adaptador: ChallengeAdapter? = null
    private var callback :IClickItemDelegate.clickItemChallenge

    init{
        this.callback = callbackclickItemChallenge
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val inf = inflater.inflate(R.layout.tab_bar_available_challenges_fragment, container, false)
        initView(inf)
        return inf
    }

    fun initView(v:View){



        sharedPreferences = this.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val data = sharedPreferences.getString("_listchallengesavailables", "")
        val challenges = gson.fromJson(data, Array<ChallengesAvailableResponse>::class.java).toList()

        val challenge = ArrayList<ChallengeModel>()
        challenges.forEach {
            val obj = ChallengeModel(0,false,0,it.challente_image, it.challenge_name, it.challenge_points, it.challenge_description, 0.0, 0.0, false)
            challenge.add(obj)
        }

        list = v.findViewById(R.id.list_availables_challenges_fragment)
        layoutManager = LinearLayoutManager(this.context)
        adaptador = ChallengeAdapter(challenge, this)
        list.isNestedScrollingEnabled = true
        list.layoutManager = layoutManager
        list.adapter = adaptador
    }

    override fun click(item: ChallengeModel) {
        callback.click(item)
    }
}