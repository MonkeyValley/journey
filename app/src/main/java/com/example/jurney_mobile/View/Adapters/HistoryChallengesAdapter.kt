package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.HistoryChallengesResponseModel


class HistoryChallengesAdapter (items:List<HistoryChallengesResponseModel>): RecyclerView.Adapter<HistoryChallengesAdapter.ViewHolder>() {

    var list:List<HistoryChallengesResponseModel>
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_history_challenge, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.lblActivity.text = ""
    }

    class ViewHolder(vista: View): RecyclerView.ViewHolder(vista){
//        val lblActivity = vista.lbl_challenge_name_challenges

    }
}
