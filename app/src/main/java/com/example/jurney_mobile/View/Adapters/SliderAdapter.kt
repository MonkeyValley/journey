package com.example.jurney_mobile.View.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.jurney_mobile.View.Fragments.SliderFragment
import com.example.jurney_mobile.View.Fragments.SliderImageFragment
import com.example.jurney_mobile.View.Fragments.SliderPermitionFragment
import com.example.jurney_mobile.View.Models.SliderModel

class SliderAdapter( manager:FragmentManager, list: ArrayList<SliderModel> ): FragmentPagerAdapter(manager) {

    var list : ArrayList<SliderModel>

    init{ this.list = list }
    override fun getItem(position: Int): Fragment{

        var fragment :Fragment

        val arridentifier = list.get(position).typeSlider.split("_")


        if(arridentifier.get(0) == "permition"){
            fragment = SliderPermitionFragment()
            fragment.setData(list.get(position))
        }else{
            if(list.get(position).typeSlider == "img_complete"){
                fragment = SliderImageFragment()
                fragment.setData(list.get(position))

            }else{
                fragment = SliderFragment()
                fragment.setData(list.get(position))
            }
        }
        return fragment
    }
    override fun getCount(): Int = list.count()

}