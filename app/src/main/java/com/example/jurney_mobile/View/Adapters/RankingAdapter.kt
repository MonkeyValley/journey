package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Models.UserRankingResponseModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_ranking.view.*
import kotlinx.android.synthetic.main.item_ranking_gold.view.*

class RankingAdapter (items:ArrayList<UserRankingResponseModel>, type:String, ctx:Fragment): RecyclerView.Adapter<RankingAdapter.ViewHolder>() {

    var list: ArrayList<UserRankingResponseModel>
    lateinit var viewHolder: ViewHolder
    var type:String
    var ctx:Fragment

    init {
        this.list = items
        this.type = type
        this.ctx = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var layout = R.layout.item_ranking

        if(type == "TopTen"){
            layout = R.layout.item_ranking_gold
        }
        
        val v = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(type=="TopTen"){
            holder.lbl_ranking_gold.text = list.get(position).ranking.toString()
            holder.lbl_name_gold.text = list.get(position).name + " " + list.get(position).first_name
            holder.lbl_level_gold.text = list.get(position).level_id.toString()
            holder.lbl_vc_gold.text = utilsClass().milesFormatNoDecimals( list.get(position).points.toDouble() )

            val lastPosition = list.get(position).last_ranking_update

            when(lastPosition){
                2->holder.img_indicator_gold.setImageDrawable(ctx.resources.getDrawable(R.drawable.ic_ranking_up))
                1, null->holder.img_indicator_gold.setImageDrawable(null)
                0->holder.img_indicator_gold.setImageDrawable(ctx.resources.getDrawable(R.drawable.ic_ranking_down))
            }

            val image = EnviromentConstants().image_serverver + list.get(position).photo
            Picasso.get().load(image).error(R.drawable.no_image).into(holder.img_user_gold)
        }else{
            holder.lbl_ranking.text = list.get(position).ranking.toString()
            holder.lbl_name.text = list.get(position).name + " " + list.get(position).first_name
            holder.lbl_level.text = list.get(position).level_id.toString()
            holder.lbl_vc.text = utilsClass().milesFormatNoDecimals( list.get(position).points.toDouble() )

            val lastPosition = list.get(position).last_ranking_update

            when(lastPosition){
                2->holder.img_indicator.setImageDrawable(ctx.resources.getDrawable(R.drawable.ic_ranking_up))
                1, null->holder.img_indicator.setImageDrawable(null)
                0->holder.img_indicator.setImageDrawable(ctx.resources.getDrawable(R.drawable.ic_ranking_down))
            }
            val image = EnviromentConstants().image_serverver + list.get(position).photo
            Picasso.get().load(image).error(R.drawable.no_image).into(holder.img_user)
        }


    }

    class ViewHolder(vista: View) : RecyclerView.ViewHolder(vista) {
        val lbl_ranking_gold = vista.lbl_ranking_totten_ranking
        val lbl_name_gold = vista.lbl_name_totten_ranking
        val lbl_level_gold = vista.lbl_num_level_totten_ranking
        val lbl_vc_gold = vista.lbl_num_vc_totten_ranking
        val img_user_gold = vista.iv_usuer_image_item_ranking_top_ten
        val img_indicator_gold = vista.indicator_ranking_change

        val lbl_ranking = vista.lbl_ranking_ranking
        val lbl_name = vista.lbl_name_ranking
        val lbl_level = vista.lbl_num_level_ranking
        val lbl_vc = vista.lbl_num_vc_ranking
        val img_user = vista.iv_usuer_image_item_ranking
        val img_indicator = vista.indicator_ranking

    }
}