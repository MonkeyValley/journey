package com.example.jurney_mobile.View.Models

data class UserModel (var id:Int, var email:String, var token:String, var type:Int, var name:String )