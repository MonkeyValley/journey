package com.example.jurney_mobile.View.Models.RealmModels

import io.realm.RealmObject

open class NotificationRealmModel(var body:String="",
                                  var title:String="",
                                  var message:String="",
                                  var type:String="",
                                  var description:String = "",
                                  var view:Boolean = false,
                                  var id:Int = 0,
                                  var extras:NotificationExtrasRealModel?=null
):RealmObject()