package com.example.jurney_mobile.View.Adapters

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.ComplaintsActivity
import com.example.jurney_mobile.View.Fragments.*
import com.example.jurney_mobile.View.Models.PreguntaModel
import kotlinx.android.synthetic.main.complaint_list_fragment.*

class ComplaintSliderAdapter(manager: FragmentManager, onitemselected:IClickItemDelegate.clickItemSlide, ctx:ComplaintsActivity): FragmentPagerAdapter(manager), onClickItemSlider {

    var data = -1
    var onitemselected:IClickItemDelegate.clickItemSlide
    var ctx:ComplaintsActivity
    init{
        this.onitemselected = onitemselected
        this.ctx = ctx
    }

    override fun getItem(position: Int): Fragment {
        var frag = ComplaintListFragment(this)
            frag.data = position
        return frag
    }
    override fun selectedItem(numbItem: Int, cat:String) {
        onitemselected.click(numbItem, cat)
    }
    override fun getCount(): Int = 5
}
