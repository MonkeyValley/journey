package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Fragments.PayrollFragment
import com.example.jurney_mobile.View.Models.DayPaysheetModel
import kotlinx.android.synthetic.main.item_disbursements_payroll.view.*

class PayrollAdapter(items:ArrayList<DayPaysheetModel>, onClickItemDelegate: IClickItemDelegate.clickItemPayroll, ctx: PayrollFragment): RecyclerView.Adapter<PayrollAdapter.ViewHolder>() {

    var list:ArrayList<DayPaysheetModel>
    lateinit var viewHolder:ViewHolder
    private var onClickItemDelegate:IClickItemDelegate.clickItemPayroll
    private var ctx:PayrollFragment

    init {
        this.list = items
        this.onClickItemDelegate = onClickItemDelegate
        this.ctx = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_disbursements_payroll, parent, false)
        viewHolder = ViewHolder(v)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.lblNumDay.text = "Día "+ list.get(position).day
        holder.lblPayment.text = "$"+utilsClass().milesFormat(list.get(position).income.toDouble())
        holder.lblDiscount.text = "$"+utilsClass().milesFormat(list.get(position).discount.toDouble())

        val splitDate = list.get(position).date.split("-")
        holder.lblDate.text = splitDate[2] + "/" + splitDate[1]

        if(!list.get(position).has_complaint){
            holder.btnComplain.background = ctx.context!!.getDrawable(R.drawable.layer_bg_yellow_button)
            holder.btnComplain.text = "Queja"
            holder.btnComplain.setTextColor(ctx.resources.getColor(R.color.charcoal))
        }else{
            holder.btnComplain.background = ctx.context!!.getDrawable(R.drawable.layer_bg_blue_button)
            holder.btnComplain.text = "Ver"
            holder.btnComplain.setTextColor(ctx.resources.getColor(R.color.white))
        }

        holder.btnComplain.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if(!list.get(position).has_complaint) {
                    onClickItemDelegate.click(list[position], position, 1)
                }else{
                    onClickItemDelegate.click(list[position], position, 2)
                }
            }
        })
    }

    class ViewHolder(vista:View):RecyclerView.ViewHolder(vista){
        val lblNumDay = vista.lbl_number_day_item_payroll
        val lblPayment = vista.lbl_payment_item_payroll
        val lblDiscount = vista.lbl_discount_item_payroll
        val btnComplain = vista.btn_complain
        val lblDate = vista.lbl_date_item_payroll
    }

}

