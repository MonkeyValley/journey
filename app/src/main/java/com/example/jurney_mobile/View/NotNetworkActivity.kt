package com.example.jurney_mobile.View

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.ConnectionReciver
import com.example.jurney_mobile.Utils.MyApplication

class NotNetworkActivity : AppCompatActivity(), ConnectionReciver.ConnectionReciverlistener {

//    var cc : ConnectedTrue = conected
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_not_network)
        MyApplication.instance.setConnectionLsitenner(this)

    }

    override fun onBackPressed() {}

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
//        cc.close()
        this.finish()
    }
}