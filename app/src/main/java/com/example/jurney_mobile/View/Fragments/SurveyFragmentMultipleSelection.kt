package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Adapters.QuestionsAdapter
import com.example.jurney_mobile.View.Models.PreguntaModel
import kotlinx.android.synthetic.main.survey_fragment.view.*

class SurveyFragmentMultipleSelection: Fragment(), IClickItemDelegate.clickAnswerSelected {

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: QuestionsAdapter
    lateinit var clickCalback:IClickItemDelegate.clickItemSurveyFragment
    lateinit var data: PreguntaModel
    var type:Int = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val inf = inflater.inflate(R.layout.survey_fragment, container, false)
        return inf
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    fun initView(v: View){
        v.lbl_question_name.text = data.description
        list = v.findViewById(R.id.list_survey)
        layoutManager = LinearLayoutManager(context)
        adaptador = QuestionsAdapter(data.choices, context!!, this, type)
        list.layoutManager = layoutManager
        list.adapter = adaptador
    }

    override fun click(item: Int, position: Int, selected:Boolean) {
        clickCalback.click(item, position, selected)
    }

}
//interface ClickCallBackSurveyFragment{
//    fun clickCallBack(item:String, position:Int, selected:Boolean)
//}