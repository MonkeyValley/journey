package com.example.jurney_mobile.View.Fragments

import android.graphics.drawable.Drawable
import android.media.Image
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.SliderModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.slider_fragment.view.*
import kotlinx.android.synthetic.main.slider_permition_fragment.view.*

class SliderFragment:Fragment() {

    var item:SliderModel? = null

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        var inf = inflater.inflate(R.layout.slider_fragment, container, false)

        if(item != null) {
            if (item!!.title != "" || item!!.description != "" || item!!.icon != "" || item!!.typeSlider != "") {
                inf.findViewById<TextView>(R.id.lbl_title_slider).text = item!!.title
                inf.findViewById<TextView>(R.id.lbl_description_slider).text = item!!.description
                inf.lbl_title_slider.setTextColor(resources.getColor(R.color.charcoal))

                if (item!!.typeSlider == "normal") {
                    val drawableResourceId = this.resources.getIdentifier(
                        item!!.icon,
                        "drawable",
                        this.activity!!.getPackageName()
                    )
                    inf.findViewById<ImageView>(R.id.icon_slider)
                        .setImageResource(drawableResourceId)
                } else {
                    Picasso.get().load(item!!.icon).error(R.drawable.no_image)
                        .into(inf.findViewById<ImageView>(R.id.icon_slider))
                }

            } else {
                inf.lbl_title_slider.text = "No se ha encontrado informacion para mostrar"
                inf.lbl_title_slider.setTextColor(resources.getColor(R.color.pinkish_red))
            }
        }
        return inf
    }

    fun setData(item:SliderModel){
        this.item = item
    }
}
