package com.example.jurney_mobile.View.Models

data class RequestEncuestaMultipleModel ( var collaborator_id:Int, var survey_id:Int, var questions:ArrayList<RequestPreguntaMultipleModel> )