package com.example.jurney_mobile.View.Models

data class GetoutResponseModel(var collaborator_id:Int, var date:String, var id:Int, var reason_for_leaving_id:Int, var status:String)