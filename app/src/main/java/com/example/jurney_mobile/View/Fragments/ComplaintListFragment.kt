package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.SnackAlert
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.complaint_list_fragment.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class ComplaintListFragment(onselecteditem:onClickItemSlider): Fragment(), View.OnClickListener {

    var data = -1
    var onselecteditem:onClickItemSlider
    var categorie = ""
    private lateinit var frag:View

    lateinit var sharedPreferences: SharedPreferences

    init {
        this.onselecteditem = onselecteditem
    }

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
         frag = inflater.inflate(R.layout.complaint_list_fragment, container, false)

        sharedPreferences = context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        when(data){
            0 -> {
                categorie = "who"
                frag.lbl_title_step.text = "Selecciona quien fue"
                frag.img_first_item_complaint_slider.setImageResource(R.drawable.ic_partner)
                frag.img_second_item_complaint_slider.setImageResource(R.drawable.ic_boss)
                frag.img_thirt_item_complaint_slider.setImageResource(R.drawable.ic_another_area)

                frag.lbl_first_item_complaint_slider.setText("Compañero de trabajo")
                frag.lbl_second_item_complaint_slider.setText("Jefe directo")
                frag.lbl_thirt_item_complaint_slider.setText("Alguien de otra área")

            }
            1 -> {
                categorie = "where"
                frag.lbl_title_step.text = "Selecciona donde fue"
                frag.img_first_item_complaint_slider.setImageResource(R.drawable.ic_greenhouse)
                frag.img_second_item_complaint_slider.setImageResource(R.drawable.ic_packing)
                frag.img_thirt_item_complaint_slider.setImageResource(R.drawable.ic_households)

                frag.lbl_first_item_complaint_slider.setText("Invernadero / Mallas")
                frag.lbl_second_item_complaint_slider.setText("Empaque")
                frag.lbl_thirt_item_complaint_slider.setText("Viviendas")

            }
            2 -> {
                categorie = "more_"
                frag.lbl_title_step.text = "Más detalles"
                frag.layer_more_details.visibility = View.VISIBLE
                frag.layer_list.visibility = View.GONE
                frag.layer_last.visibility = View.GONE
                frag.layer_date.visibility = View.GONE
            }
            3 -> {

                val curDate =  Date()
                val sdf: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                val mDate = sdf.parse(sdf.format(curDate))
                val timeInMilliseconds = mDate.getTime()

                frag.calendarComplaint.maxDate = timeInMilliseconds

                categorie = "date_"+sdf.format(curDate)
                frag.lbl_title_step.text = "Fecha"
                frag.layer_more_details.visibility = View.GONE
                frag.layer_list.visibility = View.GONE
                frag.layer_last.visibility = View.GONE
                frag.layer_last.visibility = View.GONE
                frag.layer_date.visibility = View.VISIBLE
            }
            4 -> {
                categorie = "last"
                frag.lbl_title_step.text = "Por último"
                frag.layer_more_details.visibility = View.GONE
                frag.layer_list.visibility = View.GONE
                frag.layer_date.visibility = View.GONE
                frag.layer_last.visibility = View.VISIBLE
            }
        }

        frag.calendarComplaint.setOnDateChangeListener(object : CalendarView.OnDateChangeListener{
            override fun onSelectedDayChange(view: CalendarView, year: Int, month: Int, dayOfMonth: Int) {
                val date  = year.toString() + "-" + (month + 1)+ "-" + dayOfMonth
                Log.e("PICK DATE",date)
                if(categorie == "date_"){
                    categorie = categorie + date
                }else{
                    categorie = "date_"+date
                }

            }

        })

        frag.btn_first_item_complaint_slider.setOnClickListener(this)
        frag.btn_second_item_complaint_slider.setOnClickListener(this)
        frag.btn_thirt_item_complaint_slider.setOnClickListener(this)
        frag.btn_date_complaint_next_step.setOnClickListener(this)
        frag.btn_next_step_commetn.setOnClickListener(this)
        frag.btn_next_step_finalize_complaint.setOnClickListener(this)

        frag.btn_make_call.setOnClickListener(this)

        return frag
    }

    fun fn_permission(){
        ActivityCompat.requestPermissions(activity!!,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            100)
    }

    override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.btn_first_item_complaint_slider -> onselecteditem.selectedItem(1, categorie)
                R.id.btn_second_item_complaint_slider -> onselecteditem.selectedItem(2, categorie)
                R.id.btn_thirt_item_complaint_slider -> onselecteditem.selectedItem(3, categorie)
                R.id.btn_date_complaint_next_step -> {
                    onselecteditem.selectedItem(5, categorie)
                }
                R.id.btn_next_step_commetn -> {
                    categorie = categorie + frag.txt_more_info_complaint.text
                    onselecteditem.selectedItem(4, categorie)
                }
                R.id.btn_next_step_finalize_complaint -> {
                    val isConnected = sharedPreferences.getBoolean("_isConnected", false)
                    if (isConnected) {
                        onselecteditem.selectedItem(6, categorie)
                    }
                }
                R.id.btn_make_call->{
                    val phone = sharedPreferences.getString("_phonesurvey","")

                    if(phone != "") {
                        val phoneIntent = Intent(Intent.ACTION_CALL)
                        phoneIntent.setData(Uri.parse("tel:" + phone!!))

                        if (ActivityCompat.checkSelfPermission(
                                context!!,
                                android.Manifest.permission.CALL_PHONE
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            fn_permission()
                        } else {
                            startActivity(phoneIntent)
                        }
                    }else{
                        SnackAlert.alert("Número no valido",frag.container_slider_complaint,"",Snackbar.LENGTH_LONG,"")
                        SnackAlert.showAlert()
                    }

                }
            }
    }
}

interface onClickItemSlider{
    fun selectedItem(numbItem:Int, cat:String)
}