package com.example.jurney_mobile.View

import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Home.IHome
import com.example.jurney_mobile.Presenter.HomePresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.ConnectionReciver
import com.example.jurney_mobile.Utils.MyApplication
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.View.Fragments.*
import com.example.jurney_mobile.View.Models.NotificationModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationRealmModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home.*


class Home : AppCompatActivity(), View.OnClickListener, OnClickItemsDelegate, IHome.View, ConnectionReciver.ConnectionReciverlistener, notificationSelectedHomeDelegate{

    lateinit var bottomSheet: BottomSheetDialog
    private var presenter:IHome.Presenter = HomePresenter(this, this)
    val reciverj = ConnectionReciver()
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initView()
        baseContext.registerReceiver( reciverj, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onResume() {
        super.onResume()
        MyApplication.instance.setConnectionLsitenner(this)
        presenter.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        baseContext.unregisterReceiver(reciverj)
    }

    override fun onBackPressed() {
        presenter.showFragment("home", "profile", HomeFragment(), true, "home_uno")
    }
    override fun onClick(v: View?) = presenter.onClickListener(v!!)
    override fun onClickNomina() { presenter.showFragment("nomina", "vivacoins", PayrollFragment(), false, "home_dos") }
    override fun onClickCalidad() = presenter.showFragment("calidad", "vivacoins", QualityWeekenFragment(),false, "home_tres")
    override fun onClickRetos() = presenter.showFragment("retos", "vivacoins", ChallengesFragment(),false, "home_cuatro")
    override fun onResumeDelegate() {
       presenter.onResume()
    }

    override fun closeBottomSheet() = bottomSheet.dismiss()
    override fun showBottomSheet() = bottomSheet.show()

    fun initView(){
        bottomSheet = BottomSheetDialog(this,  R.style.BottomSheetDialogTheme )
        sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val viewBottomSheet = LayoutInflater.from(this).inflate(  R.layout.bottomsheet_menu_layout, findViewById(  R.id.bottomSheetContainer ))
        bottomSheet.setContentView(viewBottomSheet)

        bottomSheet.findViewById<LinearLayout>(R.id.opc_init_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_payroll_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_logout_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_quality_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_challenges_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_quests_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.btn_goto_profile)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_complaints_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_awards_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_ranking_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_vacancies_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_notification_menu)!!.setOnClickListener(this)
        bottomSheet.findViewById<LinearLayout>(R.id.opc_reviews_menu)!!.setOnClickListener(this)

        header_profile.setOnClickListener(this)
        btn_menu.setOnClickListener(this)

        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorBackground)
        }
        presenter.initInfo(bottomSheet)
    }
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showAlert(isConnected)
    }

    fun showAlert(isConnected:Boolean){

        val editor = sharedPreferences.edit()
        editor.putBoolean("_isConnected", isConnected)
        editor.apply()

        if(isConnected){
            SnackAlert.closeAlert()
        }else{
            SnackAlert.alert("Perdiste la conexión a internet. Asegúrate de tenerlo para usar VivaJourney", findViewById(R.id.layout_container_home), "error",Snackbar.LENGTH_INDEFINITE , "BOTTOM")
            SnackAlert.showAlert()
        }
    }

    override fun notificationselected(item: NotificationRealmModel) {

        var fragent:Fragment
        var headertype = "vivacoins"

        when(item.type){
            "challenges" -> fragent = ChallengesFragment();
            "level_up", "level_up_ranking", "ranking" -> fragent = RankingFragment()
            "new_survey", "survey", "surveys" -> fragent = SurveysFragment()
            "payroll" -> fragent = ChallengesFragment()
            "payroll_complaint_comment" ->{
                fragent = PayrollFragment()
                val editor = sharedPreferences.edit()
                var extraintent = item.extras!!.id.toString()
                editor.putString("_data_notifitacion", extraintent)
                editor.apply()
            }
            "quality" -> fragent = QualityWeekenFragment()
            "vacant" -> fragent = VacainciesFragment()
            "review" -> fragent = ReviewsFragment(true)
            else -> {fragent = HomeFragment(); headertype = "profile"}
        }

        presenter.showFragment(item.type, headertype, fragent,false, "quinientos")
    }
}