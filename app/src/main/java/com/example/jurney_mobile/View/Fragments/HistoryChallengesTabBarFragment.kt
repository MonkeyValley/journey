package com.example.jurney_mobile.View.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Adapters.ChallengeAdapter
import com.example.jurney_mobile.View.Adapters.ChallengesExpandableAdapter
import com.example.jurney_mobile.View.Adapters.CustomExpandableListAdapter
import com.example.jurney_mobile.View.Adapters.HistoryChallengesAdapter
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.example.jurney_mobile.View.Models.ChallengesCompleteResponseModel
import com.example.jurney_mobile.View.Models.HistoryChallengesResponseModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.util.HashMap

class HistoryChallengesTabBarFragment(callbackclickItemChallenge: IClickItemDelegate.clickItemChallenge) : Fragment(), IClickItemDelegate.clickItemChallenge {


    var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences

    private lateinit var listExp: ExpandableListView
    private lateinit var listViewAdapter:ExpandableListAdapter
    private var callback :IClickItemDelegate.clickItemChallenge

    init{
        this.callback = callbackclickItemChallenge
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val inf = inflater.inflate(R.layout.tab_bar_history_challenges_fragment, container, false)
        initView(inf)
        return inf
    }

    fun initView(v:View){

        sharedPreferences = this.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val data = sharedPreferences.getString("_listhistorychallenges", "")
        val challenges = gson.fromJson(data, Array<HistoryChallengesResponseModel>::class.java).toList()

        challenges.forEachIndexed{index, item ->
            var totalPoints = 0
            item.challenges.forEach {
                totalPoints += it.challenge_points
            }

            item.total_points = totalPoints
            item.total_challenges = item.challenges.size

        }


        listExp = v.findViewById(R.id.list_history_challenges_fragment)
        listViewAdapter = ChallengesExpandableAdapter(this.context!!,challenges, this)

        listExp.setOnGroupExpandListener(object : ExpandableListView.OnGroupExpandListener{
            override fun onGroupExpand(groupPosition: Int) {
                challenges.forEachIndexed{ index, it ->
                    if (index != groupPosition){
                        listExp.collapseGroup(index)
                    }
                }
            }

        })
        listExp.setAdapter(listViewAdapter)

    }

    override fun click(item: ChallengeModel) {
        callback.click(item)
    }


}