package com.example.jurney_mobile.View.Fragments

import android.app.ActionBar
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TableLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Challenges.IChallenges
import com.example.jurney_mobile.Presenter.ChallengesPresenter
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.ChallengeAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.challenges_fragment.*
import kotlinx.android.synthetic.main.challenges_fragment.view.*
import java.util.ArrayList

class ChallengesFragment: Fragment(), IChallenges.View {

    private  lateinit var presenter:IChallenges.Presenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {

        val inf = inflater.inflate(R.layout.challenges_fragment, container, false)
        initView(inf)
        return inf
    }

    override fun onResume() {
        super.onResume()
        Log.e("","")
        presenter.setupViewPager()
    }

    override fun onPause() {
        super.onPause()
    }

    fun initView(v: View){
        presenter = ChallengesPresenter(this, this, v)
        presenter.initInfo()
    }


}