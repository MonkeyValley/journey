package com.example.jurney_mobile.View.Fragments
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.PreguntaModel
import kotlinx.android.synthetic.main.survey_text_fragment.view.*
import kotlin.math.log

class SurveyTextFragment:Fragment(), TextWatcher {

    lateinit var data: PreguntaModel
    lateinit var type:String
    lateinit var viewFrag:View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.survey_text_fragment, container, false)
        initView(inf)
        return  inf
    }

    fun initView(v: View){
        viewFrag = v
        v.lbl_question_name_open.text = data.description
        v.ta_response_survey_open.addTextChangedListener(this)
    }


    override fun afterTextChanged(s: Editable?) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val lengthText = s!!.length


        if(lengthText == 400){
            viewFrag.lbl_counter_message.setTextColor( this.resources.getColor(R.color.pinkish_red))
            viewFrag.lbl_counter_message.text = "Llegaste al maximo de 400 caracteres"
            viewFrag.ta_response_survey_open.setBackgroundResource(R.drawable.bg_edittext_error)
        }else{
            viewFrag.lbl_counter_message.setTextColor( this.resources.getColor(R.color.dodger_blue))
            viewFrag.lbl_counter_message.text = "" + s!!.length + "/400"
            viewFrag.ta_response_survey_open.setBackgroundResource(R.drawable.corner_radius_linear)
        }
    }
}