package com.example.jurney_mobile.View.Models

data class ComplaintPayrollResponse (var message:String, var collaborator_id:Int, var comment:String, var date:String, var id:Int, var selected_reasons:ArrayList<ReasonsPayrollComplainModel>)