package com.example.jurney_mobile.View.Models

data class ChallengeModel (var activity_id:Int, var completed:Boolean, var id:Int, var image:String, var name:String, var points:Int, var description:String="", var threshold_performance:Double, var threshold_score:Double, var status:Boolean)