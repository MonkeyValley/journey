package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Complaints.IComplaint
import com.example.jurney_mobile.Presenter.ComplaintPresenter
import com.example.jurney_mobile.R
import kotlinx.android.synthetic.main.complaint_fragment.view.*

class ComplaintsFragment: Fragment(), IComplaint.View, View.OnClickListener{

    val presenter = ComplaintPresenter(this, this)
    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inf = inflater.inflate(R.layout.complaint_fragment, container, false)
        initView(inf)
        return inf
    }

    fun initView(v:View){
        v.btn_abuse_next_step_complaint.setOnClickListener(this)
        v.btn_bullying_next_step_complaint.setOnClickListener(this)
        v.btn_threat_next_step_complaint.setOnClickListener(this)
        v.btn_discrimination_next_step_complaint.setOnClickListener(this)
        v.btn_steal_next_step_complaint.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        presenter.onClick(v!!)
    }
}