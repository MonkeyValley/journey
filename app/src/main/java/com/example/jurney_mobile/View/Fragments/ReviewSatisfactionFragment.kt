package com.example.jurney_mobile.View.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.PreguntaModel
import com.example.jurney_mobile.View.Models.TopictsModel
import kotlinx.android.synthetic.main.review_satiscation_fragment.view.*
import kotlinx.android.synthetic.main.survey_satifaction_fragment.view.*

class ReviewSatisfactionFragment: Fragment(), View.OnClickListener {

    lateinit var data: TopictsModel
    lateinit var clickCalback: IClickItemDelegate.clickItemReviewFragment
    lateinit var viewint:View
    override fun onCreateView(  inflater: LayoutInflater,  container: ViewGroup?, savedInstanceState: Bundle? ): View? {

        val inf = inflater.inflate(R.layout.review_satiscation_fragment, container, false)
        viewint = inf
        initView(inf)

        return inf
    }

    fun initView(v:View){
        v.lbl_question_name_satisfaction_review.text=data.name
        v.lbl_description_satisfaction_review.text = data.description
        v.btn_verygood_review.setOnClickListener(this)
        v.btn_good_review.setOnClickListener(this)
        v.btn_soso_review.setOnClickListener(this)
        v.btn_bad_review.setOnClickListener(this)
        v.btn_verybad_review.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        Log.e("--->>",v!!.id.toString())

        viewint.linear_selected_verygood_review.setBackgroundColor(this.resources.getColor(R.color.colorBackground))
        viewint.linear_selected_good_review.setBackgroundColor(this.resources.getColor(R.color.colorBackground))
        viewint.linear_selected_soso_review.setBackgroundColor(this.resources.getColor(R.color.colorBackground))
        viewint.linear_selected_bad_review.setBackgroundColor(this.resources.getColor(R.color.colorBackground))
        viewint.linear_selected_verybad_review.setBackgroundColor(this.resources.getColor(R.color.colorBackground))

        viewint.lbl_desc_verygood_review.setTextColor(resources.getColor(R.color.charcoal))
        viewint.lbl_desc_good_review.setTextColor(resources.getColor(R.color.charcoal))
        viewint.lbl_desc_soso_review.setTextColor(resources.getColor(R.color.charcoal))
        viewint.lbl_desc_bad_review.setTextColor(resources.getColor(R.color.charcoal))
        viewint.lbl_desc_verybad_review.setTextColor(resources.getColor(R.color.charcoal))

        when(v!!.id){
            R.id.btn_verygood_review->{
                viewint.linear_selected_verygood_review.background = this.resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_verygood_review.setTextColor(resources.getColor(R.color.colorBackground))
                clickCalback.click(data.id, 5)
            }
            R.id.btn_good_review->{
                viewint.linear_selected_good_review.background = this.resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_good_review.setTextColor(resources.getColor(R.color.colorBackground))
                clickCalback.click(data.id, 4)
            }
            R.id.btn_soso_review->{
                viewint.linear_selected_soso_review.background = this.resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_soso_review.setTextColor(resources.getColor(R.color.colorBackground))
                clickCalback.click(data.id, 3)
            }
            R.id.btn_bad_review->{
                viewint.linear_selected_bad_review.background = this.resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_bad_review.setTextColor(resources.getColor(R.color.colorBackground))
                clickCalback.click(data.id, 2)
            }
            R.id.btn_verybad_review->{
                viewint.linear_selected_verybad_review.background = this.resources.getDrawable(R.drawable.corner_radius_cool_green)
                viewint.lbl_desc_verybad_review.setTextColor(resources.getColor(R.color.colorBackground))
                clickCalback.click(data.id, 1)
            }

        }
    }

}