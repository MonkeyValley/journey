package com.example.jurney_mobile.View.Retrofit

import com.example.jurney_mobile.View.Models.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface IAPI {

    @POST("/v1/auth/sms/{num}")
    fun sms(@Path("num") phone:String):Call<SmsResponseModel>

    @Headers("Content-Type: application/json")
    @POST("/v1/auth/signin")
    fun login(@Body data:SigninRequest):Call<LoginResponse>

    @GET("/v1/auth/whoami")
    fun getUserInfo(@Header("Authorization") auth: String):Call<UserResponseModel>

    @GET("/v1/collaborator/paysheet_week?")
    fun getWeekendPayment(@Header("Authorization") auth: String, @Query("employee_code") employeeCode:String, @Query("business_unit_id") business_unit_id:String ):Call<PaymentModel>

    @GET("/v1/collaborator/total_income?")
    fun getTotalIncome(@Header("Authorization") auth: String, @Query("employee_code") employeeCode:String, @Query("business_unit_id") business_unit_id:String ):Call<TotalIncomeModel>

    @GET("/v1/collaborator/last_two_paysheet_weeks?")
    fun getTwoLastWeeks(@Header("Authorization") auth: String, @Query("employee_code") employeeCode:String, @Query("business_unit_id") business_unit_id:String ):Call<TwoLastWeekendModel>

    @Headers("Content-Type: application/json")
    @POST("/v1/complaint/payroll")
    fun sendComplain( @Header("Authorization") auth: String, @Body data:ComplaintPayrollRequest ):Call<ComplaintPayrollResponse>

    @Headers("Content-Type: application/json")
    @POST("/v1/complaint/")
    fun sendComplaint( @Header("Authorization") auth: String, @Body data:ComplaintRequest ):Call<ComplaintResponse>

    @Multipart
    @POST("/v1/upload/")
    fun uploadImage(@Header("Authorization") auth: String, @Part file: MultipartBody.Part , @Part("file_type") type: RequestBody):Call<UploadImageResponse>

    @Headers("Content-Type: application/json")
    @PUT("/v1/collaborator/{idUser}")
    fun updateImage( @Header("Authorization") auth: String, @Body data:UpdateImageRequest, @Path("idUser") id:Int ):Call<UserResponseModel>

    @Headers("Content-Type: application/json")
    @PUT("/v1/collaborator/{idUser}")
    fun updatePassword( @Header("Authorization") auth: String, @Body data:UpdatePasswordRequest, @Path("idUser") id:Int ):Call<UserResponseModel>

    @Headers("Content-Type: application/json")
    @PUT("/v1/collaborator/{idUser}")
    fun updateFirebaseToken( @Header("Authorization") auth: String, @Body data:UpdateFirebaseTokenModel, @Path("idUser") id:Int ):Call<UserResponseModel>

    @GET("/v1/survey/list?")
    fun getSurveys(@Header("Authorization") auth: String, @Query("business_unit_id") business_unit_id:Int,  @Query("collaborator_id") collaborator_id:Int, @Query("is_active") is_active:Int):Call<ResponseGetSurveys>

    @GET("/v1/survey/{idSurvey}")
    fun getSurveyByID(@Header("Authorization") auth: String, @Path("idSurvey") idSurvey:Int):Call<EncuestaModel>

    @Headers("Content-Type: application/json")
    @POST("/v1/survey/response")
    fun sendSurvey( @Header("Authorization") auth: String, @Body data:RequestEncuestaMultipleModel ):Call<ResponseSendSurvey>

    @GET("/v1/ops/quality_score?")
    fun getQualityScore(@Header("Authorization") auth: String, @Query("date_from") date_from:String, @Query("date_to") date_to:String, @Query("business_unit_id") business_unit_id:String, @Query("collaborator_id") collaborator_id:String ):Call<QualityScoreResponse>

    @GET("/v1/challenge/?")
    fun getChallengeList(@Header("Authorization") auth: String, @Query("collaborator_id") collaborator_id:Int):Call<ChallengesResponseModel>

    @GET("/v1/prize/")
    fun getAwardsList(@Header("Authorization") auth: String):Call<ArrayList<AwardsResponseModel>>

    @GET("/v1/collaborator/exchange_code")
    fun getCodesList(@Header("Authorization") auth: String):Call<ArrayList<CodeResponseModel>>


    @Headers("Content-Type: application/json")
    @POST("/v1/prize/exchange_code")
    fun sendExchangeCode( @Header("Authorization") auth: String, @Body data:GeneratorCodeRequestModel ):Call<GeneratorCodeResponseModel>

    @Headers("Content-Type: application/json")
    @PUT("/v1/prize/exchange_code/validate/{id_prize}")
    fun sendResponseCode( @Header("Authorization") auth: String, @Body data:SendResRequestModel,  @Path("id_prize") id:Int ):Call<SendResResponseModel>

    @GET("/v1/collaborator/ranking/top")
    fun getTopTen(@Header("Authorization") auth: String):Call<ArrayList<UserRankingResponseModel>>

    @GET("/v1/collaborator/ranking/between")
    fun getMyRanking(@Header("Authorization") auth: String):Call<ArrayList<UserRankingResponseModel>>

    @GET("/v1/collaborator/?")
    fun getUserRanking(@Header("Authorization") auth: String, @Query("name") nameUSer:String, @Query("business_unit_id") business_unit_id:Int):Call<RankingResponseModel>

    @GET("/v1/leaving/reasons")
    fun getReasons(@Header("Authorization") auth: String):Call<ArrayList<ReasonsResponseModel>>

    @Headers("Content-Type: application/json")
    @POST("/v1/leaving/notification")
    fun sendReason( @Header("Authorization") auth: String, @Body data:ReasonRequestModel ):Call<GetoutResponseModel>

    @Headers("Content-Type: application/json")
    @PUT("/v1/leaving/notification/cancel/{id_leaving}")
    fun putCancelGetout( @Header("Authorization") auth: String, @Path("id_leaving") id:Int ):Call<GetoutResponseModel>

    @GET("/v1/vacant/?")
    fun getVacancies(@Header("Authorization") auth: String,  @Query("collaborator_id") collaborator_id:Int):Call<ArrayList<VacancyResponseModel>>

    @Headers("Content-Type: application/json")
    @POST("/v1/vacant/postulant")
    fun sendVacancyRequest(@Header("Authorization") auth: String, @Body data:VacancyRequestModel):Call<SendVacancyResponseModel>

    @Headers("Content-Type: application/json")
    @POST("/v1/vacant/postulant")
    fun sendVacancyRequestSingle(@Header("Authorization") auth: String, @Body data:SingleVacancyRequestModel):Call<SendVacancyResponseModel>

    @GET("/v1/collaborator/payroll/complaint_reasons")
    fun getReasonsComplainsPayroll(@Header("Authorization") auth: String):Call<ArrayList<ReasonsPayrollComplainModel>>

    @GET("/v1/complaint/payroll/{id_complain}")
    fun getComplainPayroll(@Header("Authorization") auth: String, @Path("id_complain") id:Int):Call<ComplainPayrollResponseModel>

    @GET("/v1/collaborator/notifications")
    fun getNotifications(@Header("Authorization") auth: String, @Query("date_from") date_from:String, @Query("date_to") date_to:String, @Query("collaborator_id") collaborator_id:Int ):Call<ArrayList<NotificationModel>>

    @GET("/v1/collaborator/dailypay/{id_user}")
    fun getDailyPay(@Header("Authorization") auth: String, @Path("id_user") id:Int):Call<DailypayResponseModel>

    @Headers("Content-Type: application/json")
    @POST("/v1/collaborator/dailypay/{id_user}")
    fun sendReqDailyPay( @Header("Authorization") auth: String, @Body data:DayliPayRequestModel, @Path("id_user") id:Int):Call<DayliPayRequestResponseModel>

    @POST("/v1/auth/new_sms/{phone}")
    fun reSendCode(@Path("phone") phone:String):Call<newSmsResponseModel>

    @GET("/v1/review/week")
    fun getSupervisors(@Header("Authorization") auth:String):Call<ArrayList<SupervisorsResponseModel>>

    @GET("/v1/review/{id_review}")
    fun getReviewById(@Header("Authorization") auth:String, @Path("id_review") id:Int):Call<ReviewModelResponse>

    @POST("/v1/review/response")
    fun sendReview(@Header("Authorization") auth:String, @Body data:ReviewRequestModel):Call<SendReviewsResponseModel>

    @GET("/v1/challenge/available")
    fun getChallengesAvailables(@Header("Authorization") auth:String):Call<ArrayList<ChallengesAvailableResponse>>

    @GET("/v1/challenge/completed/week")
    fun getChallengesComplete(@Header("Authorization") auth:String):Call<ChallengesCompleteResponseModel>

    @GET("/v1/challenge/history")
    fun getHistoryChallenges(@Header("Authorization") auth:String):Call<ArrayList<HistoryChallengesResponseModel>>

    @POST("/v1/collaborator/schedule_message/confirm/{msg_id}")
    fun sendConfirmNotification(@Header("Authorization") auth:String,  @Path("msg_id") msg_id:Int, @Body data:ConfirmRequestModel):Call<ArrayList<String>>

}