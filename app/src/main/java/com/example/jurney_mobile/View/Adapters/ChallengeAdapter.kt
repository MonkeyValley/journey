package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_challenge_challenges.view.*

class ChallengeAdapter(items:ArrayList<ChallengeModel>, clickDelegate:IClickItemDelegate.clickItemChallenge): RecyclerView.Adapter<ChallengeAdapter.ViewHolder>() {

    var clickDelegate:IClickItemDelegate.clickItemChallenge
    var list:ArrayList<ChallengeModel>
    lateinit var viewHolder:ViewHolder

    init {
        this.list = items
        this.clickDelegate = clickDelegate
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_challenges, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblActivity.text = list.get(position).name
        holder.lblActivityDesc.text = list.get(position).description
        holder.lblActivityPoints.text = list.get(position).points.toString()
        val image = "" + list.get(position).image

        if(list.get(position).description != null) {
            val arrDescription = list.get(position).description.split("-")
            if (arrDescription.size > 0) {
                holder.lblActivityDesc.text = arrDescription[0]
            }
        }

        if(list.get(position).image != "") {
            Picasso.get().load(image).error(R.drawable.no_image).into(holder.imgActivity)
        }

        holder.btnSelected.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                clickDelegate.click(list.get(position))
            }
        })
    }

    class ViewHolder(vista: View):RecyclerView.ViewHolder(vista){
        val lblActivity = vista.lbl_challenge_name_challenges
        val lblActivityDesc = vista.lbl_challenge_desc_challenges
        val lblActivityPoints = vista.lbl_challenge_points_challenges
        val imgActivity = vista.imgChallenge
        val btnSelected = vista.item_selected_challenge
    }
}
