package com.example.jurney_mobile.View.Models

data class PaymentModel (var errors:String = "",
                         var business_unit_id:Int = 0,
                         var date_from:String = "",
                         var date_to:String = "",
                         var days:ArrayList<DayPaysheetModel>,
                         var deductions:Double = 0.0,
                         var employee_code:String = "",
                         var gross:Double = 0.0,
                         var id:Int = 0,
                         var net:Double= 0.0,
                         var week_no:Int= 0)