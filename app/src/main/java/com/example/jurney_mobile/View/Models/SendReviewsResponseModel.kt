package com.example.jurney_mobile.View.Models

class SendReviewsResponseModel (
    var errors:String,
    var collaborator_id: Int,
    var comment:String,
    var date: String,
    var  has_review: Boolean,
    var id: Int,
    var ops_supervisor_id: Int,
    var week: Int
)