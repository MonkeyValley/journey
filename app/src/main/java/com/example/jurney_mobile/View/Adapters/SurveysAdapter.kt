package com.example.jurney_mobile.View.Adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.View.Models.SurveysListModel
import kotlinx.android.synthetic.main.item_surveys.view.*

class SurveysAdapter(items:ArrayList<SurveysListModel>, itemSelect:IClickItemDelegate.clickItemSurvey): RecyclerView.Adapter<SurveysAdapter.ViewHolder>() {

    var list:ArrayList<SurveysListModel>
    lateinit var viewHolder:ViewHolder
    var itemSelect:IClickItemDelegate.clickItemSurvey
    init {
        this.list = items
        this.itemSelect = itemSelect
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_surveys, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lblTitle.text = list.get(position).name
        holder.lblActivityDesc.text = list.get(position).description
        holder.lblnumbQuestions.text = ""+ list.get(position).questions + " Preguntas"
        holder.btnDetails.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {
                itemSelect.click(list.get(position), position)
            }
        })

//        if(list.get(position) == "adios"){
//            holder.ic_check.visibility = View.VISIBLE
//            holder.btnDetails.visibility = View.GONE
//            holder.bgItemsurvey.setBackgroundColor( Color.parseColor("#eafff2") )
//        }else{
//            holder.ic_check.visibility = View.GONE
//            holder.btnDetails.visibility = View.VISIBLE
//            holder.btnDetails.setOnClickListener(object :View.OnClickListener{
//                override fun onClick(v: View?) {
//                    itemSelect.clickItem(list.get(position), position)
//                }
//            })
//        }

    }

    class ViewHolder(vista: View):RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lbl_title_survey
        val lblActivityDesc = vista.lbl_description_survey
        val lblnumbQuestions = vista.lbl_number_questions
        val bgItemsurvey = vista.ite_container_surveys
        val btnDetails = vista.btn_goto_details_survey
        var ic_check = vista.img_survey_check
    }
}
