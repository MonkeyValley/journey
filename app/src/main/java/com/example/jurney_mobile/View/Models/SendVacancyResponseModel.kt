package com.example.jurney_mobile.View.Models

data class SendVacancyResponseModel (var collaborator_id:Int, var id:Int, var origin:String, var start_date:String, var vacant_id:Int)