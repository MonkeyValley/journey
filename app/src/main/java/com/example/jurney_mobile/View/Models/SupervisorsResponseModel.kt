package com.example.jurney_mobile.View.Models

data class SupervisorsResponseModel(var area_name:String,
                                    var lots:String="Sin lote",
                                    var review_description:String,
                                    var review_id:Int,
                                    var review_name:String,
                                    var supervisor_id:Int,
                                    var supervisor_image:String="",
                                    var supervisor_name:String,
                                    var supervisor_review_id:Int,
                                    var supervisor_review_status:Boolean,
                                    var week_no:Int)