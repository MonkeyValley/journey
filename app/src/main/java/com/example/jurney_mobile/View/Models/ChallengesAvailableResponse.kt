package com.example.jurney_mobile.View.Models

data class ChallengesAvailableResponse (var challenge_description:String="",
                                        var challenge_name:String="",
                                        var challenge_points:Int=0,
                                        var challente_image:String="")