package com.example.jurney_mobile.View.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_challenge_home.view.*

class HomeAdapter(items:ArrayList<ChallengeModel>): RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    var list:ArrayList<ChallengeModel>
    lateinit var viewHolder:ViewHolder
    init {
        this.list = items
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_challenge_home, parent, false)
        viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onBindViewHolder(holder: HomeAdapter.ViewHolder, position: Int) {

        holder.lblTitle.text = list.get(position).name
        holder.lblDescription.text = list.get(position).description
        holder.lblpoints.text = list.get(position).points.toString() + " VivaCoins ganados"
        val image = EnviromentConstants().image_serverver + list.get(position).image

        if(list.get(position).description != null) {
            val arrDescription = list.get(position).description.split("-")
            if (arrDescription.size > 0) {
                holder.lblDescription.text = arrDescription[0]
            }
        }

        Picasso.get().load(image).error(R.drawable.no_image).into(holder.imgChallenge)
    }

    class ViewHolder(vista:View):RecyclerView.ViewHolder(vista){
        val lblTitle = vista.lblChallengeName
        val lblDescription = vista.lblChallengeDesc
        val imgChallenge = vista.imgChallenge
        val lblpoints = vista.lblChallengePoints
    }
}