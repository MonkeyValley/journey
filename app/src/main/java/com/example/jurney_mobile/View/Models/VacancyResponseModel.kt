package com.example.jurney_mobile.View.Models

data class VacancyResponseModel (var business_unit_id:Int, var description:String, var id:Int, var name:String, var payment_method:String, var requisites:ArrayList<String>, var salary: Double, var start_date:String, var status:Boolean, var postulated:Boolean)