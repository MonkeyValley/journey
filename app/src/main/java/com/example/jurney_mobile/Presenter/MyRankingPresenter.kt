package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Ranking.IRanking
import com.example.jurney_mobile.Model.Ranking.RamkingMy
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.RankingAdapter
import com.example.jurney_mobile.View.Fragments.TabBarMyRankingFragment
import com.example.jurney_mobile.View.Models.RankingResponseModel
import com.example.jurney_mobile.View.Models.UserRankingResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.tab_bar_myranking_fragment.view.*
import kotlinx.android.synthetic.main.tab_bar_topten_ranking_fragment.view.*
import org.json.JSONArray

class MyRankingPresenter: IRanking.PresenterMy, View.OnClickListener {

    private var v: View
    private var view: IRanking.ViewMy
    private var model: IRanking.ModelMy
    private var ctx: TabBarMyRankingFragment

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: RankingAdapter
    private lateinit var sharedPreferences: SharedPreferences

    val array = ArrayList<UserRankingResponseModel>()
    val arrayResp = ArrayList<UserRankingResponseModel>()
    lateinit var token:String
    lateinit var user :UserResponseModel
    var gson = Gson()

    constructor(presenter:IRanking.ViewMy, ctx: TabBarMyRankingFragment, v:View){
        this.view = presenter
        this.model = RamkingMy(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initView() {

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!

        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        val topten = sharedPreferences.getString("_listmyranking", "")
        if(topten != "") {
            val jsonArray = JSONArray(topten)
            for (i in 0 until jsonArray.length()) {
                array.add(gson.fromJson(jsonArray.get(i).toString(), UserRankingResponseModel::class.java))
                arrayResp.add(gson.fromJson(jsonArray.get(i).toString(), UserRankingResponseModel::class.java))
            }
        }

        list = v.findViewById(R.id.list_ranking)
        layoutManager = LinearLayoutManager(ctx.context)
        adaptador = RankingAdapter(array, "MyRanking",ctx)
        list.layoutManager = layoutManager
        list.adapter = adaptador

        v.btn_search_my_ranking.setOnClickListener(this)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

    override fun getUserByNameErrorResponse(message: String) {
        SnackAlert.alert(message, v.layer_container_ranking, "error", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }

    override fun getUserByNameResponse(response: RankingResponseModel) {
        if(response.count > 0) {
            v.list_ranking.visibility = View.VISIBLE
            v.layout_no_results.visibility = View.GONE
            adaptador = RankingAdapter(response.collaborators, "MyRanking", ctx)
            list.layoutManager = layoutManager
            list.adapter = adaptador
        }else{
            v.layout_no_results.visibility = View.VISIBLE
            v.list_ranking.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        val text = this.v.txt_search_my_ranking.text.toString()
        if(text != "") {
            model.getUserByName(token, user.business_unit_id, text)
        }else{
            adaptador = RankingAdapter(this.arrayResp, "MyRanking",ctx)
            list.layoutManager = layoutManager
            list.adapter = adaptador
        }
    }
}