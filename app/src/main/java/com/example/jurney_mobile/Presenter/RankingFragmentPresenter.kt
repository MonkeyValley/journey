package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.jurney_mobile.Model.Ranking.IRanking
import com.example.jurney_mobile.Model.Ranking.RankingModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Fragments.RankingFragment
import com.example.jurney_mobile.View.Fragments.TabBarMyRankingFragment
import com.example.jurney_mobile.View.Fragments.TabBarTopTenRankingFragment
import com.example.jurney_mobile.View.Models.RankingHistoryResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.ranking_fragment.view.*

class RankingFragmentPresenter:IRanking.Presenter, View.OnClickListener {

    private var v: View
    private var view: IRanking.View
    private var model: IRanking.Model
    private var ctx: RankingFragment
    private lateinit var manager: FragmentManager
    private lateinit var fragmentShow:String

    private lateinit var token:String
    private lateinit var user:UserResponseModel
    private lateinit var image:String
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()

    constructor(presenter:IRanking.View, ctx:RankingFragment, v:View){
        this.view = presenter
        this.model = RankingModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initView() {

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!

        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        image = EnviromentConstants().image_serverver + user.photo
        manager = ctx.activity!!.supportFragmentManager

        showFragment(TabBarMyRankingFragment(), "myranking")

        lateinit var bussinesUnity:String


        when(user.business_unit_id){
            1->bussinesUnity = "Culiacán"
            2->bussinesUnity = "Ensenada"
            3->bussinesUnity = "Sayula"
        }

        v.lbl_num_coins_history.text = user.points.toString()
        v.lbl_bussinnes_unity_user_ranking.text = bussinesUnity
        v.lbl_name_user_ranking.text = user.name

        v.lbl_num_place_ranking.text = user.ranking.toString()
        v.lbl_num_level_ranking.text = user.current_lvl.toString()
        v.btn_myranking.setOnClickListener(this)
        v.btn_topten_ranking.setOnClickListener(this)
        Picasso.get().load(image).error(R.drawable.no_image_profile).into(v.iv_user_image_user_ranking)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_myranking->{
                this.v.layer_tabbar_selector_myranking.background = ctx.activity!!.getDrawable(R.color.dodger_blue)
                this.v.layer_tabbar_selector_topten_ranking.background = ctx.activity!!.getDrawable(R.color.colorBackground)
                showFragment(TabBarMyRankingFragment(), "myranking")

            }
            R.id.btn_topten_ranking->{
                this.v.layer_tabbar_selector_myranking.background = ctx.activity!!.getDrawable(R.color.colorBackground)
                this.v.layer_tabbar_selector_topten_ranking.background = ctx.activity!!.getDrawable(R.color.dodger_blue)
                showFragment(TabBarTopTenRankingFragment(), "topten")
            }
        }
    }

    fun showFragment( fragment: Fragment, fragmentName:String){

        fragmentShow = fragmentName

        val transaction = manager.beginTransaction()
        transaction.replace( R.id.layer_container_list_ranking , fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}