package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Awards.AwardsCodesModel
import com.example.jurney_mobile.Model.Awards.IAwards
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.AwardsAdapter
import com.example.jurney_mobile.View.Adapters.ClickItemTagDelegate
import com.example.jurney_mobile.View.Adapters.FilterAdapter
import com.example.jurney_mobile.View.Fragments.TabBarListAwardsFragment
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.example.jurney_mobile.View.Models.GeneratorCodeResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.tab_bar_list_awards_fragment.view.*
import kotlinx.android.synthetic.main.vivacoin_header_component.*
import org.json.JSONArray
import java.util.*
import kotlin.Exception
import kotlin.collections.ArrayList

class AwardsCodesPresenter:IAwards.PresenterCodes, View.OnClickListener, IClickItemDelegate.clickItemAwards, ClickItemTagDelegate {

    var view: IAwards.ViewCodes
    var model: IAwards.ModelCodes
    var ctx: TabBarListAwardsFragment
    var v: View
    var gson = Gson()
    lateinit var itemSelected:AwardsResponseModel
    lateinit var sharedPreferences: SharedPreferences


    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: AwardsAdapter

    private lateinit var horizontalList: RecyclerView
    private lateinit var horizontalLayoutManager: RecyclerView.LayoutManager
    private lateinit var horizontalAdaptador: FilterAdapter

    var data = ArrayList<AwardsResponseModel>()
    var dataResp = ArrayList<AwardsResponseModel>()
    val data_filter = ArrayList<String>()
    lateinit var jsonArray:JSONArray

    lateinit var alert: LoadingDialog

    lateinit var token :String
    lateinit var user: UserResponseModel

    constructor(view: IAwards.ViewCodes, ctx: TabBarListAwardsFragment, v: View){
        this.view = view
        this.model = AwardsCodesModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initInfo() {
        alert = LoadingDialog(ctx.activity!!)
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!

        val userStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson(userStr, UserResponseModel::class.java)

        val dataStr = sharedPreferences.getString("_listawards","")
        token = sharedPreferences.getString("_tokenuser","")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        if(dataStr != "") {
            v.list_awards.visibility = View.VISIBLE
            v.list_empty_awards.visibility = View.GONE
            jsonArray = JSONArray(dataStr)

            if(jsonArray.length()>0) {
                for (i in 0 until jsonArray.length()) {

                    val item =
                        gson.fromJson(jsonArray.get(i).toString(), AwardsResponseModel::class.java)

                    if (item.status) {
                        data.add(item)
                        dataResp.add(item)
                    }
                }
            }else{
                v.list_empty_awards.visibility = View.VISIBLE
                v.list_awards.visibility = View.GONE
            }
        }else{
            v.list_empty_awards.visibility = View.VISIBLE
            v.list_awards.visibility = View.GONE

        }

        data_filter.add("Mayor a menor")
        data_filter.add("Menor a mayor")
        data_filter.add("Limpiar")
//        data_filter.add("Mas buscados")
//        data_filter.add("Mas canjeados")

        horizontalList = v.findViewById(R.id.list_filter)
        horizontalLayoutManager = LinearLayoutManager(ctx.context, LinearLayoutManager.HORIZONTAL ,false)
        horizontalAdaptador = FilterAdapter(data_filter, this)
        horizontalList.isNestedScrollingEnabled = false
        horizontalList.layoutManager = horizontalLayoutManager
        horizontalList.adapter = horizontalAdaptador

        list = v.findViewById(R.id.list_awards)
        layoutManager = LinearLayoutManager(ctx.context)
        adaptador = AwardsAdapter(data,this)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador

        v.btn_search_awards.setOnClickListener(this)
    }

    override fun getExchangeCodeResponse(response: GeneratorCodeResponseModel) {
        val clickCancel = View.OnClickListener{
            alert.dismissDialog()
        }

        try {
            ctx.activity!!.findViewById<TextView>(R.id.lbl_number_vivacoints_header_vivacoint).text = (user.coins - itemSelected.coins).toString()
        }catch (e:Exception){
            Log.e("ERROR","Components null")
        }
        user.coins = user.coins - itemSelected.coins

        val editor = sharedPreferences.edit()
        editor.putString("_userinfo", gson.toJson(user))
        editor.apply()

        model.getListCodes(token)
        alert.alertNotMultipleAnimt("Este es el codigo que necesitarás cuando vayas a canjear tu premio " + itemSelected.name.toUpperCase() + " en las oficinas de trabajo social. Este código lo podrás Consultar en el apartado de Mis códigos", "Tu código para canjear el premio \n \n "+response.code, "Cancelar", "Continuar", View.OnClickListener { }, clickCancel, false, false, "anim_ok.json")
    }

    override fun getExchangeCodeErrorResponse(message: String) {
        val clickCancel = View.OnClickListener{
            alert.dismissDialog()
        }
        if(message == ""){
            alert.alertNotMultipleAnimt(
                "No tienes suficientes VivaCoins para "+ itemSelected.name.toUpperCase() +" te faltan " + (itemSelected.coins - user.coins) + ". Sigue esforzandote para subir de nivel hasta llegar a la meta.",
                "¡OH, NO!",
                "Cancelar",
                "Aceptar",
                View.OnClickListener {  },
                clickCancel,
                false,
                false,
                "anim_error.json")
        }else{
            alert.alertNotMultipleAnimt( message, "¡OH, NO!", "Cancelar", "Aceptar", View.OnClickListener {  }, clickCancel, false, false, "anim_error.json")
        }

    }

    override fun getListCodesResponse(response: ArrayList<CodeResponseModel>) {
        Log.v("RESPONSE CODES", gson.toJson(response))

        val editor = sharedPreferences.edit()
        editor.putString("_listcodes", gson.toJson(response))
        editor.apply()
    }

    override fun getListCodesErrorResponse(message: String) {
        Log.v("E. getListCodes ->", message)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_search_awards->{
                val txtSearch = this.v.txt_search_awards.text.toString()

                utilsClass().hideSoftKeyBoard(ctx.context!!, v)
                if(txtSearch != "") {
                    data = ArrayList()
                    for (item in dataResp) {
                        if (item.name.toLowerCase(Locale.ROOT).contains(txtSearch.toLowerCase(Locale.ROOT))) {
                            data.add(item)
                        }
                    }
                }
                else{
                    data = dataResp
                }

                layoutManager = LinearLayoutManager(ctx.context)
                adaptador = AwardsAdapter(data, this)
                list.layoutManager = layoutManager
                list.adapter = adaptador
                this.adaptador.notifyDataSetChanged()

            }
        }
    }

    override fun click(item:AwardsResponseModel) {

        itemSelected = item
        val clickLeft = View.OnClickListener{
            alert.dismissDialog()
        }

        val clickRigth = View.OnClickListener{
            alert.dismissDialog()
            if(user.coins >= item.coins){
                model.getExchangeCode(token, user.id, item.id)
            }else{
                alert.alertNotMultipleAnimt(
                    "No tienes suficientes VivaCoins para "+ item.name.toUpperCase() +" te faltan " + (item.coins - user.coins) + ". Sigue esforzandote para subir de nivel hasta llegar a la meta.",
                    "¡OH, NO!",
                    "Cancelar",
                    "Aceptar",
                    View.OnClickListener {  },
                    View.OnClickListener { alert.dismissDialog() },
                    false,
                    false,
                    "anim_error.json")
            }

        }


        val price = utilsClass().milesFormat(item.coins.toDouble())
        val message = "Seleccionaste el premio "+item.name.toUpperCase()+" por " + price + " VivaCoins"


        alert.alertNotMultipleAnimt( message , "Confirmación de canje", "Cancelar", "Aceptar", clickLeft, clickRigth, true, true, "anim_cart.json")
    }

    override fun clickItem(item: String) {

        if(item == "Limpiar") {
            data = ArrayList()
            dataResp = ArrayList()
            for ( i  in 0 until  jsonArray.length()){
                data.add( gson.fromJson(jsonArray.get(i).toString(), AwardsResponseModel::class.java))
                dataResp.add( gson.fromJson(jsonArray.get(i).toString(), AwardsResponseModel::class.java))
            }
        }else if(item == "Menor a mayor"){
            data.sortBy{selector(it)}
        }
        else if(item == "Mayor a menor"){
            data.sortByDescending{selector(it)}
        }

        Log.e("-->", gson.toJson(data))
        Log.e("-->", gson.toJson(dataResp))
        layoutManager = LinearLayoutManager(ctx.context)
        adaptador = AwardsAdapter(data, this)
        list.layoutManager = layoutManager
        list.adapter = adaptador
        this.adaptador.notifyDataSetChanged()
    }

    fun selector(p: AwardsResponseModel): Int = p.coins


}