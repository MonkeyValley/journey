package com.example.jurney_mobile.Presenter

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.jurney_mobile.Model.Login.ILogin
import com.example.jurney_mobile.Model.Login.LoginModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.LoginActivity
import com.example.jurney_mobile.View.Models.ErrorSmsResponseModel
import com.example.jurney_mobile.View.Models.SmsResponseModel
import com.example.jurney_mobile.View.Models.configMessaggesWhatsappModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.example.jurney_mobile.View.UserValidate
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.alert_layout_postulation.*

class LoginPresenter: ILogin.Presenter, View.OnClickListener, View.OnLongClickListener{
    private var view: ILogin.View
    private var model: ILogin.Model
    private lateinit var ctx:LoginActivity
    private lateinit var sharedPreferences : SharedPreferences
    private lateinit var loading: LoadingDialog
    private val env = EnviromentConstants()

    var database = FirebaseDatabase.getInstance()
    lateinit var alert:LoadingDialog
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)
    var url_privacy = "http://vivaorganica.com.mx/aviso-de-privacidad.html"


    //https://softlive.com.mx/vivajourney/politica.php
    constructor(view: ILogin.View, ctx:LoginActivity ){
        this.view = view
        this.model = LoginModel(this)
        this.ctx = ctx
    }
    override fun onLoginResponse(result: SmsResponseModel) {

        var alertInfo = LoadingDialog( ctx)
        loading.dismissDialog()
        alertInfo.alertNotMultipleAnimLogin(
            "Para acceder a Viva Journey es necesario aceptar las ",
            "Aviso",
            "Cancelar",
            "Aceptar",
            View.OnClickListener {
                alertInfo.dismissDialog()
                view.onLoginError("Es necesario aceptar las condiciones de privacidad para acceder a Viva Journey.")
            },
            View.OnClickListener {
                alertInfo.dismissDialog()

                val editor = sharedPreferences.edit()
                editor.putString("_phoneuser", ctx.txt_num_emp.text.toString() )
                editor.apply()
                ctx.finish()
                val intent = Intent(ctx, UserValidate::class.java)
                intent.putExtra("isGenerated", false)
                ctx.startActivity(intent)
            },
            View.OnClickListener {
                alertInfo.dismissDialog()
                val uris = Uri.parse(url_privacy)
                val intents = Intent(Intent.ACTION_VIEW, uris)
                ctx.startActivity(intents)
            },
            true,
            true,
            "anim_notification.json"
        )
    }

    override fun onLoginResponseGenerated(result: ErrorSmsResponseModel) {

        var alertInfo = LoadingDialog( ctx)
        loading.dismissDialog()

        alertInfo.alertNotMultipleAnimLogin(
            "Para acceder a Viva Journey es necesario aceptar las ",
            "Aviso",
            "Cancelar",
            "Aceptar",
            View.OnClickListener {
                alertInfo.dismissDialog()
                view.onLoginError("Es necesario aceptar las condiciones de privacidad para acceder a Viva Journey.")
            },
            View.OnClickListener {
                alertInfo.dismissDialog()

                val editor = sharedPreferences.edit()
                editor.putString("_phoneuser", ctx.txt_num_emp.text.toString() )
                editor.apply()
                ctx.finish()
                val intent = Intent(ctx, UserValidate::class.java)
                intent.putExtra("isGenerated", true)
                ctx.startActivity(intent)
            },
            View.OnClickListener {
                alertInfo.dismissDialog()
                val uris = Uri.parse(url_privacy)
                val intents = Intent(Intent.ACTION_VIEW, uris)
                ctx.startActivity(intents)
            },
            true,
            true,
            "anim_notification.json"
        )
    }

    override fun onLoginError(msj: String) {
            loading.dismissDialog()
        if(view != null) {
            view.onLoginError(msj)
        }

    }
    override fun onLogin(noEmpleado: String) {
        if(view != null){
            loading.startLoading()
            model.onLogin(noEmpleado)
        }
    }
    override fun initInfo() {
        onGetURLFirebase()
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val stringenv = sharedPreferences.getString("_baseurl", "")
        if(stringenv == EnviromentConstants().prod_server){
            ctx.lbl_enviroment.text = ""
        }else{
            ctx.lbl_enviroment.text = stringenv

        }
        alert = LoadingDialog(ctx)
        ctx.rootButton.setOnLongClickListener(this)
        ctx.btnLogin.setOnClickListener(this)
        ctx.btn_help_login.setOnClickListener(this)
        loading = LoadingDialog( ctx)
        getInfoWhatsapp()
    }


    val selectedFirst = (object : View.OnClickListener{
        override fun onClick(v: View?) {
            sendMessaggetowhatsapp(env.first)
        }
    })
    val selectedSecond = (object : View.OnClickListener{
        override fun onClick(v: View?) {
            sendMessaggetowhatsapp(env.second)
        }
    })
    val selectedThirt = (object : View.OnClickListener{
        override fun onClick(v: View?) {
            sendMessaggetowhatsapp(env.thirt)
        }
    })

    fun getInfoWhatsapp(){
        myRef.child("data_base").child("first_time_active").child("config_messagges_whatsapp").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){
                    val value = dataSnapshot.getValue(configMessaggesWhatsappModel::class.java)
                   Log.e("", value.toString())
                    env.phone = value!!.phone
                    env.first = value.first
                    env.second = value.second
                    env.thirt = value.thirt
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    fun sendMessaggetowhatsapp(msg:String){
        loading.dismissDialog()
        if( utilsClass().isAppIntalled("com.whatsapp", ctx.applicationContext )){
            val intentWhats = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone="+env.phone+"&text="+msg))
            ctx.startActivity(intentWhats)
        }else{
            SnackAlert.alert("No tines la aplicacion de Whatsapp instalada.", ctx.contentlogin,"error", Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()
        }
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnLogin->{
                val noEmpleado:String = ctx.txt_num_emp.text.toString()

                if(noEmpleado != ""){
                    actionNotificationInfo(noEmpleado)
                }else{
                    view.onLoginError("Ingrese un número de celular válido por favor.")
                }
            }
            R.id.btn_help_login->{
                loading.alertHelp(selectedFirst, selectedSecond, selectedThirt)
            }
            R.id.btn_first_select_dev->{
                val delete = sharedPreferences.edit()
                delete.remove("_baseurl")
                delete.commit()

                val editor = sharedPreferences.edit()
                editor.putString("_baseurl", EnviromentConstants().dev_server)
                editor.commit()

                System.exit(0);
            }
            R.id.btn_second_select_qa->{
                val delete = sharedPreferences.edit()
                delete.remove("_baseurl")
                delete.commit()

                val editor = sharedPreferences.edit()
                editor.putString("_baseurl", EnviromentConstants().qa_server)
                editor.commit()

                System.exit(0);
            }
            R.id.btn_thirt_select_prod->{
                val delete = sharedPreferences.edit()
                delete.remove("_baseurl")
                delete.commit()

                val editor = sharedPreferences.edit()
                editor.putString("_baseurl", EnviromentConstants().prod_server)
                editor.commit()
                System.exit(0);
            }
        }

    }
    private fun actionNotificationInfo(noEmpleado:String){

        onLogin(noEmpleado)

    }
    override fun onLongClick(v: View?): Boolean {
        alert.alertSelectEnviroment(this, this, this)
        return true
    }

    fun onGetURLFirebase(){
        myRef.child("data_base").child("login").child("url_policy_privacy").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){
                    val value = dataSnapshot.getValue().toString()
                    if(value != ""){
                        this@LoginPresenter .url_privacy = value
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

}