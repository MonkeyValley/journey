package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.CalendarView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.GetOut.GetoutModel
import com.example.jurney_mobile.Model.GetOut.IGetout
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.GetOutAdapter
import com.example.jurney_mobile.View.GetOutActivity
import com.example.jurney_mobile.View.Models.*
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_get_out.*
import kotlinx.android.synthetic.main.complaint_list_fragment.*
import kotlinx.android.synthetic.main.complaint_list_fragment.view.*
import kotlinx.android.synthetic.main.toolbar_component.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GetoutPresenter:IGetout.Presenter, View.OnClickListener, IClickItemDelegate.clickItemReason {

    var view:IGetout.view
    var ctx:GetOutActivity
    var model:IGetout.Model

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: GetOutAdapter
    private lateinit var sharedPreferences:SharedPreferences
    private lateinit var  user:UserResponseModel
    private lateinit var  token:String
    private val gson = Gson()
    private lateinit var loading: LoadingDialog


    var step_one:ReasonsResponseModel? = null
    var step_two:String = ""

    constructor(view: IGetout.view, ctx: GetOutActivity){
        this.view = view
        this.model = GetoutModel(ctx, this)
        this.ctx = ctx
    }

    override fun initView() {

        loading = LoadingDialog(ctx)
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val userStr = sharedPreferences.getString("_userinfo","")
        token = sharedPreferences.getString("_tokenuser","")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        ctx.btn_back.setOnClickListener(this)
        ctx.toolbar_title.text = "Salida de VO"
        ctx.btn_next_step_cat.setOnClickListener(this)
        ctx.btn_next_step_date.setOnClickListener(this)
        ctx.btn_back_step_cat_reasons.setOnClickListener(this)
        ctx.btn_next_step_description.setOnClickListener(this)

        val curDate =  Date()
        val sdf: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        step_two = sdf.format(curDate)
        var arrayDate = sdf.format(curDate).split("-")
        ctx.lbl_confirm_date_getout.text  = "¿Seleccionaste "+ arrayDate[2] + "/" + arrayDate[1] +"/"+ arrayDate[0] +"?"

        val mDate = sdf.parse(sdf.format(curDate))
        val timeInMilliseconds = mDate.getTime()

        ctx.calendarGetOut.minDate = timeInMilliseconds

        ctx.calendarGetOut.setOnDateChangeListener(object : CalendarView.OnDateChangeListener{
            override fun onSelectedDayChange(view: CalendarView, year: Int, month: Int, dayOfMonth: Int) {
                val date  = year.toString() + "-" + (month + 1)+ "-" + dayOfMonth
                Log.e("PICK DATE",date)
                step_two = date
                var arrayDate = date.split("-")
                ctx.lbl_confirm_date_getout.text  = "¿Seleccionaste "+ arrayDate[2] + "/" + arrayDate[1] +"/"+ arrayDate[0] +"?"
            }
        })
        model.getReasons(token)
    }

    override fun getReasonsResponse(response: ArrayList<ReasonsResponseModel>) {

        list = ctx.list_catalog_getout
        layoutManager = LinearLayoutManager(ctx)
        adaptador = GetOutAdapter(response, this, this.ctx)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador

    }

    override fun getReasonsErrorResponse(message: String) {
        SnackAlert.alert("Error al obtener datos", ctx.conteiner_getout, "error", Snackbar.LENGTH_LONG, "" )
        SnackAlert.showAlert()
        ctx.finish()
    }

    override fun sendGetOutErrorResponse(message: String) {
        loading.dismissDialog()
        SnackAlert.alert("Error al obtener datos", ctx.conteiner_getout, "error", Snackbar.LENGTH_LONG, "" )
        SnackAlert.showAlert()
    }

    override fun sendGetOutResponse(response: GetoutResponseModel) {

        loading.dismissDialog()
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        cal.setTime(sdf.parse(step_two))
        cal.add(Calendar.DATE, -1)
        val dateTo: Date = cal.getTime()
        val date_to = sdf.format(dateTo)

        user.leaving = LeavingResponseModel(response.id, "", step_two, response.status)
        val userStr = gson.toJson(user)

        val delete = sharedPreferences.edit()
        delete.remove("_userinfo")

        val editor = sharedPreferences.edit()
        editor.putString("_userinfo", userStr)
        editor.apply()

        var arrayDate = date_to.split("-")
        val date =  arrayDate[2] + "/" + arrayDate[1] +"/"+ arrayDate[0]

        ctx.lbl_second_description_thirt_step.text = "Por favor asiste el día "+date+", a la oficina de trabajo social para terminar tu proceso de salida."
        ctx.lbl_first_description_thirt_step.text = user.name+", te deseamos buen viaje, ¡Y te esperamos pronto de nuevo \n"

        next(ctx.conteiner_date_getOut, ctx.conteiner_description)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_back->{
                ctx.finish()
            }
            R.id.btn_next_step_cat->{
                if(step_one != null){
                    //slideNext(ctx.conteiner_reasons_cat, ctx.conteiner_date_getOut)
                    next(ctx.conteiner_reasons_cat, ctx.conteiner_date_getOut)
                }else{
                    SnackAlert.alert("Selecciona una opción", ctx.conteiner_getout, "error", Snackbar.LENGTH_LONG, "" )
                    SnackAlert.showAlert()
                }
            }
            R.id.btn_next_step_date->{
                if(step_two != ""){
//                    slideNext(ctx.conteiner_date_getOut, ctx.conteiner_description)
                    loading.startLoading()
                    model.sendGetOut(token, step_two, step_one!!.id)

                }else{
                    SnackAlert.alert("Seleccione una fecha", ctx.conteiner_getout, "error", Snackbar.LENGTH_LONG, "" )
                    SnackAlert.showAlert()
                }
            }
            R.id.btn_back_step_cat_reasons -> {
                if(step_one != null){
//                    slideBack(ctx.conteiner_date_getOut, ctx.conteiner_reasons_cat)
                    next(ctx.conteiner_date_getOut, ctx.conteiner_reasons_cat)
                }else{
                    SnackAlert.alert("Seleccione una fecha", ctx.conteiner_getout, "error", Snackbar.LENGTH_LONG, "" )
                    SnackAlert.showAlert()
                }
            }
            R.id.btn_next_step_description->{
                ctx.finish()
            }
        }
    }

    fun next(viewOut:View, viewIn:View){
        viewOut.visibility = View.GONE
        viewIn.visibility = View.VISIBLE
    }


    override fun click(item: ReasonsResponseModel) {
        step_one = item
    }
}