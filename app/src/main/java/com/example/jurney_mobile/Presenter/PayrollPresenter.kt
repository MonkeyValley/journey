package com.example.jurney_mobile.Presenter

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.GetOut.IGetout
import com.example.jurney_mobile.Model.Payroll.IPayroll
import com.example.jurney_mobile.Model.Payroll.PayrollModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.*
import com.example.jurney_mobile.View.Adapters.PayrollAdapter
import com.example.jurney_mobile.View.Adapters.PayrrollComplainAdapter
import com.example.jurney_mobile.View.DailyPayActivity
import com.example.jurney_mobile.View.Fragments.PayrollFragment
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Models.DayPaysheetModel
import com.example.jurney_mobile.View.Models.PaymentModel
import com.example.jurney_mobile.View.Models.TwoLastWeekendModel
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.botomsheet_payroll_complain.*
import kotlinx.android.synthetic.main.botomsheet_payroll_complain.view.*
import kotlinx.android.synthetic.main.payroll_fragment.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.bullyboo.text_animation.AnimationBuilder
import ru.bullyboo.text_animation.TextCounter
import java.lang.Exception
import kotlin.collections.ArrayList


class PayrollPresenter:IPayroll.Presenter, IClickItemDelegate.clickItemPayroll, View.OnTouchListener, View.OnClickListener, IClickItemDelegate.clickReasonPayrollComplain {

    private var view: IPayroll.View;
    private var model: IPayroll.Model;
    private var ctx:PayrollFragment
    private var v:View
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()
    private lateinit var bottomSheet: BottomSheetDialog

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: PayrollAdapter

    private lateinit var listBottomshee: RecyclerView
    private lateinit var layoutManagerBottomshee: RecyclerView.LayoutManager
    private lateinit var adaptadorBottomshee: PayrrollComplainAdapter

    private lateinit var dataDailypayStr:String
    private lateinit var lblTitleBottomSheet: TextView
    private lateinit var viewBottomSheet:View
    private lateinit var user:UserResponseModel
    private lateinit var daySelected:DayPaysheetModel
    private lateinit var token:String
    private lateinit var alert: LoadingDialog
    private var itemPosition:Int = 0
    private var typeSelected:Int = 0
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var dataList:List<ReasonsPayrollComplainModel>
    private var arrayFirstStep=ArrayList<Int>()
    private var  putExtraDailypay:String = ""

    private lateinit var loading:LoadingDialog

    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)



    lateinit var paymentGlb:TwoLastWeekendModel
    var weekglb = 1
    lateinit var weekend :PaymentModel

    constructor(view: IPayroll.View, ctx:PayrollFragment, v: View){
        this.view = view
        this.model = PayrollModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initInfo() {
        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx.context!!)
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        alert = LoadingDialog( ctx.activity!! )
        val userStr = sharedPreferences.getString("_userinfo","")
        val dataListStr = sharedPreferences.getString("_listreasonscomplain","")

        token = sharedPreferences.getString("_tokenuser","")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        dataList  =  gson.fromJson(dataListStr, Array<ReasonsPayrollComplainModel>::class.java).toList()
        //var dataArraylList = ArrayList<ReasonsPayrollComplainModel>
        //v.lbl_total_payment_payroll.text = "-$"+Milesformat().miles(payment.week_1.net)

        model.getDailyPayList(token, user.id)
        bottomSheet = BottomSheetDialog(this.ctx.context!!,  R.style.BottomSheetDialogTheme )
        viewBottomSheet = LayoutInflater.from(this.ctx.context!!).inflate(  R.layout.botomsheet_payroll_complain, v.findViewById( R.id.bottomSheetContainer))
        bottomSheet.setContentView(viewBottomSheet)
        viewBottomSheet.findViewById<Button>(R.id.btn_send_complaint).setOnClickListener(this)
        lblTitleBottomSheet = viewBottomSheet.findViewById(R.id.title_bottomsheet_complain)
        bottomSheet.setOnDismissListener(object : DialogInterface.OnDismissListener{
            override fun onDismiss(p0: DialogInterface?) {

                dataList.forEach { it.status = false }
                arrayFirstStep.removeAll(arrayFirstStep)
                adaptadorBottomshee = PayrrollComplainAdapter(dataList, this@PayrollPresenter, ctx.context!!)
                listBottomshee.layoutManager = layoutManagerBottomshee
                listBottomshee.adapter = adaptadorBottomshee

                viewBottomSheet.layout_details_payroll_complain.visibility = View.GONE
                viewBottomSheet.layout_list_payroll_complain.visibility = View.VISIBLE
                viewBottomSheet.layout_coment_payroll_complain.visibility = View.GONE

            }

        })

        val isPaymentAvailable = sharedPreferences.getString("_nominaAvailable","")
        val payment = gson.fromJson<PaymentModel>(isPaymentAvailable, PaymentModel::class.java)

        actionNotification()
        if(payment.errors != ""){
            myRef.child("data_base").child(user.id.toString()).child("payroll").child("notificationAction").child("view").setValue(true)
        }

        val paymentInfoStr = sharedPreferences.getString("_nominainfo", "")
        if (paymentInfoStr != "") {
            paymentGlb = gson.fromJson<TwoLastWeekendModel>(paymentInfoStr, TwoLastWeekendModel::class.java)
        } else {
            paymentGlb = TwoLastWeekendModel(
                PaymentModel("", 0, "", "", ArrayList(), 0.0, "", 0.0, 0, 0.0, 0),
                PaymentModel("", 0, "", "", ArrayList(), 0.0, "", 0.0, 0, 0.0, 0)
            )
        }

        listBottomshee = bottomSheet.list_payroll_complain_fragment
        layoutManagerBottomshee = LinearLayoutManager(ctx.context)
        adaptadorBottomshee = PayrrollComplainAdapter(dataList, this, ctx.context!!)
        listBottomshee.layoutManager = layoutManagerBottomshee
        listBottomshee.adapter = adaptadorBottomshee


        val splitDatefromlast = paymentGlb.week_1.date_from.split("-")
        val splitDatetolast = paymentGlb.week_1.date_to.split("-")

        val splitDatefromthis = paymentGlb.week_2.date_from.split("-")
        val splitDatetothis = paymentGlb.week_2.date_to.split("-")

        val dateformLast =  splitDatefromlast[2] + "/" + splitDatefromlast[1]
        val datetoLast =  splitDatetolast[2] + "/" + splitDatetolast[1]

        val datefromThis =  splitDatefromthis[2] + "/" + splitDatefromthis[1]
        val datetoThis =  splitDatetothis[2] + "/" + splitDatetothis[1]

        viewBottomSheet.btn_next_step_payroll_complain.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                Log.e("---->", gson.toJson(arrayFirstStep))
                if(arrayFirstStep.size > 0) {
                    viewBottomSheet.layout_coment_payroll_complain.visibility = View.VISIBLE
                    viewBottomSheet.layout_list_payroll_complain.visibility = View.GONE
                }else{
                    SnackAlert.alert("Selecciona almenos una razón", v.findViewById(R.id.layout_container_payroll) ,"error",Snackbar.LENGTH_LONG, "")
                    SnackAlert.showAlert()
                }
            }
        })

        viewBottomSheet.btn_back_step_payroll_complain.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                viewBottomSheet.layout_coment_payroll_complain.visibility = View.GONE
                viewBottomSheet.layout_list_payroll_complain.visibility = View.VISIBLE
            }
        })

        v.lbl_lastweekend_button.text = "ver semana del " + dateformLast + " al " +  datetoLast
        v.lbl_thisweekend_button.text = "ver semana del " + datefromThis + " al " +  datetoThis

    }
    fun setData(week:Int) {

        weekend = paymentGlb.week_2
        weekglb = week
        Log.e("", gson.toJson(weekend))

        if(weekend != null) {

            if (week == 2) {
                weekend = paymentGlb.week_1
            }

            val splitDatefrom = weekend.date_from.split("-")
            val splitDateto = weekend.date_to.split("-")

            val datefrom =  splitDatefrom[2] +"/"+ splitDatefrom[1]
            val dateto =  splitDateto[2] +"/"+ splitDateto[1]

            v.lbl_title_payroll_fragment.text = "Nómina del " + datefrom + " al " + dateto

            val modeBuilderOneSecond = AnimationBuilder.newBuilder().addPart(800, 60).build()
            val modeBuilderTwoSecond = AnimationBuilder.newBuilder().addPart(500, 60).build()
            val modeBuilderThreeSecond = AnimationBuilder.newBuilder().addPart(1000, 60).build()

            TextCounter.newBuilder().setTextView(v.lbl_ganacias_payroll).setType(TextCounter.DOUBLE)
                .setCustomAnimation(modeBuilderOneSecond).from(0).to(weekend.gross).setRound(2)
                .build()
                .start()
            TextCounter.newBuilder().setTextView(v.lbl_descuentos_payroll)
                .setType(TextCounter.DOUBLE)
                .setCustomAnimation(modeBuilderTwoSecond).from(0).to(weekend.deductions).setRound(2)
                .build().start()
            TextCounter.newBuilder().setTextView(v.lbl_total_payment_payroll)
                .setType(TextCounter.DOUBLE).setCustomAnimation(modeBuilderThreeSecond).from(0)
                .to(weekend.net).setRound(2).build().start()

            if (weekend.days.isEmpty()) {
                v.list_payroll_fragment.visibility = View.GONE
                v.empty_view_payroll.visibility = View.VISIBLE
            } else {
                list = v.findViewById(R.id.list_payroll_fragment)
                layoutManager = LinearLayoutManager(ctx.context)
                adaptador = PayrollAdapter(weekend.days, this, ctx)
                list.layoutManager = layoutManager
                list.adapter = adaptador
            }
        }
    }

    override fun onClick(v: View) {
            when (v.id) {
                R.id.btnLastWeekend -> {
                    this.v.btnActualWeekend.visibility = View.VISIBLE;
                    v.btnLastWeekend.visibility = View.GONE
                    setData(2)
                }
                R.id.btnActualWeekend -> {
                    this.v.btnLastWeekend.visibility = View.VISIBLE;
                    v.btnActualWeekend.visibility = View.GONE
                    setData(1)
                }

                R.id.btn_send_complaint -> {

                    alert.startLoading()

                    val req = ComplaintPayrollRequest(
                        user.id,
                        daySelected.date,
                        bottomSheet.ta_complaint_bottom_sheet.text.toString(),
                        arrayFirstStep
                    )

                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone",user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("envio_queja_nomina", bundle)

                    model.sendComplaint(token, req)
                }
                R.id.btnActionAlert -> alert.dismissDialog()
                R.id.btn_goto_dailypay -> {
                    Log.e("---","HELO")
                    val intent = Intent(ctx.context, DailyPayActivity::class.java)
                    intent.putExtra("listData", putExtraDailypay)
                    ctx.startActivity(intent)
                }
            }
    }

    override fun getDetailFromNotification(id: Int) {
        val isConnected = sharedPreferences.getBoolean("_isConnected", false)
        if (isConnected) {
                viewBottomSheet.layout_details_payroll_complain.visibility = View.VISIBLE
                viewBottomSheet.layout_list_payroll_complain.visibility = View.GONE
                model.getComplainDetail(token, id)
        }
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

    override fun sendComplaintResponse(response: ComplaintPayrollResponse) {

        dataList.forEach { it.status = false }
        arrayFirstStep.removeAll(arrayFirstStep)
        adaptadorBottomshee = PayrrollComplainAdapter(dataList, this@PayrollPresenter, ctx.context!!)
        listBottomshee.layoutManager = layoutManagerBottomshee
        listBottomshee.adapter = adaptadorBottomshee

        viewBottomSheet.layout_details_payroll_complain.visibility = View.GONE
        viewBottomSheet.layout_list_payroll_complain.visibility = View.VISIBLE
        viewBottomSheet.layout_coment_payroll_complain.visibility = View.GONE

        alert.dismissDialog()
        alert.alertNot(
            "Nóminas revisará tu caso y presentará los hallazgos en el día de revisión acordado.",
            "¡Tu queja ha sido enviada!",
            "Continuar",
            R.drawable.ic_send_complain,
            this,
            this,
            false,
            false,
            ""
        )
        bottomSheet.hide()
        bottomSheet.ta_complaint_bottom_sheet.setText("")

        this.weekend.days.get(itemPosition).has_complaint = true
        Log.e("->",gson.toJson(this.paymentGlb))

        if(weekglb == 1){
            paymentGlb.week_2.days.get(itemPosition).has_complaint = true
            paymentGlb.week_2.days.get(itemPosition).payroll_complaint_id = response.id

        }else{
            paymentGlb.week_1.days.get(itemPosition).has_complaint = true
            paymentGlb.week_1.days.get(itemPosition).payroll_complaint_id = response.id
        }

        val editor = sharedPreferences.edit()
        editor.putString("_nominainfo",gson.toJson(paymentGlb))
        editor.apply()
    }

    override fun sendComplaintErrorResponse(message: String) {

        dataList.forEach { it.status = false }
        arrayFirstStep.removeAll(arrayFirstStep)
        adaptadorBottomshee = PayrrollComplainAdapter(dataList, this@PayrollPresenter, ctx.context!!)
        listBottomshee.layoutManager = layoutManagerBottomshee
        listBottomshee.adapter = adaptadorBottomshee

        viewBottomSheet.layout_details_payroll_complain.visibility = View.GONE
        viewBottomSheet.layout_list_payroll_complain.visibility = View.VISIBLE
        viewBottomSheet.layout_coment_payroll_complain.visibility = View.GONE

        SnackAlert.alert(message, v.findViewById(R.id.layout_container_payroll) ,"error",Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
        bottomSheet.hide()
        bottomSheet.ta_complaint_bottom_sheet.setText("")
        alert.dismissDialog()
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (v!!.id == R.id.ta_complaint_bottom_sheet){

        }
        return true
    }


    private fun actionNotification(){
        myRef.child("data_base").child(user.id.toString()).child("payroll").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if( dataSnapshot.value != null){
                    val value = dataSnapshot.getValue(NotificationModel::class.java)!!
                    if(!value.view){
                        getPaymentTwoWeekends()
                    }else{
                        setData(1)
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }


    private fun getPaymentTwoWeekends() {
        val userStr = sharedPreferences.getString("_userinfo", "")
        val token = sharedPreferences.getString("_tokenuser", "")
        var user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        RetrofitClient.instance.getTwoLastWeeks("Bearer "+ token, user.employee_code, user.business_unit_id.toString()  ).enqueue(object :
            Callback<TwoLastWeekendModel> {
            override fun onFailure(call: Call<TwoLastWeekendModel>, t: Throwable) {
                Log.e("ERROR PUSH: ",t.message.toString())
            }

            override fun onResponse(call: Call<TwoLastWeekendModel>, response: Response<TwoLastWeekendModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if(response.body() == null){
                        Log.e("ERROR PUSH: ","No se pudo obtener los datos")

                    }else{
                        val responseStr = gson.toJson(response.body()!!)
                        val editor = sharedPreferences.edit()
                        editor.putString("_nominainfo", responseStr)
                        editor.apply()
                        setData(1)
                    }
                }else{
                    Log.e("ERROR PUSH: ","Invalid token")
                }
            }
        })
    }

    override fun clickReason(item: ReasonsPayrollComplainModel) {
       if(item.status){
           arrayFirstStep.add(item.id)
       }else{
           arrayFirstStep.remove(item.id)
       }
    }

    override fun click(item:DayPaysheetModel, pos:Int, type:Int) {

        val isConnected = sharedPreferences.getBoolean("_isConnected", false)
        if (isConnected) {
            this.daySelected = item
            this.itemPosition = pos
            this.typeSelected = type

            if(type == 1){
                viewBottomSheet.layout_details_payroll_complain.visibility = View.GONE
                viewBottomSheet.layout_list_payroll_complain.visibility = View.VISIBLE
                bottomSheet.show()
            }else{
                viewBottomSheet.layout_details_payroll_complain.visibility = View.VISIBLE
                viewBottomSheet.layout_list_payroll_complain.visibility = View.GONE
                model.getComplainDetail(token, item.payroll_complaint_id)
            }


        }
    }



    override fun getComplainDetailResponse(response: ComplainPayrollResponseModel) {

        viewBottomSheet.lbl_comments_complain_payroll.text = response.comment
        if(response.selected_reasons.size > 0){
            var reasons = ""
            response.selected_reasons.forEach {
                reasons += "-" + it.name + "\n"
            }
            viewBottomSheet.lbl_reasons_complain_payroll.text = reasons

            if(response.payroll_complaint_viability_status_id == 1){
                viewBottomSheet.layout_bg_color.setBackgroundColor(ctx.resources.getColor(R.color.cool_green_low))
                if(response.cause_comment != null  && response.cause_comment!!.size > 0) {
                    viewBottomSheet.lbl_title_response_complain.text = response.viability_status
                    viewBottomSheet.lbl_comment_response_complain.text =
                        response.cause_comment!!.get(0)
                }else{
                    viewBottomSheet.lbl_title_response_complain.text = "Pendiente"
                    viewBottomSheet.lbl_comment_response_complain.text = "Nómina se encuentra resolviendo tu queja."
                }
            }else if(response.payroll_complaint_viability_status_id == 2){
                viewBottomSheet.layout_bg_color.setBackgroundColor(ctx.resources.getColor(R.color.eggshell))
                if(response.cause_comment != null  && response.cause_comment!!.size > 0) {
                    viewBottomSheet.lbl_title_response_complain.text = response.viability_status
                    viewBottomSheet.lbl_comment_response_complain.text =
                        response.cause_comment!!.get(0)
                }else{
                    viewBottomSheet.lbl_title_response_complain.text = "Pendiente"
                    viewBottomSheet.lbl_comment_response_complain.text = "Nómina se encuentra resolviendo tu queja."
                }
            }else{
                viewBottomSheet.layout_bg_color.setBackgroundColor(ctx.resources.getColor(R.color.light_grey))
                if(response.cause_comment != null  && response.cause_comment!!.size > 0) {
                    viewBottomSheet.lbl_title_response_complain.text = response.viability_status
                    viewBottomSheet.lbl_comment_response_complain.text =
                        response.cause_comment!!.get(0)
                }else{
                    viewBottomSheet.lbl_title_response_complain.text = "Pendiente"
                    viewBottomSheet.lbl_comment_response_complain.text = "Nómina se encuentra resolviendo tu queja."
                }
            }

        }else{
            viewBottomSheet.lbl_reasons_complain_payroll.text = "No se seleccionaron razónes."
        }
        bottomSheet.show()
    }

    override fun getComplainDetailErrorResponse(message: String) {
        SnackAlert.alert(message,v.layout_container_payroll,"error",Snackbar.LENGTH_LONG,"")
        SnackAlert.showAlert()
    }

    override fun getDailyPayListResponse(response: DailypayResponseModel) {
        Log.v("RES DailyPay", gson.toJson(response))

        putExtraDailypay = gson.toJson(response)
        v.btn_goto_dailypay.background = ctx.resources.getDrawable(R.drawable.corner_radius_cool_green)
        v.btn_goto_dailypay.setOnClickListener(this)
        v.icon_image.setImageResource(R.drawable.ic_money)
    }

    override fun getDailyPayListErrorResponse(response: String, errorCode:String) {
        Log.v("E. getDailyPayList ->", response)

        when(errorCode){
            "400"->{
                v.btn_goto_dailypay.background = ctx.resources.getDrawable(R.drawable.corner_radius_light_gray)
                v.btn_goto_dailypay.setOnClickListener(object : View.OnClickListener{
                    override fun onClick(vw: View?) {
                        SnackAlert.alert("¿Quieres recibir tu nómina el mismo día?. Solicítalo en trabajo social" ,this@PayrollPresenter.v.layout_container_payroll,"info",Snackbar.LENGTH_LONG,"")
                        SnackAlert.showAlert()
                    }

                })
                v.icon_image.visibility = View.GONE
            }
            else ->{
                v.btn_goto_dailypay.background = ctx.resources.getDrawable(R.drawable.corner_radius_light_gray)
                v.btn_goto_dailypay.setOnClickListener(object : View.OnClickListener{
                    override fun onClick(vw: View?) {
                        SnackAlert.alert("Opción no disponible" ,this@PayrollPresenter.v.layout_container_payroll,"info",Snackbar.LENGTH_LONG,"")
                        SnackAlert.showAlert()
                    }

                })
                v.icon_image.visibility = View.GONE
            }
        }
    }
}
