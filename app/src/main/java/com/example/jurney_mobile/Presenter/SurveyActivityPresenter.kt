package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.Model.Surveys.ISurveys
import com.example.jurney_mobile.Model.Surveys.SurveyActivityModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.SurveySliderAdapter
import com.example.jurney_mobile.View.Fragments.SurveyFragmentMultipleOption
import com.example.jurney_mobile.View.Fragments.SurveyFragmentMultipleSelection
import com.example.jurney_mobile.View.Fragments.SurveyFragmentSatifaction
import com.example.jurney_mobile.View.Fragments.SurveyTextFragment
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.SurveyActivity
import com.facebook.share.Share
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_survey.*
import kotlinx.android.synthetic.main.survey_text_fragment.*
import kotlinx.android.synthetic.main.toolbar_component.*
import java.util.ArrayList

class SurveyActivityPresenter:ISurveys.PresenterActivity {

    private var model:ISurveys.ModelActivity
    private var view:ISurveys.ViewActivity
    private var ctx:SurveyActivity
    private var gson = Gson()
    private var position = 0
    private lateinit var request:RequestEncuestaModel
    private lateinit var requestMultiple:RequestEncuestaMultipleModel
    private lateinit var responseQuestions:ArrayList<PreguntaModel>
    private  var  requestMultipleQuestions: ArrayList<RequestPreguntaMultipleModel> = ArrayList()
    private var  arrayRespuestas: ArrayList<Int> = ArrayList()
    private var  arrayRespuestasSec: ArrayList<Int> = ArrayList()
    private var arrayFragments= ArrayList<PreguntaModel>()
    private lateinit var sharedPreferences:SharedPreferences
    private  lateinit var token:String
    private  lateinit  var encuesta:EncuestaModel
    private lateinit var user:UserResponseModel
    private var flat=false
    private var frag = ArrayList<Fragment>()
    private lateinit var loading: LoadingDialog
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    lateinit var shareDialog : ShareDialog

    constructor(view:ISurveys.ViewActivity, ctx:SurveyActivity){
        this.view = view
        this.model = SurveyActivityModel(this)
        this.ctx = ctx
    }

    override fun onClickSurvey(v: View) {
        when(v.id){
            R.id.btn_back -> ctx.finish()
            R.id.btn_previus_question -> {

                if(this.position - 1 == 0 ){
                    ctx.btn_previus_question.visibility = View.GONE
                }

                if(responseQuestions.get(this.position).question_type_id == 2 || responseQuestions.get(this.position).question_type_id == 1 || responseQuestions.get(this.position).question_type_id == 3){
                    this.arrayRespuestasSec = arrayRespuestas
                    val item = requestMultipleQuestions.get(position - 1).choice
                    this.arrayRespuestas = item
                    this.flat = true
                    requestMultipleQuestions.remove(requestMultipleQuestions.get(position - 1))
                }

                ctx.container_slider_survey.currentItem--
            }
            R.id.btn_next_question -> {

                if(this.position + 1 > 0 && this.responseQuestions.count() > 1 ){
                    ctx.btn_previus_question.visibility = View.VISIBLE
                }

                if(responseQuestions.get(this.position).question_type_id == 2 || responseQuestions.get(this.position).question_type_id == 1 || responseQuestions.get(this.position).question_type_id == 3 ) {
                    if (arrayRespuestas.isEmpty()) {
                        ctx.container_slider_survey.currentItem = this.position
                        SnackAlert.alert(
                            "Seleccione almenos una respuesta",
                            ctx.findViewById(R.id.container_surveys),
                            "error",
                            Snackbar.LENGTH_LONG,
                            ""
                        )
                        SnackAlert.showAlert()
                        return
                    }

                    val item = RequestPreguntaMultipleModel( encuesta.questions.get(position).question_id, arrayRespuestas, "")

                    if(!requestMultipleQuestions.contains(item)) {
                        requestMultipleQuestions.add(
                            RequestPreguntaMultipleModel(
                                encuesta.questions.get(
                                    position
                                ).question_id, arrayRespuestas
                            ,""
                            )
                        )
                    }
                    if(flat){
                        arrayRespuestas = arrayRespuestasSec
                        this.flat = false
                    }else{
                        arrayRespuestas = ArrayList()
                    }

                }else{
                    val vi = frag.get(position) as SurveyTextFragment
                    val value = vi.ta_response_survey_open.text.toString()

                    if (value.isEmpty()) {
                        ctx.container_slider_survey.currentItem = this.position
                        SnackAlert.alert(
                            "Es necesario escribir una respuesta.",
                            ctx.findViewById(R.id.container_surveys),
                            "error",
                            Snackbar.LENGTH_LONG,
                            ""
                        )
                        SnackAlert.showAlert()
                        return
                    }

                    val item = RequestPreguntaMultipleModel( encuesta.questions.get(position).question_id, arrayRespuestas, value)
                    if(!requestMultipleQuestions.contains(item)) {
                        requestMultipleQuestions.add(RequestPreguntaMultipleModel(encuesta.questions.get(position).question_id, arrayRespuestas, value))
                    }
                    if(flat){
                        arrayRespuestas = arrayRespuestasSec
                        this.flat = false
                    }else{
                        arrayRespuestas = ArrayList()
                    }
                    Log.e("EDITTEXT", value )
                }

                if(position == arrayFragments.count() - 1){
                    requestMultiple = RequestEncuestaMultipleModel(user.id, encuesta.survey_id, requestMultipleQuestions)
                    Log.e("END ------->", gson.toJson(requestMultiple))
                    loading.startLoading()
                    model.sendSurvey(requestMultiple, token)

                }else{
                    ctx.container_slider_survey.currentItem++
                }
            }
            R.id.btnActionAlert -> view.closeAlert()
            R.id.layer_facebook_share -> shareOnFacebook(ctx.resources.getDrawable(R.drawable.ic_surveys_green).toBitmap(), "Gracias por ayudarnos a conocer tu opinión. Sigue contestando más encuestas y gana vivacoins.")
        }
    }

    fun shareOnFacebook(bitmap: Bitmap, msj:String){
        if (ShareDialog.canShow(ShareLinkContent::class.java)) {

            val photo = SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("#VivaSmarth")
                .setUserGenerated(true)
                .build()
            val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()

            shareDialog.show(content)
        }
    }

    override fun onPageSelected(pos:Int) {

        this.position = pos
        ctx.progress_bar_questions.progress = (((pos+1)*100) / arrayFragments.count())
        ctx.lbl_steps_for_questions.text = "" + (pos+1)+" de "+ arrayFragments.count() + " pasos"

        if(this.position == arrayFragments.count() - 1){
            ctx.btn_next_question.setImageResource(R.drawable.ic_check)
        }else{
            ctx.btn_next_question.setImageResource(R.drawable.ic_arrow_right)
        }
    }

    override fun getData(request: Int) {
        model.getData(request, token)
    }

    override fun onSurveyByIdResponse(response: EncuestaModel) {

        for ( (i, item) in  response.questions.withIndex()){
            when(item.question_type_id){
                1-> {
                    val fragment = SurveyFragmentMultipleOption()
                    frag.add(fragment)
                }
                2->{
                    val fragment = SurveyFragmentMultipleSelection()
                    frag.add(fragment)
                }
                3->{
                    val fragment = SurveyFragmentSatifaction()
                    frag.add(fragment)
                }
                4->{
                    val fragment = SurveyTextFragment()
                    frag.add(fragment)
                }
            }
        }

        this.responseQuestions = response.questions
        encuesta = response
        arrayFragments.addAll(response.questions)
        ctx.toolbar_title.setText(response.name)
        if(response.questions.count() > 0) {
            ctx.progress_bar_questions.progress = 100 / response.questions.count()
            ctx.lbl_steps_for_questions.text = "1 de " + response.questions.count() + " pasos"
            if(this.position == arrayFragments.count() - 1){
                ctx.btn_next_question.setImageResource(R.drawable.ic_check)
            }
            ctx.adapter = SurveySliderAdapter(ctx.supportFragmentManager,  response.questions, ctx, frag)
            ctx.container_slider_survey.adapter = ctx.adapter
            ctx.container_slider_survey.addOnPageChangeListener(ctx)
        }
    }

    override fun onSurveyByIdErrorResponse(message: String) {
        SnackAlert.alert(message, ctx.findViewById(R.id.container_surveys), "error",Snackbar.LENGTH_LONG,"" )
        SnackAlert.showAlert()
//        loading.dismissDialog()
    }

    override fun onSendSurveyResponse(response: ResponseSendSurvey) {
        loading.dismissDialog()
        val bundle = Bundle()
        bundle.putInt("business_unit_id", user.business_unit_id)
        bundle.putInt("coins", user.coins)
        bundle.putString("name", user.name)
        bundle.putString("first_name", user.first_name)
        bundle.putString("cellphone",user.cellphone)
        bundle.putString("last_name", user.last_name)
        bundle.putString("employee_code", user.employee_code)
        bundle.putString("id", user.business_unit_id.toString())
        bundle.putBoolean("is_staff", user.is_staff)
        bundle.putInt("points", user.points)
        firebaseAnalytics.logEvent("envío_encuesta", bundle)
        val editor = sharedPreferences.edit()
        user.coins = user.coins + response.coins

        editor.putString("_userinfo", gson.toJson(user) )
        editor.apply()

        view.showAlert(response)
    }

    override fun onSendSurveyErrorResponse(message: String) {
        loading.dismissDialog()
        SnackAlert.alert(message, ctx.findViewById(R.id.container_surveys), "error",Snackbar.LENGTH_LONG,"" )
        SnackAlert.showAlert()
    }

    override fun initView() {
        shareDialog = ShareDialog(ctx)
        loading = LoadingDialog(ctx)
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx)
    }

    override fun clickCallBack(item: Int, position: Int, selected:Boolean) {
        if(responseQuestions.get(this.position).question_type_id == 1) {
            arrayRespuestas = arrayListOf(item)
        }else if(responseQuestions.get(this.position).question_type_id == 2){
            if(selected){
                arrayRespuestas.add(item)
            }else{
                arrayRespuestas.remove(item)
            }
        }else if(responseQuestions.get(this.position).question_type_id == 3){
            arrayRespuestas = arrayListOf(item)
        }
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }
}