package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.jurney_mobile.Model.Complaints.ComplaintsActivityModel
import com.example.jurney_mobile.Model.Complaints.IComplaint
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.*
import com.example.jurney_mobile.View.ComplaintsActivity
import com.example.jurney_mobile.View.Models.ComplaintRequest
import com.example.jurney_mobile.View.Models.ComplaintResponse
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_complaints.*
import kotlinx.android.synthetic.main.alert_place_layout.*
import kotlinx.android.synthetic.main.toolbar_component.*
import java.text.SimpleDateFormat
import java.util.*


class ComplaintsActivityPresenter: IComplaint.PresenterActivity, View.OnClickListener, clickOkDelegate {

    private var model:IComplaint.ModelActivity
    private var view:IComplaint.ViewActivity
    private  var ctx:ComplaintsActivity
    private  var complaint_category_id:Int = 0

    private  var complaint_place_id:Int = 0
    private  var complaint_place_type:Int = 0
    private  var complaint_place_name:String = ""
    private  var complaint_date:String = ""

    private  var complaint_denounced_id:Int = 0
    private lateinit var complaint_comment:String
    private lateinit var token:String
    private lateinit var user: UserResponseModel
    private var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var alert: LoadingDialog
    private lateinit var alertPlace: AlertDialog
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    constructor(view:IComplaint.ViewActivity, ctx:ComplaintsActivity) {
        this.view = view
        this.model = ComplaintsActivityModel(this)
        this.ctx = ctx
    }

    override fun onClick(v: View) {
        when(v.id){
           R.id.btn_back_complaints -> ctx.container_slider_complaint_dos.currentItem --
            R.id.btn_back -> view.close()
            R.id.btnActionAlert -> {alert.dismissDialog(); view.close()}
        }
    }

    override fun onPageSelected(position: Int) {
        if(position == 0){
            ctx.btn_back_complaints.visibility = View.GONE
        }else{
            ctx.btn_back_complaints.visibility = View.VISIBLE
        }

        ctx.progress_bar_complaints.progress = ((position + 1) * 100)/4
        ctx.lbl_steps_for_complaints.text = "" + (position + 1)+ " de 4 pasos"
    }

    override fun initInfo() {

        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx)
        alert = LoadingDialog( ctx)
        alertPlace = AlertDialog(ctx, this)

        complaint_category_id = ctx.intent.getIntExtra("opc", 0)
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        ctx.btn_back_complaints.setOnClickListener(this)
        ctx.toolbar_title.text = "Denuncias"
        ctx.btn_back.setOnClickListener(this)
        ctx.progress_bar_complaints.progress = 100/4
        ctx.lbl_steps_for_complaints.text = "1 de 4 pasos"
    }

    override fun selectedItem(item: Int, cat: String) {
        if(item <= 5){
            if(cat == "who"){
                ctx.container_slider_complaint_dos.currentItem ++
                complaint_denounced_id = item
            }else if(cat == "where"){
                complaint_place_id = item
                if(item == 1){
                    alertPlace.alertPlace()
                }else{
                    ctx.container_slider_complaint_dos.currentItem ++
                }

            }else if(cat.split("_")[0] == "more" ){
                val text = cat.split("_")[1]
                if(text != ""){
                    ctx.container_slider_complaint_dos.currentItem ++
                    complaint_comment = cat.split("_")[1]
                }else{
                    SnackAlert.alert("Por favor ingresa el nombre o descripción del agresor", ctx.findViewById(R.id.container_complaints), "error", Snackbar.LENGTH_LONG, "" )
                    SnackAlert.showAlert()
                }
            }else if(cat.split("_")[0] == "date" ){
                val text = cat.split("_")[1]
                if(text != ""){
                    ctx.container_slider_complaint_dos.currentItem ++
                    complaint_date = cat.split("_")[1]
                }else{
                    SnackAlert.alert("Seleccione una fecha", ctx.findViewById(R.id.container_complaints), "error", Snackbar.LENGTH_LONG, "" )
                    SnackAlert.showAlert()
                }
            }
        }else{
            alert.startLoading()
            val pattern = "yyyy-MM-dd"
            val simpleDateFormat = SimpleDateFormat(pattern)
            val date: String = simpleDateFormat.format(Date())

            val req = ComplaintRequest(user.id, date, complaint_category_id, complaint_place_id, complaint_denounced_id, complaint_comment, complaint_place_name, complaint_place_type, complaint_date )
            Log.e("----->", gson.toJson(req))
            model.sendComplaint(token, req)

        }
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun sendComplaintResponse(response: ComplaintResponse) {
        alert.dismissDialog()

        val bundle = Bundle()
        bundle.putInt("business_unit_id", user.business_unit_id)
        bundle.putString("name", user.name)
        bundle.putString("first_name", user.first_name)
        bundle.putString("cellphone",user.cellphone)
        bundle.putString("last_name", user.last_name)
        bundle.putInt("coins", user.coins)
        bundle.putString("employee_code", user.employee_code)
        bundle.putString("id", user.business_unit_id.toString())
        bundle.putBoolean("is_staff", user.is_staff)
        bundle.putInt("points", user.points)
        firebaseAnalytics.logEvent("envio_denuncia", bundle)

        alert.alertNot(
                "Trabajo social investigará y dará solución a tu denuncia. Ellos te contactarán por teléfono para más detalles.",
        "¡Gracias por tu denuncia!",
        "Continuar",
        R.drawable.ic_send_complain,

            this,
            this,
            false,
            false,
            ""
        )
    }

    override fun sendComplaintErrorResponse(message: String) {
        alert.dismissDialog()
        SnackAlert.alert(message, ctx.findViewById(R.id.container_complaints), "error", Snackbar.LENGTH_LONG, "" )
        SnackAlert.showAlert()
    }

    override fun clickContinue(complaint_place_name: String, complaint_place_type:Int) {
            this.complaint_place_name = complaint_place_name
            this.complaint_place_type = complaint_place_type
            alertPlace.dismissDialog()
            ctx.container_slider_complaint_dos.currentItem ++
    }

}