package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import com.example.jurney_mobile.Model.Splash.ISplash
import com.example.jurney_mobile.Model.Splash.SplashModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Home
import com.example.jurney_mobile.View.LoginActivity
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.example.jurney_mobile.View.SliderActivity
import com.example.jurney_mobile.View.SplashActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_splash.view.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class SplashPresenter:ISplash.Presenter {

    private var view: ISplash.View
    private var model: ISplash.Model
    private var ctx: SplashActivity
    lateinit var sharedPreferences: SharedPreferences
    private var gson = Gson()
    private lateinit var token: String
    private lateinit var user: UserResponseModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var intent: Intent
    private lateinit var homeIntent: Intent
    private var v:View

    constructor(view: ISplash.View, ctx: SplashActivity, v: View) {
        this.view = view
        this.model = SplashModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun intiInfo() {

        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        RetrofitClient.BASE_URL = sharedPreferences.getString("_baseurl", EnviromentConstants().prod_server)!!
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")
        intent = ctx.intent
        homeIntent = Intent(ctx, Home::class.java)
        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                Log.d("TAG", "PUSH Key: $key Value: $value")
                if (key == "type") {
                    homeIntent.putExtra("isNotificastion", value)
                }
            }
        } else {
            homeIntent.putExtra("isNotificastion", "home")
        }

        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx)

        if (token == "") {
            ctx.startActivity(Intent(ctx, LoginActivity::class.java))
            ctx.finish()
        } else {
            try {
                Log.e("TOKEN_USER", token)
                user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
                firebaseAnalytics.setUserId(user.employee_code)
                firebaseAnalytics.setUserProperty("Staff", user.is_staff.toString())
                firebaseAnalytics.setUserProperty("Inactivo", user.status.toString())
                firebaseAnalytics.setUserProperty("Unidad", user.business_unit_id.toString())
                sendRequests()
            }catch (e:Exception){
                e.printStackTrace()
                SnackAlert.alert("No se ha podido obtener la informacion del usuario, por favor contacte con el administrador.", v, "error", Snackbar.LENGTH_LONG, "")
                SnackAlert.showAlert()
            }
        }
    }

    fun sendRequests() {
        getQualityScore()
        model.getUserInfo(token)
        model.getInfoWeekendPaysheet(token, user.employee_code, user.business_unit_id.toString())
        model.getInfoTwoLastWeeks(token, user.employee_code, user.business_unit_id.toString())
        model.getListChallenges(token, user.id)
        model.getListChallengesCompletes(token)
        model.getListChallengesAvailables(token)
        model.getListChallengesHistory(token)
        model.getVacancies(token, user.id)
        model.getListAwards(token)
        model.getListCodes(token)
        model.getTopTen(token)
        model.getMyRanking(token)
//        model.getDailyPayList(token, user.id)
        model.getReasonsPayrollComplain(token)
        model.getTotalIncome(token, user.employee_code, user.business_unit_id.toString())
        model.getSupervisors(token)
    }

    override fun getQualityScore() {

        val currentDate = Date()
        val simpleDateformat = SimpleDateFormat("EEEE");

        val currentDay = simpleDateformat.format(currentDate)
        val foundData = sharedPreferences.getString("_qualityScore", "")

        Log.e("->>>> current ", currentDay)


        if (currentDay == "Friday" || currentDay == "viernes" || foundData != "") {
            sendRequest(currentDay)
        }
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    private fun sendRequest(currentDate: String) {

        val calTo = Calendar.getInstance()
        val calFrom = Calendar.getInstance()
        val id_user = user.id
        val business_unit_id = user.business_unit_id

        when (currentDate) {

            "viernes", "Friday" -> {
                calTo.add(Calendar.DATE, -2)
                calFrom.add(Calendar.DATE, -8)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "sabado", "Saturday" -> {
                calTo.add(Calendar.DATE, -3)
                calFrom.add(Calendar.DATE, -9)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "domingo", "Sunday" -> {
                calTo.add(Calendar.DATE, -4)
                calFrom.add(Calendar.DATE, -10)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "lunes", "Monday" -> {
                calTo.add(Calendar.DATE, -5)
                calFrom.add(Calendar.DATE, -11)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "martes", "Tuesday" -> {
                calTo.add(Calendar.DATE, -6)
                calFrom.add(Calendar.DATE, -12)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "miercoles", "Wednesday" -> {
                calTo.add(Calendar.DATE, -7)
                calFrom.add(Calendar.DATE, -13)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }
            "jueves", "Thursday" -> {
                calTo.add(Calendar.DATE, -8)
                calFrom.add(Calendar.DATE, -14)
                val dateTo: Date = calTo.getTime()
                val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                val dateFrom: Date = calFrom.getTime()
                val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                val date_from = sdfFrom.format(dateFrom)
                val date_to = sdfTo.format(dateTo)

                Log.e("->>>> datTo ", date_to)
                Log.e("->>>> dateFrom ", date_from)
                model.getQualityScore(
                    token,
                    id_user.toString(),
                    business_unit_id.toString(),
                    date_to,
                    date_from
                )
            }

        }


    }

//    ---------------------RESPONSE SERVICES-------------------------

    override fun getInfoWeekendPaysheetResponse(response: PaymentModel) {
        Log.v("RESPONSE WEEKPAYSHEET", gson.toJson(response))

        val responseStr = gson.toJson(response)
        val editor = sharedPreferences.edit()
        val delete = sharedPreferences.edit()
        delete.remove("_nominaAvailable")
        editor.putString("_nominaAvailable", responseStr)
        editor.apply()

    }

    override fun getUserInfoResponse(response: UserResponseModel) {
        Log.v("RESPONSE USER INFO", gson.toJson(response))

        val editor = sharedPreferences.edit()
        val delete = sharedPreferences.edit()
        delete.remove("_userinfo")
        val delete2 = sharedPreferences.edit()
        delete2.remove("_image")
        editor.putString("_userinfo", gson.toJson(response))
        editor.putString("_image", response.photo)
        editor.apply()

    }

    override fun getInfoTwoLastWeeksResponse(response: TwoLastWeekendModel) {
        Log.v("RESPONSE TWO WEEKS", gson.toJson(response))

        val responseStr = gson.toJson(response)
        val delete = sharedPreferences.edit()
        delete.remove("_nominainfo")

        val editor = sharedPreferences.edit()
        editor.putString("_nominainfo", responseStr)
        editor.apply()
    }

    override fun getTotalIncomeResponse(response: TotalIncomeModel) {
        Log.v("RESPONSE INCOME", gson.toJson(response))
        val delete = sharedPreferences.edit()
        delete.remove("_totalIncome")
        val editor = sharedPreferences.edit()
        editor.putString("_totalIncome", gson.toJson(response))
        editor.apply()
//        val isOnboardingvies = sharedPreferences.getBoolean("_isOnboardingView", false)
//        if(isOnboardingvies) {
            ctx.startActivity(homeIntent)
//        }else{
//            val intentOn = Intent(ctx, SliderActivity::class.java)
//            ctx.startActivity(intentOn)
//        }
        ctx.finish()
    }

    override fun getQualityScoreResponse(response: QualityScoreResponse) {
        Log.v("RESPONSE QUALITY SCORE", gson.toJson(response))

        val currentDate = Date()
        val simpleDateformat = SimpleDateFormat("yyyy-MM-dd");
        val currentDay = simpleDateformat.format(currentDate)

        val delete = sharedPreferences.edit()
        delete.remove("_qualityScore")
        val delete2 = sharedPreferences.edit()
        delete2.remove("_lastUpdate")

        val editor = sharedPreferences.edit()
        editor.putString("_qualityScore", gson.toJson(response))
        editor.putString("_lastUpdate", currentDay)
        editor.apply()

    }

    override fun getListChallengesResponse(response: ChallengesResponseModel) {
        Log.v("RESPONSE CHALLENGES", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listchallenges")

        val editor = sharedPreferences.edit()
        editor.putString("_listchallenges", gson.toJson(response))
        editor.apply()
    }

    override fun getListAwardsResponse(response: ArrayList<AwardsResponseModel>) {
        Log.v("RESPONSE AWARDS", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listawards")

        val editor = sharedPreferences.edit()
        editor.putString("_listawards", gson.toJson(response))
        editor.apply()
    }

    override fun getListCodesResponse(response: ArrayList<CodeResponseModel>) {
        Log.v("RESPONSE CODES", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listcodes")

        val editor = sharedPreferences.edit()
        editor.putString("_listcodes", gson.toJson(response))
        editor.apply()
    }

    override fun getTopTenResponse(response: ArrayList<UserRankingResponseModel>) {
        Log.v("RESPONSE TOPTEN", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listtopten")

        val editor = sharedPreferences.edit()
        editor.putString("_listtopten", gson.toJson(response))
        editor.apply()
    }
    override fun getMyRankingResponse(response: ArrayList<UserRankingResponseModel>) {
        val delete = sharedPreferences.edit()
        delete.remove("_listmyranking")

        val editor = sharedPreferences.edit()
        editor.putString("_listmyranking", gson.toJson(response))
        editor.apply()
    }
    override fun getVacanciesResponse(response: ArrayList<VacancyResponseModel>) {
        Log.v("RESPONSE VACANCIES", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listvacancies")

        val editor = sharedPreferences.edit()
        editor.putString("_listvacancies", gson.toJson(response))
        editor.apply()
    }

    override fun getReasonsPayrollComplainResponse(response: ArrayList<ReasonsPayrollComplainModel>) {
        Log.v("RES REASONSCOMPLAIN", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listreasonscomplain")

        val editor = sharedPreferences.edit()
        editor.putString("_listreasonscomplain", gson.toJson(response))
        editor.apply()
    }

    override fun getDailyPayListResponse(response: DailypayResponseModel) {
        Log.v("RES DailyPay", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listdailypay")

        val editor = sharedPreferences.edit()
        editor.putString("_listdailypay", gson.toJson(response))
        editor.apply()
    }

    override fun getSupervisorsResponse(response: ArrayList<SupervisorsResponseModel>) {
        Log.v("RES SUPERVISORS", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listsupervisors")

        val editor = sharedPreferences.edit()
        editor.putString("_listsupervisors", gson.toJson(response))
        editor.apply()
    }

    override fun getListChallengesCompletesResponse(response: ChallengesCompleteResponseModel) {
        Log.v("RES CompleteChallenges", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listchallengescomplete")

        val editor = sharedPreferences.edit()
        editor.putString("_listchallengescomplete", gson.toJson(response))
        editor.apply()
    }

    override fun getListChallengesAvailablesResponse(response: ArrayList<ChallengesAvailableResponse>) {
        Log.v("RES AvailablesChallenge", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listchallengesavailables")

        val editor = sharedPreferences.edit()
        editor.putString("_listchallengesavailables", gson.toJson(response))
        editor.apply()
    }

    override fun getDailyPayListErrorResponse(response: String, errorCode:String) {
        Log.v("E. getDailyPayList ->", response)

        val delete = sharedPreferences.edit()
        delete.remove("_listdailypay")
        delete.apply()
        delete.commit()

        val editor = sharedPreferences.edit()
        editor.putString("_listdailypay", errorCode)
        editor.apply()
    }

    override fun getListChallengesHistoryResponse(response: ArrayList<HistoryChallengesResponseModel>) {
        Log.v("geteHistoryChallenges->", gson.toJson(response))


        val delete = sharedPreferences.edit()
        delete.remove("_listhistorychallenges")

        val editor = sharedPreferences.edit()
        editor.putString("_listhistorychallenges", gson.toJson(response))
        editor.apply()

    }


    //    --------------------- ERROR RESPONSE SERVICES-------------------------

    override fun getInfoWeekendPaysheetErrorResponse(message: String) {
        Log.e("E.getInfoPaysheet ->", message)
    }

    override fun getUserInfoErrorRespose(message: String) {
        Log.e("E. getUserInfo->", message)
    }

    override fun getTotalIncomeErrorResponse(message: String) {
        Log.e("E. getTotalIncome->", message)
    }

    override fun getInfoTwoLastWeeksErrorResponse(message: String) {
        Log.e("E.getInfoTwoLast->", message)
    }

    override fun getQualityScoreErrorResponse(message: String) {
        Log.e("E. getQualityScore->", message)
    }

    override fun getListChallengesErrorResponse(message: String) {
        Log.v("E. getListChallenges ->", message)
    }

    override fun getListChallengesCompletesErrorResponse(message: String) {
        Log.v("ChallengesCompletes->", message)
    }
    override fun getListChallengesAvailablesErrorResponse(message: String) {
        Log.v("ChallengesAvailables->", message)
    }

    override fun getListAwardsErrorResponse(message: String) {
        Log.v("E. getListAwards ->", message)
    }

    override fun getListCodesErrorResponse(message: String) {
        Log.v("E. getListCodes ->", message)
    }

    override fun getTopTenErrorResponse(message: String) {
        Log.v("E. getTopTen ->", message)
    }
    override fun getMyRankingErrorResponse(message: String) {
        Log.v("E. getMyRanking ->", message)
    }
    override fun getVacanciesErrorResponse(message: String) {
        Log.v("E. getVacancies ->", message)
    }

    override fun getReasonsPayrollComplainErrorResponse(message: String) {
        Log.v("E. getReasonsPayroll ->", message)
    }

    override fun getSupervisorsErrorResponse(message: String) {
        Log.v("E. getSupervisors ->", message)
    }
    override fun getListChallengesHistoryErrorResponse(message: String) {
        Log.v("E.HistoryChallenges ->", message)
    }
}