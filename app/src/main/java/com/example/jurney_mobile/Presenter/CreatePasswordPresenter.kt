package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.core.text.toSpanned
import com.example.jurney_mobile.Model.Complaints.IComplaint
import com.example.jurney_mobile.Model.CreatePassword.CreatePasswordModel
import com.example.jurney_mobile.Model.CreatePassword.ICreatePassword
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.ComplaintsActivity
import com.example.jurney_mobile.View.CreatePasswordActivity
import com.example.jurney_mobile.View.Models.UpdatePasswordRequest
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.SliderActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_create_password.*
import java.nio.file.WatchEvent

class CreatePasswordPresenter: ICreatePassword.Presenter, View.OnClickListener, TextWatcher {

    private var model: ICreatePassword.Model
    private var view: ICreatePassword.View
    private  var ctx: CreatePasswordActivity

    private lateinit var token:String
    private lateinit var user: UserResponseModel
    private lateinit var loading: LoadingDialog
    private var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences

    constructor(view:ICreatePassword.View, ctx:CreatePasswordActivity) {
        this.view = view
        this.model = CreatePasswordModel(this)
        this.ctx = ctx
    }

    override fun initInfo() {
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        loading = LoadingDialog(ctx)

        ctx.closeButton.setOnClickListener(this)
        ctx.btnSendNewPassword.setOnClickListener(this)

        ctx.newPasswordEditText.addTextChangedListener(this)
        ctx.reNewPasswordEditText.addTextChangedListener(this)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnSendNewPassword -> {
                loading.startLoading()
                if(ctx.newPasswordEditText.text.toString() != "" && ctx.reNewPasswordEditText.text.toString() != ""){
                    if(ctx.newPasswordEditText.text.toString() == ctx.reNewPasswordEditText.text.toString()){
                        val pin = ctx.newPasswordEditText.text.toString()
                        model.updatePassword(token, user.id, UpdatePasswordRequest(pin))
                    }else{
                        loading.dismissDialog()
                        Toast.makeText(ctx.applicationContext, "Las contraseñas no coinciden.", Toast.LENGTH_LONG).show()
                    }
                }else{
                    loading.dismissDialog()
                    Toast.makeText(ctx.applicationContext, "Se Requieren todos los campos", Toast.LENGTH_LONG).show()
                }
            }
            R.id.closeButton -> {
                ctx.finish()
            }
        }
    }

    override fun updatePasswordResponse(response: UserResponseModel) {
        loading.dismissDialog()
        if(ctx.intent.getStringExtra("screenName") == "UserValidatePresenter") {
            var intent = Intent(ctx, SliderActivity::class.java)
            ctx.startActivity(intent)
        }
        Toast.makeText(ctx, "Contraseña actualizada correctamente.", Toast.LENGTH_LONG).show()
        ctx.finish()
    }

    override fun updatePasswordErrorResponse(message: String) {
        loading.dismissDialog()
        SnackAlert.alert("Error al cabiar contraseña, intente de nuevo.",ctx.changePasswordView,"error", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }

    override fun afterTextChanged(s: Editable?) {
        Log.e(s.toString(),"")
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    var psswordOk = false
    var rePasswordOk = false

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val value = s.toString()
        if(ctx.newPasswordEditText.text.toString().count() == 4 &&  ctx.reNewPasswordEditText.text.toString().count() == 4){
            ctx.btnSendNewPassword.isEnabled = true
            ctx.btnSendNewPassword.setBackgroundDrawable(ctx.resources.getDrawable(R.drawable.layer_bg_yellow_button))
        }else{
            Log.e("NEL","---<")
            ctx.btnSendNewPassword.isEnabled = false
            ctx.btnSendNewPassword.setBackgroundDrawable(ctx.resources.getDrawable(R.drawable.layer_bg_disable_button))
        }
    }
}