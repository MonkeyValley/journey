package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.DailyPay.DailypayModel
import com.example.jurney_mobile.Model.DailyPay.IDailypay
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.ClickItemDailypayDelegate
import com.example.jurney_mobile.View.Adapters.DailypayAdapter
import com.example.jurney_mobile.View.DailyPayActivity
import com.example.jurney_mobile.View.Models.*
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_daily_pay.*

class DailypayPresenter : IDailypay.Presenter, ClickItemDailypayDelegate {

    private var view: IDailypay.View;
    private var model: IDailypay.Model;
    private var ctx:DailyPayActivity

    lateinit var sharedPreferences: SharedPreferences
    private lateinit var token: String
    private lateinit var user: UserResponseModel

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: DailypayAdapter

    private var gson = Gson()
    val arr = arrayListOf<DailyPayModel>()

    constructor(view: IDailypay.View, ctx:DailyPayActivity){
        this.view = view
        this.model = DailypayModel(this)
        this.ctx = ctx
    }

    override fun initInfo() {
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        val dataListdailuypayStr = ctx.intent.getStringExtra("listData")
        val data = gson.fromJson(dataListdailuypayStr, DailypayResponseModel::class.java)

        arr.addAll(data.days)
        list = ctx.findViewById(R.id.list_dailypay)
        layoutManager = LinearLayoutManager(ctx.applicationContext)
        adaptador = DailypayAdapter(ctx, arr, this)
        list.layoutManager = layoutManager
        list.adapter = adaptador
    }
    lateinit var itemSelected:DailyPayModel
    var positionSelected: Int = -1

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun sendReqDailypayResponse(response: DayliPayRequestResponseModel) {
        arr.get(positionSelected).available = false

        val request = DailypayResponseModel(arr)

        val editor = sharedPreferences.edit()
        editor.putString("_listdailypay", gson.toJson(request))
        editor.apply()

        SnackAlert.alert("Peticion enviada", ctx.layout_conteiner_dailypay,"success", Snackbar.LENGTH_LONG,  "")
        SnackAlert.showAlert()

        adaptador.notifyItemChanged(positionSelected)
    }

    override fun sendReqDailypayErrorResponse(message: String) {
        SnackAlert.alert(message, ctx.layout_conteiner_dailypay,"error", Snackbar.LENGTH_LONG,  "")
        SnackAlert.showAlert()
        adaptador.notifyItemChanged(positionSelected)
    }



    override fun clickItem(item: DailyPayModel, position:Int) {
        positionSelected = position
        itemSelected = item
        model.sendReqDailypay(token, user.id, DayliPayRequestModel(item.date))
    }

}