package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Canvas
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Notifications.INotifications
import com.example.jurney_mobile.Model.Notifications.NotificationsModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.NotificationAdapter
import com.example.jurney_mobile.View.Adapters.selectedNotificationDelegate
import com.example.jurney_mobile.View.Fragments.NotificationsFragment
import com.example.jurney_mobile.View.Fragments.notificationSelectedFragmentDelegate
import com.example.jurney_mobile.View.Fragments.onClickItemSlider
import com.example.jurney_mobile.View.Models.ExtrasModel
import com.example.jurney_mobile.View.Models.NotificationModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationExtrasRealModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationRealmModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import io.realm.exceptions.RealmException
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import kotlinx.android.synthetic.main.notifications_fragment.*
import kotlinx.android.synthetic.main.notifications_fragment.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NotificationsFragmentPresenter: INotifications.Presenter, selectedNotificationDelegate {

    var v: View
    private var view: INotifications.View
    private var model: INotifications.Model
    private var ctx: NotificationsFragment
    private var exp:notificationSelectedFragmentDelegate
    private lateinit var arrayReuslt:RealmResults<NotificationRealmModel>
    private  var arrayList = ArrayList<NotificationRealmModel>()
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: NotificationAdapter
    lateinit var realm: Realm
    lateinit var token:String
    lateinit var user : UserResponseModel
    var onclickMarkAll = (object : View.OnClickListener{
        override fun onClick(v: View?) {

            realm.beginTransaction()
            try {
                val query = realm.where(NotificationRealmModel::class.java).sort("id",Sort.DESCENDING).findAll()
                val isAllTrue = allIsTrue(query)

                query.forEach {
                    if(isAllTrue){
                        it.view = false
                    }else {
                        it.view = true
                    }
                }

                if(allIsTrue(query)){
                    v!!.btn_mark_all_read.setText("Marcar todos como no leidos")
                }else{
                    v!!.btn_mark_all_read.setText("Marcar todos como leidos")
                }
                arrayList.removeAll(arrayList)
                arrayReuslt = query
                arrayList.addAll(realm.copyFromRealm(query))
                adaptador.notifyDataSetChanged()
//                setData()

                realm.commitTransaction()
            }catch (e:RealmException){

            }
        }
    })

    var gson = Gson()
    private lateinit var sharedPreferences: SharedPreferences


    constructor(presenter:INotifications.View, ctx: NotificationsFragment, v: View, exaple: notificationSelectedFragmentDelegate){
        this.view = presenter
        this.model = NotificationsModel(this)
        this.ctx = ctx
        this.v = v
        this.exp = exaple
    }

//fun setData(response:ArrayList<NotificationModel>){
    fun setData(){
        if(this.arrayList.size > 0){
            v.list_notification.visibility = View.VISIBLE
            v.empty_view_notifications.visibility = View.GONE

            list = v.findViewById(R.id.list_notification)
            layoutManager = LinearLayoutManager(ctx.context)
            adaptador = NotificationAdapter(ctx.context!!,this, this.arrayList)
            list.isNestedScrollingEnabled = false
            list.layoutManager = layoutManager
            list.adapter = adaptador

            val ith = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT){
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                    val position = viewHolder.adapterPosition
                    realm.beginTransaction()
                    try {
                        val result = realm.where(NotificationRealmModel::class.java).equalTo( "id" , arrayList[position].id).findFirst()
                        val resultExtras = realm.where(NotificationExtrasRealModel::class.java).equalTo( "notificationId" , arrayList[position].id).findFirst()
                        result!!.deleteFromRealm()
                        resultExtras!!.deleteFromRealm()
                        realm.commitTransaction()
                    }catch (e:IndexOutOfBoundsException){
                    }

                    arrayList.removeAt(position)
                    adaptador.notifyItemRemoved(position)

                    if(arrayList.size == 0) {
                        v.list_notification.visibility = View.GONE
                        v.empty_view_notifications.visibility = View.VISIBLE
                        v.btn_delete_all.visibility = View.GONE
                        v.btn_mark_all_read.visibility = View.GONE


                    }

                    arrayList.removeAt(position)
                    adaptador.notifyItemRemoved(position)

                    if(arrayList.size == 0){
                        v.list_notification.visibility = View.GONE
                        v.empty_view_notifications.visibility = View.VISIBLE
                        v.btn_delete_all.visibility = View.GONE
                        v.btn_mark_all_read.visibility = View.GONE
                    }
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ctx.resources.getColor(R.color.pinkish_red))
                        .addActionIcon(R.drawable.ic_delete_black_24dp)
                        .create()
                        .decorate()
                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )


                }
            })

            ith.attachToRecyclerView(list)
        }else{
            v.list_notification.visibility = View.GONE
            v.empty_view_notifications.visibility = View.VISIBLE
        }
    }

    override fun getnotificationListResponse(response: ArrayList<NotificationModel>) {
        Log.e("Response", gson.toJson(response))
//        this.setData(response)

    }

    override fun getnotificationListErrorResponse(msg: String) {
        Log.e("E.getInfoPaysheet ->", msg)
        v.list_notification.visibility = View.GONE
        v.empty_view_notifications.visibility = View.VISIBLE
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }


    override fun initInfo() {
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!

        v.btn_mark_all_read.setOnClickListener(onclickMarkAll)
        v.btn_delete_all.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v2: View?) {

                realm.beginTransaction()
                try {
                    val query = realm.where(NotificationRealmModel::class.java).findAll()
                    val queryExtras = realm.where(NotificationExtrasRealModel::class.java).findAll()
                    query.deleteAllFromRealm()
                    queryExtras.deleteAllFromRealm()

                    arrayReuslt = query
                    arrayList.addAll(realm.copyFromRealm(query))
                    adaptador.notifyDataSetChanged()
                    realm.commitTransaction()

                    v.list_notification.visibility = View.GONE
                    v.empty_view_notifications.visibility = View.VISIBLE
                    v2!!.visibility = View.GONE
                    v.btn_mark_all_read.visibility = View.GONE



                }catch (e:RealmException){

                }

            }
        })
        Realm.init(ctx.context)
        realm = Realm.getDefaultInstance()
        val query = realm.where(NotificationRealmModel::class.java).sort("id",Sort.DESCENDING).findAll()

        if(query.count() > 0){
            arrayReuslt = query
            arrayList.addAll(realm.copyFromRealm(query))
            setData()

            val isAllTrue = this.allIsTrue(query)
            if(isAllTrue){
                v.btn_mark_all_read.setText("Marcar todos como no leidos")
            }else{
                v.btn_mark_all_read.setText("Marcar todos como leidos")
            }
        }else{
            v.btn_mark_all_read.visibility = View.GONE
            v.btn_delete_all.visibility = View.GONE

            v.list_notification.visibility = View.GONE
            v.empty_view_notifications.visibility = View.VISIBLE
        }

        /*
        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        val calTo = Calendar.getInstance()
        val dateTo: Date = calTo.getTime()
        val sdfTo = SimpleDateFormat("yyyy-MM-dd")


        val calFrom = Calendar.getInstance()
        calFrom.add(Calendar.DATE, -7)
        val dateFrom: Date = calFrom.getTime()
        val sdfFrom = SimpleDateFormat("yyyy-MM-dd")

        al date_from = sdfFrom.format(dateFrom)
        val date_to = sdfTo.format(dateTo)
        model.getnotificationList(token,date_from,date_to, user.id)
        */
    }

    fun allIsTrue(arrayReuslt:RealmResults<NotificationRealmModel>): Boolean{
        return arrayReuslt.all{ it.view == true }
    }


    override fun selectedNotification(index:Int) {

        realm.beginTransaction()
        try {
            var query = realm.where(NotificationRealmModel::class.java).equalTo("id", arrayReuslt[index]!!.id).findFirst()

            query!!.view = true
            realm.commitTransaction()
        }catch (e:RealmException){

        }

        exp.notificationselected(arrayReuslt[index]!!)
    }
}
