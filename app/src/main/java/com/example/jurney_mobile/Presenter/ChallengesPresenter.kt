package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.Challenges.ChallengesModel
import com.example.jurney_mobile.Model.Challenges.IChallenges
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.ChallengeAdapter
import com.example.jurney_mobile.View.Adapters.ChallengesTabBarAdapter
import com.example.jurney_mobile.View.Fragments.AvailableChallengesTabBarFragment
import com.example.jurney_mobile.View.Fragments.ChallengesFragment
import com.example.jurney_mobile.View.Fragments.HistoryChallengesTabBarFragment
import com.example.jurney_mobile.View.Fragments.MyChallengesTabBarFragment
import com.example.jurney_mobile.View.Models.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bottomsheet_details_challenge.view.*
import kotlinx.android.synthetic.main.challenges_fragment.*
import kotlinx.android.synthetic.main.challenges_fragment.view.*

class ChallengesPresenter(view:IChallenges.View, ctx:ChallengesFragment, v:View):IChallenges.Presenter, IClickItemDelegate.clickItemChallenge{

    var view:IChallenges.View
    var model:IChallenges.Model
    var ctx:ChallengesFragment
    var v:View
    var gson = Gson()
    lateinit var challenges:ChallengesCompleteResponseModel
    lateinit var sharedPreferences:SharedPreferences
    private lateinit var bottomSheet: BottomSheetDialog
    private lateinit var viewBottomSheet:View
    private lateinit var user:UserResponseModel
    private var adapter :ChallengesTabBarAdapter? = null

    init {
        this.view = view
        this.model = ChallengesModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initInfo() {

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val data = sharedPreferences.getString("_listchallengescomplete", "")
        val userStr = sharedPreferences.getString("_userinfo", "")
        challenges = gson.fromJson<ChallengesCompleteResponseModel>(data, ChallengesCompleteResponseModel::class.java)
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        bottomSheet = BottomSheetDialog(this.ctx.context!!,  R.style.BottomSheetDialogTheme )
        viewBottomSheet = LayoutInflater.from(this.ctx.context!!).inflate(  R.layout.bottomsheet_details_challenge, v.findViewById( R.id.bottomSheetContainerChallenges))
        bottomSheet.setContentView(viewBottomSheet)

        Log.e("CHALLENGES", data)

        if(challenges.challenges.isEmpty()){
            v.empty_view_payroll.visibility = View.VISIBLE
           // v.list_challenges_fragment.visibility = View.GONE
            // v.empty_view_payroll.layoutParams = LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT )
        }

        if(!user.status){
            val challenge = ArrayList<ChallengesAvailableResponse>()
            challenges.challenges.forEach {
                if(it.challenge_name == "Ver Actividades disponibles" ){
                    challenge.add(it)
                }
            }
        }else{
            v.lbl_challenges_wings.text = "Retos ganados: " + challenges.challenges_no
            v.lbl_challenges_count.text = "VC ganados: " + challenges.points
        }


    }

    override fun setupViewPager() {
       setUpPager()
    }

    override fun click(item: ChallengeModel) {
        viewBottomSheet.lbl_title_modal.text = item.name
        viewBottomSheet.lbl_description_modal.text = item.description.split("-").get(0)
        viewBottomSheet.lbl_vivacoins_modal.text = "+" + item.points

        if (item.completed) {
            viewBottomSheet.is_completed_icon_modal.background =
                ctx.resources.getDrawable(R.drawable.ic_completed)
        } else {
            viewBottomSheet.is_completed_icon_modal.background =
                ctx.resources.getDrawable(R.drawable.ic_no_complete)
        }

        val image = "" + item.image
        if (image != ""){
            Picasso.get().load(image).error(R.drawable.no_image).into(viewBottomSheet.ic_challenge_modal)
        }else{
            viewBottomSheet.ic_challenge_modal.setImageResource(R.drawable.no_image)
        }
        bottomSheet.show()
    }

    fun setUpPager(){

        if(adapter != null) {
            adapter!!.mFragmentTitleList = ArrayList()
            adapter!!.mFragmentList = ArrayList()

            adapter!!.notifyDataSetChanged()
        }
        adapter = ChallengesTabBarAdapter(ctx.activity!!.supportFragmentManager )
        adapter!!.addFragment(MyChallengesTabBarFragment(this),"Mis retos de la semana" )
        adapter!!.addFragment(HistoryChallengesTabBarFragment(this),"Mi histórico" )
        adapter!!.addFragment(AvailableChallengesTabBarFragment(this),"Retos disponibles" )

        v.pager_challenges.adapter = adapter
        v.tabs.setupWithViewPager(v.pager_challenges)

        val arrDateFrom = challenges.date_from.split("-")
        val arrDateto = challenges.date_to.split("-")
        val date_from = arrDateFrom[2] + "-" +utilsClass().convertIntMonthToStringMonth(arrDateFrom[1].toInt())
        val date_to = arrDateto[2] + "-" +utilsClass().convertIntMonthToStringMonth(arrDateto[1].toInt())
        v.lbl_description_challenges.setTextColor(ctx.resources.getColor(R.color.sun_yellow))
        v.lbl_description_challenges.text = date_from + " al " + date_to

        v.tabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                Log.e("----> ",tab.position.toString())
                when(tab.position){
                    0->{
                        v.header_fragment_my_challenges.visibility = View.VISIBLE

                        val arrDateFrom = challenges.date_from.split("-")
                        val arrDateto = challenges.date_to.split("-")
                        val date_from = arrDateFrom[2] + "-" +utilsClass().convertIntMonthToStringMonth(arrDateFrom[1].toInt())
                        val date_to = arrDateto[2] + "-" +utilsClass().convertIntMonthToStringMonth(arrDateto[1].toInt())
                        v.lbl_description_challenges.setTextColor(ctx.resources.getColor(R.color.sun_yellow))
                        v.lbl_description_challenges.text = date_from + " al " + date_to
                    }
                    1->{
                        v.header_fragment_my_challenges.visibility = View.GONE
                        v.lbl_description_challenges.text = "Mis retos logrados y VC obtenidos cada semana"
                        v.lbl_description_challenges.setTextColor(ctx.resources.getColor(R.color.charcoal))
                    }
                    2->{
                        v.header_fragment_my_challenges.visibility = View.GONE
                        v.lbl_description_challenges.setTextColor(ctx.resources.getColor(R.color.charcoal))
                        v.lbl_description_challenges.text = "Obten VivaCoins al completarlos retos que vivaorganica tiene para ti."
                    }
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {
            }
            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }
}