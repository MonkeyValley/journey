package com.example.jurney_mobile.Presenter

import android.content.Intent
import android.view.View
import com.example.jurney_mobile.View.ComplaintsActivity
import com.example.jurney_mobile.Model.Complaints.ComplaintsModel
import com.example.jurney_mobile.Model.Complaints.IComplaint
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Fragments.ComplaintsFragment

class ComplaintPresenter:IComplaint.Presenter {

    var model:IComplaint.Model
    var view:IComplaint.View
    var ctx:ComplaintsFragment
    constructor(view:IComplaint.View, ctx:ComplaintsFragment){
        this.view = view
        this.model = ComplaintsModel(this)
        this.ctx = ctx
    }

    override fun onClick(v:View) {
        when(v.id){
            R.id.btn_abuse_next_step_complaint-> ctx.startActivity(Intent(ctx.context, ComplaintsActivity::class.java).putExtra("opc",1))
            R.id.btn_bullying_next_step_complaint->ctx.startActivity(Intent(ctx.context, ComplaintsActivity::class.java).putExtra("opc",2))
            R.id.btn_threat_next_step_complaint->ctx.startActivity(Intent(ctx.context, ComplaintsActivity::class.java).putExtra("opc",3))
            R.id.btn_discrimination_next_step_complaint->ctx.startActivity(Intent(ctx.context, ComplaintsActivity::class.java).putExtra("opc",4))
            R.id.btn_steal_next_step_complaint->ctx.startActivity(Intent(ctx.context, ComplaintsActivity::class.java).putExtra("opc",5))
        }
    }


}