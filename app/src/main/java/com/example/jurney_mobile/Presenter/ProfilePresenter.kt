package com.example.jurney_mobile.Presenter

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.SpannableStringBuilder
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.text.bold
import com.example.jurney_mobile.Model.Profile.IProfile
import com.example.jurney_mobile.Model.Profile.ProfileModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.CreatePasswordActivity
import com.example.jurney_mobile.View.Fragments.ProfileFragment
import com.example.jurney_mobile.View.GetOutActivity
import com.example.jurney_mobile.View.Models.GetoutResponseModel
import com.example.jurney_mobile.View.Models.UploadImageResponse
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.SliderActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.home_fragment.view.*
import kotlinx.android.synthetic.main.profile_component.*
import kotlinx.android.synthetic.main.profile_fragment.*
import kotlinx.android.synthetic.main.profile_fragment.view.*
import kotlinx.android.synthetic.main.profile_fragment.view.progress_bar_current_experience
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Dispatcher
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import kotlin.coroutines.CoroutineContext

class ProfilePresenter:IProfile.Presenter, View.OnClickListener {

    private var view:IProfile.View
    private var model:IProfile.Model
    private var ctx:ProfileFragment
    private var v: View
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()
    private var image = ""
    private lateinit var token:String
    var image_uri: Uri? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private  lateinit var  user:UserResponseModel
    private lateinit var bottomSheet: BottomSheetDialog
    private lateinit var viewBottomSheet:View

    private val GALLERY = 1
    private val CAMERA = 2

    constructor(view:IProfile.View, ctx:ProfileFragment, v:View){
        this.view = view
        this.model = ProfileModel(this)
        this.ctx = ctx
        this.v = v
    }


    override fun initInfo() {

        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx.context!!)

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!

        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        image = EnviromentConstants().image_serverver + user.photo

        bottomSheet = BottomSheetDialog(this.ctx.context!!,  R.style.BottomSheetDialogTheme )
        viewBottomSheet = LayoutInflater.from(this.ctx.context!!).inflate(  R.layout.bottomsheet_image_settings, v.findViewById( R.id.bottomSheetContainer))
        bottomSheet.setContentView(viewBottomSheet)

        viewBottomSheet.findViewById<LinearLayout>(R.id.sel_get_image_from_galery).setOnClickListener(this)
        viewBottomSheet.findViewById<LinearLayout>(R.id.sel_get_image_from_camera).setOnClickListener(this)

        setdata(user)

//        v.iv_user_image_profile.

        Picasso.get().load(image).error(R.drawable.no_image_profile).into(v.iv_user_image_profile)
    }

    fun setdata(obj:UserResponseModel){

        val text_descripcion = "Te faltan "
        val text_descripcion2 = " VivaCoins para llegar al nivel "

        if(obj.status){
            v.layer_request_getout.visibility = View.VISIBLE
        }else{
            v.layer_request_getout.visibility = View.GONE
        }

        if( obj.leaving != null){

            var arrayDate = obj.leaving!!.date.split("-")
            val dateFormat = arrayDate[2]+"/"+arrayDate[1]+"/"+arrayDate[0]

            val date = SpannableStringBuilder()
                .append("Dia de mi salida: ")
                .bold { append(dateFormat) }

            v.lbl_date_out.text = date
            v.btn_send_resignation.setText("Cancelar peticion")
            v.lbl_date_out.visibility = View.VISIBLE
        }else{
            Log.e("NEL", "nel")
            v.btn_send_resignation.setText("Anunciar")
        }

        val description = SpannableStringBuilder()
            .append(text_descripcion)
            .bold { append(obj.to_next_lvl.toString()) }
            .append(text_descripcion2)
            .bold { append(obj.next_lvl.toString()) }

        val value_progress = SpannableStringBuilder()
            .bold { append(obj.points.toString()) }
            .append(" de " + ( obj.points + obj.to_next_lvl ) + " VivaCoins")

        val persent = ((obj.points * 100 ) / (obj.to_next_lvl + obj.points))

        v.progress_bar_current_experience.progress = persent
        v.lbl_name_user_profile.text = obj.name + " " + obj.first_name + " " + obj.last_name
        v.lbl_level_user_profile.text = "Nivel: "+obj.current_lvl
        v.lbl_value_progress_bar_profile.text = value_progress
        v.lbl_description_profile.text = description
        v.lbl_num_employee_profile.text = obj.employee_code
        v.lbl_num_cellphone_profile.text = obj.cellphone
        v.changePasswordButton.setOnClickListener(this)
        if(obj.status){v.lbl_bussiness_unity_desc.text = utilsClass().chooseUnityBussiness(obj.business_unit_id)}
        else{v.lbl_bussiness_unity_desc.text = "Ninguna"}

        v.btn_send_resignation.setOnClickListener(this)

        if(obj.ranking_history.size > 0){
            if (obj.ranking_history.get(obj.ranking_history.size - 1).level_category != null) {
                val categorie = obj.ranking_history.get(obj.ranking_history.size - 1).level_category
                v.lbl_category_level_user_profile.text = categorie
                v.view_category_color_profile.setBackgroundColor(utilsClass().getColorCategory(categorie!!))
            } else {
                val objCat = utilsClass().rangeCategories(obj.current_lvl)
                v.lbl_category_level_user_profile.text = objCat.category
                v.view_category_color_profile.setBackgroundColor(ctx.resources.getColor(objCat.color))
            }
        }else {
            val objCat = utilsClass().rangeCategories(obj.current_lvl)
            v.lbl_category_level_user_profile.text = objCat.category
            v.view_category_color_profile.setBackgroundColor(ctx.resources.getColor(objCat.color))
        }
    }

    override fun click(v: View) {
        bottomSheet.show()
    }

    fun choosePhotoFromGallary() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        ctx.startActivityForResult(intent, GALLERY)
    }

    fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = ctx.context!!.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        ctx.startActivityForResult(cameraIntent, CAMERA)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.sel_get_image_from_galery -> {
                if(getpermitionGalery()) {
                    choosePhotoFromGallary()
                }
            }
            R.id.sel_get_image_from_camera -> {
               if(getpermitionCamera() && getpermitionGalery()) {
                   openCamera()
                }
            }
            R.id.btn_send_resignation -> {
                if(user.leaving != null){
                    model.cancelLeaving(token, user.leaving!!.id)
                }else{
                    val intent = Intent(ctx.context, GetOutActivity::class.java)
                    ctx.startActivity(intent)
                }
            }
            R.id.changePasswordButton ->{
                var intent = Intent( ctx.context  , CreatePasswordActivity::class.java)
                intent.putExtra("screenName", "ProfilePresenter")
                ctx.startActivity(intent)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (resultCode == Activity.RESULT_OK && requestCode == GALLERY){
                loadImage(data!!.data!!)
            } else if (resultCode == Activity.RESULT_OK && requestCode == CAMERA){
                loadImage(image_uri!!)
            }

    }

    fun loadImage(image:Uri){
        bottomSheet.hide()

        val filePath = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = ctx.context!!.getContentResolver().query(image, filePath, null, null, null)
        cursor!!.moveToFirst()
        val imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]))
        try{
            CoroutineScope(Dispatchers.Main).launch {
                val file:File
                val compressedImageFile = Compressor.compress(ctx.context!!, File(imagePath))
                file = compressedImageFile
                Log.e("--> ", "--- " + file.length())
                model.loadImageHTTPCLIENT(token, file)
            }
        }catch (e: Exception){
            SnackAlert.alert("Error al cambiar la imagen", v.layout_container_profile  ,"error",Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()
        }

    }

    private fun convertphoto(selectedImage:Uri) {
        lateinit var originBitmap:Bitmap
        var imageStream:InputStream
        //Uri selectedImage = data.getData();
        // Toast.makeText(getActivity(), selectedImage.toString(), Toast.LENGTH_LONG).show();

        try {
            imageStream = ctx.activity!!.contentResolver.openInputStream(selectedImage)!!   //getActivity().getContentResolver().openInputStream(selectedImage);
            originBitmap = BitmapFactory.decodeStream(imageStream);
        } catch (e: FileNotFoundException) {
            System.out.println(e.message.toString());
        }
        if (originBitmap != null) {
            Log.w("Image Setted in", "Done Loading Image")
            try {
                val image =  originBitmap
                val byteArrayOutputStream =  ByteArrayOutputStream()

                image.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream)
                val byteArray = byteArrayOutputStream.toByteArray()

                val encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                // Calling the background process so that application wont slow down

                //End Calling the background process so that application wont slow down
            } catch (e:Exception) {
                Log.w("OOooooooooo", "exception");
            }
            // Toast.makeText(getActivity(), "Conversion Done", Toast.LENGTH_SHORT).show();
        }
        // End getting the selected image, setting in imageview and converting it to byte and base 64
    }
    override fun loadImageHTTPCLIENTResponse(response: UploadImageResponse) {
        val userStr = sharedPreferences.getString("_userinfo", "")
        var user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        model.updateImage(token, response.relative_url, user.id)
    }

    override fun loadImageHTTPCLIENTErrorResponse(message: String) {
        SnackAlert.alert(message, v.layout_container_profile  ,"error",Snackbar.LENGTH_LONG,"")
        SnackAlert.showAlert()
    }

    override fun updateImageResponse(response: UserResponseModel) {

        model.getInfoUser(token)

        val bundle = Bundle()
        bundle.putInt("business_unit_id", user.business_unit_id)
        bundle.putInt("coins", user.coins)
        bundle.putString("name", user.name)
        bundle.putString("first_name", user.first_name)
        bundle.putString("cellphone",user.cellphone)
        bundle.putString("last_name", user.last_name)
        bundle.putString("employee_code", user.employee_code)
        bundle.putString("id", user.business_unit_id.toString())
        bundle.putBoolean("is_staff", user.is_staff)
        bundle.putInt("points", user.points)
        firebaseAnalytics.logEvent("editar_imagen_perfil", bundle)
        v.layout_loading_image.visibility = View.VISIBLE
        v.image_edit_icon.visibility = View.GONE
        image = EnviromentConstants().image_serverver + response.photo
        Picasso.get().load(image).error(R.drawable.no_image).into(ctx.activity!!.iv_user_image_header)
        Picasso.get().load(image).error(R.drawable.no_image).into(v.iv_user_image_profile, (object : Callback{
            override fun onSuccess() {
                v.layout_loading_image.visibility = View.GONE
                v.image_edit_icon.visibility = View.VISIBLE
            }

            override fun onError(e: java.lang.Exception?) {
                v.layout_loading_image.visibility = View.GONE
                v.image_edit_icon.visibility = View.VISIBLE
            }
        }))
        bottomSheet.hide()
    }
    override fun onResume() {
    }

    override fun cancelLeavingResponse(response: GetoutResponseModel) {

        user.leaving = null

        val userStr = gson.toJson(user)
        var editor = sharedPreferences.edit()
        editor.putString("_userinfo", userStr)
        editor.apply()

        ctx.btn_send_resignation.setText("Anunciar")
        ctx.lbl_date_out.text = ""
        ctx.lbl_date_out.visibility = View.GONE
    }

    override fun cancelLeavingErrorResponse(message: String) {
        SnackAlert.alert("Error al cancelar la petición de baja de VO. por favor asista a RH para continuar su petición.", v.layout_container_profile  ,"error",Snackbar.LENGTH_LONG,"")
        SnackAlert.showAlert()
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.requireActivity())
    }

    override fun getInfoUserErrorRespose(message: String) {

    }

    override fun getInfoUserResponse(response: UserResponseModel) {
        val responseStr = gson.toJson(response)
        val editor = sharedPreferences.edit()
        editor.putString("_userinfo", responseStr)
        editor.apply()

        setdata(response)
    }

    fun getpermitionCamera():Boolean{
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission( ctx.context!!, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ) {

                val permission = arrayOf(android.Manifest.permission.CAMERA)
                ctx.requestPermissions(permission, 2)
                return false
            }else{
                Log.e("response","OK")
                return true
            }
        }else{
            Log.e("response","OK 2")
            return true
        }
    }

    fun getpermitionGalery():Boolean{
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission( ctx.context!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED && ContextCompat.checkSelfPermission( ctx.context!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

                val permission = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                ctx.requestPermissions(permission, 1)
                return false
            }else{
                Log.e("response","OK")
                return true
            }
        }else{
            Log.e("response","OK 2")
            return true
        }
    }

}

