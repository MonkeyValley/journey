package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Reviews.IReviews
import com.example.jurney_mobile.Model.Reviews.ReviewsModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.ReviewsAdapter
import com.example.jurney_mobile.View.Fragments.ReviewsFragment
import com.example.jurney_mobile.View.Models.ReasonsPayrollComplainModel
import com.example.jurney_mobile.View.Models.ReviewsModelResponse
import com.example.jurney_mobile.View.Models.SupervisorsResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.SliderReviewsActivity
import com.google.gson.Gson
import io.sentry.util.Util
import kotlinx.android.synthetic.main.reviews_fragment.view.*

class ReviewPresenter: IReviews.Presenter, IClickItemDelegate.clickReviews{


    private var v: View
    private var view: IReviews.View
    private var model: IReviews.Model
    private var ctx: ReviewsFragment

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: ReviewsAdapter

    private lateinit var  loading:LoadingDialog
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()
    private lateinit var  user: UserResponseModel
    private lateinit var  token:String
    private  lateinit var arrReviews:List<SupervisorsResponseModel>

    constructor(presenter:IReviews.View, ctx: ReviewsFragment, v: View){
        this.view = presenter
        this.model = ReviewsModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initView(v: View, reload:Boolean) {

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val userStr = sharedPreferences.getString("_userinfo","")
//        val supervisorsList = sharedPreferences.getString("_listsupervisors","")

        token = sharedPreferences.getString("_tokenuser","")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

//        if(supervisorsList != "") {
//            arrReviews =
//                gson.fromJson(supervisorsList, Array<SupervisorsResponseModel>::class.java).toList()
//        }else{
//            arrReviews = ArrayList()
//        }

//        var arrayReviews = ArrayList<ReviewsModelResponse>()
//        val obj = ReviewsModelResponse("Cristian Canedo","Labores Culturales","Invernadero 1", false, "")
//        arrayReviews.add(obj)

        loading = LoadingDialog(ctx.activity!!)


        /*if(arrReviews.isEmpty()){
            v.empty_view_reviews.visibility = View.VISIBLE
            v.list_reviews_fragment.visibility = View.GONE
            // v.empty_view_payroll.layoutParams = LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT )
        }else{*/

//        }

        loading.startLoading()
        model.getSupervisors(token)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 10) {
            val result = data?.getIntExtra("id_review", 0)
            if(result != null) {
                arrReviews.forEachIndexed { index, it ->
                    if (it.supervisor_review_id == result) {

                        /*arrReviews.get(index).supervisor_review_status = true
                        adaptador.list.get(index).supervisor_review_status = true
                        adaptador.notifyItemChanged(index)*/
                        arrReviews.get(index).supervisor_review_status = true

                        list = v.findViewById(R.id.list_reviews_fragment)
                        layoutManager = LinearLayoutManager(ctx.context)
                        adaptador = ReviewsAdapter(arrReviews, this)
                        list.layoutManager = layoutManager
                        list.adapter = adaptador
                    }
                }

                Log.e("ARR ->>",gson.toJson(arrReviews))
            }
        }

    }

    override fun getSupervisorsResponse(response: ArrayList<SupervisorsResponseModel>) {

        loading.dismissDialog()
        if(response.isNotEmpty()) {
            Log.v("RES SUPERVISORS", gson.toJson(response))
            val delete = sharedPreferences.edit()
            delete.remove("_listsupervisors")

            val editor = sharedPreferences.edit()
            editor.putString("_listsupervisors", gson.toJson(response))
            editor.apply()

            arrReviews = response

            list = v.findViewById(R.id.list_reviews_fragment)
            layoutManager = LinearLayoutManager(ctx.context)
            adaptador = ReviewsAdapter(arrReviews, this)
            list.layoutManager = layoutManager
            list.adapter = adaptador

            if(arrReviews.isNotEmpty()){
                v.lbl_number_week.visibility = View.VISIBLE
                v.lbl_number_week.text = "Semana: " + arrReviews.last().week_no.toString()
            }else{
                v.lbl_number_week.visibility = View.GONE
            }

            loading.dismissDialog()
        }
    }

    override fun getSupervisorsErrorResponse(message: String) {
        Log.e("ERROR --->> ", message)
        loading.dismissDialog()
    }

    override fun unauthorizedError() {
        loading.dismissDialog()
        utilsClass().logOut(ctx.activity!!)
    }

    override fun click(item: SupervisorsResponseModel) {
        val intent = Intent(ctx.context!!, SliderReviewsActivity::class.java)
        intent.putExtra("review_id",item.review_id)
        intent.putExtra("supervisor_review_id", item.supervisor_review_id )
        ctx.startActivityForResult(intent, 10)
    }


}