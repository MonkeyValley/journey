package com.example.jurney_mobile.Presenter

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.startActivity
import com.example.jurney_mobile.Model.UserValidate.IUserValidate
import com.example.jurney_mobile.Model.UserValidate.UserValidateModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.*
import com.example.jurney_mobile.View.*
import com.example.jurney_mobile.View.Models.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.uxcam.UXCam
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_user_validate.*
import kotlinx.android.synthetic.main.complaint_list_fragment.view.*
import kotlinx.android.synthetic.main.vivacoin_header_component.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class UserValidatePresenter : IUserValidate.Presenter, View.OnClickListener{

    var view: IUserValidate.View;
    var model: IUserValidate.Model;
    private lateinit var sharedPreferences: SharedPreferences
    lateinit var textFocus:EditText
    var ctx:UserValidate
    val gson = Gson()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var flat = false
    lateinit var phoneUser:String
    lateinit var timer:CountDownTimer
    private var phone:String = "+526671997860"
    private var first:String = "Hola, no me llega el código PIN, ¿Me ayuda por favor?"
    private var second:String = "Hola. me llego el código PIN pero no es correcto, ¿Me ayuda por favor?"
    private var thirt:String = "Hola, no puedo acceder a Journey con mi numero de celular. ¿Me ayuda por favor?"
    private var isGenerated:Boolean = false
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)
    var url_privacy = "http://vivaorganica.com.mx/aviso-de-privacidad.html"
    lateinit var dateGetCode:String

    constructor(view: IUserValidate.View, ctx:UserValidate ){
        this.view = view
        this.model = UserValidateModel(this, ctx)
        this.ctx = ctx
    }

    private lateinit var loading: LoadingDialog

    override fun initInfo() {
        UXCam.startWithKey("a07bh4va72fmgjb");
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        loading = LoadingDialog(ctx)
        ctx.btn_help_validate.setOnClickListener(this)
        ctx.btngetpasswordd.setOnClickListener(this)
        timerInit()
        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx)

        isGenerated = ctx.intent.getBooleanExtra("isGenerated", false)
        val lblTitle = ctx.findViewById<TextView>(R.id.lbl_uno)
        val lblBody = ctx.findViewById<TextView>(R.id.lbl_dos)

        phoneUser = sharedPreferences.getString("_phoneuser","")!!

        dateGetCode = sharedPreferences.getString("_lastDate","")!!
        val date = utilsClass().toSimpleString(Date())

        if( date  != dateGetCode) ctx.btngetpasswordd.setTextColor( ctx.resources.getColor( R.color.dodger_blue))
        else ctx.btngetpasswordd.setTextColor( ctx.resources.getColor( R.color.charcoal))


        getInfoWhatsapp()

        if(isGenerated){
            lblTitle.text = "Ingresa tu contraseña"
            lblBody.text = "Ingresa tu contraseña de VivaJourney. Si no te acuerdas de ella puedes pedir apoyo usando el botón de ayuda."
        }else{
            lblTitle.text = "Ingresa la contraseña"
            lblBody.text = "Se te enviará por SMS un PIN para verificar tu número de telefono, espera unos minutos."
        }
    }

    override fun onBackPressed() {
        val intent = Intent( ctx  , LoginActivity::class.java)
        ctx.startActivity(intent)
        timer.cancel()
        ctx.finish()
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnValidateCode -> {loading.startLoading(); onValidate(ctx.txtOne.text.toString(), ctx.txtTwo.text.toString(), ctx.txtThree.text.toString(), ctx.txtFour.text.toString())}
            R.id.btn_help_validate ->  sendCode()
            R.id.btngetpasswordd -> sendNewCode()
        }
    }

    fun timerInit(){
        Log.e("","CLICK")

        timer = object: CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.e("Timer",millisUntilFinished.toString())
                val milliToSec = millisUntilFinished/1000
                val convertToInt = milliToSec.toInt()
            }
            override fun onFinish() {
                ctx.btn_help_validate.background = ctx.resources.getDrawable(R.drawable.corner_radius_sun_yellow)
                flat = true
            }
        }
        timer.start()
    }

    override fun onValidate(data1:String, data2:String, data3:String, data4:String) {
        if (model != null) {
            model.onValidate(data1, data2, data3, data4, phoneUser)
        }
    }

    fun getQualityScore( user:UserResponseModel, token:String) {
        val currentDate = Date()
        val simpleDateformat = SimpleDateFormat("EEEE");

        val calTo = Calendar.getInstance()
        calTo.add(Calendar.DATE, -2)

        val calFrom = Calendar.getInstance()
        calFrom.add(Calendar.DATE, -8)

        val dateTo: Date = calTo.getTime()
        val sdfTo = SimpleDateFormat("yyyy-MM-dd")

        val dateFrom: Date = calFrom.getTime()
        val sdfFrom = SimpleDateFormat("yyyy-MM-dd")


        val currentDay = simpleDateformat.format(currentDate)
        val date_from = sdfFrom.format(dateFrom)
        val date_to =sdfTo.format(dateTo)
        val id_user = user.id
        val business_unit_id = user.business_unit_id

        val isView = sharedPreferences.getString("_lastUpdate", "")

        if(currentDay == "Friday" || currentDay == "viernes"){
            //if(isView == "" || currentDateCalendar != isView){
            model.getQualityScore(token, id_user.toString(), business_unit_id.toString() ,date_to,date_from)
            //}
        }

        Log.e("->>>> current ", currentDay)
        Log.e("->>>> datTo ", date_to)
        Log.e("->>>> dateFrom ", date_from)
    }

    override fun onKeyPressed(v: View, keyCode: Int, event: KeyEvent):Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            val id = v.id

            if (keyCode === KeyEvent.KEYCODE_DEL) {
                when(id){
                    R.id.txtOne -> return false
                    R.id.txtTwo -> {
                        if(ctx.txtTwo.text.toString().equals("")){
                            ctx.txtOne.setText("")
                            setFocus(ctx.txtOne)
                        }else{
                            return false
                        }
                    }
                    R.id.txtThree -> {
                        if(ctx.txtThree.text.toString().equals("")){
                            ctx.txtTwo.setText("")
                            setFocus(ctx.txtTwo)
                        }else{
                            return false
                        }
                    }
                    R.id.txtFour -> {
                        if(ctx.txtFour.text.toString().equals("")){
                            ctx.txtThree.setText("")
                            setFocus(ctx.txtThree)
                        }else{
                            return false
                        }
                    }
                    else -> return true
                }
            }
        }
        return false
    }

    override fun afterTextChanged() {
        when(textFocus){
            ctx.txtOne -> setFocus(ctx.txtTwo)
            ctx.txtTwo ->  setFocus(ctx.txtThree)
            ctx.txtThree -> setFocus(ctx.txtFour)
            ctx.txtFour -> {
                val imm: InputMethodManager = ctx.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(ctx.txtFour.windowToken, 0)
                ctx.btnValidateCode.isEnabled = true
                ctx.btnValidateCode.setBackgroundResource(R.drawable.layer_bg_yellow_button)
            }
        }
    }

    override fun initTextFocus(editText: EditText) {
        textFocus = editText
    }

    fun setFocus(editText: EditText?) {
        if (editText == null) return
            ctx.btnValidateCode.isEnabled = false
            ctx.btnValidateCode.setBackgroundResource(R.drawable.layer_bg_disable_button)
            textFocus = editText
            editText.isFocusable = true
            editText.isFocusableInTouchMode = true
            editText.requestFocus()
        }

    fun sendNewCode() {
        loading.startLoading()
        val date = utilsClass().toSimpleString(Date())
        if (date != dateGetCode) model.getNewCode( phoneUser )
        else {
            loading.dismissDialog()
            SnackAlert.alert(
                "Solo puedes solicitar tu codigo una vez por día.",
                ctx.contentValidate,
                "error",
                Snackbar.LENGTH_LONG,
                ""
            )
         SnackAlert.showAlert()
        }
    }
    fun sendCode(){
        if(flat) {
            Log.e("Click ", "Click")
            loading.alertHelpPIN(callingHelp, smsWhatsappHelp)
        }
    }

    val callingHelp = (object : View.OnClickListener {
        override fun onClick(v: View?) {
            calling()
        }
    })

    fun calling(){
        loading.dismissDialog()
        if(phone != "") {
            val phoneIntent = Intent(Intent.ACTION_CALL)
            phoneIntent.setData(Uri.parse("tel:" + phone))

            if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                fn_permission()
            } else {
                ctx.startActivity(phoneIntent)
            }
        }else{
            SnackAlert.alert("Número no valido", ctx.contentValidate ,"",Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()
        }
    }

    fun fn_permission(){
        ActivityCompat.requestPermissions(ctx,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            100)
    }

    val smsWhatsappHelp = View.OnClickListener {
        loading.dismissDialog()
        if(isGenerated){
            sendMessaggetowhatsapp("No recuerdo mi contraseña, ¿me pueden ayudar por favor?")
        }else{
            sendMessaggetowhatsapp("No me llega mi contraseña, ¿me pueden ayudar por favor?")
        }
    }

    fun getInfoWhatsapp(){
        myRef.child("data_base").child("first_time_active").child("config_messagges_whatsapp").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){
                    val value = dataSnapshot.getValue(configMessaggesWhatsappModel::class.java)
                    Log.e("", value.toString())
                    phone = value!!.phone
                    first = value.first
                    second = value.second
                    thirt = value.thirt
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    fun sendMessaggetowhatsapp(msg:String){
        if( utilsClass().isAppIntalled("com.whatsapp", ctx.applicationContext )){
            val intentWhats = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone="+phone+"&text="+msg))
            ctx.startActivity(intentWhats)
        }else{
            SnackAlert.alert("No tines la aplicacion de Whatsapp instalada.", ctx.contentValidate,"error", Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()
        }
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    // ---------------------SEND REQUEST SERVICE-----------

    override fun onValidateResponse(response: String) {
        if(view != null){
            val editor = sharedPreferences.edit()
            editor.putString("_tokenuser", response )
            editor.apply()
            model.getInfoUser(response)
        }
    }


    override fun getInfoUserResponse(response: UserResponseModel) {
        val editor = sharedPreferences.edit()
        editor.putString("_userinfo", gson.toJson(response) )
        editor.putString("_image", response.photo)
        editor.apply()

        val bundle = Bundle()
        bundle.putInt("business_unit_id", response.business_unit_id)
        bundle.putInt("coins", response.coins)
        bundle.putString("name", response.name)
        bundle.putString("first_name", response.first_name)
        bundle.putString("cellphone",response.cellphone)
        bundle.putString("last_name", response.last_name)
        bundle.putString("employee_code", response.employee_code)
        bundle.putString("id", response.business_unit_id.toString())
        bundle.putBoolean("is_staff", response.is_staff)
        bundle.putInt("points", response.points)
        firebaseAnalytics.logEvent("inicio_sesion", bundle)
        val token = sharedPreferences.getString("_tokenuser", "")

        model.getWeekendPayment(token!!, response.employee_code, response.business_unit_id.toString())
        model.getInfoTwoLastWeeks(token, response.employee_code, response.business_unit_id.toString())
        getQualityScore(response, token)
        model.getVacancies(token, response.id)
        model.getMyRanking(token)
        model.getTopTen(token)
        model.getListCodes(token)
        model.getListAwards(token)
        model.getListChallenges(token, response.id)
        model.getReasonsPayrollComplain(token)
        model.getTotalIncome(token, response.employee_code, response.business_unit_id.toString())
        model.getSupervisors(token)
    }
    override fun getInfoTwoLastWeeksResponse(response: TwoLastWeekendModel) {
        val responseStr = gson.toJson(response)
        val editor = sharedPreferences.edit()
        editor.putString("_nominainfo", responseStr)
        editor.apply()
    }
    override fun getInfoResponse(response: PaymentModel) {
        val responseStr = gson.toJson(response)
        val editor = sharedPreferences.edit()
        editor.putString("_nominaAvailable", responseStr)
        editor.apply()
    }
    override fun getTotalIncomeResponse(response: TotalIncomeModel) {
        val editor = sharedPreferences.edit()
        editor.putString("_totalIncome", gson.toJson(response) )
        editor.apply()

        var intent = Intent( ctx  , SliderActivity::class.java)
        if(!isGenerated){
            intent = Intent( ctx  , CreatePasswordActivity::class.java)
            intent.putExtra("screenName", "UserValidatePresenter")
        }


        ctx.startActivity(intent)
        timer.cancel()
        ctx.finish()
    }
    override fun getQualityScoreResponse(response: QualityScoreResponse) {
        Log.v("RESPONSE QUALITY SCORE", gson.toJson(response))

        val currentDate = Date()
        val simpleDateformat = SimpleDateFormat("yyyy-MM-dd");
        val currentDay = simpleDateformat.format(currentDate)

        val editor = sharedPreferences.edit()
        editor.putString("_qualityScore", gson.toJson(response))
        editor.putString("_lastUpdate", currentDay)
        editor.apply()

    }
    override fun getListChallengesResponse(response:ChallengesResponseModel) {
        Log.v("RESPONSE CHALLENGES", gson.toJson(response))

        val editor = sharedPreferences.edit()
        editor.putString("_listchallenges", gson.toJson(response))
        editor.apply()
    }
    override fun onResendCodeResponse(response: SmsResponseModel) {
        SnackAlert.alert("Mensaje enviado", ctx.findViewById(R.id.contentValidate),"success", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }

    override fun getVacanciesResponse(response: ArrayList<VacancyResponseModel>) {
        Log.v("RESPONSE VACANCIES", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listvacancies")

        val editor = sharedPreferences.edit()
        editor.putString("_listvacancies", gson.toJson(response))
        editor.apply()
    }

    override fun getReasonsPayrollComplainResponse(response: ArrayList<ReasonsPayrollComplainModel>) {
        Log.v("RES REASONSCOMPLAIN", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listreasonscomplain")

        val editor = sharedPreferences.edit()
        editor.putString("_listreasonscomplain", gson.toJson(response))
        editor.apply()
    }

    override fun getListAwardsResponse(response: ArrayList<AwardsResponseModel>) {
        Log.v("RESPONSE AWARDS", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listawards")

        val editor = sharedPreferences.edit()
        editor.putString("_listawards", gson.toJson(response))
        editor.apply()
    }

    override fun getTopTenResponse(response: ArrayList<UserRankingResponseModel>) {
        Log.v("RESPONSE TOPTEN", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listtopten")

        val editor = sharedPreferences.edit()
        editor.putString("_listtopten", gson.toJson(response))
        editor.apply()
    }

    override fun getMyRankingResponse(response: ArrayList<UserRankingResponseModel>) {
        val delete = sharedPreferences.edit()
        delete.remove("_listmyranking")

        val editor = sharedPreferences.edit()
        editor.putString("_listmyranking", gson.toJson(response))
        editor.apply()
    }

    override fun getListCodesResponse(response: ArrayList<CodeResponseModel>) {
        Log.v("RESPONSE CODES", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listcodes")

        val editor = sharedPreferences.edit()
        editor.putString("_listcodes", gson.toJson(response))
        editor.apply()
    }
    override fun getDailyPayListResponse(response: DailypayResponseModel) {
        Log.v("RES DailyPay", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listdailypay")

        val editor = sharedPreferences.edit()
        editor.putString("_listdailypay", gson.toJson(response))
        editor.apply()
    }

    override fun getNewCodeResponse(response: newSmsResponseModel) {
        if(response.success){

            val delete = sharedPreferences.edit()
            delete.remove("_lastDate")
            delete.apply()
            delete.commit()

            var date = utilsClass().toSimpleString(Date())
            val editor = sharedPreferences.edit()
            editor.putString("_lastDate", date)
            editor.apply()

            ctx.btngetpasswordd.setTextColor(ctx.resources.getColor(R.color.charcoal))
            dateGetCode = date

            SnackAlert.alert("Código enviado, Encaso de no recibir tu contraseña, háblanos por whatsapp", ctx.contentValidate,"success",Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()

        }else{
            SnackAlert.alert("Error al enviar código, intente nuevamente", ctx.contentValidate,"error",Snackbar.LENGTH_LONG,"")
            SnackAlert.showAlert()
        }

        loading.dismissDialog()
    }
    override fun getSupervisorsResponse(response: ArrayList<SupervisorsResponseModel>) {
        Log.v("RES SUPERVISORS", gson.toJson(response))

        val delete = sharedPreferences.edit()
        delete.remove("_listsupervisors")

        val editor = sharedPreferences.edit()
        editor.putString("_listsupervisors", gson.toJson(response))
        editor.apply()
    }

// ---------EROOR SEND REQUEST SERVICE-----------

    override fun getQualityScoreErrorResponse(message: String) { Log.e("ERRORRR->" , message)}
    override fun getListChallengesErrorResponse(message:String) { Log.e("ERROR ->", message) }
    override fun getInfoErrorResponse(message: String) {Log.e("ERRORRR->" , message)}
    override fun getInfoUserErrorRespose(message: String) {Log.e("ERRORRR->" , message)}
    override fun getInfoTwoLastWeeksErrorResponse(message: String) {Log.e("ERRORRR->" , message)}
    override fun getTotalIncomeErrorResponse(message: String) {Log.e("ERRORRR->" , message)}
    override fun getVacanciesErrorResponse(message: String){Log.v("ERROR ->", message)}
    override fun getListCodesErrorResponse(message: String) {Log.v("ERROR ->", message)}
    override fun getReasonsPayrollComplainErrorResponse(message: String) {Log.v("ERROR ->", message)}
    override fun getTopTenErrorResponse(message: String) {Log.v("ERROR ->", message)}
    override fun getMyRankingErrorResponse(message: String) { Log.v("ERROR ->", message) }
    override fun getListAwardsErrorResponse(message: String){Log.v("ERROR ->", message)}
    override fun onResendCodeErrorResponse(message: String) {
        loading.dismissDialog()
        SnackAlert.alert(message, ctx.findViewById(R.id.contentValidate),"error", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }
    override fun getSupervisorsErrorResponse(message: String) {Log.v("ERROR ->", message)}

    override fun onValidateError(msj: String) {
        loading.dismissDialog()
        SnackAlert.alert(msj, ctx.findViewById(R.id.contentValidate),"error", Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }
    override fun getDailyPayListErrorResponse(response: String, errorCode:String) {
        Log.v("E. getDailyPayList ->", response)

        val delete = sharedPreferences.edit()
        delete.remove("_listdailypay")
        delete.apply()
        delete.commit()

        val editor = sharedPreferences.edit()
        editor.putString("_listdailypay", errorCode)
        editor.apply()
    }
    override fun getNewCodeErrorResponse(message: String) {
        loading.dismissDialog()
        SnackAlert.alert("Error al enviar código, intente nuevamente", ctx.contentValidate,"error",Snackbar.LENGTH_LONG,"")
        SnackAlert.showAlert()
    }


}
