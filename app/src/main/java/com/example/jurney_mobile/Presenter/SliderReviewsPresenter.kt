package com.example.jurney_mobile.Presenter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import androidx.core.graphics.drawable.toBitmap
import androidx.viewpager.widget.ViewPager
import com.example.jurney_mobile.Model.Reviews.IReviews
import com.example.jurney_mobile.Model.Reviews.SliderReviewModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.ReviewsSliderAdapter
import com.example.jurney_mobile.View.Adapters.selectedNotificationDelegate
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.SliderReviewsActivity
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_reviews.*
import kotlinx.android.synthetic.main.toolbar_component.*
import java.util.ArrayList

class SliderReviewsPresenter(view:IReviews.SliderView, ctx: SliderReviewsActivity):IReviews.SliderPresenter, IClickItemDelegate.clickItemReviewFragment, IClickItemDelegate.afterTextChangedDelegate, ViewPager.OnPageChangeListener, View.OnClickListener {

    private var model:IReviews.SliderModel
    private var vista:IReviews.SliderView
    private var ctx: SliderReviewsActivity

    private var gson = Gson()
    lateinit var  alert:LoadingDialog
    private lateinit var loading: LoadingDialog
    private var position = 0
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var token:String
    private lateinit var user:UserResponseModel
    private lateinit var review:ReviewModelResponse
    private var supervisor_review_id  = -1
    var commentReq=""
    private var arrayQuestions = ArrayList<TopictsModel>()
    private var arrayRequest = ArrayList<TopicsRequestModel>()
    lateinit var shareDialog : ShareDialog

    init {
        this.model = SliderReviewModel(this)
        this.vista = view
        this.ctx = ctx
    }

    override fun initView() {

        val idReview = ctx.intent.getIntExtra("review_id", 0)
        supervisor_review_id = ctx.intent.getIntExtra("supervisor_review_id", -1)
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        loading = LoadingDialog(ctx)
        alert = LoadingDialog(ctx)
        shareDialog = ShareDialog(ctx)

        token = sharedPreferences.getString("_tokenuser","")!!
        val responseStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )
        ctx.btn_back.setOnClickListener(this)
        ctx.btn_next_question_reviews.setOnClickListener(this)
        ctx.btn_previus_question_reviews.setOnClickListener(this)
        model.getReviewById(token,idReview)
    }

    override fun getReviewByIdResponse(response: ReviewModelResponse) {

        arrayQuestions = response.topics
        review = response
        arrayQuestions.add(TopictsModel("",-1,"",-1))

        ctx.toolbar_title.setText(response.name)
        if(arrayQuestions.count() > 0) {
            ctx.progress_bar_questions_reviews.progress = 100 / arrayQuestions.count()
            ctx.lbl_steps_for_questions_reviews.text = "1 de " + arrayQuestions.count() + " pasos"
            if(arrayQuestions.count()  == 1){
                ctx.btn_next_question_reviews.setImageResource(R.drawable.ic_check)
            }
        }

        var adapter  = ReviewsSliderAdapter(ctx.supportFragmentManager,  arrayQuestions, this, this)
        ctx.container_slider_reviews.adapter = adapter
        ctx.container_slider_reviews.addOnPageChangeListener(this)

    }

    override fun getReviewByIdErrorResponse(message: String) {
        Log.e("Error ->>",message)
    }

    override fun sendReviewResponse(response: SendReviewsResponseModel) {
        loading.dismissDialog()

        if(review.coins > 0) {
            alert.alertNot(
                "Gracias por ayudarnos a conocer tu opinión. Sigue contestando más revisiones y gana vivacoins. Esta vez acabas de ganar:",
                "¡Has terminado la Revisión!", "Continuar",
                R.drawable.ic_reviews_share,
                this,
                this,
                false,
                true,
                "+" + review.coins
            )
        }else{
            alert.alertNot(
                "Gracias por ayudarnos a conocer tu opinión. Sigue apoyandonos contestando más revisiones.",
                "¡Has terminado la Revisión!", "Continuar",
                R.drawable.ic_reviews_share,
                this,
                this,
                false,
                false,
                ""
            )
        }
    }

    override fun sendReviewErrorResponse(message: String) {
        loading.dismissDialog()
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun click(item: Int, selected: Int) {
        val res = ifExist(item, arrayRequest)
        if(res >= 0){
            arrayRequest.get(res).response = selected
        }else{
            val itemReq = TopicsRequestModel(item, selected)
            arrayRequest.add(itemReq)
        }
        Log.e("---","TERMINO")
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        this.position = position
        ctx.progress_bar_questions_reviews.progress = (((position+1)*100) / arrayQuestions.count())
        ctx.lbl_steps_for_questions_reviews.text = "" + (position+1)+" de "+ arrayQuestions.count() + " pasos"

        if(this.position == arrayQuestions.count() - 1){
            ctx.btn_next_question_reviews.setImageResource(R.drawable.ic_check)
        }else{
            ctx.btn_next_question_reviews.setImageResource(R.drawable.ic_arrow_right)
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_previus_question_reviews -> {

                if(this.position - 1 == 0 ){
                    ctx.btn_previus_question_reviews.visibility = View.GONE
                }
                ctx.container_slider_reviews.currentItem--
            }
            R.id.btn_next_question_reviews->{

                if(position < arrayQuestions.size - 1) {
                    if (arrayRequest.size > this.position) {
                        if (this.position + 1 > 0 && this.arrayQuestions.count() > 1) {
                            ctx.btn_previus_question_reviews.visibility = View.VISIBLE
                        }

                        ctx.container_slider_reviews.currentItem += 1
                    } else {

                        SnackAlert.alert(
                            "Debe seleccionar una opción",
                            ctx.container_reviews,
                            "alert",
                            Snackbar.LENGTH_LONG,
                            "top"
                        )
                        SnackAlert.showAlert()
                    }
                }else{

                    var coment = commentReq.trimEnd().replace("\n", "")

                    var wordCounter = coment.split(" ")
                    if(wordCounter.size >= 5) {
                        val req = ReviewRequestModel(this.supervisor_review_id, coment, arrayRequest)
                        Log.e("END ------->", gson.toJson(req))
                        loading.startLoading()
                        model.sendReview(token, req)

                    }else{
                        SnackAlert.alert(
                            "Déjanos un comentario, como mínimo 5 palabras.",
                            ctx.container_reviews,
                            "alert",
                            Snackbar.LENGTH_LONG,
                            "top"
                        )
                        SnackAlert.showAlert()
                    }
                }
            }
            R.id.btn_back -> ctx.finish()
            R.id.btnActionAlert -> {
                alert.dismissDialog()

                val editor = sharedPreferences.edit()
                user.coins = user.coins + review.coins
                editor.putString("_userinfo", gson.toJson(user) )
                editor.apply()

                val intent = Intent()
                intent.putExtra("id_review", supervisor_review_id)
                ctx.setResult(Activity.RESULT_OK, intent)
                ctx.finish()
            }
            R.id.layer_facebook_share -> shareOnFacebook(ctx.resources.getDrawable(R.drawable.ic_surveys_green).toBitmap(), "Gracias por ayudarnos a conocer tu opinión. Sigue contestando más encuestas y gana vivacoins.")

        }
    }

    fun shareOnFacebook(bitmap: Bitmap, msj:String){
        if (ShareDialog.canShow(ShareLinkContent::class.java)) {

            val photo = SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("#VivaSmarth")
                .setUserGenerated(true)
                .build()
            val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()

            shareDialog.show(content)
        }
    }

    fun ifExist(id:Int, array:ArrayList<TopicsRequestModel>): Int{

        var response = -1
        array.forEachIndexed{ index, it ->
            if(id == it.id){
                response = index
            }
        }
        return  response
    }

    override fun afterTextChanged(charSecuense: String) {
        commentReq = charSecuense
    }
}
