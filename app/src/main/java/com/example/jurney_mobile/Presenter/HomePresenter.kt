package com.example.jurney_mobile.Presenter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.core.text.bold
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.jurney_mobile.Model.Home.HomeModel
import com.example.jurney_mobile.Model.Home.IHome
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Fragments.*
import com.example.jurney_mobile.View.Home
import com.example.jurney_mobile.View.LoginActivity
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.example.jurney_mobile.View.SurveyActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.uxcam.UXCam
import io.sentry.Sentry
import io.sentry.android.AndroidSentryClientFactory
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.bottomsheet_menu_layout.*
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.profile_component.*
import kotlinx.android.synthetic.main.single_vivacoint_component.*
import kotlinx.android.synthetic.main.vivacoin_header_component.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class HomePresenter: IHome.Presenter {

    private var view:IHome.View
    private var model:IHome.Model
    private var ctx:Home
    private lateinit var token:String
    private var fragmentActiveString = ""
    private lateinit var manager:FragmentManager
    private var image = ""
    private lateinit var viewBottomSheet:BottomSheetDialog
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()
    private lateinit var user:UserResponseModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    var isShow = true
    var isShowLevelUp = true
    var isShowConfirmNot = true
    var isShowLevelUpRanking = true
    var isShowReview = true
    var isShowFirsTime = true
    var isShowInfo = true
    var isShowExchangeCode = true
    var isShowVacancies = true
    var isInFistTimeActive = true
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    lateinit var msjPicasso:String
    lateinit var callBack: CallbackManager
    lateinit var shareDialog : ShareDialog

    constructor(view: IHome.View , ctx: Home){
        this.view = view
        this.model = HomeModel(this, ctx)
        this.ctx = ctx
    }

    override fun onResume() {

        val responseStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )

        if(user.status){
            viewBottomSheet.opc_complaints_menu.visibility = View.VISIBLE
            viewBottomSheet.opc_ranking_menu.visibility = View.VISIBLE
            viewBottomSheet.opc_quality_menu.visibility = View.VISIBLE
            viewBottomSheet.opc_quests_menu.visibility = View.VISIBLE
        }else{
            viewBottomSheet.opc_complaints_menu.visibility = View.GONE
            viewBottomSheet.opc_ranking_menu.visibility = View.GONE
            viewBottomSheet.opc_quality_menu.visibility = View.GONE
            viewBottomSheet.opc_quests_menu.visibility = View.GONE
            viewBottomSheet.opc_reviews_menu.visibility = View.GONE
        }

        ctx.bottomSheet.lbl_single_vivacoin_number.text = utilsClass().milesFormat(user.coins.toDouble())
        ctx.lbl_single_vivacoin_number.text = utilsClass().milesFormat(user.coins.toDouble())
        ctx.lbl_number_vivacoints_header_vivacoint.text = utilsClass().milesFormat(user.coins.toDouble())

        ctx.lbl_number_level_header_vivacoint.text = user.current_lvl.toString()
    }

    fun isReviewsActive(): Boolean{

        var response = false
        val supervisorsList = sharedPreferences.getString("_listsupervisors","")
        if(supervisorsList != "") {
            val arrReviews =
                gson.fromJson(supervisorsList, Array<SupervisorsResponseModel>::class.java).toList()
            var counter = 0

            arrReviews.forEachIndexed { index, it ->
                if (!it.supervisor_review_status) {
                    counter++
                }
            }

            if (counter > 0) {
                val alertNewVersion = LoadingDialog(ctx)
                alertNewVersion.alertNotMultiple(
                    "Ya está disponible la evaluación da tu Supervisor, ¿deseas evaluarlo?",
                    "¡Nueva Evaluación!",
                    "Ir",
                    "Cancelar",
                    R.drawable.img_survey_notifitation,
                    View.OnClickListener {
                        alertNewVersion.dismissDialog()

                        this@HomePresenter.showFragment(
                            "reviews",
                            "vivacoins",
                            ReviewsFragment(true),
                            false,
                            "10000"
                        )

//                            myRef.child("data_base").child(user.id.toString()).child("review").child("notificationAction").child("view").setValue(true)
                    },
                    View.OnClickListener {
                        alertNewVersion.dismissDialog()

                    },
                    false,
                    "isCancelable"
                )
                alertNewVersion.showAlertDialogMultiple()
            }
        }
        return response
    }

    override fun onClickListener(v: View) {
        when (v.id) {
            R.id.opc_init_menu -> { showFragment("home", "profile", HomeFragment(), false, "uno") }
            R.id.opc_payroll_menu -> { showFragment("nomina", "vivacoins", PayrollFragment(),false, "dos") }
            R.id.opc_quality_menu -> { showFragment("calidad", "vivacoins", QualityWeekenFragment(),false, "tres") }
            R.id.opc_challenges_menu -> { showFragment("retos", "vivacoins", ChallengesFragment(),false, "cuatro") }
            R.id.opc_quests_menu -> { showFragment("encuestas", "vivacoins", SurveysFragment(),false, "cinco") }
            R.id.opc_awards_menu -> { showFragment("premios", "vivacoins", AwardsFragment(),false, "seis") }
            R.id.opc_ranking_menu -> { showFragment("ranking", "vivacoins", RankingFragment(),false, "siente") }
            R.id.opc_vacancies_menu -> { showFragment("vacante", "vivacoins", VacainciesFragment(),false, "ocho") }
            R.id.opc_notification_menu -> { showFragment("notificaciones", "vivacoins", NotificationsFragment(),false, "doce") }
            R.id.opc_reviews_menu -> { showFragment("reviews", "vivacoins", ReviewsFragment(true),false, "trece") }
            R.id.btn_goto_profile -> {
                showFragment("perfil", "vivacoins", ProfileFragment(),false, "nueve");
                val bundle = Bundle()
                bundle.putInt("business_unit_id", user.business_unit_id)
                bundle.putInt("coins", user.coins)
                bundle.putString("name", user.name)
                bundle.putString("first_name", user.first_name)
                bundle.putString("cellphone",user.cellphone)
                bundle.putString("last_name", user.last_name)
                bundle.putString("employee_code", user.employee_code)
                bundle.putString("id", user.business_unit_id.toString())
                bundle.putBoolean("is_staff", user.is_staff)
                bundle.putInt("points", user.points)
                firebaseAnalytics.logEvent("mostrar_pantalla_perfil", bundle)
            }
            R.id.opc_complaints_menu -> { showFragment("denuncias", "vivacoins", ComplaintsFragment(),false, "diez") }
            R.id.opc_logout_menu -> utilsClass().logOut(ctx)
            R.id.btn_menu -> {
                view.showBottomSheet()

                val responseStr = sharedPreferences.getString("_userinfo", "")
                val response = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )
                var imagelate = sharedPreferences.getString("_image", "")
                if(imagelate != response.photo){
                    setImage()
                    val editor = sharedPreferences.edit()
                    editor.putString("_image", response.photo)
                    editor.apply()
                }
            }
            R.id.header_profile -> {
                val bundle = Bundle()
                bundle.putInt("business_unit_id", user.business_unit_id)
                bundle.putInt("coins", user.coins)
                bundle.putString("name", user.name)
                bundle.putString("first_name", user.first_name)
                bundle.putString("cellphone",user.cellphone)
                bundle.putString("last_name", user.last_name)
                bundle.putString("employee_code", user.employee_code)
                bundle.putString("id", user.business_unit_id.toString())
                bundle.putBoolean("is_staff", user.is_staff)
                bundle.putInt("points", user.points)
                firebaseAnalytics.logEvent("mostrar_pantalla_perfil", bundle)
                showFragment("perfil", "vivacoins", ProfileFragment(),false, "once")
            }
        }
    }

    override fun initInfo(viewBottomSheet: BottomSheetDialog) {

        FacebookSdk.sdkInitialize(ctx.applicationContext)
        callBack = CallbackManager.Factory.create()
        shareDialog = ShareDialog(ctx)
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        val url = RetrofitClient.BASE_URL
        val userStr =  sharedPreferences.getString("_userinfo", "")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)


        if(url == EnviromentConstants().prod_server ){
            UXCam.startWithKey(EnviromentConstants().sentry_apk_id);
            Sentry.init(EnviromentConstants().sentry_server , AndroidSentryClientFactory(ctx) )
            val topic = "bu"+user.business_unit_id
            FirebaseMessaging.getInstance().subscribeToTopic(topic)
            FirebaseMessaging.getInstance().unsubscribeFromTopic("dev-bu"+user.business_unit_id)

        }else{
            val topic = "dev-bu"+user.business_unit_id
            FirebaseMessaging.getInstance().unsubscribeFromTopic("bu"+user.business_unit_id)
            FirebaseMessaging.getInstance().subscribeToTopic(topic)
        }

        this.viewBottomSheet = viewBottomSheet
        manager = ctx.supportFragmentManager
        token = sharedPreferences.getString("_tokenuser","")!!

        firebaseAnalytics = FirebaseAnalytics.getInstance(ctx)

        Log.e("photo", image)
        ctx.progressBarMenuProfile.progress = ((user.points * 100) / (user.points + user.to_next_lvl))
        ctx.lbl_user_name_header.text =  user.name
        ctx.lbl_user_level_header.text = "Nivel " + user.current_lvl


        ctx.bottomSheet.progressBarMenu.progress = ((user.points * 100) / (user.points + user.to_next_lvl))
        ctx.bottomSheet.lbl_name_user_menu_bar.text = user.name + " " + user.first_name
        ctx.bottomSheet.lbl_level_user_menu_bar.text = "Nivel " + user.current_lvl


        this.setImage()


        shareDialog.registerCallback(callBack, object : FacebookCallback<Sharer.Result> {
            override fun onSuccess(result: Sharer.Result?) {
                Log.e("Response","OK")
            }

            override fun onCancel() {
                Log.e("Response","Cancel")
            }

            override fun onError(error: FacebookException?) {
                Log.e("Response","NEL")
                Log.e("ERROR",error!!.message)
            }

        })

        onGetPhoneFirebase()
        actionNotificationChallenges()
        actionNotificationQuality()
        actionNotificationLevelUp()
        actionNotificationFirstTimeActive()
        actionNotificationManteiner()
        actionNotificationInfo()
        actionNotificationLevelUpRanking()
        actionNotificationExchangeCode()
        actionNotificationNewVersion()
        actionNotificationConfirmNot()
//        actionNotificationNewReview()
        initFCM()

        val isFirsttime = sharedPreferences.getBoolean("_isfirsttime", false)
        if(isFirsttime){
            val delete = sharedPreferences.edit()
            delete.remove("_isfirsttime")
            delete.apply()
            isShow = false

            val value = NotificationModel("¡Bienvenido a Journey!", "Felicidades completaste Abrir la app por primera vez", false, "", ExtrasModel("challenge/deshoje-tomate-grape.png",200.0,0,0,""), ExtrasRedeemModel(0,"",0,0,"","") , "")
            var alertChallenges = LoadingDialog( ctx)
            alertChallenges.alertNotImageCustom(
                value.body,
                value.title,
                "Continuar",
                EnviromentConstants().image_serverver+value.extras.icon,
                View.OnClickListener {
                    clickContinuealert(alertChallenges)
                },
                View.OnClickListener {
                    clickContinuealert(alertChallenges)
                    msjPicasso = value.title + "-" + value.body + "#VivaSmarth #Journey"
                    Picasso.get().load(EnviromentConstants().image_serverver+value.extras.icon).into(object : Target {
                        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        }

                        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                            SnackAlert.alert("Error al cargar imagen", ctx!!.layout_container_home,"error", Snackbar.LENGTH_LONG,"")
                            SnackAlert.showAlert()
                        }

                        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                            shareOnFacebook(bitmap!!, msjPicasso)
                        }

                    })
                },
                true,
                true,
                "" + value.extras.points.toInt()
            )
        }
    }

    fun setImage(){
        val responseStr = sharedPreferences.getString("_userinfo", "")
        val response = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )
       image = EnviromentConstants().image_serverver + response.photo

        Picasso.get().load(image).error(R.drawable.no_image_profile).into(ctx.iv_user_image_header) //.error(R.drawable.no_image_profile)
        Picasso.get().load(image).error(R.drawable.no_image_profile).into(ctx.bottomSheet.iv_user_image_header_menu)
    }

    fun onGetPhoneFirebase(){
        myRef.child("data_base").child("surveys").child("phone_survey").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.getValue() != null){
                    val value = dataSnapshot.getValue().toString()

                    val editor = sharedPreferences.edit()
                    editor.putString("_phonesurvey", value)
                    editor.apply()
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }
    override fun showFragment(idScreen:String, headerType:String, fragment: Fragment, isClose:Boolean, from:String){

        Log.e("--->",from)
        if(idScreen == "manteiner") {
            if (fragmentActiveString != idScreen) {
                this.fragmentActiveString = idScreen
                ctx.header_home_conteiner.visibility = View.GONE

                try {
                    val transaction = manager.beginTransaction()
                    transaction.replace(R.id.layer_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }catch (e:Exception){

                }
            } else {
                if (isClose) {
                    ctx.finish()
                }
            }

        }else{
            ctx.bottomSheet.dismiss()
            ctx.header_home_conteiner.visibility = View.VISIBLE
            if (headerType == "profile") {
                ctx.layer_container_header.visibility = View.VISIBLE
                ctx.header_vivacoints.visibility = View.GONE
            } else {
                ctx.layer_container_header.visibility = View.GONE
                ctx.header_vivacoints.visibility = View.VISIBLE
            }

            if (fragmentActiveString != idScreen) {
                this.fragmentActiveString = idScreen
                val transaction = manager.beginTransaction()
                transaction.replace(R.id.layer_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()

            } else {
                if (idScreen == "home" && isClose) {
                    ctx.finish()
                }
            }

            if(idScreen == "ranking"){
                ctx.layer_header_home.setBackgroundColor(ctx.resources.getColor(R.color.ice_blue))
            }else{
                ctx.layer_header_home.setBackgroundColor(ctx.resources.getColor(R.color.white))
            }

            if(idScreen != "reviews") {
                this.isReviewsActive()
            }

            viewBottomSheet.findViewById<ImageView>(R.id.ic_home_menu)!!.setImageResource(R.drawable.ic_home_menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_payroll_menu)!!.setImageResource(R.drawable.ic_payment_menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_quality_menu)!!.setImageResource(R.drawable.ic_quality_menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_challenges_menu)!!.setImageResource(R.drawable.ic_challenges_menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_surveys_menu)!!.setImageResource(R.drawable.ic_surveys__menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_complaint_menu)!!.setImageResource(R.drawable.ic_complaints_menu_diabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_awards_menu)!!.setImageResource(R.drawable.ic_awards_disable)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_ranking_menu)!!.setImageResource(R.drawable.ic_ranking_menu_disabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_vacancies_menu)!!.setImageResource(R.drawable.ic_vacancie_disabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_notification_menu)!!.setImageResource(R.drawable.ic_notification_disabled)
            viewBottomSheet.findViewById<ImageView>(R.id.ic_reviews_menu)!!.setImageResource(R.drawable.ic_reviews_menu_disabled)


            val viewHeader = ctx.findViewById<LinearLayout>(R.id.header_home_conteiner)
            viewHeader.background = ctx.resources.getDrawable(R.color.colorBackground)
            if (Build.VERSION.SDK_INT >= 21) {
                val window = ctx.window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.statusBarColor = ctx.resources.getColor(R.color.colorBackground)
            }

            when (idScreen) {
                "home" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_home_menu)!!.setImageResource(R.drawable.ic_home_menu_enabled)
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_inicio", bundle)
                }
                "nomina" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_payroll_menu)!!.setImageResource(
                        R.drawable.ic_payment_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_nomina", bundle)
                }
                "calidad" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_quality_menu)!!.setImageResource(
                        R.drawable.ic_quality_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_calidad", bundle)
                }
                "retos" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_challenges_menu)!!.setImageResource(
                        R.drawable.ic_challenges_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_retos", bundle)
                }
                "encuestas" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_surveys_menu)!!.setImageResource(
                        R.drawable.ic_surveys_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_encuestas", bundle)
                }
                "denuncias" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_complaint_menu)!!.setImageResource(
                        R.drawable.ic_complaints_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_denuncias", bundle)
                }
                "premios" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_awards_menu)!!.setImageResource(
                        R.drawable.ic_awards_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_premios", bundle)
                }
                "ranking" -> {
                    viewHeader.background = ctx.resources.getDrawable(R.color.ice_blue)
                    if (Build.VERSION.SDK_INT >= 21) {
                        val window = ctx.window
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                        window.statusBarColor = ctx.resources.getColor(R.color.ice_blue)
                    }
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_ranking_menu)!!.setImageResource(
                        R.drawable.ic_ranking_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_ranking", bundle)

                }
                "vacante" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_vacancies_menu)!!.setImageResource(
                        R.drawable.ic_vacancie_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_ofertas_empleo", bundle)
                }
                "notificaciones" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_notification_menu)!!.setImageResource(
                        R.drawable.ic_notification_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_notificaciones", bundle)
                }
                "reviews" -> {
                    viewBottomSheet.findViewById<ImageView>(R.id.ic_reviews_menu)!!.setImageResource(
                        R.drawable.ic_reviews_menu_enabled
                    )
                    val bundle = Bundle()
                    bundle.putInt("business_unit_id", user.business_unit_id)
                    bundle.putInt("coins", user.coins)
                    bundle.putString("name", user.name)
                    bundle.putString("first_name", user.first_name)
                    bundle.putString("cellphone", user.cellphone)
                    bundle.putString("last_name", user.last_name)
                    bundle.putString("employee_code", user.employee_code)
                    bundle.putString("id", user.business_unit_id.toString())
                    bundle.putBoolean("is_staff", user.is_staff)
                    bundle.putInt("points", user.points)
                    firebaseAnalytics.logEvent("mostrar_pantalla_evaluaciones", bundle)
                }
            }
        }
    }

    private fun actionNotificationChallenges(){
        myRef.child("data_base").child(user.id.toString()).child("challenges").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if( dataSnapshot.value != null){
                    val value = dataSnapshot.getValue(NotificationModel::class.java)

                    if( value != null && !value.view && isShow){
                        isShow = false
                        val editor = sharedPreferences.edit()
                        user.coins = user.coins + value.extras.points.toInt()

                        editor.putString("_userinfo", gson.toJson(user) )
                        editor.apply()

                        try {
                            ctx.lbl_number_vivacoints_header_vivacoint.text = user.points.toString()
                        }catch (e:Exception){
                            Log.e("ERROR","Components null")
                        }
                        var alertChallenges = LoadingDialog( ctx)
                        alertChallenges.alertNotImageCustom(
                            value.body,
                            value.title,
                            "Continuar",
                            EnviromentConstants().image_serverver+value.extras.icon,
                            View.OnClickListener {
                                clickContinuealert(alertChallenges)
                            },
                            View.OnClickListener {
                                clickContinuealert(alertChallenges)
                                msjPicasso = value.title + "-" + value.body + "#VivaSmarth #Journey"
                                Picasso.get().load(EnviromentConstants().image_serverver+value.extras.icon).into(object : Target {
                                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                                    }

                                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                                        SnackAlert.alert("Error al cargar imagen", ctx!!.layout_container_home,"error", Snackbar.LENGTH_LONG,"")
                                        SnackAlert.showAlert()
                                    }

                                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                                        shareOnFacebook(bitmap!!, msjPicasso)
                                    }

                                })
                            },
                            true,
                            true,
                            "" + value.extras.points.toInt()
                        )
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun actionNotificationQuality() {
        myRef.child("data_base").child(user.id.toString()).child("quality")
            .child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if (dataSnapshot.value != null) {
                    val value = dataSnapshot.getValue(NotificationModel::class.java)
                    val calTo = Calendar.getInstance()
                    val calFrom = Calendar.getInstance()

                    calTo.add(Calendar.DATE, -2)
                    calFrom.add(Calendar.DATE, -8)
                    val dateTo: Date = calTo.getTime()
                    val sdfTo = SimpleDateFormat("yyyy-MM-dd")
                    val dateFrom: Date = calFrom.getTime()
                    val sdfFrom = SimpleDateFormat("yyyy-MM-dd")
                    val date_from = sdfFrom.format(dateFrom)
                    val date_to = sdfTo.format(dateTo)

                    Log.e("->>>> datTo ", date_to)
                    Log.e("->>>> dateFrom ", date_from)
                    this@HomePresenter.model.getQualityScore(
                        token,
                        user.id.toString(),
                        user.business_unit_id.toString(),
                        date_to,
                        date_from
                    )
                    if (value != null && !value.view) {
                        showFragment(
                            "calidad",
                            "vivacoins",
                            QualityWeekenFragment(),
                            false,
                            "trece"
                        )

                    }
                }
            }

            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun actionNotificationConfirmNot() {
        myRef.child("data_base").child(user.id.toString()).child("confirm")
            .child("notificationAction").addValueEventListener(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                    if( dataSnapshot.value != null){
                        val value = dataSnapshot.getValue(NotificationModel::class.java)

                        if(value != null && !value.view && isShowConfirmNot){
                            isShowConfirmNot = false

                            var alertconfirm = LoadingDialog( ctx)
                            alertconfirm.alertConfirmNotification(
                                value.body,
                                value.title,
                                value.extras,
                                "Omitir",
                                "Confirmar",
                                View.OnClickListener {
                                    clickContinuealert(alertconfirm)
                                    model.confirmNotification(token, value.extras.message_id.toInt(), false)
                                },
                                View.OnClickListener {
                                    clickContinuealert(alertconfirm)
                                    model.confirmNotification(token, value.extras.message_id.toInt(), true)
                                })

                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) { // Failed to read value
                    Log.e("ERROR", "No es posible conectar con FIREBASE")
                }
            })
    }

    private fun actionNotificationLevelUp(){
        myRef.child("data_base").child(user.id.toString()).child("level_up").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if( dataSnapshot.value != null){
                    val value = dataSnapshot.getValue(NotificationModel::class.java)

                    if(value != null && !value.view && isShowLevelUp){
                        isShowLevelUp = false
                        val editor = sharedPreferences.edit()
                        user.coins = user.coins + value.extras.points.toInt()

                        editor.putString("_userinfo", gson.toJson(user) )
                        editor.apply()

                        model.getInfoUser(token)

                        try {
                            //ctx.lbl_number_points_header_vivacoint.text = user.points.toString()
                            ctx.bottomSheet.lbl_single_vivacoin_number.text = user.coins.toString()
                            ctx.lbl_single_vivacoin_number.text = user.coins.toString()
                            ctx.lbl_number_vivacoints_header_vivacoint.text = user.coins.toString()

                        }catch (e:Exception){
                            Log.e("ERROR","Components null")
                        }
                        var alertLevelUp = LoadingDialog( ctx)
                        alertLevelUp.alertNot(
                            value.body,
                            value.title,
                            "Continuar",
                            R.drawable.ic_leaves,
                            View.OnClickListener {
                                clickContinuealert(alertLevelUp)
                            },
                            View.OnClickListener {
                                clickContinuealert(alertLevelUp)
                                val image  = BitmapFactory.decodeResource(ctx.applicationContext.resources, R.drawable.img_leaves)
                                this@HomePresenter.shareOnFacebook(image, value.title+" - "+value.body+" #VivaSmarth #Journey")
                            },
                            true,
                            true,
                            "" + value.extras.points
                        )
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun actionNotificationInfo(){
        myRef.child("data_base").child(user.id.toString()).child("info").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.getValue(NotificationModel::class.java)

                if( value != null && !value.view && isShowInfo){
                    isShowInfo = false
                    var alertInfo = LoadingDialog( ctx)
                    if(value.extras.icon != "") {

                        alertInfo.alertNotImageCustom(
                            value.body,
                            value.title,
                            "Continuar",
                            EnviromentConstants().image_serverver + value.extras.icon,
                            View.OnClickListener {
                                clickContinuealert(alertInfo)
                            },
                            View.OnClickListener {
                                clickContinuealert(alertInfo)
                            },
                            false,
                            false,
                            ""
                        )
                    }else{

                        alertInfo.alertNotMultipleAnimt(
                            value.body,
                            value.title,
                            "Continuar",
                            "Continuar",
                            View.OnClickListener {
                                clickContinuealert(alertInfo)
                            },
                            View.OnClickListener {
                                clickContinuealert(alertInfo)
                            },
                            false,
                            true,
                            "anim_notification.json")
                    }
                }

            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }
    private fun actionNotificationLevelUpRanking(){
        myRef.child("data_base").child(user.id.toString()).child("ranking").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.getValue(NotificationModel::class.java)

                if( value != null && !value.view && isShowLevelUpRanking){
                    isShowLevelUpRanking = false

                    var newText:String

                    if(user.current_lvl == value.extras.lvl){
                        newText = "Muy bien te haz mantenido en tu posicion " + user.name + ", si te sigues esforzando podrás mejorar aún mas!"
                    }else if( user.current_lvl < value.extras.lvl) {
                        newText = "¡Haz bajado de lugar " + user.name + ", Sigue esforzandote para mejorar dia a dia!"
                    }else{
                        newText = "¡Haz subido de lugar " + user.name + ", cada vez estas mas cerca de la cima, ¡no dejes de esforzarte!"
                    }


                    var alertLevelUpRanking = LoadingDialog( ctx)
                    alertLevelUpRanking.alertNot(
                        newText,
                        value.body,
                        "Continuar",
                        R.drawable.ic_ranking,
                        View.OnClickListener {
                            clickContinuealert(alertLevelUpRanking)
                        },
                        View.OnClickListener {
                            clickContinuealert(alertLevelUpRanking)
                        },
                        false,
                        false,
                        ""
                    )
                }

            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun actionNotificationNewReview(){
        myRef.child("data_base").child(user.id.toString()).child("review").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.getValue(NotificationModel::class.java)

                if( value != null && !value.view){
                    val alertNewVersion = LoadingDialog(ctx)
                    alertNewVersion.alertNotMultiple(
                        "Ya está disponible la evaluación da tu Supervisor, ¿deseas evaluarlo?",
                        "¡Nueva Evaluación!",
                        "Ir",
                        "Cancelar",
                        R.drawable.img_survey_notifitation,
                        View.OnClickListener{
                            alertNewVersion.dismissDialog()
                            this@HomePresenter.showFragment("reviews","vivacoins",ReviewsFragment(true),false,"10000")
                            myRef.child("data_base").child(user.id.toString()).child("review").child("notificationAction").child("view").setValue(true)
                        },
                        View.OnClickListener{
                            alertNewVersion.dismissDialog()
                            myRef.child("data_base").child(user.id.toString()).child("review").child("notificationAction").child("view").setValue(true)
                        },
                        true,
                        ""
                    )

                    alertNewVersion.showAlertDialogMultiple()
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }
    private fun actionNotificationNewVersion(){

        myRef.child("data_base").child("first_time_active").child("version_app").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.value as? String
                val localVersion = utilsClass().getversionName(ctx.applicationContext)
                val alertNewVersion = LoadingDialog(ctx)
                alertNewVersion.alertNotMultiple(
                    "Hay una nueva versión disponible de VivaJourney, ¿deseas actualizarla?",
                    "¡Nueva Versión!",
                    "Aceptar",
                    "Cancelar",
                    R.drawable.img_new_version,
                    View.OnClickListener{
                        alertNewVersion.dismissDialog()
                        val openURL = Intent(Intent.ACTION_VIEW)
                        openURL.data = Uri.parse(EnviromentConstants().googlepay_server)
                        ctx.startActivity(openURL)
                    },
                    View.OnClickListener{
                        alertNewVersion.dismissDialog()

                    },
                    true,
                    ""
                )

                if(value != null){
                    if(value != localVersion){
                        alertNewVersion.showAlertDialogMultiple()
                    }else{
                        try {
                            alertNewVersion.dismissDialog()
                        }catch (e:Exception){
                            e.printStackTrace()
                        }
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })

    }

    private fun actionNotificationExchangeCode(){
        myRef.child("data_base").child(user.id.toString()).child("prize_redeem").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.getValue(NotificationModel::class.java)
                if( value != null && !value.view && isShowExchangeCode){
                    isShowExchangeCode = false
                    val alertExchangeCode = LoadingDialog(ctx)
                    alertExchangeCode.alertNotMultipleAnimt(
                        "Hola "+user.name+", ¿Ya estas canjenado tu premio?, si es asi confirma para que te puedan dar tu premio. De lo contrario cancela.",
                        "¡Enhorabuena!",
                        "Cancelar",
                        "Aceptar",
                        View.OnClickListener{
                            clickContinuealert(alertExchangeCode)
                            model.sendResponsecode(token, "GENERATED", value.extrasRedeem.id)
                        },
                        View.OnClickListener{
                            clickContinuealert(alertExchangeCode)
                            model.sendResponsecode(token, "EXCHANGED", value.extrasRedeem.id )
                        },
                        true,
                        true,
                        "anim_gift_one.json"
                        )
                }

            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun actionNotificationFirstTimeActive(){
        myRef.child("data_base").child("first_time_active").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                val value = dataSnapshot.value as? Boolean
                if(value != null){
                    this@HomePresenter.isInFistTimeActive = value
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }
    private fun actionNotificationManteiner(){
        myRef.child("data_base").child("is_manteiner").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if(dataSnapshot.value != null) {
                    val value = dataSnapshot.value as Boolean
                    if(value) {
                        showFragment("manteiner", "", ManteinerFragment(), true, "Catorce")
                    }else{
                        if(ctx.intent.extras != null) {
                            if (ctx.intent.extras!!.getString("isNotificastion") == null || ctx.intent.extras!!.getString("isNotificastion") == "home") {
                                if (fragmentActiveString == "" || fragmentActiveString == "manteiner" || fragmentActiveString == "home") {
                                    showFragment("home", "profile", HomeFragment(), false, "init_quince")
                                }
                            } else {
                                var extraintent = ctx.intent.extras?.getString("isNotificastion")
                                if(extraintent == null){
                                    extraintent = "home"
                                }
                                when (extraintent) {
                                    "", "home", "level_up" -> showFragment("home", "profile", HomeFragment(), true, "init_one")
                                    "payroll" -> showFragment("nomina", "vivacoins", PayrollFragment(), false, "init_dos")
                                    "challenges" -> showFragment("retos", "vivacoins", ChallengesFragment(),false, "init_tres")
                                    "quality" -> showFragment("calidad", "vivacoins", QualityWeekenFragment(),false, "init_Cuatro")
                                    "surveys" -> showFragment("encuestas", "vivacoins", SurveysFragment(),false, "init_cinco")
                                    "vacant" -> showFragment("vacante", "vivacoins", VacainciesFragment(),false, "init_seis")
                                    "payroll_complaint_comment" -> {
                                        showFragment("nomina", "vivacoins", PayrollFragment(),false, "init_dos_uno")
                                        val editor = sharedPreferences.edit()
                                        var extraintent = ctx.intent.extras?.getString("data")
                                        editor.putString("_data_notifitacion", extraintent)
                                        editor.apply()
                                    }
                                    "review" -> showFragment("reviews", "vivacoins", ReviewsFragment(true),false, "init_15")
                                    "new_survey" ->
                                    {
                                        showFragment("encuestas", "vivacoins", SurveysFragment(), false, "init_siete")
                                        val intentSurvey = Intent( ctx  , SurveyActivity::class.java)
                                        intentSurvey.putExtra("isNotification", "true" )
                                        ctx.startActivity(intentSurvey)
                                        Log.e("ID",user.id.toString())
                                    }
                                    else-> showFragment("home", "profile", HomeFragment(), true, "init_one")
                                }
                            }
                        }else{
                            var extraintent = ctx.intent.extras?.getString("isNotificastion")
                            if(extraintent == null){
                                extraintent = "home"
                            }
                            when (extraintent) {
                                "", "home", "level_up" -> showFragment("home", "profile", HomeFragment(), true, "init_ocho")
                                "payroll" -> showFragment("nomina", "vivacoins", PayrollFragment(), false, "init_nueve")
                                "challenges" -> showFragment("retos", "vivacoins", ChallengesFragment(),false, "init_diez")
                                "quality" -> showFragment("calidad", "vivacoins", QualityWeekenFragment(),false, "init_once")
                                "surveys" -> showFragment("encuestas", "vivacoins", SurveysFragment(),false, "init_doce")
                                "vacant" -> showFragment("vacante", "vivacoins", VacainciesFragment(),false, "init_trece")
                                "payroll_complaint_comment" -> {
                                    showFragment("nomina", "vivacoins", PayrollFragment(),false, "init_dos_dos")
                                }
                                "review" -> showFragment("reviews", "vivacoins", ReviewsFragment(true),false, "init_15")
                                "new_survey" -> {
                                    showFragment("encuestas", "vivacoins", SurveysFragment(), false, "init_catorce")
                                    val intentSurvey = Intent( ctx  , SurveyActivity::class.java)
                                    intentSurvey.putExtra("isNotification", "true" )
                                    ctx.startActivity(intentSurvey)
                                    Log.e("ID",user.id.toString())
                                }
                                else-> showFragment("home", "profile", HomeFragment(), true, "init_one")

                            }
                        }
                        //showFragment("home", "profile", HomeFragment(), false, "quince")
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    private fun clickContinuealert(instanceAlert:LoadingDialog){
        instanceAlert.dismissDialog()

        if(!isShow){
            isShow = true
            myRef.child("data_base").child(user.id.toString()).child("challenges").child("notificationAction").child("view").setValue(true)
        }else if(!isShowLevelUp){
            isShowLevelUp = true
            myRef.child("data_base").child(user.id.toString()).child("level_up").child("notificationAction").child("view").setValue(true)
        }else if(!isShowFirsTime){
            isShowFirsTime = true
            myRef.child("data_base").child(user.id.toString()).child("first_time").setValue(true)
        }else if(!isShowInfo){
            isShowInfo = true
            myRef.child("data_base").child(user.id.toString()).child("info").child("notificationAction").child("view").setValue(true)
        } else if(!isShowExchangeCode){
            isShowExchangeCode = true
            myRef.child("data_base").child(user.id.toString()).child("prize_redeem").child("notificationAction").child("view").setValue(true)
        } else if(!isShowLevelUpRanking){
            isShowLevelUpRanking = true
            myRef.child("data_base").child(user.id.toString()).child("ranking").child("notificationAction").child("view").setValue(true)
        }else if(!isShowConfirmNot){
            isShowConfirmNot = true
            myRef.child("data_base").child(user.id.toString()).child("confirm").child("notificationAction").child("view").setValue(true)
        }

    }
    private fun initFCM(){
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val tokenFirebase = task.result?.token
                Log.e("Firebase", tokenFirebase)
                model.updateTokenFirebase(token, tokenFirebase!!, user.id)

            })
    }
    override fun updateTokenFirebaseErrorResponse(message: String) {
        Log.e("Error","FIREBASE TOKEN NO SE PUEDO GUARDAD")
    }

    override fun updateTokenFirebaseResponse(response: UserResponseModel) {
        Log.e("OK","FIREBASE TOKEN GUARDADO")
    }

    override fun sendResponsecodeResponse(response: SendResResponseModel) {
        if(response.status == "GENERATED"){
            SnackAlert.alert("Hemos cancelado la solicitud", ctx.layout_container_home, "success", Snackbar.LENGTH_LONG, "" )
            SnackAlert.showAlert()

        }else{
            val alertCodeResponse = LoadingDialog(ctx)
            alertCodeResponse.alertNotMultipleAnimt("Tu codigo ha sido enviado correctamente, disfruta tu premio "+user.name + " "+ user.first_name+", ¡Siguete esforzando para nuevas sorpresas!",  "¡Excelente!",  "Cancelar", "Continuar", View.OnClickListener { alertCodeResponse.dismissDialog() }, View.OnClickListener { alertCodeResponse.dismissDialog()}, false, false, "anim_ok.json")
        }

    }

    override fun sendResponsecodeErrorResponse(message: String) {
        SnackAlert.alert(message, ctx.layout_container_home, "error", Snackbar.LENGTH_LONG, "" )
        SnackAlert.showAlert()
    }


    override fun unauthorizedError() {
        utilsClass().logOut(ctx)
    }

    override fun getQualityScoreResponse(response: QualityScoreResponse) {
        val editor = sharedPreferences.edit()
        editor.putString("_qualityScore", gson.toJson(response))
        editor.apply()
    }

    override fun getQualityScoreErrorResponse(message: String) {

    }

    override fun getInfoUserResponse(response: UserResponseModel){
        val editor = sharedPreferences.edit()
        editor.putString("_userinfo", gson.toJson(response) )
        editor.putString("_image", response.photo)
        editor.apply()

        /*val lblcurrentlevel = ctx.findViewById<TextView>(R.id.lbl_current_level_home_fragment)
        val lblnextlevel = ctx.findViewById<TextView>(R.id.lbl_next_level_home_fragment)
        val lbldescriptionlevel = ctx.findViewById<TextView>(R.id.lbl_descrition_four_view_home)
        val lbldescriptionProgressBar = ctx.findViewById<TextView>(R.id.lbl_value_progress_bar_five_view_home)
        val progressBar = ctx.findViewById<ProgressBar>(R.id.progress_bar_current_experience)

        val lblCategorie = ctx.findViewById<TextView>(R.id.lbl_category_level_user)
        val viewColorCategory = ctx.findViewById<LinearLayout>(R.id.view_category_color)


        val texto_actual = "Nivel actual: "
        val texto_siguiente = "Nivel siguiente: "
        val text_descripcion = "Te faltan "
        val text_descripcion2 = " VivaCoins para llegar al nivel "

        val current_level = SpannableStringBuilder()
            .append(texto_actual)
            .bold { append(response.current_lvl.toString()) }

        val next_level = SpannableStringBuilder()
            .append(texto_siguiente)
            .bold { append(response.next_lvl.toString()) }

        val description = SpannableStringBuilder()
            .append(text_descripcion)
            .bold { append(response.to_next_lvl.toString()) }
            .append(text_descripcion2)
            .bold { append(response.next_lvl.toString()) }

        val value_progress = SpannableStringBuilder()
            .bold {append(response.points.toString())}
            .append(" de " + ( response.points + response.to_next_lvl ) + " VivaCoins")

        val persent = ((response.points * 100 ) / (response.to_next_lvl + user.points))


        lblcurrentlevel.text = current_level
        lblnextlevel.text = next_level
        lbldescriptionlevel.text = description
        lbldescriptionProgressBar.text = value_progress
        progressBar.progress = persent

        if(response.ranking_history.size > 0){
            if (response.ranking_history.get(response.ranking_history.size - 1).level_category != null) {
                val categorie =
                    response.ranking_history.get(response.ranking_history.size - 1).level_category
                lblCategorie.text = categorie
                viewColorCategory.setBackgroundColor(utilsClass().getColorCategory(categorie!!))
            } else {
                val objCat = utilsClass().rangeCategories(response.current_lvl)
                lblCategorie.text = objCat.category
                viewColorCategory.setBackgroundColor(ctx.resources.getColor(objCat.color))
            }
        }else {
            val objCat = utilsClass().rangeCategories(response.current_lvl)
            lblCategorie.text = objCat.category
            viewColorCategory.setBackgroundColor(ctx.resources.getColor(objCat.color))
        }
*/
    }

    override fun getInfoUserErrorResponse(message: String) {
        SnackAlert.alert("Error al obtener datos del colaborador",ctx.layout_container_home,"error",Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }

    override fun confirmNotificationResponse() {
        val alertNot = LoadingDialog(ctx)
        alertNot.alertNotMultipleAnimt(
            "",
            "¡Gracias por tu apoyo!",
            "",
            "Aceptar",
            View.OnClickListener{
            },
            View.OnClickListener{
                alertNot.dismissDialog()
            },
            false,
            false,
            "anim_ok.json"
        )
    }

    override fun confirmNotificationErrorResponse(message: String) {
        val alertErrornot = LoadingDialog(ctx)
        alertErrornot.alertNotMultipleAnimt(
            "",
            "¡Ha ocurrido un error intenta mas tarde!",
            "",
            "Aceptar",
            View.OnClickListener{
            },
            View.OnClickListener{
                alertErrornot.dismissDialog()
            },
            false,
            false,
            "anim_error.json"
        )    }

    fun shareOnFacebook(bitmap:Bitmap, msj:String){
        if (ShareDialog.canShow(ShareLinkContent::class.java)) {

            val photo = SharePhoto.Builder()
                .setBitmap(bitmap)
                .setCaption("#VivaSmarth")
                .setUserGenerated(true)
                .build()
            val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()

            shareDialog.show(content)
        }
    }


}
