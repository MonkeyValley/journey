package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.UserValidate.IUserValidate
import com.example.jurney_mobile.Model.Vacancies.IVacancies
import com.example.jurney_mobile.Model.Vacancies.VacanciesModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.*
import com.example.jurney_mobile.View.Adapters.RankingAdapter
import com.example.jurney_mobile.View.Adapters.VacanciesAdapter
import com.example.jurney_mobile.View.Fragments.VacainciesFragment
import com.example.jurney_mobile.View.Models.SendVacancyResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.Models.VacancyResponseModel
import com.example.jurney_mobile.View.UserValidate
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.alert_layout_postulation.view.*
import kotlinx.android.synthetic.main.bottomsheet_details_vacancie.view.*
import kotlinx.android.synthetic.main.ranking_fragment.*
import kotlinx.android.synthetic.main.vacancies_fragment.*
import kotlinx.android.synthetic.main.vacancies_fragment.view.*

class VacanciePresenter:IVacancies.Presenter, View.OnClickListener, IClickItemDelegate.clickVacancies, SendVacancieDelegate {

    var view: IVacancies.View;
    var model: IVacancies.Model;
    var ctx: VacainciesFragment
    val gson = Gson()
    var itemActive = 1
    private lateinit var alert:LoadingDialog
    private lateinit var holder:View
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var itemSelected : VacancyResponseModel
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: VacanciesAdapter
    private lateinit var user:UserResponseModel
    private lateinit var token:String
    private lateinit var bottomSheet: BottomSheetDialog
    private lateinit var viewBottomSheet:View
    private lateinit var arrayVacacnies:List<VacancyResponseModel>
    var arrayCuliacan = ArrayList<VacancyResponseModel>()
    var arrayEnsenada = ArrayList<VacancyResponseModel>()
    var arraySayula = ArrayList<VacancyResponseModel>()

    constructor(view: IVacancies.View, ctx:VacainciesFragment ){
        this.view = view
        this.model = VacanciesModel(this, ctx.context!!)
        this.ctx = ctx
    }


    override fun initInfo(v: View) {
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!
        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        val vacanciesArrayStr = sharedPreferences.getString("_listvacancies","")

        if(vacanciesArrayStr != "") {
            arrayVacacnies =
                gson.fromJson(vacanciesArrayStr, Array<VacancyResponseModel>::class.java).toList()
            arrayVacacnies.forEach {
                if (it.status) {
                    if (it.business_unit_id == 1) arrayCuliacan.add(it)
                    else if (it.business_unit_id == 2) arrayEnsenada.add(it)
                    else arraySayula.add(it)
                }
            }
        }
        alert = LoadingDialog(ctx.activity!!)
        holder = v
        v.btn_selected_culiacan.setOnClickListener(this)
        v.btn_selected_ensenada.setOnClickListener(this)
        v.btn_selected_sayula.setOnClickListener(this)

        bottomSheet = BottomSheetDialog(this.ctx.context!!,  R.style.BottomSheetDialogTheme )
        viewBottomSheet = LayoutInflater.from(this.ctx.context!!).inflate(  R.layout.bottomsheet_details_vacancie, v.findViewById( R.id.bottomSheetContainerVacancieDetails))
        bottomSheet.setContentView(viewBottomSheet)

        holder.lbl_indicator_items_culiacan.text  = arrayCuliacan.size.toString()
        holder.lbl_indicator_items_ensenada.text  = arrayEnsenada.size.toString()
        holder.lbl_indicator_items_sayula.text  = arraySayula.size.toString()
        setDataRecyclerView(arrayCuliacan)
    }

    override fun sendVacancyResponse(response: SendVacancyResponseModel){
        when(itemSelected.business_unit_id){
            1->{
                arrayCuliacan.forEach { if(it.id == itemSelected.id) it.postulated = true }
                adaptador.notifyDataSetChanged()
            }
            2->{
                arrayEnsenada.forEach { if(it.id == itemSelected.id)it.postulated = true}
                adaptador.notifyDataSetChanged()
            }
            3->{
                arraySayula.forEach { if(it.id == itemSelected.id)it.postulated = true}
                adaptador.notifyDataSetChanged()
            }
        }

        arrayVacacnies.forEach { if(it.id == itemSelected.id) it.postulated = true }
        val arrayStr = gson.toJson(arrayVacacnies)

        val delete = sharedPreferences.edit()
        delete.remove("_listvacancies")

        val editor = sharedPreferences.edit()
        editor.putString("_listvacancies", arrayStr)
        editor.apply()

        SnackAlert.alert("Peticion enviada",holder.layout_conteiner_vacancies,"success",Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }
    override fun sendVacancyErrorResponse(message: String) {
        SnackAlert.alert(message,holder.layout_conteiner_vacancies,"error",Snackbar.LENGTH_LONG, "")
        SnackAlert.showAlert()
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

    fun setDataRecyclerView(array: ArrayList<VacancyResponseModel>){

        if(array.size > 0) {
            holder.empty_view_payroll.visibility = View.GONE
            holder.list_vacancies_fragment.visibility = View.VISIBLE
            list = holder.findViewById(R.id.list_vacancies_fragment)
            layoutManager = LinearLayoutManager(ctx.context)
            adaptador = VacanciesAdapter(array, this)
            list.layoutManager = layoutManager
            list.adapter = adaptador
        }else{
            holder.empty_view_payroll.visibility = View.VISIBLE
            holder.list_vacancies_fragment.visibility = View.GONE
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id){

            R.id.btn_selected_culiacan->{
                itemActive = 1
                holder.layer_tabbar_selector_culiacan.visibility = View.VISIBLE
                holder.layer_tabbar_selector_ensenada.visibility = View.INVISIBLE
                holder.layer_tabbar_selector_sayula.visibility = View.INVISIBLE

                setDataRecyclerView(arrayCuliacan)

            }
            R.id.btn_selected_ensenada->{
                itemActive = 2
                holder.layer_tabbar_selector_culiacan.visibility = View.INVISIBLE
                holder.layer_tabbar_selector_ensenada.visibility = View.VISIBLE
                holder.layer_tabbar_selector_sayula.visibility = View.INVISIBLE

                setDataRecyclerView(arrayEnsenada)
            }
            R.id.btn_selected_sayula->{
                itemActive = 3
                holder.layer_tabbar_selector_culiacan.visibility = View.INVISIBLE
                holder.layer_tabbar_selector_ensenada.visibility = View.INVISIBLE
                holder.layer_tabbar_selector_sayula.visibility = View.VISIBLE

                setDataRecyclerView(arraySayula)
            }
            R.id.btn_send_postulation ->{


                    bottomSheet.hide()

                    var user_bussiness_unity = ""
                    when(user.business_unit_id){
                        1->user_bussiness_unity = "Culiacán"
                        2->user_bussiness_unity = "Ensenada"
                        3->user_bussiness_unity = "Sayula"
                    }

                    if(user.status){
                        if(user.business_unit_id == itemActive){
                            val description = ctx.resources.getString(R.string.description_postulation_active_sbu)
                            alert.alertVacancy( itemSelected, description, itemSelected.name, itemActive, 1, this)
                        }else{
                            val description = ctx.resources.getString(R.string.description_postulation_active_obu) +" "+ user_bussiness_unity
                            alert.alertVacancy(itemSelected, description, itemSelected.name, itemActive, 1, this)
                        }
                    }else{
                        var user_bussiness_unity_inactive = ""
                        when(itemSelected.business_unit_id){
                            1->user_bussiness_unity_inactive = "Culiacán"
                            2->user_bussiness_unity_inactive = "Ensenada"
                            3->user_bussiness_unity_inactive = "Sayula"
                        }

                        val description = "Una persona del departamento de Trabajo social de la agrícola "+user_bussiness_unity_inactive+" se pondrá en contacto contigo a tu teléfono " + user.cellphone
                        alert.alertVacancy(itemSelected, description , itemSelected.name, itemActive, 2, this)
                    }

            }
        }
    }

    override fun click(item: VacancyResponseModel) {

        if(itemActive == 1){
            viewBottomSheet.lbl_unity_bussiness_vacancie_detail.text = "Culiacán "
        }else if(itemActive == 2){
            viewBottomSheet.lbl_unity_bussiness_vacancie_detail.text = "Ensenada "
        }else{
            viewBottomSheet.lbl_unity_bussiness_vacancie_detail.text = "Sayula "
        }

        itemSelected = item
        var payment_method = ""

        when(item.payment_method){
            "PER_PIECEWORK"->payment_method="Destajo"
            "PER_HOUR"->payment_method="Hora"
        }

        viewBottomSheet.lbl_title_vacancie_detail.text = item.name
        viewBottomSheet.lbl_description_vacancie_detail.text = item.description
        viewBottomSheet.lbl_pay_for.text = item.salary.toString() + " " + payment_method
        viewBottomSheet.lbl_start_date.text = item.start_date

        if(item.requisites.size > 0) {
            var req = ""
            item.requisites.forEach {
                req = req + "·" + it + "\n"
            }
            viewBottomSheet.lbl_requirements_vacancy.text = req

        }else{
            viewBottomSheet.lbl_requirements_vacancy.text = "Sin requisitos"
        }

        if(!item.postulated){
            viewBottomSheet.btn_send_postulation.setBackgroundDrawable(ctx.resources.getDrawable(R.drawable.layer_bg_yellow_button))
            viewBottomSheet.btn_send_postulation.setOnClickListener(this)

        }else{

            viewBottomSheet.btn_send_postulation.setBackgroundDrawable(ctx.resources.getDrawable(R.drawable.layer_bg_disable_button))
            viewBottomSheet.btn_send_postulation.setOnClickListener(null)

        }
        bottomSheet.show()
    }

    override fun sendVacancie(state: String, date: String, type:Int) {
        if(type == 1){
            model.sendVacancySingle(token,user.id,itemSelected.id)
        }else{
            model.sendVacancy(token,user.id,itemSelected.id,state,date)
        }
    }
}

/*
Escenario de un colaborador que se postula para un puesto de la misma unidad de negocio en la que actualmente esta trabajando: Aparecerá el mensaje de: "Recuerda que para cambios de puesto en la misma agrícola es necesario ir a Trabajo social por el formato de permiso para cambiarte de área".
Escenario de un colaborador activo que se postula para un puesto en una unidad de negocio diferente a la que actualmente esta trabajando: Aparecerá el mensaje de: "Recuerda que para realizar un cambio entre unidades debes finalizar tu contrato actual en la agrícola (Unidad de negocio en la que actualmente esta laborando).
* */