package com.example.jurney_mobile.Presenter

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.text.bold
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Home.HomeFragmentModel
import com.example.jurney_mobile.Model.Home.IHome
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.HomeAdapter
import com.example.jurney_mobile.View.Fragments.HomeFragment
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.home_fragment.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.bullyboo.text_animation.AnimationBuilder
import ru.bullyboo.text_animation.TextCounter
import java.lang.Exception

class HomeFragmentPresenter: IHome.PresenterFragment, View.OnLongClickListener {

    private var view:IHome.ViewFragment
    private var model:IHome.ModelFragment
    private var ctx:HomeFragment
    private lateinit var sharedPreferences: SharedPreferences


    private val gson = Gson()
    private val v:View
    private lateinit var  user:UserResponseModel
    private lateinit var  token:String
    private var isPaymentAvailableG = false

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: HomeAdapter


    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    constructor(view: IHome.ViewFragment , ctx: HomeFragment, v:View){
        this.view = view
        this.model = HomeFragmentModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun onClickListener(v: View) {
        when(v.id){
            R.id.btn_goto_payroll-> view.onClickNomina(isPaymentAvailableG)
            R.id.btn_goto_weekend_quality-> view.onClickCalidad()
            R.id.btn_goto_challenges -> view.onClickRetos()
        }
    }
    override fun initNoNetwork(activity: Activity) {}
    override fun getInfoPayment() {
//        v.lbl_incomin_total_home_frangment.setOnLongClickListener(this)
        val isPaymentAvailable = sharedPreferences.getString("_nominaAvailable","")
        val payment = gson.fromJson<PaymentModel>(isPaymentAvailable, PaymentModel::class.java)
        if(payment.errors == ""){
            v.btn_goto_payroll.background = ctx.context!!.getDrawable(R.drawable.layer_bg_yellow_button)
            v.is_ayment_available.text = ctx.resources.getString(R.string.label_description_secondview_home)
            isPaymentAvailableG = true
        }
    }

    override fun getPaymentResponse(response: PaymentModel) {
        val responseStr = gson.toJson(response)
        Log.v("ERROR", responseStr)

        val editor = sharedPreferences.edit()
        editor.putString("_nominaAvailable", responseStr)
        editor.apply()
    }

    override fun getPAymentErrorResponse(message: String) {
       Log.e("ERROR", message)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }


    override fun initInfo() {
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val userStr = sharedPreferences.getString("_userinfo","")
        token = sharedPreferences.getString("_tokenuser","")!!
        val incomeStr = sharedPreferences.getString("_totalIncome","")
        val income = gson.fromJson<TotalIncomeModel>(incomeStr, TotalIncomeModel::class.java)
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)

        if(!user.status){
            v.card_home_challenges.visibility = View.GONE
            v.card_home_quality.visibility = View.GONE
        }

        val texto_actual = "Nivel actual: "
        val texto_siguiente = "Nivel siguiente: "
        val text_descripcion = "Te faltan "
        val text_descripcion2 = " VivaCoins para llegar al nivel "

        val current_level = SpannableStringBuilder()
            .append(texto_actual)
            .bold { append(user.current_lvl.toString()) }

        val next_level = SpannableStringBuilder()
            .append(texto_siguiente)
            .bold { append(user.next_lvl.toString()) }

        val description = SpannableStringBuilder()
            .append(text_descripcion)
            .bold { append(user.to_next_lvl.toString()) }
            .append(text_descripcion2)
            .bold { append(user.next_lvl.toString()) }

        val value_progress = SpannableStringBuilder()
            .bold {append(user.points.toString())}
            .append(" de " + ( user.points + user.to_next_lvl ) + " VivaCoins")

        val persent = ((user.points * 100 ) / (user.to_next_lvl + user.points))

        v.lbl_current_level_home_fragment.text = current_level
        v.lbl_next_level_home_fragment.text = next_level
        v.lbl_descrition_four_view_home.text = description
        v.lbl_value_progress_bar_five_view_home.text = value_progress
        v.progress_bar_current_experience.progress = persent

        if(user.ranking_history.size > 0){
            if (user.ranking_history.get(user.ranking_history.size - 1).level_category != null) {
                val categorie =
                    user.ranking_history.get(user.ranking_history.size - 1).level_category
                v.lbl_category_level_user.text = categorie
                v.view_category_color.setBackgroundColor(utilsClass().getColorCategory(categorie!!))
            } else {
                val objCat = utilsClass().rangeCategories(user.current_lvl)
                v.lbl_category_level_user.text = objCat.category
                v.view_category_color.setBackgroundColor(ctx.resources.getColor(objCat.color))
            }
        }else {
            val objCat = utilsClass().rangeCategories(user.current_lvl)
            v.lbl_category_level_user.text = objCat.category
            v.view_category_color.setBackgroundColor(ctx.resources.getColor(objCat.color))
        }


//        val modeBuilderTwoSecond = AnimationBuilder.newBuilder().addPart(900, 60).build()
//        val textCounter = TextCounter.newBuilder()
//        textCounter.setTextView(v.lbl_incomin_total_home_frangment)
//        textCounter.setType(TextCounter.DOUBLE)
//        textCounter.setCustomAnimation(modeBuilderTwoSecond)
//        textCounter.from(0).to(income.total_income)
//        textCounter.setListener(TextCounter.TextCounterListener {
//            val incomedouble = utilsClass().milesFormatNoDecimals(income.total_income)
//            v.lbl_incomin_total_home_frangment.text = incomedouble
//        }).build().start()

        actionNotification()

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val data = sharedPreferences.getString("_listchallenges", "")
        val challenges = gson.fromJson<ChallengesResponseModel>(data, ChallengesResponseModel::class.java)
        var dataChallenges = ArrayList<ChallengeModel>()
        for( i in challenges.challenges ){
            if(i.completed){
                dataChallenges.add(i)
            }
        }

        if(challenges.challenges.isEmpty()){
            v.list_challenges_view.visibility = View.GONE
            v.empty_view.visibility = View.VISIBLE
        }
        list = v.findViewById(R.id.list)
        layoutManager = LinearLayoutManager(ctx.context)
        adaptador = HomeAdapter(dataChallenges)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador

    }

    private fun actionNotification(){
        myRef.child("data_base").child(user.id.toString()).child("payroll").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if( dataSnapshot.value != null){
                    val value = dataSnapshot.getValue(NotificationModel::class.java)!!
                    if(!value.view){
                        try {
                            ctx.is_ayment_available.text = "Ya está disponible tu nómina de la semana"
                        }catch (e:Exception){
                            Log.e("Error",e.toString())
                        }
                        model.getPayment(token, user.employee_code, user.business_unit_id.toString())
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }

    override fun onLongClick(v: View?): Boolean {
        Toast.makeText(ctx.context, RetrofitClient.BASE_URL, Toast.LENGTH_LONG).show()
        return true
    }
}
