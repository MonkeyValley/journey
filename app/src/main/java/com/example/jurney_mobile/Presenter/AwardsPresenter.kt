package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Awards.AwardsModel
import com.example.jurney_mobile.Model.Awards.IAwards
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Adapters.AwardsAdapter
import com.example.jurney_mobile.View.Adapters.FilterAdapter
import com.example.jurney_mobile.View.Fragments.AwardsFragment
import com.example.jurney_mobile.View.Fragments.TabBarListAwardsFragment
import com.example.jurney_mobile.View.Fragments.TabBarlistMyCodesFragment
import com.example.jurney_mobile.View.Models.AwardsResponseModel
import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.awards_fragment.*
import kotlinx.android.synthetic.main.awards_fragment.view.*
import kotlinx.android.synthetic.main.awards_fragment.view.lbl_awards_tabbar
import org.json.JSONArray

class AwardsPresenter:IAwards.Presenter, View.OnClickListener {

    var view:IAwards.View
    var model:IAwards.Model
    var ctx:AwardsFragment
    var v:View
    var gson = Gson()
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var manager: FragmentManager
    lateinit var currentFragment:String

    lateinit var token :String
    lateinit var user:UserResponseModel

    constructor(view: IAwards.View, ctx: AwardsFragment, v: View){
        this.view = view
        this.model = AwardsModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun initInfo() {

        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        val responseStr = sharedPreferences.getString("_userinfo", "")
        user = gson.fromJson<UserResponseModel>(responseStr, UserResponseModel::class.java )

        manager = ctx.activity!!.supportFragmentManager

        if(user.status){
            v.btn_my_codes_tab_bar.setOnClickListener(this)
            v.btn_awards_tab_bar.setOnClickListener(this)
            showFragment(TabBarListAwardsFragment(), "premios")

        }else{
            v.lbl_awards_tabbar.setTextColor( ctx.resources.getColorStateList(R.color.light_grey_blue))
            showFragment(TabBarlistMyCodesFragment(), "myCodes")
        }
    }

    fun showFragment(fragment:Fragment, current:String){
        this.currentFragment = current
        if(current == "premios"){
            v.indicator_awards.visibility = View.VISIBLE
            v.indicator_my_code.visibility = View.INVISIBLE
        }else{
            v.indicator_awards.visibility = View.INVISIBLE
            v.indicator_my_code.visibility = View.VISIBLE
        }
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.layout_container_awards , fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_awards_tab_bar->showFragment(TabBarListAwardsFragment(), "premios")
            R.id.btn_my_codes_tab_bar->showFragment(TabBarlistMyCodesFragment(), "myCodes")
        }
    }
}