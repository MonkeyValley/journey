package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.Surveys.ISurveys
import com.example.jurney_mobile.Model.Surveys.SurveysModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.IClickItemDelegate
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.SurveysAdapter
import com.example.jurney_mobile.View.Fragments.SurveysFragment
import com.example.jurney_mobile.View.Models.ChallengeModel
import com.example.jurney_mobile.View.Models.ResponseGetSurveys
import com.example.jurney_mobile.View.Models.SurveysListModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.SurveyActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.surveys_fragment.view.*
import java.util.ArrayList

class SurveysPresenter:ISurveys.Presenter {

    private var view:ISurveys.View
    private var model:ISurveys.Model
    private var interfaceview:View
    private var ctx:SurveysFragment
    private lateinit var token:String
    private lateinit var user: UserResponseModel
    private lateinit var sharedPreferences: SharedPreferences
    private var gson = Gson()
    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: SurveysAdapter
    private var callback:IClickItemDelegate.clickItemSurvey
    
    constructor(view:ISurveys.View, interfaceview:View, ctx:SurveysFragment, callback: IClickItemDelegate.clickItemSurvey){
        this.view = view
        this.model = SurveysModel(this)
        this.interfaceview = interfaceview
        this.ctx = ctx
        this.callback = callback
    }

    override fun onClickItemActivity(item: SurveysListModel, position: Int) {
        val intent = Intent( ctx.context!!  , SurveyActivity::class.java)
        intent.putExtra("id", item.id)
        ctx.startActivity(intent)
    }

    override fun ongetSurveysResponse(response: ResponseGetSurveys) {
        if(response.surveys.isEmpty()){
            interfaceview.empty_view_Surveys.visibility = View.VISIBLE
            interfaceview.list_surveys_fragment.visibility = View.GONE
        }

        var dataChallenges = ArrayList<SurveysListModel>()
        for( i in response.surveys ){
            if(i.is_active && i.type != 2 && (i.audience_origin == user.type_origin || i.audience_origin == null )){
                dataChallenges.add(i)
            }
        }

        list = interfaceview.findViewById(R.id.list_surveys_fragment)
        layoutManager = LinearLayoutManager(ctx.context!!)
        adaptador = SurveysAdapter(dataChallenges, callback)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager
        list.adapter = adaptador
    }

    override fun ongetSurveysErrorResponse(message: String) {
    }

    override fun initView() {
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser", "")!!
        val userStr = sharedPreferences.getString("_userinfo", "")!!
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
        model.getSurveys(token,user.business_unit_id, user.id)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.requireActivity())
    }


}