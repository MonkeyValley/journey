package com.example.jurney_mobile.Presenter

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jurney_mobile.Model.WeekendQuality.IQualityWeekend
import com.example.jurney_mobile.Model.WeekendQuality.QualityWeekendModel
import com.example.jurney_mobile.R
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Adapters.QualityWeekwndAdapter
import com.example.jurney_mobile.View.Fragments.QualityWeekenFragment
import com.example.jurney_mobile.View.Models.ActivitiesQualityScore
import com.example.jurney_mobile.View.Models.NotificationModel
import com.example.jurney_mobile.View.Models.QualityScoreResponse
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import kotlinx.android.synthetic.main.quality_weeken_fragment.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class QualityWeekendPresenter:IQualityWeekend.Presenter {

    private lateinit var list: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adaptador: QualityWeekwndAdapter
    private var v:View
    private var view:IQualityWeekend.View
    private var model:IQualityWeekend.Model
    private var ctx:QualityWeekenFragment
    private lateinit var token:String
    private lateinit var sharedPreferences: SharedPreferences
    private val gson = Gson()
    private lateinit var user:UserResponseModel

    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    constructor(presenter:IQualityWeekend.View, ctx:QualityWeekenFragment, v:View){
        this.view = presenter
        this.model = QualityWeekendModel(this)
        this.ctx = ctx
        this.v = v
    }

    override fun getQualityScore() {
        val qualitiScore = sharedPreferences.getString("_qualityScore", "")
        val score = gson.fromJson<QualityScoreResponse>(qualitiScore, QualityScoreResponse::class.java)

        list = v.findViewById(R.id.list_payroll_fragment)
        layoutManager = LinearLayoutManager(ctx.context)
        list.isNestedScrollingEnabled = false
        list.layoutManager = layoutManager


        if(qualitiScore != ""){
            v.progressBarQualityScore.progress = score.average_score.toInt()
            v.lblProgressQualityScore.text = score.average_score.toInt().toString() + "%"

            if(score.activities.isEmpty()){
                v.progressBarQualityScore.progress = 0
                v.empty_view_Quality_weekend.visibility = View.VISIBLE
                v.list_payroll_fragment.visibility = View.GONE
            }

            adaptador = QualityWeekwndAdapter(score.activities)

        }else{
            v.progressBarQualityScore.progress = 0
            v.lblProgressQualityScore.text = "0%"
            v.empty_view_Quality_weekend.visibility = View.VISIBLE
            v.list_payroll_fragment.visibility = View.GONE
            adaptador = QualityWeekwndAdapter(ArrayList())
        }

        list.adapter = adaptador

    }

    private fun actionNotificationQuality(){
        myRef.child("data_base").child(user.id.toString()).child("quality").child("notificationAction").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) { // This method is called once with the initial value and again
                if( dataSnapshot.value != null){
                    val value = dataSnapshot.getValue(NotificationModel::class.java)!!
                   getQualityScore()
                }
            }
            override fun onCancelled(error: DatabaseError) { // Failed to read value
                Log.e("ERROR", "No es posible conectar con FIREBASE")
            }
        })
    }
    override fun initData() {
        sharedPreferences = ctx.context!!.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        token = sharedPreferences.getString("_tokenuser","")!!
        val userStr = sharedPreferences.getString("_userinfo","")
        user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
    }

    override fun unauthorizedError() {
        utilsClass().logOut(ctx.activity!!)
    }

}