package com.example.jurney_mobile

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.jurney_mobile.Utils.EnviromentConstants
import com.example.jurney_mobile.Utils.SnackAlert
import com.example.jurney_mobile.View.Home
import com.example.jurney_mobile.View.Models.ExtrasModel
import com.example.jurney_mobile.View.Models.ExtrasRedeemModel
import com.example.jurney_mobile.View.Models.NotificationModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationExtrasRealModel
import com.example.jurney_mobile.View.Models.RealmModels.NotificationRealmModel
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import io.realm.Realm
import io.realm.exceptions.RealmException
import java.lang.Exception
import kotlin.math.max

class MyFirebaseMessagingService: FirebaseMessagingService(){

    lateinit var realm:Realm
    lateinit var sharedPreferences: SharedPreferences
    val gson = Gson()
    var database = FirebaseDatabase.getInstance()
    var myRef = database.getReferenceFromUrl(EnviromentConstants().firebase_server)

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChanel:NotificationChannel
    lateinit var builder : Notification.Builder
    lateinit var not : NotificationCompat.Builder
    private val channelID = "com.example.jurney_mobile"
    private val description ="Notificaciones para el usuario"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val TAG = "FirebaseMessagingService"

        Realm.init(this.applicationContext)
        realm = Realm.getDefaultInstance()
        sharedPreferences = baseContext.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        if(remoteMessage.data.isNotEmpty()){
            Log.e("DATA", remoteMessage.data.toString())
            val data=  remoteMessage.data
            var title = ""
            var body = ""
            var type = ""
            var description = ""
            var extras = ExtrasModel()
            var extrasRedeem = ExtrasRedeemModel()

            if( data.get("title") != null){
                 title = data.get("title")!!
            }
            if( data.get("body") != null){
                 body = data.get("body")!!
            }
            if( data.get("type") != null){
                 type = data.get("type")!!
            }
            if( data.get("description") != null){
                description = data.get("description")!!
            }
            if( data.get("extra") != null){
                if(type == "prize_redeem"){
                    try{ extrasRedeem = gson.fromJson( data.get("extra")!!, ExtrasRedeemModel::class.java) }catch (e:Exception){ }
                }else{
                    try{ extras = gson.fromJson( data.get("extra")!!, ExtrasModel::class.java)}catch (e:Exception){ }
                }
            }

            if(type != null && type != ""){
                notificationShow(title, body, type, extras )
                notificationOption(title, body, type, extras, extrasRedeem, description)
            }
        }
        if (remoteMessage.notification != null) {
        }
    }

    fun notificationShow(title:String, text:String, type:String, dataExtra:ExtrasModel) {

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(baseContext, Home::class.java)
        intent.putExtra("isNotificastion", type)
        if (dataExtra != null) {
            try {
                intent.putExtra("data", gson.toJson(dataExtra))
            } catch (e: Exception) {

            }
        }
        val pendingIntent = PendingIntent.getActivity(baseContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChanel =
                NotificationChannel(channelID, description, NotificationManager.IMPORTANCE_LOW)
            notificationChanel.enableLights(true)
            notificationChanel.lightColor = Color.RED
            notificationChanel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChanel)

            /*builder = Notification.Builder(baseContext, channelID)
                .setContentText(text)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle().bigText(""))*/

            not = NotificationCompat.Builder(baseContext, channelID)
                .setSmallIcon(R.drawable.ic_circle_viva)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(text)
                )
        } else {
            /*builder = Notification.Builder(baseContext)
                .setContentText(text)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)*/
            not = NotificationCompat.Builder(baseContext)
                .setSmallIcon(R.drawable.ic_circle_viva)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(text)
                )

        }
//        notificationManager.notify(100, builder.build())
        val rnds = (0..10).random()
        notificationManager.notify(rnds, not.build())
    }

    private fun notificationOption(title:String, body:String, type:String, extras:ExtrasModel, extrasRedeemModel: ExtrasRedeemModel, description:String) {

        realm.beginTransaction()
        try {
            val maxId = realm.where(NotificationRealmModel::class.java).max("id")


            var notifiRealm = realm.createObject(NotificationRealmModel::class.java)
            var extrasReal = realm.createObject(NotificationExtrasRealModel::class.java)
            if(maxId == null) {
                notifiRealm.id = 0
            }else{
                notifiRealm.id = maxId.toInt() + 1
            }
            notifiRealm.title = title
            notifiRealm.body = body
            notifiRealm.type = type
                extrasReal.description = extras.description
                extrasReal.id = extras.id
                extrasReal.lvl = extras.lvl
                if(maxId == null) {
                    notifiRealm.id = 0
                    extrasReal.notificationId = 0
                }else{
                    extrasReal.notificationId = maxId.toInt() + 1
                }

                extrasReal.points = extras.points.toInt()
            notifiRealm.extras = extrasReal
            notifiRealm.description = description
            notifiRealm.view = false
            realm.commitTransaction()
        }catch (e:RealmException){}

        val userStr = sharedPreferences.getString("_userinfo", "")
        val ext = extras.description.split("-")

        if(ext.size > 1 && ext.get(1) == "challenge_modal"){
            val delete = sharedPreferences.edit()
            delete.remove("_isfirsttime")

            val editor = sharedPreferences.edit()
            editor.putBoolean("_isfirsttime", true)
            editor.apply()
        }

        if (userStr != "") {
            var user = gson.fromJson<UserResponseModel>(userStr, UserResponseModel::class.java)
            var isView = false
            when (type) {
                "quality" -> isView = true
                "new_survey" -> {
                    val editor = sharedPreferences.edit()
                    editor.putInt("_idsurvey", extras.id)
                    editor.apply()
                }
            }
            val value = NotificationModel(title, body, isView, type, extras, extrasRedeemModel, description)
            myRef.child("data_base").child(user.id.toString()).child(type).child("notificationAction").setValue(value)
        }
    }
}