package com.example.jurney_mobile.Model.Awards

import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.example.jurney_mobile.View.Models.GeneratorCodeResponseModel

interface IAwards {

    interface View{}

    interface Presenter{
        fun initInfo()
    }

    interface Model{}

    interface ViewCodes{}
    interface PresenterCodes {
        fun getExchangeCodeResponse(response: GeneratorCodeResponseModel)
        fun getExchangeCodeErrorResponse(message: String)

        fun getListCodesResponse(response: ArrayList<CodeResponseModel>)
        fun getListCodesErrorResponse(message: String)

        fun unauthorizedError()
        fun initInfo()
    }
    interface ModelCodes{
        fun getExchangeCode(token:String, collaborator_id:Int, prize_id:Int)
        fun getListCodes(token: String)
    }
}
