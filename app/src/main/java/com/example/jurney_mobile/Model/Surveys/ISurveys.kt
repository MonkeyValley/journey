package com.example.jurney_mobile.Model.Surveys

import android.view.View
import com.example.jurney_mobile.View.Models.*

interface ISurveys {

    interface View{
    }
    interface Presenter{
        fun onClickItemActivity(item: SurveysListModel, position:Int)
        fun ongetSurveysResponse(response: ResponseGetSurveys)
        fun ongetSurveysErrorResponse(message: String)
        fun initView()
        fun unauthorizedError()
    }
    interface Model{
        fun getSurveys(token:String, business_unit_id:Int, collaborator_id:Int)
    }


    interface ViewActivity{
        fun closeAlert()
        fun showAlert(response:ResponseSendSurvey)
    }
    interface PresenterActivity{
        fun onClickSurvey(v:android.view.View)
        fun onPageSelected (pos:Int)
        fun getData(request:Int)
        fun initView()
        fun clickCallBack(item: Int, position: Int, selected:Boolean)

        fun unauthorizedError()


        fun onSurveyByIdResponse(response:EncuestaModel)
        fun onSurveyByIdErrorResponse(message:String)

        fun onSendSurveyResponse(response: ResponseSendSurvey)
        fun onSendSurveyErrorResponse(message: String)

    }
    interface ModelActivity{
        fun getData(request:Int, token:String)
        fun sendSurvey(request: RequestEncuestaMultipleModel, token:String)
    }
}