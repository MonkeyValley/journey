package com.example.jurney_mobile.Model.Complaints

import android.view.View
import com.example.jurney_mobile.View.Models.ComplaintRequest
import com.example.jurney_mobile.View.Models.ComplaintResponse

interface IComplaint {

    interface View{
    }

    interface Presenter{
        fun onClick(v:android.view.View)
    }

    interface Model{
    }

    interface ViewActivity{
        fun close()
    }

    interface PresenterActivity{
        fun onClick(v:android.view.View)
        fun onPageSelected(position: Int)
        fun initInfo()
        fun selectedItem(item: Int, cat: String)
        fun unauthorizedError()
        fun sendComplaintResponse(response:ComplaintResponse)
        fun sendComplaintErrorResponse(message:String)
    }

    interface ModelActivity{
        fun sendComplaint(token:String, body:ComplaintRequest)
    }
}