package com.example.jurney_mobile.Model.Ranking

import com.example.jurney_mobile.View.Models.RankingResponseModel
import com.example.jurney_mobile.View.Models.UserRankingResponseModel

interface IRanking {

    interface View{}

    interface Presenter{
        fun initView()
        fun unauthorizedError()
    }

    interface Model{}

    interface ViewMy{}

    interface PresenterMy{
        fun initView()
        fun unauthorizedError()

        fun getUserByNameErrorResponse(message:String)
        fun getUserByNameResponse(response: RankingResponseModel)
    }

    interface ModelMy {
        fun getUserByName(token:String, business_unit_id:Int, name:String)
    }
}