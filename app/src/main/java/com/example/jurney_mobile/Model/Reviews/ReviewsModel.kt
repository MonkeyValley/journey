package com.example.jurney_mobile.Model.Reviews

import android.util.Log
import com.example.jurney_mobile.View.Models.SupervisorsResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewsModel: IReviews.Model {

    var presenter:IReviews.Presenter

    constructor(presenter:IReviews.Presenter){
        this.presenter = presenter
    }

    override fun getSupervisors(token: String) {
        RetrofitClient.instance.getSupervisors("Bearer "+token).enqueue(object:
            Callback<ArrayList<SupervisorsResponseModel>> {
            override fun onFailure(call: Call<ArrayList<SupervisorsResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getSupervisorsErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<SupervisorsResponseModel>>, response: Response<ArrayList<SupervisorsResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getSupervisorsResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getSupervisorsErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getSupervisorsErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }
}