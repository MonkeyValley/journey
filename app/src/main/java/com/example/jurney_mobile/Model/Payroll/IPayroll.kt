package com.example.jurney_mobile.Model.Payroll

import android.view.View
import com.example.jurney_mobile.View.Models.ComplainPayrollResponseModel
import com.example.jurney_mobile.View.Models.ComplaintPayrollRequest
import com.example.jurney_mobile.View.Models.ComplaintPayrollResponse
import com.example.jurney_mobile.View.Models.DailypayResponseModel

interface IPayroll {
    interface View{}
    interface Presenter{
        fun initInfo()
        fun onClick(v:android.view.View)
        fun getDetailFromNotification(id:Int)
        fun unauthorizedError()

        fun sendComplaintResponse(response:ComplaintPayrollResponse)
        fun sendComplaintErrorResponse(message:String)

        fun getComplainDetailResponse(response:ComplainPayrollResponseModel)
        fun getComplainDetailErrorResponse(message:String)

        fun getDailyPayListResponse(response: DailypayResponseModel)
        fun getDailyPayListErrorResponse(response: String, errorCode:String)
    }
    interface Model{
        fun sendComplaint(token:String, data:ComplaintPayrollRequest)
        fun getComplainDetail(token:String, id:Int)
        fun getDailyPayList(token:String, collaborator_id:Int)
    }
}