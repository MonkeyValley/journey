package com.example.jurney_mobile.Model.Reviews

import android.util.Log
import com.example.jurney_mobile.View.Models.ReviewModelResponse
import com.example.jurney_mobile.View.Models.ReviewRequestModel
import com.example.jurney_mobile.View.Models.SendReviewsResponseModel
import com.example.jurney_mobile.View.Models.SupervisorsResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SliderReviewModel:IReviews.SliderModel {

    var presenter:IReviews.SliderPresenter

    constructor(presenter:IReviews.SliderPresenter) {
        this.presenter = presenter
    }

    override fun getReviewById(token: String, idReview: Int) {
        RetrofitClient.instance.getReviewById("Bearer "+token, idReview).enqueue(object:
            Callback<ReviewModelResponse> {
            override fun onFailure(call: Call<ReviewModelResponse>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getReviewByIdErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ReviewModelResponse>, response: Response<ReviewModelResponse>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getReviewByIdResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getReviewByIdErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getReviewByIdErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }

    override fun sendReview(token: String, body: ReviewRequestModel) {
        RetrofitClient.instance.sendReview("Bearer "+token, body).enqueue(object:
            Callback<SendReviewsResponseModel> {
            override fun onFailure(call: Call<SendReviewsResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.sendReviewErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<SendReviewsResponseModel>, response: Response<SendReviewsResponseModel>) {
                if(response.code() == 200 || response.code() == 201){
                    if(response.body() != null){
                        presenter.sendReviewResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.sendReviewErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendReviewErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }

}