package com.example.jurney_mobile.Model.Surveys

import android.util.Log
import com.example.jurney_mobile.View.Models.EncuestaModel
import com.example.jurney_mobile.View.Models.RequestEncuestaMultipleModel
import com.example.jurney_mobile.View.Models.ResponseSendSurvey
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SurveyActivityModel:ISurveys.ModelActivity {

    private var presenter:ISurveys.PresenterActivity
    private var gson = Gson()

    constructor(presenter:ISurveys.PresenterActivity){
        this.presenter = presenter
    }

    override fun getData(request: Int, token:String) {
        RetrofitClient.instance.getSurveyByID("Bearer "+ token, request).enqueue(object:Callback<EncuestaModel>{
            override fun onFailure(call: Call<EncuestaModel>, t: Throwable) {
                presenter.onSurveyByIdErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<EncuestaModel>, response: Response<EncuestaModel>) {
                if(response.code() >= 200 && response.code() <= 205){
                    if(response.body()!!.errors == null){
                        presenter.onSurveyByIdResponse(response.body()!!)
                    }else if(response.code() == 401){
                        presenter.unauthorizedError()
                    }else{
                        presenter.onSurveyByIdErrorResponse("Error al obtener la informacion, intentelo más tarde.")
                    }
                }else{
                    presenter.onSurveyByIdErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
                }
            }

        })
    }

    override fun sendSurvey(request: RequestEncuestaMultipleModel, token:String) {
        RetrofitClient.instance.sendSurvey("Bearer "+ token, request).enqueue(object:Callback<ResponseSendSurvey>{
            override fun onFailure(call: Call<ResponseSendSurvey>, t: Throwable) {
                presenter.onSendSurveyErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<ResponseSendSurvey>, response: Response<ResponseSendSurvey>) {
                if(response.code() >= 200 && response.code() <= 205){
                    if(response.body()!!.errors == null){
                        presenter.onSendSurveyResponse(response.body()!!)
                    }else if(response.code() == 401){
                        presenter.unauthorizedError()
                    }else{
                        presenter.onSendSurveyErrorResponse(response.body()!!.errors)
                    }

                }else{
                    presenter.onSendSurveyErrorResponse("Error al obtener datos, intente mas tarde.")
                }
            }
        })
    }
}