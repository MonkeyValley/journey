package com.example.jurney_mobile.Model.Profile

import android.util.Log
import com.example.jurney_mobile.View.Models.GetoutResponseModel
import com.example.jurney_mobile.View.Models.UpdateImageRequest
import com.example.jurney_mobile.View.Models.UploadImageResponse
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import id.zelory.compressor.Compressor
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class ProfileModel:IProfile.Model {

    private  var presenter:IProfile.Presenter
    constructor(presenter:IProfile.Presenter) {
        this.presenter = presenter
    }

    override fun updateImage(token:String, data:String, id:Int) {

        RetrofitClient.instance.updateImage("Bearer "+token, UpdateImageRequest(data) , id).enqueue(object:Callback<UserResponseModel> {
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.loadImageHTTPCLIENTErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(
                call: Call<UserResponseModel>,
                response: Response<UserResponseModel>
            ) {
                Log.e("------",response.body().toString())

                if (response.code() == 200 || response.code() == 201){
                    presenter.updateImageResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.loadImageHTTPCLIENTErrorResponse("Error al editar la foto")
                }
            }

        })
    }

    override fun loadImageHTTPCLIENT(token: String, file:File) {

        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        val body = MultipartBody.Part.createFormData("file", file.getName(), requestFile)
        val fullName = RequestBody.create(MediaType.parse("text/plain"), "collaborator/profile");


        RetrofitClient.instance.uploadImage("Bearer "+token, body, fullName).enqueue(object:Callback<UploadImageResponse> {
            override fun onFailure(call: Call<UploadImageResponse>, t: Throwable) {
                presenter.loadImageHTTPCLIENTErrorResponse(t.message!!)
            }

            override fun onResponse(
                call: Call<UploadImageResponse>,
                response: Response<UploadImageResponse>
            ) {
                Log.e("------",response.body().toString())

                if (response.code() == 200 || response.code() == 201){
                    presenter.loadImageHTTPCLIENTResponse(response.body()!!)
                }else{
                    presenter.loadImageHTTPCLIENTErrorResponse("Error al cargar la foto")
                }
            }

        })
    }

    override fun cancelLeaving(token: String, id_leaving: Int) {
        RetrofitClient.instance.putCancelGetout("Bearer "+token , id_leaving).enqueue(object:Callback<GetoutResponseModel> {
            override fun onFailure(call: Call<GetoutResponseModel>, t: Throwable) {
                presenter.cancelLeavingErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<GetoutResponseModel>, response: Response<GetoutResponseModel>) {
                Log.e("------",response.body().toString())

                if (response.code() == 200 || response.code() == 201){
                    presenter.cancelLeavingResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.cancelLeavingErrorResponse("Error al editar usuario, intente mas tarde")
                }
            }

        })
    }

    override fun getInfoUser(token: String) {
        RetrofitClient.instance.getUserInfo("Bearer "+ token).enqueue(object :Callback<UserResponseModel>{
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.getInfoUserErrorRespose("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<UserResponseModel>, response: Response<UserResponseModel>) {
                if (response.code() == 200 && response.body() != null) {
                    presenter.getInfoUserResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoUserErrorRespose("Error al obtener información, intentelo más tarde.")
                }
            }

        })
    }
}
