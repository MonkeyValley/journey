package com.example.jurney_mobile.Model.Profile

import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import com.example.jurney_mobile.View.Models.GetoutResponseModel
import com.example.jurney_mobile.View.Models.UploadImageResponse
import com.example.jurney_mobile.View.Models.UserResponseModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

interface IProfile {

    interface View{

    }

    interface Presenter{
        fun initInfo()
        fun click(v:android.view.View)
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
        fun loadImageHTTPCLIENTResponse(response:UploadImageResponse)
        fun loadImageHTTPCLIENTErrorResponse(message:String)
        fun updateImageResponse(response:UserResponseModel)
        fun onResume()

        fun cancelLeavingResponse(response:GetoutResponseModel)
        fun cancelLeavingErrorResponse(message:String)
        fun unauthorizedError()

        fun getInfoUserErrorRespose(message: String)
        fun getInfoUserResponse(response:UserResponseModel)
    }

    interface Model{
        fun updateImage(token:String, data:String, id:Int)
        fun loadImageHTTPCLIENT(token:String, file:File )
        fun cancelLeaving(token:String, id_leaving:Int)
        fun getInfoUser(token: String)
    }

}