package com.example.jurney_mobile.Model.DailyPay

import com.example.jurney_mobile.View.Models.DayliPayRequestModel
import com.example.jurney_mobile.View.Models.DayliPayRequestResponseModel

interface IDailypay {

    interface View{}

    interface Presenter{
        fun initInfo()

        fun unauthorizedError()

        fun sendReqDailypayResponse(response:DayliPayRequestResponseModel)
        fun sendReqDailypayErrorResponse(message:String)

    }

    interface  Model{
        fun sendReqDailypay(token:String, collaborator_id:Int, data:DayliPayRequestModel  )
    }
}