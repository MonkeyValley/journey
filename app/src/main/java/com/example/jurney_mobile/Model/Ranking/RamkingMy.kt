package com.example.jurney_mobile.Model.Ranking

import android.util.Log
import com.example.jurney_mobile.View.Models.RankingResponseModel
import com.example.jurney_mobile.View.Models.UserRankingResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RamkingMy:IRanking.ModelMy {

    var presenter: IRanking.PresenterMy

    constructor(presenter: IRanking.PresenterMy){
        this.presenter = presenter
    }

    override fun getUserByName(token: String, business_unit_id:Int, name:String) {
        RetrofitClient.instance.getUserRanking("Bearer "+token, name, business_unit_id ).enqueue(object:
            Callback<RankingResponseModel> {
            override fun onFailure(call: Call<RankingResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getUserByNameErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<RankingResponseModel>, response: Response<RankingResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getUserByNameResponse(response.body()!!)
                    }else{
                        presenter.getUserByNameErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getUserByNameErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }
}