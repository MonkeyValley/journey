package com.example.jurney_mobile.Model.WeekendQuality

import com.example.jurney_mobile.View.Models.QualityScoreResponse
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QualityWeekendModel:IQualityWeekend.Model
{
    var presenter:IQualityWeekend.Presenter
    constructor(presenter:IQualityWeekend.Presenter) {
        this.presenter = presenter
    }
}