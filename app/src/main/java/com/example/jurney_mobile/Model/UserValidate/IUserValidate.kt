package com.example.jurney_mobile.Model.UserValidate

import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import com.example.jurney_mobile.View.Models.*

interface IUserValidate {

    interface View{
        //fun onValidateResponse(response:String)
    }

    interface Presenter{
        fun onValidateResponse(response:String)
        fun onValidateError(msj:String)
        fun onValidate(data1:String, data2:String, data3:String, data4:String)
        fun onKeyPressed(v: android.view.View, keyCode: Int, event: KeyEvent):Boolean
        fun afterTextChanged()
        fun initTextFocus(editText:EditText)
        fun onBackPressed()
        fun initInfo()
        fun onClick(v:android.view.View)

        fun getInfoErrorResponse(message:String)
        fun getInfoResponse(response:PaymentModel)
        fun getInfoUserErrorRespose(message: String)
        fun getInfoUserResponse(response:UserResponseModel)
        fun getTotalIncomeErrorResponse(message:String)
        fun getTotalIncomeResponse(response: TotalIncomeModel)

        fun getInfoTwoLastWeeksResponse(response:TwoLastWeekendModel)
        fun getInfoTwoLastWeeksErrorResponse(message: String)

        fun onResendCodeResponse(response: SmsResponseModel)
        fun onResendCodeErrorResponse(message:String)

        fun getListAwardsResponse(response:ArrayList<AwardsResponseModel>)
        fun getListAwardsErrorResponse(message:String)

        fun getQualityScoreResponse(response: QualityScoreResponse)
        fun getQualityScoreErrorResponse(message:String)

        fun getListChallengesResponse(response:ChallengesResponseModel)
        fun getListChallengesErrorResponse(message:String)

        fun getVacanciesResponse(response:ArrayList<VacancyResponseModel>)
        fun getVacanciesErrorResponse(message:String)

        fun getListCodesResponse(response:ArrayList<CodeResponseModel>)
        fun getListCodesErrorResponse(message:String)

        fun getReasonsPayrollComplainResponse(response:ArrayList<ReasonsPayrollComplainModel>)
        fun getReasonsPayrollComplainErrorResponse(message:String)

        fun getTopTenResponse(response:ArrayList<UserRankingResponseModel>)
        fun getTopTenErrorResponse(message:String)

        fun getMyRankingResponse(response:ArrayList<UserRankingResponseModel>)
        fun getMyRankingErrorResponse(message:String)

        fun getDailyPayListResponse(response: DailypayResponseModel)
        fun getDailyPayListErrorResponse(response: String, errorCode:String)

        fun getNewCodeResponse(response: newSmsResponseModel)
        fun getNewCodeErrorResponse(message:String)

        fun getSupervisorsResponse(response:ArrayList<SupervisorsResponseModel>)
        fun getSupervisorsErrorResponse(message:String)

        fun unauthorizedError()

    }

    interface Model{
        fun onResendCode(noEmpleado: String)
        fun onValidate(data1:String, data2:String, data3:String, data4:String, phone:String)
        fun getWeekendPayment(token: String, employee_code: String, business_unit_id: String)
        fun getInfoUser(token: String)
        fun getTotalIncome(token: String, employee_code:String, business_unit_id:String)
        fun getInfoTwoLastWeeks(token: String, employee_code: String, business_unit_id: String)
        fun getListAwards(token:String)
        fun getListCodes(token: String)
        fun getTopTen(token:String)
        fun getMyRanking(token:String)
        fun getQualityScore(token:String, user_id:String, busines_id:String, date_to:String, dateFrom:String)
        fun getListChallenges(token: String, collaborator_id: Int)
        fun getVacancies(token:String, collaborator_id:Int)
        fun getReasonsPayrollComplain(token:String)
        fun getDailyPayList(token:String, collaborator_id:Int)
        fun getNewCode(phone:String)
        fun getSupervisors(token:String)
    }
}