package com.example.jurney_mobile.Model.GetOut

import android.content.Context
import android.util.Log
import com.example.jurney_mobile.View.Models.GetoutResponseModel
import com.example.jurney_mobile.View.Models.PaymentModel
import com.example.jurney_mobile.View.Models.ReasonRequestModel
import com.example.jurney_mobile.View.Models.ReasonsResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetoutModel: IGetout.Model {

    var presenter:IGetout.Presenter
    var ctx:Context
    constructor(ctx: Context, presenter:IGetout.Presenter ){
        this.ctx = ctx
        this.presenter = presenter
    }

    override fun getReasons(token: String) {
        RetrofitClient.instance.getReasons( "Bearer "+ token ).enqueue(object : Callback<ArrayList<ReasonsResponseModel>> {
            override fun onFailure(call: Call<ArrayList<ReasonsResponseModel>>, t: Throwable) {
                presenter.getReasonsErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<ArrayList<ReasonsResponseModel>>, response: Response<ArrayList<ReasonsResponseModel>>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.getReasonsResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getReasonsErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }

    override fun sendGetOut(token: String, date:String, reason_for_leaving_id:Int) {
        RetrofitClient.instance.sendReason( "Bearer "+ token, ReasonRequestModel(date, reason_for_leaving_id)  ).enqueue(object : Callback<GetoutResponseModel> {
            override fun onFailure(call: Call<GetoutResponseModel>, t: Throwable) {
                presenter.sendGetOutErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<GetoutResponseModel>, response: Response<GetoutResponseModel>) {
                if(response.code() == 200 || response.code() == 201){
                    Log.e("----->", response.body()!!.toString())
                    presenter.sendGetOutResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendGetOutErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }


}