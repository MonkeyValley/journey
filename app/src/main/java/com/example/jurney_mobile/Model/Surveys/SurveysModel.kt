package com.example.jurney_mobile.Model.Surveys

import com.example.jurney_mobile.View.Models.ResponseGetSurveys
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SurveysModel:ISurveys.Model {

    var presenter:ISurveys.Presenter

    constructor(presenter:ISurveys.Presenter){
        this.presenter = presenter
    }

    override fun getSurveys(token: String, business_unit_id: Int, collaborator_id:Int) {
        RetrofitClient.instance.getSurveys("Bearer "+ token, business_unit_id, collaborator_id, 1 ).enqueue(object :
            Callback<ResponseGetSurveys> {
            override fun onFailure(call: Call<ResponseGetSurveys>, t: Throwable) {
                presenter.ongetSurveysErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<ResponseGetSurveys>, response: Response<ResponseGetSurveys>) {
                if(response.code() == 200){
                    presenter.ongetSurveysResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.ongetSurveysErrorResponse("Error al obtener los datos, intente de nuevo." )
                }
            }
        })
    }

}