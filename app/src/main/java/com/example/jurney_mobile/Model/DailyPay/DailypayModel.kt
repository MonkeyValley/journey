package com.example.jurney_mobile.Model.DailyPay

import android.util.Log
import com.example.jurney_mobile.View.Models.DayliPayRequestModel
import com.example.jurney_mobile.View.Models.DayliPayRequestResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DailypayModel: IDailypay.Model {

    var presenter:IDailypay.Presenter

    constructor(presenter:IDailypay.Presenter){
        this.presenter = presenter
    }

    override fun sendReqDailypay(token: String, collaborator_id: Int, data: DayliPayRequestModel) {

        Log.e("SEND", Gson().toJson(data))
        RetrofitClient.instance.sendReqDailyPay("Bearer "+ token,  data, collaborator_id  ).enqueue(object:Callback<DayliPayRequestResponseModel> {
            override fun onFailure(call: Call<DayliPayRequestResponseModel>, t: Throwable) {
                presenter.sendReqDailypayErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<DayliPayRequestResponseModel>, response: Response<DayliPayRequestResponseModel>) {
                if(response.code() == 200 || response.code() == 201){
                    presenter.sendReqDailypayResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendReqDailypayErrorResponse("Ya haz hecho una peticion para este dia")
                }
            }
        })

    }
}