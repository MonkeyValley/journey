package com.example.jurney_mobile.Model.Vacancies

import android.view.View
import com.example.jurney_mobile.View.Models.SendVacancyResponseModel

interface IVacancies {
    interface View{}
    interface Presenter{
        fun initInfo(v:android.view.View)
        fun sendVacancyResponse(response:SendVacancyResponseModel)
        fun sendVacancyErrorResponse(message:String)

        fun unauthorizedError()

    }
    interface Model{
        fun sendVacancy(token:String, collaborator_id:Int, vacant_id:Int, origin:String, start_date:String)
        fun sendVacancySingle(token:String, collaborator_id:Int, vacant_id:Int)
    }
}