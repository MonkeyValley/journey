package com.example.jurney_mobile.Model.Splash

import android.telephony.SmsMessage
import com.example.jurney_mobile.View.Models.*

interface ISplash {

    interface View{}

    interface Presenter{
        fun intiInfo()
        fun getInfoWeekendPaysheetResponse(response: PaymentModel)
        fun getInfoWeekendPaysheetErrorResponse(message:String)
        fun getUserInfoResponse(response: UserResponseModel)
        fun getUserInfoErrorRespose(message:String)
        fun getTotalIncomeResponse(response: TotalIncomeModel)
        fun getTotalIncomeErrorResponse(message:String)
        fun getInfoTwoLastWeeksResponse(response:TwoLastWeekendModel)
        fun getInfoTwoLastWeeksErrorResponse(message:String)

        fun getQualityScoreResponse(response: QualityScoreResponse)
        fun getQualityScoreErrorResponse(message:String)
        fun getQualityScore()

        fun getListChallengesResponse(response:ChallengesResponseModel)
        fun getListChallengesErrorResponse(message:String)

        fun getListChallengesCompletesResponse(response:ChallengesCompleteResponseModel)
        fun getListChallengesCompletesErrorResponse(message:String)

        fun getListChallengesAvailablesResponse(response:ArrayList<ChallengesAvailableResponse>)
        fun getListChallengesAvailablesErrorResponse(message:String)

        fun getListAwardsResponse(response:ArrayList<AwardsResponseModel>)
        fun getListAwardsErrorResponse(message:String)

        fun getListCodesResponse(response:ArrayList<CodeResponseModel>)
        fun getListCodesErrorResponse(message:String)

        fun getTopTenResponse(response:ArrayList<UserRankingResponseModel>)
        fun getTopTenErrorResponse(message:String)

        fun getMyRankingResponse(response:ArrayList<UserRankingResponseModel>)
        fun getMyRankingErrorResponse(message:String)

        fun getVacanciesResponse(response:ArrayList<VacancyResponseModel>)
        fun getVacanciesErrorResponse(message:String)

        fun getReasonsPayrollComplainResponse(response:ArrayList<ReasonsPayrollComplainModel>)
        fun getReasonsPayrollComplainErrorResponse(message:String)

        fun getDailyPayListResponse(response: DailypayResponseModel)
        fun getDailyPayListErrorResponse(response: String, errorCode:String)

        fun getSupervisorsResponse(response:ArrayList<SupervisorsResponseModel>)
        fun getSupervisorsErrorResponse(message:String)

        fun getListChallengesHistoryResponse(response:ArrayList<HistoryChallengesResponseModel>)
        fun getListChallengesHistoryErrorResponse(message:String)

        fun unauthorizedError()
    }

    interface Model{
        fun getInfoWeekendPaysheet(token:String, employee_code:String, business_unit_id:String)
        fun getUserInfo(token:String)
        fun getTotalIncome(token:String, employee_code:String, business_unit_id:String)
        fun getInfoTwoLastWeeks(token:String, employee_code:String, business_unit_id:String)
        fun getQualityScore(token:String, user_id:String, busines_id:String, date_to:String, dateFrom:String)
        fun getListChallenges(token: String, collaborator_id: Int)
        fun getListChallengesAvailables(token: String)
        fun getListChallengesCompletes(token: String)
        fun getListChallengesHistory(token: String)
        fun getListAwards(token:String)
        fun getListCodes(token: String)
        fun getTopTen(token:String)
        fun getMyRanking(token:String)
        fun getVacancies(token:String, collaborator_id:Int)
        fun getReasonsPayrollComplain(token:String)
        fun getDailyPayList(token:String, collaborator_id:Int)
        fun getSupervisors(token:String)
    }
}