package com.example.jurney_mobile.Model.CreatePassword

import com.example.jurney_mobile.View.Models.UpdatePasswordRequest
import com.example.jurney_mobile.View.Models.UserResponseModel

interface ICreatePassword {

    interface View{}
    interface Presenter{
        fun initInfo()
        fun unauthorizedError()

        fun updatePasswordResponse(response: UserResponseModel)
        fun updatePasswordErrorResponse(message:String)
    }
    interface Model{
        fun updatePassword(token:String, collaborator_id:Int, data:UpdatePasswordRequest)
    }
}