package com.example.jurney_mobile.Model.CreatePassword

import android.util.Log
import com.example.jurney_mobile.View.Models.UpdatePasswordRequest
import com.example.jurney_mobile.View.Models.UserResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreatePasswordModel:ICreatePassword.Model {

        var presenter:ICreatePassword.Presenter

        constructor(presenter:ICreatePassword.Presenter){
            this.presenter = presenter
        }

    override fun updatePassword(token: String, collaborator_id: Int, data: UpdatePasswordRequest) {
        Log.e("SEND", Gson().toJson(data))

        RetrofitClient.instance.updatePassword("Bearer "+ token,  data, collaborator_id  ).enqueue(object:Callback<UserResponseModel> {
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.updatePasswordErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<UserResponseModel>, response: Response<UserResponseModel>) {
                if(response.code() == 200 || response.code() == 201){
                    presenter.updatePasswordResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.updatePasswordErrorResponse("Error")
                }
            }
        })

    }

}
