package com.example.jurney_mobile.Model.Reviews

import android.content.Intent
import android.view.View
import com.example.jurney_mobile.View.Models.ReviewModelResponse
import com.example.jurney_mobile.View.Models.ReviewRequestModel
import com.example.jurney_mobile.View.Models.SendReviewsResponseModel
import com.example.jurney_mobile.View.Models.SupervisorsResponseModel

interface IReviews {

    interface View{
    }

    interface Presenter{
        fun initView(v:android.view.View, reload:Boolean)
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

        fun getSupervisorsResponse(response:ArrayList<SupervisorsResponseModel>)
        fun getSupervisorsErrorResponse(message:String)
        fun unauthorizedError()

    }
    interface Model{
        fun getSupervisors(token:String)
    }

    interface SliderView{
    }

    interface SliderPresenter{
        fun initView()
        fun getReviewByIdResponse(response:ReviewModelResponse)
        fun getReviewByIdErrorResponse(message:String)

        fun sendReviewResponse(response:SendReviewsResponseModel)
        fun sendReviewErrorResponse(message:String)

        fun unauthorizedError()

    }
    interface SliderModel{
        fun getReviewById(token:String, idReview:Int)
        fun sendReview(token:String, body:ReviewRequestModel)
    }
}