package com.example.jurney_mobile.Model.Payroll

import android.util.Log
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PayrollModel:IPayroll.Model {

    private var presenter:IPayroll.Presenter
    constructor(presenter:IPayroll.Presenter)
    {
        //Comentario
        this.presenter = presenter
    }

    override fun sendComplaint(token:String, data: ComplaintPayrollRequest) {
        Log.e("SEND", Gson().toJson(data))
        RetrofitClient.instance.sendComplain("Bearer "+ token, data  ).enqueue(object :
            Callback<ComplaintPayrollResponse> {
            override fun onFailure(call: Call<ComplaintPayrollResponse>, t: Throwable) {
                presenter.sendComplaintErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<ComplaintPayrollResponse>, response: Response<ComplaintPayrollResponse>) {
                if(response.code() == 200){
                    presenter.sendComplaintResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendComplaintErrorResponse("Ya enviaste una queja de este día.")
                }
            }
        })
    }

    override fun getComplainDetail(token: String, id: Int) {
        RetrofitClient.instance.getComplainPayroll("Bearer "+ token, id  ).enqueue(object :
            Callback<ComplainPayrollResponseModel> {
            override fun onFailure(call: Call<ComplainPayrollResponseModel>, t: Throwable) {
                presenter.getComplainDetailErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<ComplainPayrollResponseModel>, response: Response<ComplainPayrollResponseModel>) {
                if(response.code() == 200){
                    presenter.getComplainDetailResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getComplainDetailErrorResponse("Error al obtener datos.")
                }
            }
        })
    }

    override fun getDailyPayList(token: String, collaborator_id: Int) {
        RetrofitClient.instance.getDailyPay("Bearer "+token, collaborator_id).enqueue(object: Callback<DailypayResponseModel> {
            override fun onFailure(call: Call<DailypayResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getDailyPayListErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.", "000")
            }

            override fun onResponse(call: Call<DailypayResponseModel>, response: Response<DailypayResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getDailyPayListResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getDailyPayListErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.", response.code().toString())
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getDailyPayListErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'", response.code().toString())
                }
            }
        })
    }
}