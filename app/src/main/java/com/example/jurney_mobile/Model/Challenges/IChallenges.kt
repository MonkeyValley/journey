package com.example.jurney_mobile.Model.Challenges

interface IChallenges {

    interface View{
    }
    interface Presenter{
        fun initInfo()
        fun setupViewPager()
    }

    interface Model{
    }
}