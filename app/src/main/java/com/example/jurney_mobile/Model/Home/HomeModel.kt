package com.example.jurney_mobile.Model.Home

import android.util.Log
import com.example.jurney_mobile.View.Home
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeModel:IHome.Model{

    private var presenter:IHome.Presenter
    private var ctx:Home
    private var gson = Gson()

    constructor(presenter: IHome.Presenter, ctx:Home){
        this.presenter = presenter
        this.ctx = ctx
    }
    override fun updateTokenFirebase(token:String, data:String, id:Int) {

        RetrofitClient.instance.updateFirebaseToken("Bearer "+token, UpdateFirebaseTokenModel(data) , id).enqueue(object:
            Callback<UserResponseModel> {
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.updateTokenFirebaseErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(
                call: Call<UserResponseModel>,
                response: Response<UserResponseModel>
            ) {
                Log.e("------",response.body().toString())

                if (response.code() == 200 || response.code() == 201){
                    presenter.updateTokenFirebaseResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.updateTokenFirebaseErrorResponse("Error al editar la foto")
                }
            }

        })
    }

    override fun getQualityScore(token: String, user_id: String, busines_id: String, date_to: String, dateFrom: String) {
        RetrofitClient.instance.getQualityScore("Bearer "+token, dateFrom, date_to, busines_id,  user_id).enqueue(object: Callback<QualityScoreResponse> {
            override fun onFailure(call: Call<QualityScoreResponse>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }
            override fun onResponse(call: Call<QualityScoreResponse>, response: Response<QualityScoreResponse>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getQualityScoreResponse(response.body()!!)
                    }else{
                        presenter.getQualityScoreErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun sendResponsecode(token: String, response: String, id_code:Int) {
        RetrofitClient.instance.sendResponseCode("Bearer "+token, SendResRequestModel(response), id_code ).enqueue(object: Callback<SendResResponseModel> {

            override fun onFailure(call: Call<SendResResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.sendResponsecodeErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<SendResResponseModel>, response: Response<SendResResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.sendResponsecodeResponse(response.body()!!)
                    }else{
                        presenter.sendResponsecodeErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendResponsecodeErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getInfoUser(token: String) {
        RetrofitClient.instance.getUserInfo("Bearer "+ token).enqueue(object :Callback<UserResponseModel>{
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.getInfoUserErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<UserResponseModel>, response: Response<UserResponseModel>) {
                if (response.code() == 200 && response.body() != null) {
                    presenter.getInfoUserResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoUserErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }

        })
    }

    override fun confirmNotification(token: String, msg_id: Int, res:Boolean) {
        RetrofitClient.instance.sendConfirmNotification("Bearer "+ token, msg_id , ConfirmRequestModel(res)).enqueue(object :Callback<ArrayList<String>>{
            override fun onFailure(call: Call<ArrayList<String>>, t: Throwable) {
                presenter.confirmNotificationErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }
            override fun onResponse(call: Call<ArrayList<String>>, response: Response<ArrayList<String>>) {
                if (response.code() == 200) {
                    presenter.confirmNotificationResponse()
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.confirmNotificationErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }

        })

    }

//    SendResResponseModel



}