package com.example.jurney_mobile.Model.Notifications
import android.util.Log
import com.example.jurney_mobile.View.Models.NotificationModel
import retrofit2.Callback
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response

class NotificationsModel:INotifications.Model {

    private var presenter: INotifications.Presenter
    private val gson = Gson()

    private val TAG = "LOGINACTIVITY"

    constructor(presenter: INotifications.Presenter) {
        this.presenter = presenter
    }

    override fun getnotificationList(token: String, date_from: String, date_to: String, user_id: Int) {
        RetrofitClient.instance.getNotifications( "Bearer "+ token, date_from, date_to, user_id ).enqueue(object : Callback<ArrayList<NotificationModel>>{
            override fun onFailure(call: Call<ArrayList<NotificationModel>>, t: Throwable) {
                presenter.getnotificationListErrorResponse("No se pudo conectar con los servicios, intentelo más tarde.")
            }
            override fun onResponse(call: Call<ArrayList<NotificationModel>>, response: Response<ArrayList<NotificationModel>>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.getnotificationListResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getnotificationListErrorResponse("Error al obtener información, intentelo más tarde.")

                }
            }
        })
    }

}