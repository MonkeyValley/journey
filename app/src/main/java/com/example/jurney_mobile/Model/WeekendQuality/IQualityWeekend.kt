package com.example.jurney_mobile.Model.WeekendQuality

import com.example.jurney_mobile.View.Models.QualityScoreResponse

interface IQualityWeekend {

    interface View {}
    interface Presenter {
        fun initData()
        fun unauthorizedError()
        fun getQualityScore()
    }
    interface Model {}
}