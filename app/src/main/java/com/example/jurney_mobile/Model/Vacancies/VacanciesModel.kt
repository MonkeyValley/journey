package com.example.jurney_mobile.Model.Vacancies

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.jurney_mobile.Model.UserValidate.IUserValidate
import com.example.jurney_mobile.View.Models.PaymentModel
import com.example.jurney_mobile.View.Models.SendVacancyResponseModel
import com.example.jurney_mobile.View.Models.SingleVacancyRequestModel
import com.example.jurney_mobile.View.Models.VacancyRequestModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.example.jurney_mobile.View.UserValidate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VacanciesModel: IVacancies.Model {

    private var presenter: IVacancies.Presenter
    constructor(presenter: IVacancies.Presenter, ctx: Context){
        this.presenter = presenter
    }

    override fun sendVacancy(token: String, collaborator_id: Int, vacant_id: Int, origin: String, start_date: String) {
        var data = VacancyRequestModel(collaborator_id, vacant_id, origin, start_date)
        RetrofitClient.instance.sendVacancyRequest( "Bearer "+ token, data ).enqueue(object :
            Callback<SendVacancyResponseModel> {
            override fun onFailure(call: Call<SendVacancyResponseModel>, t: Throwable) {
                presenter.sendVacancyErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }
            override fun onResponse(call: Call<SendVacancyResponseModel>, response: Response<SendVacancyResponseModel>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.sendVacancyResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendVacancyErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }

    override fun sendVacancySingle(token: String, collaborator_id: Int, vacant_id: Int) {
        var data = SingleVacancyRequestModel(collaborator_id, vacant_id)
        RetrofitClient.instance.sendVacancyRequestSingle( "Bearer "+ token, data ).enqueue(object :
            Callback<SendVacancyResponseModel> {
            override fun onFailure(call: Call<SendVacancyResponseModel>, t: Throwable) {
                presenter.sendVacancyErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }
            override fun onResponse(call: Call<SendVacancyResponseModel>, response: Response<SendVacancyResponseModel>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.sendVacancyResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendVacancyErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }
}