package com.example.jurney_mobile.Model.GetOut

import com.example.jurney_mobile.View.Models.GetoutResponseModel
import com.example.jurney_mobile.View.Models.ReasonsResponseModel

interface IGetout {

    interface view{}
    interface Presenter{
        fun initView()

        fun getReasonsResponse(response:ArrayList<ReasonsResponseModel>)
        fun getReasonsErrorResponse(message:String)

        fun sendGetOutErrorResponse(message:String)
        fun sendGetOutResponse(response: GetoutResponseModel)

        fun unauthorizedError()
    }
    interface Model{
        fun getReasons(token:String)
        fun sendGetOut(token:String, date:String, reason_for_leaving_id:Int)
    }
}