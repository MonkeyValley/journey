package com.example.jurney_mobile.Model.Complaints

import android.util.Log
import com.example.jurney_mobile.Utils.utilsClass
import com.example.jurney_mobile.View.Models.ComplaintPayrollResponse
import com.example.jurney_mobile.View.Models.ComplaintRequest
import com.example.jurney_mobile.View.Models.ComplaintResponse
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplaintsActivityModel: IComplaint.ModelActivity {

    var presenter:IComplaint.PresenterActivity
    var gson = Gson()
    constructor(presenter:IComplaint.PresenterActivity){
        this.presenter = presenter
    }

    override fun sendComplaint(token: String, body: ComplaintRequest) {

        Log.e("token->", token)

        Log.e("body->", gson.toJson(body))

        RetrofitClient.instance.sendComplaint("Bearer "+ token, body  ).enqueue(object :
            Callback<ComplaintResponse> {
            override fun onFailure(call: Call<ComplaintResponse>, t: Throwable) {
                presenter.sendComplaintErrorResponse("Error de conexión, intentelo mas tarde")
            }

            override fun onResponse(call: Call<ComplaintResponse>, response: Response<ComplaintResponse>) {
                if(response.code() == 200){
                    presenter.sendComplaintResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.sendComplaintErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
                }
            }
        })
    }
}