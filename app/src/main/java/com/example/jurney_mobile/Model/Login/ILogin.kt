package com.example.jurney_mobile.Model.Login

import com.example.jurney_mobile.View.Models.ErrorSmsResponseModel
import com.example.jurney_mobile.View.Models.SmsResponseModel

public interface ILogin {
    interface View{
        fun onLoginError(msj:String)
    }

    interface Presenter{
        fun onLoginResponse(result: SmsResponseModel)
        fun onLoginResponseGenerated(result: ErrorSmsResponseModel)
        fun onLoginError(msj:String)
        fun onLogin(noEmpleado:String)
        fun initInfo()
    }

    interface Model {
        fun onLogin(noEmpleado:String)
    }
}