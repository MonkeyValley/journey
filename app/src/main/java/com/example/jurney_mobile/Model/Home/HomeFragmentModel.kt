package com.example.jurney_mobile.Model.Home

import android.util.Log
import com.example.jurney_mobile.View.Models.PaymentModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragmentModel:IHome.ModelFragment {

    var presenter:IHome.PresenterFragment
    constructor(presenter: IHome.PresenterFragment){
        this.presenter = presenter
    }

    override fun getPayment(token: String, employee_code: String, business_unit_id: String) {
        RetrofitClient.instance.getWeekendPayment( "Bearer "+ token, employee_code , business_unit_id ).enqueue(object:
            Callback<PaymentModel> {
            override fun onFailure(call: Call<PaymentModel>, t: Throwable) {
                Log.e("ERROR PUSH: ",t.message.toString())
                presenter.getPAymentErrorResponse("Error al contactar con nuestros servicios, intentar mas tarde")
            }
            override fun onResponse(call: Call<PaymentModel>, response: Response<PaymentModel>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.getPaymentResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    Log.e("----->","Badrequest service [/paysheet_week] = "+response.errorBody() +"\n"+ response.raw() + "\n" + response.headers() + "-\nBODY:" + response.body() +  " \nMESSAGE" + response.message())
                    presenter.getPAymentErrorResponse("Error al obtener información de pago, intentar mas tarde")
                }
            }
        })
    }



}