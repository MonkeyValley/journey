package com.example.jurney_mobile.Model.Splash

import android.util.Log
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashModel:ISplash.Model {

    var presenter:ISplash.Presenter

    constructor(presenter:ISplash.Presenter){
        this.presenter = presenter
    }

    override fun getInfoWeekendPaysheet(token: String, employee_code: String, business_unit_id: String) {
        RetrofitClient.instance.getWeekendPayment( "Bearer "+ token, employee_code, business_unit_id ).enqueue(object : Callback<PaymentModel>{
            override fun onFailure(call: Call<PaymentModel>, t: Throwable) {
                presenter.getInfoWeekendPaysheetErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<PaymentModel>, response: Response<PaymentModel>) {
                if(response.code() == 200){
                    Log.e("----->", response.body()!!.toString())
                    presenter.getInfoWeekendPaysheetResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoWeekendPaysheetErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }
    override fun getUserInfo(token: String) {
        RetrofitClient.instance.getUserInfo("Bearer "+ token).enqueue(object :Callback<UserResponseModel>{
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.getUserInfoErrorRespose("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<UserResponseModel>, response: Response<UserResponseModel>) {
                if (response.code() == 200 && response.body() != null) {
                    presenter.getUserInfoResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getUserInfoErrorRespose("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }

    override fun getTotalIncome(token: String, employee_code:String, business_unit_id:String) {
        RetrofitClient.instance.getTotalIncome("Bearer "+ token, employee_code, business_unit_id  ).enqueue(object :Callback<TotalIncomeModel>{
            override fun onFailure(call: Call<TotalIncomeModel>, t: Throwable) {
                presenter.getTotalIncomeErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<TotalIncomeModel>, response: Response<TotalIncomeModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if(response.body() == null){
                        presenter.getTotalIncomeErrorResponse("Error al obtener la información, intentelo más tarde.")
                    }else if(response.code() == 401){
                        presenter.unauthorizedError()
                    }else{
                        presenter.getTotalIncomeResponse(response.body()!!)
                    }
                }else{
                    presenter.getTotalIncomeErrorResponse("Error al obtener información, intentelo más tarde.")
                }
            }
        })
    }

    override fun getInfoTwoLastWeeks(token: String, employee_code: String, business_unit_id: String) {
        RetrofitClient.instance.getTwoLastWeeks("Bearer "+ token, employee_code, business_unit_id  ).enqueue(object :Callback<TwoLastWeekendModel>{
            override fun onFailure(call: Call<TwoLastWeekendModel>, t: Throwable) {
                presenter.getInfoTwoLastWeeksErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente.")
            }

            override fun onResponse(call: Call<TwoLastWeekendModel>, response: Response<TwoLastWeekendModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if(response.body() == null){
                        presenter.getInfoTwoLastWeeksErrorResponse("No se pudo obtener los datos, intentelo mas tarde")
                    }else if(response.code() == 401){
                        presenter.unauthorizedError()
                    }else{
                        presenter.getInfoTwoLastWeeksResponse(response.body()!!)
                    }
                }else{
                    presenter.getInfoTwoLastWeeksErrorResponse("Error al obtener la información")
                }
            }
        })
    }

    override fun getQualityScore(token: String, user_id: String, busines_id: String, date_to: String, dateFrom: String) {
        RetrofitClient.instance.getQualityScore("Bearer "+token, dateFrom, date_to, busines_id,  user_id).enqueue(object: Callback<QualityScoreResponse> {

            override fun onFailure(call: Call<QualityScoreResponse>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<QualityScoreResponse>, response: Response<QualityScoreResponse>) {

                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getQualityScoreResponse(response.body()!!)
                    }else{
                        presenter.getQualityScoreErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListChallenges( token: String, collaborator_id: Int) {
        RetrofitClient.instance.getChallengeList("Bearer "+token, collaborator_id ).enqueue(object: Callback<ChallengesResponseModel> {

            override fun onFailure(call: Call<ChallengesResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ChallengesResponseModel>, response: Response<ChallengesResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListChallengesResponse(response.body()!!)
                    }else{
                        presenter.getListChallengesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListChallengesAvailables(token: String) {

        RetrofitClient.instance.getChallengesAvailables("Bearer "+token ).enqueue(object: Callback<ArrayList<ChallengesAvailableResponse>> {

            override fun onFailure(call: Call<ArrayList<ChallengesAvailableResponse>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesAvailablesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<ChallengesAvailableResponse>>, response: Response<ArrayList<ChallengesAvailableResponse>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListChallengesAvailablesResponse(response.body()!!)
                    }else{
                        presenter.getListChallengesAvailablesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListChallengesAvailablesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })

    }

    override fun getListChallengesCompletes(token: String) {
        RetrofitClient.instance.getChallengesComplete("Bearer "+token ).enqueue(object: Callback<ChallengesCompleteResponseModel> {

            override fun onFailure(call: Call<ChallengesCompleteResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesCompletesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ChallengesCompleteResponseModel>, response: Response<ChallengesCompleteResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListChallengesCompletesResponse(response.body()!!)
                    }else{
                        presenter.getListChallengesCompletesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListChallengesCompletesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListChallengesHistory(token: String) {
        RetrofitClient.instance.getHistoryChallenges("Bearer "+token ).enqueue(object: Callback<ArrayList<HistoryChallengesResponseModel>> {

            override fun onFailure(call: Call<ArrayList<HistoryChallengesResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesHistoryErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<HistoryChallengesResponseModel>>, response: Response<ArrayList<HistoryChallengesResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListChallengesHistoryResponse(response.body()!!)
                    }else{
                        presenter.getListChallengesHistoryErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListChallengesHistoryErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListAwards(token: String) {

        RetrofitClient.instance.getAwardsList("Bearer "+token ).enqueue(object: Callback<ArrayList<AwardsResponseModel>> {

            override fun onFailure(call: Call<ArrayList<AwardsResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<AwardsResponseModel>>, response: Response<ArrayList<AwardsResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListAwardsResponse(response.body()!!)
                    }else{
                        presenter.getListAwardsErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListAwardsErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })

    }

    override fun getListCodes(token: String) {
        //getListCodes
        RetrofitClient.instance.getCodesList("Bearer "+token ).enqueue(object: Callback<ArrayList<CodeResponseModel>> {

            override fun onFailure(call: Call<ArrayList<CodeResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<CodeResponseModel>>, response: Response<ArrayList<CodeResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListCodesResponse(response.body()!!)
                    }else{
                        presenter.getListCodesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getTopTen(token: String) {
        RetrofitClient.instance.getTopTen("Bearer "+token ).enqueue(object: Callback<ArrayList<UserRankingResponseModel>> {
            override fun onFailure(call: Call<ArrayList<UserRankingResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getTopTenErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<UserRankingResponseModel>>, response: Response<ArrayList<UserRankingResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getTopTenResponse(response.body()!!)
                    }else{
                        presenter.getTopTenErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getTopTenErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getMyRanking(token: String) {
        RetrofitClient.instance.getMyRanking("Bearer "+token ).enqueue(object: Callback<ArrayList<UserRankingResponseModel>> {
            override fun onFailure(call: Call<ArrayList<UserRankingResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getMyRankingErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<UserRankingResponseModel>>, response: Response<ArrayList<UserRankingResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getMyRankingResponse(response.body()!!)
                    }else{
                        presenter.getMyRankingErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getMyRankingErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getVacancies(token: String, collaborator_id:Int) {
        RetrofitClient.instance.getVacancies("Bearer "+token, collaborator_id ).enqueue(object: Callback<ArrayList<VacancyResponseModel>> {
            override fun onFailure(call: Call<ArrayList<VacancyResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getVacanciesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<VacancyResponseModel>>, response: Response<ArrayList<VacancyResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getVacanciesResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getVacanciesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getVacanciesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getReasonsPayrollComplain(token: String) {
        RetrofitClient.instance.getReasonsComplainsPayroll("Bearer "+token ).enqueue(object: Callback<ArrayList<ReasonsPayrollComplainModel>> {
            override fun onFailure(call: Call<ArrayList<ReasonsPayrollComplainModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getReasonsPayrollComplainErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<ReasonsPayrollComplainModel>>, response: Response<ArrayList<ReasonsPayrollComplainModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getReasonsPayrollComplainResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getReasonsPayrollComplainErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getReasonsPayrollComplainErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getDailyPayList(token: String, collaborator_id: Int) {
        RetrofitClient.instance.getDailyPay("Bearer "+token, collaborator_id).enqueue(object: Callback<DailypayResponseModel> {
            override fun onFailure(call: Call<DailypayResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getDailyPayListErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.", "000")
            }

            override fun onResponse(call: Call<DailypayResponseModel>, response: Response<DailypayResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getDailyPayListResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getDailyPayListErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.", response.code().toString())
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getDailyPayListErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'", response.code().toString())
                }
            }
        })
    }

    override fun getSupervisors(token: String) {
        RetrofitClient.instance.getSupervisors("Bearer "+token).enqueue(object: Callback<ArrayList<SupervisorsResponseModel>> {
            override fun onFailure(call: Call<ArrayList<SupervisorsResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getSupervisorsErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<SupervisorsResponseModel>>, response: Response<ArrayList<SupervisorsResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getSupervisorsResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getSupervisorsErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getSupervisorsErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }
}