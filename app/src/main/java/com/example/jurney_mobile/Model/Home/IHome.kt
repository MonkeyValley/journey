package com.example.jurney_mobile.Model.Home

import android.app.Activity
import androidx.fragment.app.Fragment
import com.example.jurney_mobile.View.Models.*
import com.google.android.material.bottomsheet.BottomSheetDialog

interface IHome {

    interface View{
        fun closeBottomSheet()
        fun showBottomSheet()
    }

    interface Presenter{
        fun onResume()
        fun onClickListener(v: android.view.View)
        fun showFragment(idScreen:String, headerType:String, fragment: Fragment, isClose:Boolean, from:String )
        fun initInfo(viewBottomSheet: BottomSheetDialog)

        fun updateTokenFirebaseErrorResponse(message:String)
        fun updateTokenFirebaseResponse(response: UserResponseModel)

        fun sendResponsecodeResponse(response: SendResResponseModel)
        fun sendResponsecodeErrorResponse(message: String)

        fun unauthorizedError()

        fun getQualityScoreResponse(response: QualityScoreResponse)
        fun getQualityScoreErrorResponse(message:String)

        fun getInfoUserResponse(response:UserResponseModel)
        fun getInfoUserErrorResponse(message:String)

        fun confirmNotificationResponse()
        fun confirmNotificationErrorResponse(message:String)
    }

    interface Model {
        fun updateTokenFirebase(token:String, data:String, id:Int)
        fun getQualityScore(token:String, user_id:String, busines_id:String, date_to:String, dateFrom:String)
        fun sendResponsecode(token: String, response:String, id_prize:Int)
        fun getInfoUser(token:String)
        fun confirmNotification(token:String, msg_id:Int, res:Boolean)
    }

    interface ViewFragment{
        fun onClickNomina(isEnabled:Boolean)
        fun onClickCalidad()
        fun onClickRetos()
    }

    interface PresenterFragment{
        fun onClickListener(v:android.view.View)
        fun initInfo()
        fun initNoNetwork(activity:Activity)
        fun getInfoPayment()
        fun getPaymentResponse(response: PaymentModel)
        fun getPAymentErrorResponse(message:String)

        fun unauthorizedError()

    }

    interface ModelFragment {
        fun getPayment(token:String, employee_code:String, business_unit_id:String)
    }

}