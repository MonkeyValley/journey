package com.example.jurney_mobile.Model.Login

import android.util.Log
import com.example.jurney_mobile.Model.Login.ILogin
import com.example.jurney_mobile.Utils.LoadingDialog
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginModel: ILogin.Model  {

    private var presenter: ILogin.Presenter
    private val gson = Gson()

    private val TAG = "LOGINACTIVITY"
    constructor(presenter: ILogin.Presenter){
        this.presenter = presenter
    }
    override fun onLogin(noEmpleado: String) {
//        presenter.onLoginResponse(SmsResponseModel(true,"ok"))
        RetrofitClient.instance.sms(noEmpleado).enqueue(object : Callback<SmsResponseModel> {
            override fun onFailure(call: Call<SmsResponseModel>, t: Throwable) {
                presenter.onLoginError("Error en conexión de servicios, reportarlo a atención al cliente.")
            }
            override fun onResponse(call: Call<SmsResponseModel>, response: Response<SmsResponseModel>) {
                if(response.code() == 200){
                    presenter.onLoginResponse(response.body()!!)
                }else if(response.code() == 422){
                    val errorBody = gson.fromJson<ErrorSmsResponseModel>(response.errorBody()!!.string(), ErrorSmsResponseModel::class.java)
                    presenter.onLoginResponseGenerated(errorBody)
                }
                else{
                    presenter.onLoginError("El número de teléfono no existe en VivaJourney, contacta a trabajo social.")
                }
            }
        })
    }
}