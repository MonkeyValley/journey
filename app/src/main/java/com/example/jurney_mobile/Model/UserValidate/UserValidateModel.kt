package com.example.jurney_mobile.Model.UserValidate

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.*
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import com.example.jurney_mobile.View.UserValidate
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user_validate.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserValidateModel: IUserValidate.Model {

    private var presenter: IUserValidate.Presenter
    private lateinit var sharedPreferences: SharedPreferences
    private var ctx:UserValidate
    constructor(presenter: IUserValidate.Presenter, ctx:UserValidate){
        this.presenter = presenter
        this.ctx = ctx
    }

    override fun onValidate(data1:String, data2:String, data3:String, data4:String, phone:String) {
        val parameters = SigninRequest(phone!!, data1+data2+data3+data4 ,"mobile")

        Log.e("BODY ->>>",Gson().toJson(parameters))

        RetrofitClient.instance.login( parameters).enqueue(object : Callback<LoginResponse>{
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.e("----->", t.message!! )
                presenter.onValidateError("Error en conexión de servicios, reportarlo a atención al cliente")
            }
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                Log.e("------------->",response.raw().toString())
                Log.e("------------->",response.body().toString())

                if (response.code() == 200){
                    presenter.onValidateResponse(response.body()!!.token)
                }else{
                    presenter.onValidateError("El código de contraseña no es el correcto. Por favor revíselo.")
                }
            }
        })
    }


    override fun getInfoUser(token: String) {
        RetrofitClient.instance.getUserInfo("Bearer "+ token).enqueue(object :Callback<UserResponseModel>{
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                presenter.getInfoUserErrorRespose("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<UserResponseModel>, response: Response<UserResponseModel>) {
                if (response.code() == 200 && response.body() != null) {
                    presenter.getInfoUserResponse(response.body()!!)
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoUserErrorRespose("Error al obtener información, intentelo más tarde.")
                }
            }

        })
    }


    override fun getWeekendPayment(token: String, employee_code: String, business_unit_id: String) {

        RetrofitClient.instance.getWeekendPayment( "Bearer "+ token, employee_code, business_unit_id ).enqueue(object : Callback<PaymentModel>{
            override fun onFailure(call: Call<PaymentModel>, t: Throwable) {
                presenter.getInfoErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<PaymentModel>, response: Response<PaymentModel>) {

                if(response.code() == 200 && response.body() != null){

                    Log.e("----->", response.body()!!.toString())
                    presenter.getInfoResponse(response.body()!!)

                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoErrorResponse("Badrequest service [/paysheet_week] = "+response.errorBody() +"\n"+ response.raw() + "\n" + response.headers() + "-\nBODY:" + response.body() +  " \nMESSAGE" + response.message())
                }
            }
        })
    }



    override fun getTotalIncome(token: String, employee_code:String, business_unit_id:String) {
        RetrofitClient.instance.getTotalIncome("Bearer "+ token, employee_code, business_unit_id  ).enqueue(object :Callback<TotalIncomeModel>{
            override fun onFailure(call: Call<TotalIncomeModel>, t: Throwable) {
                presenter.getTotalIncomeErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<TotalIncomeModel>, response: Response<TotalIncomeModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if(response.body() == null){
                        presenter.getTotalIncomeErrorResponse(response.body()!!.errors)
                    }else{
                        presenter.getTotalIncomeResponse(response.body()!!)
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getTotalIncomeErrorResponse("Invalid token")
                }
            }
        })
    }

    override fun getInfoTwoLastWeeks(token: String, employee_code: String, business_unit_id: String) {
        RetrofitClient.instance.getTwoLastWeeks("Bearer "+ token, employee_code, business_unit_id  ).enqueue(object :Callback<TwoLastWeekendModel>{
            override fun onFailure(call: Call<TwoLastWeekendModel>, t: Throwable) {
                Log.e("ERRORRRR",t.message.toString())
                presenter.getInfoTwoLastWeeksErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }

            override fun onResponse(call: Call<TwoLastWeekendModel>, response: Response<TwoLastWeekendModel>) {
                if (response.code() == 200 && response.body() != null) {
                    if(response.body() == null){
                        presenter.getInfoTwoLastWeeksErrorResponse("Datos no disponibles")
                    }else{
                        presenter.getInfoTwoLastWeeksResponse(response.body()!!)
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getInfoTwoLastWeeksErrorResponse("Invalid token")
                }
            }
        })
    }
    override fun getQualityScore(token: String, user_id: String, busines_id: String, date_to: String, dateFrom: String) {
        RetrofitClient.instance.getQualityScore("Bearer "+token, dateFrom, date_to, busines_id,  user_id).enqueue(object: Callback<QualityScoreResponse> {

            override fun onFailure(call: Call<QualityScoreResponse>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<QualityScoreResponse>, response: Response<QualityScoreResponse>) {

                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getQualityScoreResponse(response.body()!!)
                    }else{
                        presenter.getQualityScoreErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getQualityScoreErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListChallenges( token: String, collaborator_id: Int) {
        RetrofitClient.instance.getChallengeList("Bearer "+token, collaborator_id ).enqueue(object: Callback<ChallengesResponseModel> {

            override fun onFailure(call: Call<ChallengesResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ChallengesResponseModel>, response: Response<ChallengesResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListChallengesResponse(response.body()!!)
                    }else{
                        presenter.getListChallengesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun onResendCode(noEmpleado: String) {
        //presenter.onLoginResponse(SmsResponseModel(true,"ok"))
        RetrofitClient.instance.sms(noEmpleado).enqueue(object : Callback<SmsResponseModel> {
            override fun onFailure(call: Call<SmsResponseModel>, t: Throwable) {
                Log.e("TAG",t.message!!)
                presenter.onResendCodeErrorResponse("Error en conexión de servicios, reportarlo a atención al cliente")
            }
            override fun onResponse(call: Call<SmsResponseModel>, response: Response<SmsResponseModel>) {
                if(response.code() == 200 && response.body() != null){
                    presenter.onResendCodeResponse(response.body()!!)
                }else{
                    presenter.onResendCodeErrorResponse("Número de celular no encontrado")
                }
            }
        })
    }

    override fun getVacancies(token: String, collaborator_id:Int) {
        RetrofitClient.instance.getVacancies("Bearer "+token, collaborator_id ).enqueue(object: Callback<ArrayList<VacancyResponseModel>> {
            override fun onFailure(call: Call<ArrayList<VacancyResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getVacanciesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<VacancyResponseModel>>, response: Response<ArrayList<VacancyResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getVacanciesResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getVacanciesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getVacanciesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getReasonsPayrollComplain(token: String) {
        RetrofitClient.instance.getReasonsComplainsPayroll("Bearer "+token ).enqueue(object: Callback<ArrayList<ReasonsPayrollComplainModel>> {
            override fun onFailure(call: Call<ArrayList<ReasonsPayrollComplainModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getReasonsPayrollComplainErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<ReasonsPayrollComplainModel>>, response: Response<ArrayList<ReasonsPayrollComplainModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getReasonsPayrollComplainResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getReasonsPayrollComplainErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getReasonsPayrollComplainErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }


    override fun getListAwards(token: String) {

        RetrofitClient.instance.getAwardsList("Bearer "+token ).enqueue(object: Callback<ArrayList<AwardsResponseModel>> {

            override fun onFailure(call: Call<ArrayList<AwardsResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListChallengesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<AwardsResponseModel>>, response: Response<ArrayList<AwardsResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListAwardsResponse(response.body()!!)
                    }else{
                        presenter.getListAwardsErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListAwardsErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })

    }

    override fun getListCodes(token: String) {
        //getListCodes
        RetrofitClient.instance.getCodesList("Bearer "+token ).enqueue(object: Callback<ArrayList<CodeResponseModel>> {

            override fun onFailure(call: Call<ArrayList<CodeResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<CodeResponseModel>>, response: Response<ArrayList<CodeResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListCodesResponse(response.body()!!)
                    }else{
                        presenter.getListCodesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getTopTen(token: String) {
        RetrofitClient.instance.getTopTen("Bearer "+token ).enqueue(object: Callback<ArrayList<UserRankingResponseModel>> {
            override fun onFailure(call: Call<ArrayList<UserRankingResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getTopTenErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<UserRankingResponseModel>>, response: Response<ArrayList<UserRankingResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getTopTenResponse(response.body()!!)
                    }else{
                        presenter.getTopTenErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getTopTenErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getMyRanking(token: String) {
        RetrofitClient.instance.getMyRanking("Bearer "+token ).enqueue(object: Callback<ArrayList<UserRankingResponseModel>> {
            override fun onFailure(call: Call<ArrayList<UserRankingResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getMyRankingErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<UserRankingResponseModel>>, response: Response<ArrayList<UserRankingResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getMyRankingResponse(response.body()!!)
                    }else{
                        presenter.getMyRankingErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getMyRankingErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getDailyPayList(token: String, collaborator_id: Int) {
        RetrofitClient.instance.getDailyPay("Bearer "+token, collaborator_id).enqueue(object: Callback<DailypayResponseModel> {
            override fun onFailure(call: Call<DailypayResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getDailyPayListErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.", "000")
            }

            override fun onResponse(call: Call<DailypayResponseModel>, response: Response<DailypayResponseModel>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getDailyPayListResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getDailyPayListErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.", response.code().toString())
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getDailyPayListErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'", response.code().toString())
                }
            }
        })
    }

    override fun getNewCode(phone:String) {
        RetrofitClient.instance.reSendCode(phone).enqueue(object: Callback<newSmsResponseModel> {
            override fun onFailure(call: Call<newSmsResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getNewCodeErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<newSmsResponseModel>, response: Response<newSmsResponseModel>) {
                if(response.code() == 200 || response.code() == 201){
                    if(response.body() != null ){
                        presenter.getNewCodeResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getNewCodeErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getNewCodeErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }

    override fun getSupervisors(token: String) {
        RetrofitClient.instance.getSupervisors("Bearer "+token).enqueue(object: Callback<ArrayList<SupervisorsResponseModel>> {
            override fun onFailure(call: Call<ArrayList<SupervisorsResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getSupervisorsErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<SupervisorsResponseModel>>, response: Response<ArrayList<SupervisorsResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getSupervisorsResponse(response.body()!!)
                        Log.d("Response--->", Gson().toJson(response.body()))
                    }else{
                        presenter.getSupervisorsErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getSupervisorsErrorResponse("No cumples con los requisitos para poder solicitar 'Depósito Diario'")
                }
            }
        })
    }

}