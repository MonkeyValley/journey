package com.example.jurney_mobile.Model.Notifications
import com.example.jurney_mobile.View.Models.NotificationModel

interface INotifications {

    interface View {}

    interface Presenter {
        fun getnotificationListResponse(response:ArrayList<NotificationModel>)
        fun getnotificationListErrorResponse(msg:String)
        fun unauthorizedError()
        fun initInfo()
    }

    interface Model {
        fun getnotificationList(token:String, date_from:String, date_to:String, user_id:Int)
    }
}