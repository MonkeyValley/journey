package com.example.jurney_mobile.Model.Awards

import android.util.Log
import com.example.jurney_mobile.View.Models.CodeResponseModel
import com.example.jurney_mobile.View.Models.GeneratorCodeRequestModel
import com.example.jurney_mobile.View.Models.GeneratorCodeResponseModel
import com.example.jurney_mobile.View.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AwardsCodesModel:IAwards.ModelCodes {

    var presenter:IAwards.PresenterCodes
    constructor(presenter:IAwards.PresenterCodes){this.presenter = presenter}

    override fun getExchangeCode(token:String, collaborator_id:Int, prize_id:Int) {
        Log.e("Data ","{ collaborator_id:" + collaborator_id+ "prize_id"+ prize_id + "}")
        RetrofitClient.instance.sendExchangeCode("Bearer "+token, GeneratorCodeRequestModel(collaborator_id, prize_id) ).enqueue(object:
            Callback<GeneratorCodeResponseModel> {

            override fun onFailure(call: Call<GeneratorCodeResponseModel>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getExchangeCodeErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<GeneratorCodeResponseModel>, response: Response<GeneratorCodeResponseModel>) {
                if(response.code() == 200 || response.code() == 201){

                    if(response.body() != null){
                        presenter.getExchangeCodeResponse(response.body()!!)
                    }else{
                        presenter.getExchangeCodeErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else if(response.code() == 400){
                    presenter.getExchangeCodeErrorResponse("")
                }else{
                    presenter.getExchangeCodeErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }

    override fun getListCodes(token: String) {
        //getListCodes
        RetrofitClient.instance.getCodesList("Bearer "+token ).enqueue(object: Callback<ArrayList<CodeResponseModel>> {

            override fun onFailure(call: Call<ArrayList<CodeResponseModel>>, t: Throwable) {
                Log.e("--<<",t.message!!)
                presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
            }

            override fun onResponse(call: Call<ArrayList<CodeResponseModel>>, response: Response<ArrayList<CodeResponseModel>>) {
                if(response.code() == 200){
                    if(response.body() != null){
                        presenter.getListCodesResponse(response.body()!!)
                    }else{
                        presenter.getListCodesErrorResponse("No se ha podido obtener la información, por favor intente mas tarde.")
                    }
                }else if(response.code() == 401){
                    presenter.unauthorizedError()
                }else{
                    presenter.getListCodesErrorResponse("No se ha podido procesar la información, por favor intente mas tarde.")
                }
            }
        })
    }
}