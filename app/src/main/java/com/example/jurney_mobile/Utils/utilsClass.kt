package com.example.jurney_mobile.Utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.LoginActivity
import com.example.jurney_mobile.View.Models.categoryLevelModel
import java.io.IOException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.contracts.Returns


class utilsClass {

    private lateinit var sharedPreferences: SharedPreferences

    fun milesFormat(number:Double):String{

        val formatter = DecimalFormat("#,###,###.##")
        val formatedString: String = formatter.format(number)

        return  formatedString
    }
    fun milesFormatNoDecimals(number:Double):String{

        val formatter = DecimalFormat("#,###,###")
        val formatedString: String = formatter.format(number)

        return  formatedString
    }

    fun chooseUnityBussiness(id:Int):String{
        when(id){
            1-> return "Culiacan"
            2-> return "Ensenada"
            3-> return "Sayula"
            else -> return "error"
        }
    }
    fun logOut(ctx:Activity){
        sharedPreferences = ctx.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.remove("_tokenuser")
        editor.remove("_nominaAvailable")
        editor.remove("_userinfo")
        editor.remove("_totalIncome")
        editor.remove("_isOnBoardingView")
        editor.remove("_nominainfo")
        editor.remove("_isConnected")
        editor.remove("_image")
        editor.remove("_phonesurvey")
        editor.remove("_qualityScore")
        editor.remove("_lastUpdate")
        editor.remove("isNotificastion")
        editor.remove("_phoneuser")
        editor.apply()
        ctx.finish()
        ctx.startActivity( Intent(ctx, LoginActivity::class.java) )
    }

    fun rangeCategories(current_lvl:Int):categoryLevelModel{
        when(current_lvl){
            0,1,2,3,4,5,6,7,8,9,10-> return categoryLevelModel("NOVATO", getColorCategory("NOVATO"))
            11,12,13,14,15,16,17,18,19,20-> return categoryLevelModel("APRENDIZ", getColorCategory("APRENDIZ"))
            21,22,23,24,25,26,27,28,29,30->return categoryLevelModel("PRINCIPIANTE", getColorCategory("PRINCIPIANTE"))
            31,32,33,34,35,36,37,38,39,40->return categoryLevelModel("JUNIOR", getColorCategory("JUNIOR"))
            41,42,43,44,45,46,47,48,49,50->return categoryLevelModel("SENIOR", getColorCategory("SENIOR"))
            51,52,53,54,55,56,57,58,59,50->return categoryLevelModel("SENIOR PRO", getColorCategory("SENIOR PRO"))
            61,62,63,64,65,66,67,68,69,60->return categoryLevelModel("EXPERTO", getColorCategory("EXPERTO"))
            71,72,73,74,75,76,77,78,79,80->return categoryLevelModel("MAESTRO", getColorCategory("MAESTRO"))
            81,82,83,84,85,86,87,88,89,90->return categoryLevelModel("MAESTRO PLUS", getColorCategory("MAESTRO PLUS"))
            91,92,93,94,95,96,97,98,99,100->return categoryLevelModel("REY", getColorCategory("REY"))
            101,102,103,104,105,106,107,108,109,110->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            111,112,113,114,115,116,117,118,119,120->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            121,122,123,124,125,126,127,128,129,130->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            131,132,133,134,135,136,137,138,139,140->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            141,142,143,144,145,146,147,148,149,150->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            151,152,153,154,155,156,157,158,159,150->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            161,162,163,164,165,166,167,168,169,160->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            171,172,173,174,175,176,177,178,179,180->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            181,182,183,184,185,186,187,188,189,190->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            191,192,193,194,195,196,197,198,199,200->return categoryLevelModel("LEYENDA", getColorCategory("LEYENDA"))
            else->return categoryLevelModel("NO DISPONIBLE", R.color.colorBackgroundDark)
        }
    }

    fun getColorCategory(categorie:String):Int{

        when(categorie){
            "NOVATO"-> return R.color.novatocolor
            "APRENDIZ"-> return R.color.aprendizcolor
            "PRINCIPIANTE"-> return R.color.principiantecolor
            "SENIOR"-> return R.color.seniorcolor
            "SENIOR PRO"-> return R.color.seniorprocolor
            "EXPERTO"-> return R.color.expertocolor
            "MAESTRO"-> return R.color.maestrocolor
            "MAESTRO PLUS"-> return R.color.maestropluscolor
            "REY"-> return R.color.reycolor
            "LEYENDA"-> return R.color.leyendacolor
            else-> return  R.color.colorBackgroundDark
        }
    }

    fun hideSoftKeyBoard(context: Context, view: View) {
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            // TODO: handle exception
            e.printStackTrace()
        }

    }

    fun View.showKeyboard() {
        this.requestFocus()
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun isAppIntalled(appNamePackage:String, ctx:Context):Boolean{
        val pm = ctx.packageManager
        try {
            pm.getPackageInfo(appNamePackage, PackageManager.GET_ACTIVITIES)
            return true
        }catch (e:PackageManager.NameNotFoundException){
            return false
        }
    }
    fun getversionName(ctx:Context):String{
        try {
            val pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0)
            val version = pInfo.versionName
            return version
        } catch ( e:PackageManager.NameNotFoundException) {
            e.printStackTrace()
            return "0.0.0"
        }
    }

    fun toSimpleString(date: Date) : String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }

    fun convertStirngMonthtoNumberMonth(monthStr:String):String{
        var month = ""
        when(monthStr){
            "Jan" -> month = "01"
            "Feb" -> month = "02"
            "Mar" -> month = "03"
            "Apr" -> month = "04"
            "May" -> month = "05"
            "Jun" -> month = "06"
            "Jul" -> month = "07"
            "Aug" -> month = "08"
            "Sep" -> month = "09"
            "Oct" -> month = "10"
            "Nov" -> month = "11"
            "Dec" -> month = "12"
        }

        return month
    }

    fun convertIntMonthToStringMonth(monthInt:Int):String {
        var month = ""
        when (monthInt) {
            1 -> month = "Enero"
            2 -> month = "Febrero"
            3 -> month = "Marzo"
            4 -> month = "Abril"
            5 -> month = "Mayo"
            6 -> month = "Junio"
            7 -> month = "Julio"
            8 -> month = "Agosto"
            9 -> month = "Septiembre"
            10 -> month = "Octubre"
            11 -> month = "Nobiembre"
            12 -> month = "Diciembre"
        }

        return month
    }

    fun convertIntMonthToStrinh(month:Int):String{
        var res = ""
        when(month){
            0 -> res = "Enero"
            1 -> res = "Febrero"
            2 -> res = "Marzo"
            3 -> res = "Abril"
            4 -> res = "Mayo"
            5 -> res = "Junio"
            6 -> res = "Julio"
            7 -> res = "Agosto"
            8 -> res = "Septiembre"
            9 -> res = "Octubre"
            10 -> res = "Noviembre"
            11 -> res = "Diciembre"
        }

        return res
    }

    fun getDayOfWeekFromCalendar(calendar:Calendar):String{

        var res = ""
        val day = calendar.get(Calendar.DAY_OF_WEEK)
        when(day){
            Calendar.MONDAY -> res = "Lunes"
            Calendar.THURSDAY -> res = "Jueves"
            Calendar.WEDNESDAY -> res = "Miercoles"
            Calendar.TUESDAY -> res = "Martes"
            Calendar.FRIDAY -> res = "Viernes"
            Calendar.SATURDAY -> res = "Sabado"
            Calendar.SUNDAY -> res = "Domingo"
        }

        return  res
    }

}