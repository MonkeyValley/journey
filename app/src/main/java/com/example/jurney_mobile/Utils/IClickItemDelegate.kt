package com.example.jurney_mobile.Utils

import com.example.jurney_mobile.View.Models.*

interface IClickItemDelegate {

    interface clickItemAwards{
        fun click(item: AwardsResponseModel)
    }

    interface clickItemChallenge{
        fun click(item: ChallengeModel)
    }

    interface clickItemSlide{
        fun click(item:Int, cat:String)
    }

    interface clickItemReason{
        fun click(item: ReasonsResponseModel)
    }

    interface clickItemPayroll {
        fun click(item: DayPaysheetModel, pos:Int, type:Int)
    }

    interface clickReasonPayrollComplain{
        fun clickReason(item: ReasonsPayrollComplainModel)
    }

    interface clickAnswerSelected{
        fun click(item:Int, position:Int, selected:Boolean)
    }

    interface clickItemSurvey{
        fun click(item:SurveysListModel, position:Int)
    }
    interface clickItemSurveyFragment {
        fun click(item:Int, position:Int, selected:Boolean)
    }

    interface clickItemReviewFragment {
        fun click(item:Int, selected:Int)
    }

    interface clickVacancies{
        fun click(item: VacancyResponseModel)
    }

    interface clickReviews{
        fun click(item: SupervisorsResponseModel)
    }

    interface afterTextChangedDelegate{
        fun afterTextChanged(charSecuense:String)
    }

    interface callbackclickItemChallenge{
        fun clickItemCallback(item:ChallengeModel)
    }
}