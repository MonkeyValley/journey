package com.example.jurney_mobile.Utils

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import com.example.jurney_mobile.R
import com.google.android.material.snackbar.Snackbar

class SnackAlert {

    companion object {
        lateinit var snack: Snackbar
        fun alert(msj: String, v: View, type:String, time:Int, position:String) {

            snack = Snackbar.make(v, msj, time)
            val view = snack.view

            if(type.equals("error")){
                view.setBackgroundResource(R.drawable.layer_bg_error_alert)
            }else if(type.equals("success")){
                view.setBackgroundResource(R.drawable.layer_bg_success_alert)
            }else{
                view.setBackgroundResource(R.drawable.layer_bg_warning_alert)
            }


            val params = view.layoutParams as FrameLayout.LayoutParams

            if(position == "BOTTOM"){
                params.gravity = Gravity.BOTTOM
            }else{
                params.gravity = Gravity.TOP
            }

            params.setMargins(
                params.leftMargin + 30,
                params.topMargin + 30,
                params.rightMargin + 30,
                params.bottomMargin + 30
            )
            view.layoutParams = params
        }

        fun closeAlert(){
            try {
                snack.dismiss()
            }catch (e:Exception){

            }
        }

        fun showAlert(){
            if(snack != null) {
                snack.show()
            }
        }
    }
}
