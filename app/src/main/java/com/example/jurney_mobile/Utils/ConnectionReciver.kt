package com.example.jurney_mobile.Utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import androidx.core.content.getSystemService

class ConnectionReciver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        val isConnected =  checkConnection(context!!)
        if(connectionReciverListener != null ) connectionReciverListener!!.onNetworkConnectionChanged(isConnected)
    }

    interface ConnectionReciverlistener{
        fun onNetworkConnectionChanged(isConnected:Boolean)
    }
    companion object{
        var connectionReciverListener: ConnectionReciverlistener? = null
        val isConnected:Boolean
        get(){
            val cm = MyApplication.instance.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return (activeNetwork != null && activeNetwork.isConnectedOrConnecting)
        }
    }
    private fun checkConnection (ctx:Context):Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting)
    }
}