package com.example.jurney_mobile.Utils

import android.app.Application

class MyApplication:Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun setConnectionLsitenner(listenner: ConnectionReciver.ConnectionReciverlistener) {
        ConnectionReciver.connectionReciverListener = listenner
    }
    companion object{
        @get:Synchronized
        lateinit var instance:MyApplication
    }
}