package com.example.jurney_mobile.Utils

class EnviromentConstants {
    //ENVIROMENT
    var prod_server = "https://journey.vivasmart.com.mx"
    var qa_server = "https://qa-journey.vivasmart.com.mx"
    var dev_server = "https://dev-journey.vivasmart.com.mx"

    var image_serverver = "https://backend-filestorage.s3-us-west-1.amazonaws.com/vivasmart-journey/"

    var firebase_server = "https://vivasmartjourneyandroid.firebaseio.com/"

    var googlepay_server = "https://play.google.com/store/apps/details?id=com.vivasmart.journey"

    var sentry_server = "https://6f0db47364ce4c539635fe8a5e39e152@sentry.io/4282969"



    //IDS
    var sentry_apk_id = "a07bh4va72fmgjb"

    //HELP OPTION
    var phone:String = "+526671997860"
    var first:String = "Hola, no me llega el código PIN, ¿Me ayuda por favor?"
    var second:String = "Hola. me llego el código PIN pero no es correcto, ¿Me ayuda por favor?"
    var thirt:String = "Hola, no puedo acceder a Journey con mi numero de celular. ¿Me ayuda por favor?"
}
