package com.example.jurney_mobile.Utils

import android.app.Activity
import android.app.AlertDialog
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.jurney_mobile.R

class AlertDialog: View.OnClickListener {
    private var activity: Activity
    private lateinit var alertDialog: AlertDialog
    private var clickOkDelegate: clickOkDelegate
    private lateinit var viewInf:View
    var complaint_place_type = 0
    constructor(myActivity: Activity, clickok:clickOkDelegate){
        activity = myActivity
        this.clickOkDelegate = clickok
    }

    fun alertPlace(){
        var builder: AlertDialog.Builder = AlertDialog.Builder(activity)

        var inflater: LayoutInflater = activity.layoutInflater
        viewInf = inflater.inflate(R.layout.alert_place_layout,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        val editText = viewInf.findViewById<EditText>(R.id.txtNamePlaceAlert)
        val errorLabel = viewInf.findViewById<TextView>(R.id.txt_required)

        val btnMaya = viewInf.findViewById<Button>(R.id.btnMayas)
        val btnInvernadero = viewInf.findViewById<Button>(R.id.btnInvernaderos)
        val btnCancel = viewInf.findViewById<Button>(R.id.btnCancel)

        btnMaya.setOnClickListener(this)
        btnInvernadero.setOnClickListener(this)
        btnCancel.setOnClickListener(this)

        val btnOk = viewInf.findViewById<Button>(R.id.btnContinue)
        btnOk.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                if(complaint_place_type > 0 && editText.text.toString() != "" ){
                    errorLabel.visibility = View.GONE
                    clickOkDelegate.clickContinue(editText.text.toString(), complaint_place_type)
                }else{
                    errorLabel.visibility = View.VISIBLE
                }
            }
        })

        alertDialog.show()
    }

    fun dismissDialog() {
        alertDialog.dismiss()
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnMayas -> {
                v.background = activity.resources.getDrawable(R.drawable.layer_bg_yellow_button)
                viewInf.findViewById<Button>(R.id.btnInvernaderos).background = activity.resources.getDrawable(R.drawable.corner_radius_linear)
                this.complaint_place_type = 2
            }
            R.id.btnInvernaderos -> {
                v.background = activity.resources.getDrawable(R.drawable.layer_bg_yellow_button)
                viewInf.findViewById<Button>(R.id.btnMayas).background = activity.resources.getDrawable(R.drawable.corner_radius_linear)
                this.complaint_place_type = 1
            }
            R.id.btnCancel -> alertDialog.dismiss()

        }
    }
}

interface clickOkDelegate{
    fun clickContinue(complaint_place_name:String, complaint_place_type:Int)
}