package com.example.jurney_mobile.Utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.service.autofill.OnClickAction
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.example.jurney_mobile.R
import com.example.jurney_mobile.View.Models.ExtrasModel
import com.example.jurney_mobile.View.Models.StateArrayModel
import com.example.jurney_mobile.View.Models.VacancyResponseModel
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.alert_layout.view.*
import kotlinx.android.synthetic.main.alert_layout_animation.view.*
import kotlinx.android.synthetic.main.alert_layout_animation_login.view.*
import kotlinx.android.synthetic.main.alert_layout_postulation.view.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField
import java.time.temporal.WeekFields
import java.util.*
import java.util.logging.SimpleFormatter
import kotlin.collections.ArrayList

class LoadingDialog: View.OnClickListener{
    private var activity:Activity
    private lateinit var alertDialog:AlertDialog
    private var step = 0
    private lateinit var viewInf:View
    var arrayNames = ArrayList<String>()
    private lateinit var sendVacancie:SendVacancieDelegate

    constructor(myActivity:Activity){
        activity = myActivity
    }
    fun startLoading(){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)

         var inflater: LayoutInflater  = activity.layoutInflater
        builder.setView(inflater.inflate(R.layout.custom_dialog, null))

        builder.setCancelable(false)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        alertDialog.show()
    }

//    fun alertNot(message:String, title:String, buttonTittle:String, icon:Int, click:View.OnClickListener, ifVivaCoints:Boolean, data:String){
    fun alertNot(message:String, title:String, buttonTittle:String, icon:Int, click:View.OnClickListener, clickFb:View.OnClickListener, ifFacebookShare:Boolean, ifVivaCoints:Boolean, data:String){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        viewInf.findViewById<TextView>(R.id.lblTitleAlert).text = title
        viewInf.findViewById<TextView>(R.id.lblDescriptionAlert).text = message
        viewInf.icon_alert.setImageResource(icon)

        if (ifVivaCoints && data != "" && data.toDouble() > 0 ){
            viewInf.layer_vivacoints.visibility = View.VISIBLE
            viewInf.lbl_number_vivacoints.text = data
        }

        if(ifFacebookShare){
            viewInf.layer_facebook_share.visibility = View.VISIBLE
        }

        val btnShareFacebook = viewInf.findViewById<LinearLayout>(R.id.layer_facebook_share)
        btnShareFacebook.setOnClickListener(clickFb)

        val btn = viewInf.findViewById<Button>(R.id.btnActionAlert)
        btn.setOnClickListener(click)
        btn.text = buttonTittle
        alertDialog.show()
    }

    fun alertNotMultiple(message:String, title:String, buttonTittle:String, secondButtonTittle:String , icon:Int, click:View.OnClickListener, secondClick:View.OnClickListener, isMultiple:Boolean, data:String){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout,null)
        builder.setView( viewInf)

        if(data == "isCancelable")  builder.setCancelable(false)
        else builder.setCancelable(true)


        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        viewInf.findViewById<TextView>(R.id.lblTitleAlert).text = title
        viewInf.findViewById<TextView>(R.id.lblDescriptionAlert).text = message
        viewInf.icon_alert.setImageResource(icon)

        if(isMultiple){
            viewInf.btn_second_button.visibility = View.VISIBLE
        }

        val btn_second_button = viewInf.findViewById<Button>(R.id.btn_second_button)
        btn_second_button.setText(secondButtonTittle)
        btn_second_button.setOnClickListener(secondClick)

        val btn = viewInf.findViewById<Button>(R.id.btnActionAlert)
        btn.setOnClickListener(click)
        btn.text = buttonTittle
    }

    fun alertNotImageCustom(message:String, title:String, buttonTittle:String, icon:String, click:View.OnClickListener, clickFb:View.OnClickListener, ifFacebookShare:Boolean, ifVivaCoints:Boolean, data:String){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)

        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        viewInf.findViewById<TextView>(R.id.lblTitleAlert).text = title
        viewInf.findViewById<TextView>(R.id.lblDescriptionAlert).text = message

        try {
            Picasso.get().load(icon).error(R.drawable.no_image).into(viewInf.icon_alert)
        }catch (e:Exception){
            viewInf.icon_alert.setImageResource(R.drawable.no_image)
        }

        if (ifVivaCoints){
            viewInf.layer_vivacoints.visibility = View.VISIBLE
            viewInf.lbl_number_vivacoints.text = data
        }

        if(ifFacebookShare){
            viewInf.layer_facebook_share.visibility = View.VISIBLE
        }

        val btnShareFacebook = viewInf.findViewById<LinearLayout>(R.id.layer_facebook_share)
        btnShareFacebook.setOnClickListener(clickFb)

        val btn = viewInf.findViewById<Button>(R.id.btnActionAlert)
        btn.setOnClickListener(click)
        btn.text = buttonTittle
        alertDialog.show()
    }
    fun alertNotMultipleAnimt(message:String, title:String, buttonLeftTittle:String, buttonRigthTittle:String, clickLeft:View.OnClickListener, clickRigth:View.OnClickListener, isMultiple:Boolean, isInfinityLoop:Boolean, name:String){

        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_animation,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        viewInf.findViewById<TextView>(R.id.lbl_title_alert_animation).text = title
        viewInf.findViewById<TextView>(R.id.lbl_description_alert_animation).text = message
        val lottieView = viewInf.anim_lottie_multiple
        lottieView.loop(isInfinityLoop)
        if(name != "") {
            lottieView.setAnimation(name)
        }else{
            lottieView.setAnimation("anim_error.json")
        }

        val btnLeft = viewInf.findViewById<Button>(R.id.btnActionLeftAlert)
        btnLeft.visibility = View.GONE
        if(isMultiple) {
            btnLeft.visibility = View.VISIBLE
            btnLeft.setOnClickListener(clickLeft)
            btnLeft.text = buttonLeftTittle
        }

        val btnRigth = viewInf.findViewById<Button>(R.id.btnActionRigthAlert)
        btnRigth.setOnClickListener(clickRigth)
        btnRigth.text = buttonRigthTittle


        alertDialog.show()
    }

    fun alertConfirmDailypay(date:String, extra:String, buttonLeftTittle:String, buttonRigthTittle:String, clickLeft:View.OnClickListener, clickRigth:View.OnClickListener){

        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_confirm,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        viewInf.findViewById<TextView>(R.id.lbl_title_alert_confirm).text = "¿Confirmas la solicitud de pago del día " + date + "?"

        val btnLeft = viewInf.findViewById<Button>(R.id.btnActionLeftAlertConfirm)
        btnLeft.setOnClickListener(clickLeft)
        btnLeft.setText(buttonLeftTittle)

        val btnRigth = viewInf.findViewById<Button>(R.id.btnActionRigthAlertConfirm)
        btnRigth.setOnClickListener(clickRigth)
        btnRigth.text = buttonRigthTittle

        alertDialog.show()
    }

    fun alertNotMultipleAnimLogin(message:String, title:String, buttonLeftTittle:String, buttonRigthTittle:String, clickLeft:View.OnClickListener, clickRigth:View.OnClickListener, clickPolicy:View.OnClickListener, isMultiple:Boolean, isInfinityLoop:Boolean, name:String){

        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_animation_login,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        viewInf.findViewById<TextView>(R.id.lbl_title_alert_animation_policy).text = title
        viewInf.findViewById<TextView>(R.id.lbl_description_alert_animation_policy).text = message

        val lottieView = viewInf.anim_lottie_multiple_policy
        lottieView.loop(isInfinityLoop)
        if(name != "") {
            lottieView.setAnimation(name)
        }else{
            lottieView.setAnimation("anim_error.json")
        }

        val btnLeft = viewInf.findViewById<Button>(R.id.btnActionLeftAlert_policy)
        btnLeft.visibility = View.GONE
        if(isMultiple) {
            btnLeft.visibility = View.VISIBLE
            btnLeft.setOnClickListener(clickLeft)
            btnLeft.text = buttonLeftTittle
        }

        val btnRigth = viewInf.findViewById<Button>(R.id.btnActionRigthAlert_policy)
        btnRigth.setOnClickListener(clickRigth)
        btnRigth.text = buttonRigthTittle

        val btnPolicity = viewInf.findViewById<TextView>(R.id.lbl_policity_alert_animation_policy)
        btnPolicity.setOnClickListener(clickPolicy)

        alertDialog.show()
    }



    private var step_one = ""
    private var step_two = ""
    private var type = -1
    fun alertVacancy(item: VacancyResponseModel, descripton:String, activity_desc:String, bussiness_unity_id:Int, type:Int, sendVacancie:SendVacancieDelegate){

        this.type = type
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        this.sendVacancie = sendVacancie
        var inflater: LayoutInflater  = activity.layoutInflater
        viewInf = inflater.inflate(R.layout.alert_layout_postulation,null)
        builder.setView( viewInf)
        builder.setCancelable(false)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))
        var unity_bussiness = ""

        when(bussiness_unity_id){
            1-> unity_bussiness = "Culiacán"
            2-> unity_bussiness = "Ensenada"
            3-> unity_bussiness = "Sayula"
        }

        val arrayDate = item.start_date.split("-")

        viewInf.findViewById<TextView>(R.id.lbl_bussiness_unity_alert).text = unity_bussiness
        viewInf.findViewById<TextView>(R.id.lbl_activity_alert).text = activity_desc
        viewInf.findViewById<TextView>(R.id.lbl_description_postulation_alert).text = descripton
        viewInf.findViewById<TextView>(R.id.lbl_description_calendar).text = "La fecha de inicio de trabajo es el "+ arrayDate[2]+"/"+arrayDate[1]+"/"+ arrayDate[0] +", ¿Que fecha pudieras estar en la agricola?"
        viewInf.btn_close_modal_vacancy.setOnClickListener(this)
        viewInf.btn_gotback_vacancy_modal.setOnClickListener(this)
        viewInf.btn_gotit_vacancy_modal.setOnClickListener(this)

        if(type == 1){
            viewInf.layer_buttons_vacancy.visibility = View.GONE
            viewInf.layer_alert_vacancy.visibility = View.VISIBLE
        }else{
            val jsonFileValue = utilsClass().getJsonDataFromAsset(activity, "estados.json")
            var state = Gson().fromJson<StateArrayModel>(jsonFileValue, StateArrayModel::class.java)
            state.estados.forEach {arrayNames.add(it.nombre)}
            val adapter = ArrayAdapter(this.activity, android.R.layout.simple_spinner_item, arrayNames)
            viewInf.spinner.adapter = adapter
            viewInf.spinner.onItemSelectedListener = onSpinnerSelected
            viewInf.calendarVacancy.setOnDateChangeListener(onDateSelected)

            viewInf.layer_state_selected_vacancy.visibility = View.VISIBLE
            viewInf.btn_gotit_vacancy_modal.setOnClickListener(this)
            viewInf.btn_next_step_vacancy.setOnClickListener(this)
        }
        alertDialog.show()
    }
    private var onSpinnerSelected = object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
            step_one = arrayNames.get(position)
        }
        override fun onNothingSelected(parent: AdapterView<*>) {}
    }

    private var onDateSelected = object : CalendarView.OnDateChangeListener{
        override fun onSelectedDayChange(view: CalendarView, year: Int, month: Int, dayOfMonth: Int) {
            step_two = year.toString() + "-" + (month + 1)+ "-" + dayOfMonth
        }
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_close_modal_vacancy->{
                step = 0
                dismissDialog()
            }
            R.id.btn_gotit_vacancy_modal->{
                step = 0
                sendVacancie.sendVacancie(step_one, step_two, type)
                dismissDialog()
            }
            R.id.btn_next_step_vacancy->{
                when(step){
                    0->{
                        if(step_one != "") {
                            viewInf.lbl_error_message_vacancy_modal.visibility=View.GONE
                            viewInf.layer_state_selected_vacancy.visibility = View.GONE
                            viewInf.layer_alert_vacancy.visibility = View.GONE
                            viewInf.layer_date_selected_vacancy.visibility = View.VISIBLE
                            step = 1
                        }else{
                            viewInf.lbl_error_message_vacancy_modal.visibility=View.VISIBLE
                            viewInf.lbl_error_message_vacancy_modal.text="*Seleccione una opción"
                        }

                    }
                    1->{
                        if(step_two != "") {
                            viewInf.lbl_error_message_vacancy_modal.visibility=View.GONE
                            viewInf.layer_state_selected_vacancy.visibility = View.GONE
                            viewInf.layer_alert_vacancy.visibility = View.VISIBLE
                            viewInf.layer_date_selected_vacancy.visibility = View.GONE
                            viewInf.layer_conteiner_next.visibility = View.GONE
                            step = 2
                        }else{
                            viewInf.lbl_error_message_vacancy_modal.visibility=View.VISIBLE
                            viewInf.lbl_error_message_vacancy_modal.text="*Seleccione una fecha"
                        }

                    }
                }

            }
            R.id.btn_gotback_vacancy_modal->{
                step = 0
                dismissDialog()
            }
        }
    }

    fun alertHelp(clickFirst:View.OnClickListener, clickSecond:View.OnClickListener, clickThirt:View.OnClickListener){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)

        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_help,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()


        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        val btnFirst = viewInf.findViewById<Button>(R.id.btn_first_select_help)
        val btnSecond = viewInf.findViewById<Button>(R.id.btn_second_select_help)
        val btnThirt = viewInf.findViewById<Button>(R.id.btn_thirt_select_help)

        btnFirst.setOnClickListener(clickFirst)
        btnSecond.setOnClickListener(clickSecond)
        btnThirt.setOnClickListener(clickThirt)
        alertDialog.show()
    }

    fun alertHelpPIN(clickFirst:View.OnClickListener, clickSecond:View.OnClickListener){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)

        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_help_pin,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()


        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        val btnFirst = viewInf.findViewById<Button>(R.id.btn_first_select_help_pin)
        val btnSecond = viewInf.findViewById<Button>(R.id.btn_second_select_help_pin)

        btnFirst.setOnClickListener(clickFirst)
        btnSecond.setOnClickListener(clickSecond)
        alertDialog.show()
    }

    fun alertSelectEnviroment(clickFirst:View.OnClickListener, clickSecond:View.OnClickListener, clickThirt:View.OnClickListener){
        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)

        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_envirometn,null)
        builder.setView( viewInf)

        builder.setCancelable(true)
        alertDialog = builder.create()


        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        val btnFirst = viewInf.findViewById<Button>(R.id.btn_first_select_dev)
        val btnSecond = viewInf.findViewById<Button>(R.id.btn_second_select_qa)
        val btnThirt = viewInf.findViewById<Button>(R.id.btn_thirt_select_prod)

        btnFirst.setOnClickListener(clickFirst)
        btnSecond.setOnClickListener(clickSecond)
        btnThirt.setOnClickListener(clickThirt)
        alertDialog.show()
    }


    fun alertConfirmNotification(description:String, title:String, data:ExtrasModel?, buttonLeftTittle:String, buttonRigthTittle:String, clickLeft:View.OnClickListener, clickRigth:View.OnClickListener){

        var builder:AlertDialog.Builder = AlertDialog.Builder(activity)
        var inflater: LayoutInflater  = activity.layoutInflater
        val viewInf = inflater.inflate(R.layout.alert_layout_confirm_notification,null)
        builder.setView( viewInf)

        builder.setCancelable(false)
        alertDialog = builder.create()
        alertDialog.window!!.setBackgroundDrawable( ColorDrawable(activity.resources.getColor(R.color.whiteAlpha)))

        viewInf.findViewById<TextView>(R.id.lbl_title_alert_confirm_notification).text = title
        viewInf.findViewById<TextView>(R.id.lbl_description_notification_confirm).text = description

        if(data != null){

            /*DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
            LocalDate date = LocalDate.parse("2018-04-10T04:00:00.000Z", inputFormatter);
            String formattedDate = outputFormatter.format(date);
            System.out.println(formattedDate); // prints 10-04-2018*/

//            2021-02-18T00:00:00
            val arrDate = data.confirm_date.split("T")
            if(arrDate.get(0) != "") {
                val date = SimpleDateFormat("yyyy-MM-dd").parse(arrDate[0])
                val calendar = Calendar.getInstance()
                calendar.time = date!!
                val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
                val dayWeeak = utilsClass().getDayOfWeekFromCalendar(calendar)
                val monthNumber  = calendar.get(Calendar.MONTH)

                val dateToShow =
                    dayWeeak + " " + dayOfMonth + " de " + utilsClass().convertIntMonthToStrinh(monthNumber)

                viewInf.findViewById<TextView>(R.id.lbl_date_notification_confirm).text = dateToShow
                viewInf.findViewById<TextView>(R.id.lbl_place_notification_confirm).text =
                    data.message_place
            }else{
                viewInf.findViewById<TextView>(R.id.lbl_date_notification_confirm).text = ""
                viewInf.findViewById<TextView>(R.id.lbl_place_notification_confirm).text = ""
            }
        }



        val btnLeft = viewInf.findViewById<Button>(R.id.btn_left_button_alert_confirm_not)
        btnLeft.setOnClickListener(clickLeft)
        btnLeft.text = buttonLeftTittle

        val btnRigth = viewInf.findViewById<Button>(R.id.btn_right_button_alert_confirm_not)
        btnRigth.setOnClickListener(clickRigth)
        btnRigth.text = buttonRigthTittle


        alertDialog.show()
    }


    fun dismissDialog() {
        alertDialog.dismiss()
    }
    fun showAlertDialogMultiple(){
        alertDialog.show()
    }
}

interface SendVacancieDelegate{
    fun sendVacancie(state:String, date:String, type:Int)
}